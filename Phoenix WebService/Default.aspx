﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
    <style type="text/css">
        #Text1
        {
            width: 1129px;
            height: 328px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divInitial" runat="server">
        <table>
            <tr>
                <td>
                    Brand:
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="cmbBrand" Style="width: 150px">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                        <asp:ListItem Text="Britz" Value="B"></asp:ListItem>
                        <asp:ListItem Text="Maui" Value="M"></asp:ListItem>
                        <asp:ListItem Text="Backpacker" Value="P"></asp:ListItem>
                        <asp:ListItem Text="ExploreMore" Value="X"></asp:ListItem>
                        <asp:ListItem Text="Alpha" Value="A"></asp:ListItem>
                        <asp:ListItem Text="Thrifty" Value="T"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--<tr>
                <td>
                    CompanyCode:
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="cmbCompanyCode" Style="width: 150px">
                        <asp:ListItem Text="THL Rentals" Value="THL"></asp:ListItem>
                        <asp:ListItem Text="ExploreMore" Value="KXS"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td>
                    CountryCode
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="cmbCountryCode" Style="width: 150px">
                        <asp:ListItem Text="Australia" Value="AU"></asp:ListItem>
                        <asp:ListItem Text="New Zealand" Value="NZ"></asp:ListItem>
                        <asp:ListItem Text="USA" Value="US"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    Vehicle Code
                </td>
                <td>
                    <asp:TextBox ID="txtVehicleCode" runat="server" Style="width: 150px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    CheckOutZoneCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckOutZoneCode">
                    </asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td>
                    CheckOutTownCityCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckOutTownCityCode">
                    </asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    CheckOutDate
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckOutDate">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    CheckInZoneCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckInZoneCode">
                    </asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td>
                    CheckInTownCityCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckInTownCityCode">
                    </asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    CheckInDate
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCheckInDate">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    AgentCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtAgentCode">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    PackageCode
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtPackageCode">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Number Of Adults
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNumberOfAdults">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Number Of Children
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNoOfChildren">
                    </asp:TextBox>
                </td>
            </tr>
            <%--<tr>
                <td>
                    Number Of Infants
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtNoOfInfants">
                    </asp:TextBox>
                </td>
            </tr>--%>
            <tr>
                <td>
                    IsVan
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsVan" />
                </td>
            </tr>
            <%--<tr>
                <td>
                    IsInclusivePackage
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsInclusivePackage" />
                </td>
            </tr>--%>
            <tr>
                <td>
                    IsBestBuy
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsBestBuy" />
                </td>
            </tr>
            <tr>
                <td>
                    CountryOfResidence
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCountryOfResidence">
                    </asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    IsTestMode
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsTestMode" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    IsGross
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsGross" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    ** isAuroraCall(V2 only) 
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkIsAurora" Checked="True"  Enabled="true" />
                </td>
            </tr>
            <tr>
                <td>
                    ** isQuickAvailability(V2 only)
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkisQuickAvail" Checked="true" Enabled="true"/>
                </td>
            </tr>
           
            <tr>
                <td valign="top">
                    <asp:Button ID="Button2" runat="server" Text="Thrifty WS Status" />
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Get Vehicles" />
                </td>
                <td>
                    <asp:Button ID="btnGetAvailabilityv2" runat="server" Text="Get Vehicles (v2)" Enabled="true" />
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Force Book" />
                </td>
                <td>
                    <asp:Button ID="btnGetAvailabilityInGrid" runat="server" Text="Get Vehicles(-/+ days)" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divResultSet" runat="server">
        <asp:Literal ID="LitResultSet" runat="server"></asp:Literal>
    </div>
    <%--<div id="divSecond" runat="server">
        <asp:Literal ID="ltrVehicleList" runat="server"></asp:Literal>
    </div>--%>
    <%--<div id="div1" runat="server">
        <asp:Literal ID="ltrForceResult" runat="server"></asp:Literal>
    </div>--%>
    <div id="div2" runat="server">
        <%--<asp:DropDownList ID="ddlCriteria" runat="server" AutoPostBack="true">
        <asp:ListItem Value="1"> MoveForward </asp:ListItem>
        <asp:ListItem Value="2"> MoveBackWard </asp:ListItem>
        <asp:ListItem Value="3"> SwitchLocation </asp:ListItem>
        <asp:ListItem Value="4"> PickUpBased </asp:ListItem>
        <asp:ListItem Value="5"> DropOffBased </asp:ListItem>
        <asp:ListItem Value="0"> Normal </asp:ListItem>
    </asp:DropDownList>--%>
        In Order:
        <br />
        <asp:CheckBoxList ID="cblCriteria" runat="server" AutoPostBack="false">
            <asp:ListItem Value="0" Enabled='false'> Normal </asp:ListItem>
            <asp:ListItem Value="1"> MoveForward(1) </asp:ListItem>
            <asp:ListItem Value="2"> MoveBackWard(2) </asp:ListItem>
            <asp:ListItem Value="3"> SwitchLocation(3) </asp:ListItem>
            <asp:ListItem Value="4"> PickUpBased(4) </asp:ListItem>
            <asp:ListItem Value="5"> DropOffBased(5) </asp:ListItem>
            <asp:ListItem Value="6"> OnewaySwitchLocationOnPickup(6)</asp:ListItem>
            <asp:ListItem Value="7"> Simulate Aurora Call(7)</asp:ListItem>
        </asp:CheckBoxList>
        <asp:Button ID="alternateAvailability" runat="server" Text="Get Alternate Availability"
            Width="187px" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <hr />
        <label id="Urllabel" runat="server">
        </label>
        <hr />
        <br />
        <asp:Literal ID="alternateAvailabilityLiteral" runat="server"></asp:Literal>
        <input id="alternateAvailabilityText" type="text" runat="server" style="width: 100%;
            height: 100%" />


    </div>
    <table width="100%">
            <tr>
                <td>
                    Rental ID(1234567-1)
                </td>
                <td>
                    <asp:TextBox runat="server" ID="RentalIDTextBox">
                    </asp:TextBox>
                </td>
                <td>
                    <asp:Button ID="CreateAndSendButton" runat="server" Text="Create/Send Confirmation(SCI)" />
                </td>
            </tr>
            </table>
    </form>
</body>
</html>
