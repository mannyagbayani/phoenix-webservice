﻿Imports System.Xml

Partial Class ModifyBooking
    Inherits System.Web.UI.Page

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim xmlData As New XmlDocument
        xmlData.LoadXml(TextBox1.Text)
        Dim result As XmlDocument
        
        result = oService.ModifyBooking(xmlData)

        TextBox1.Text = ""
        TextBox1.Text = result.OuterXml
    End Sub
End Class
