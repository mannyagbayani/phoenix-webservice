-- This script creates 3 stored procedures needed for the save quote enhancement to the phoenix webservice
-- 1. WEBP_GetInsuranceAndExtraHireItems
-- 2. WEBP_GetUserCodeForRentalModify
-- 3. WEBP_RetrieveBooking


/*
Script created by Toad at 6/10/2010 10:41:57 a.m.
Please back up your database before running this script
*/

PRINT N'Synchronizing objects from [Aurora] to [Aurora]'
GO


PRINT N'If another transaction is open, commit it'
IF @@TRANCOUNT>0 BEGIN
COMMIT TRANSACTION
END
GO					
	
PRINT N'Begin transaction'
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO

PRINT N'Create procedure [dbo.WEBP_GetInsuranceAndExtraHireItems]'
GO
/*
Header line. Object: dbo.WEBP_GetInsuranceAndExtraHireItems. Script date 6/10/2010 10:41:57 a.m.
*/
GO
CREATE Procedure [dbo].[WEBP_GetInsuranceAndExtraHireItems]    
	@sRentalNumber VARCHAR(64),
	@sBpdId VARCHAR(64),
	@sErrors VARCHAR(MAX) OUTPUT
AS          
BEGIN          
     SET DATEFORMAT DMY
     SET NOCOUNT ON       

     DECLARE @sRntId  VARCHAR(64), 
             @sBooNum VARCHAR(64),
             @RntNum  VARCHAR(256)

     IF LTRIM(RTRIM(ISNULL(@sRentalNumber,''))) <> ''
          BEGIN
               -- Pick up info from Rental Number
               SET @sBooNum = Aurora.dbo.getsplitedData(@sRentalNumber, '/', 1)

               IF CHARINDEX('/', @sRentalNumber)= 0
                    BEGIN
                         SELECT @sErrors = 'Invalid rental number entered'
                         RETURN
                    END
               ELSE
                    BEGIN
                         SELECT @RntNum = Aurora.dbo.getsplitedData(@sRentalNumber, '/', 2)
                    END

               SELECT TOP 1 
                    @sRntId =RntId 
               FROM 
                    Rental WITH (NOLOCK) INNER JOIN 
                    Booking WITH (NOLOCK) ON RntBooId = BooId
               WHERE  
                    BooNum = @sBooNum 
                    AND 
                    RntNum = @RntNum
          END
     ELSE
          BEGIN
               -- Pick up info from BpdId
               SELECT TOP 1 
                    @sRntId =RntId 
               FROM 
                    Rental WITH (NOLOCK) INNER JOIN 
                    Booking WITH (NOLOCK) ON RntBooId = BooId INNER JOIN
                    BookedProduct WITH (NOLOCK) ON BpdRntId = RntId
               WHERE  
                    BpdId = @sBpdId
          END
 
     IF LTRIM(RTRIM(ISNULL(@sRntID,''))) = ''
          BEGIN
               SELECT @sErrors = 'Invalid rental number or bpdid entered'
               RETURN
          END

     ---- DEBUG      
     --DECLARE @sRntId  VARCHAR(64)          
     --SELECT @sRntId = 'W3414099-1'      
     ---- DEBUG      

     DECLARE @bIsInclusivePackage BIT  
     IF EXISTS(  
               SELECT BpdId FROM Aurora.dbo.BookedProduct (NOLOCK), Aurora.dbo.SaleableProduct (NOLOCK), Aurora.dbo.Product (NOLOCK)   
               WHERE BpdRntId = @sRntId and BpdStatus = 'KK' AND BpdSapId = SapId AND SapPrdId = PrdId and IsNull(PrdIsVehicle,0) = 0 AND IsNull(PrdIsInclusive,0) = 1  AND PrdShortName IN ('BONUSPAC','SMARTPAC', 'PREMPAC')
          )  
          SET @bIsInclusivePackage = 1  
     ELSE  
          SET @bIsInclusivePackage = 0  
   
     DECLARE       
          @sUsrCode VARCHAR(64),          
          @sComCode VARCHAR(64)      
  
--     UPDATE   
--          TProcessedBpds WITH (ROWLOCK)  
--     SET   
--          RntId = 'not needed'  
--          ,spid = 10000  
--          ,SapId = 'not needed'  
--     WHERE  
--          RntId = @sRntId      
           
     BEGIN TRAN          
           
          DECLARE @tProductsInsurence TABLE(ID BIGINT IDENTITY, PrdShortName VARCHAR(64), PrdName VARCHAR(64), HireItemId BIGINT)          
          DECLARE @tProductsExtraHireItem TABLE(ID BIGINT IDENTITY, PrdShortName VARCHAR(64), PrdName VARCHAR(64), HireItemId BIGINT)          
          DECLARE @tProductsFerries TABLE(ID BIGINT IDENTITY, PrdShortName VARCHAR(64), PrdName VARCHAR(64), HireItemId BIGINT)          
          DECLARE @tProductsSurcharge TABLE(ID BIGINT IDENTITY, PaymentName VARCHAR(64), Prdshortname VARCHAR(64))          
          
          DECLARE @sRetErr   VARCHAR(64),          
               @VehsPrdId   VARCHAR(64),          
               @sCkoLocationCode VARCHAR(12),          
               @sCtyCode   VARCHAR(12),          
               @sBrand    VARCHAR(3),          
               @sClassCode   VARCHAR(12),          
               @iHireItemId  BIGINT,          
               @sPtmName   VARCHAR(64)          
             
          EXEC WEBA_GetDetailsFromRntId      
                @sRntId = @sRntId,      
                @sPrdId = @VehsPrdId OUTPUT,      
                @dCkoWhen = NULL,      
                @dCkiWhen = NULL,      
                @sCkoLocationCode = @sCkoLocationCode OUTPUT,      
                @sCkiLocationCode = NULL,      
                @sAgnid = NULL,      
                @sPkgid = NULL,      
                @sBooId = NULL,      
                @dBooWhen = NULL,
                @nRentalNo = NULL
      
          SELECT DISTINCT         
               @sCtyCode = dbo.getCountryForLocation(@sCkoLocationCode, NULL, NULL),        
               @sClassCode = Class.ClaCode,       
               @sBrand = Package.PkgBrdCode      
          FROM               
               Rental WITH (NOLOCK) INNER JOIN      
               Package WITH (NOLOCK) ON Rental.RntPkgId = Package.PkgId INNER JOIN      
               PackageProduct WITH (NOLOCK) ON Package.PkgId = PackageProduct.PplPkgId INNER JOIN      
               SaleableProduct WITH (NOLOCK) ON PackageProduct.PplSapId = SaleableProduct.SapId INNER JOIN      
               Product WITH (NOLOCK) INNER JOIN      
               Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN      
               Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID ON SaleableProduct.SapPrdId = Product.PrdId      
          WHERE           
               Product.PrdId = @VehsPrdId       
               AND       
               Rental.RntId = @sRntId       
               AND       
               Product.PrdIsActive = 1       
               AND       
               Product.PrdIsVehicle = 1      
          
          SELECT @sComCode = dbo.fun_getCompany            
                                   (            
                                        NULL,  --@sUserCode VARCHAR(64),            
                                        NULL,  --@sLocCode VARCHAR(64),            
                                        @sBrand,   --@sBrdCode VARCHAR(64),            
                                        NULL,  --@sPrdId  VARCHAR(64),            
                                        NULL,  --@sPkgId  VARCHAR(64),            
                                        NULL,  --@sdummy1 VARCHAR(64),            
                                        NULL,  --@sdummy2 VARCHAR(64),            
                                        NULL   --@sdummy3 VARCHAR(64)            
                                   )            
      
          SELECT @sUsrCode = dbo.WEBP_getValueForCompanyORUserCode(@sComCode)          
           
    --IF @bIsInclusivePackage = 0  
     INSERT INTO       
       @tProductsInsurence(PrdShortName,PrdName,HireItemId)          
     SELECT       
       PrdShortName,       
       PrdName,       
       HreItmId       
     FROM       
       dbo.WebNonVehicleSetup (NOLOCK),       
       dbo.WebHireItems (NOLOCK),       
       dbo.Product (NOLOCK),       
       dbo.Class A (NOLOCK),       
       [Type] (NOLOCK),       
       Class B (NOLOCK)          
     WHERE       
       WebSetupId = HreItmWebSetupId       
       AND       
       HreItmPrdId = PrdId       
       AND       
       WebClaId = A.ClaId          
       AND       
       PrdTypId = TypId       
       AND       
       TypClaId = B.ClaId       
       AND       
       B.ClaCode = 'IN'          
       AND       
       WebCtyCode = @sCtyCode       
       AND       
       WebBrdCode = @sBrand       
       AND       
       A.ClaCode = @sClassCode     
     ORDER BY 
		WebHireItems.HreItmRating DESC     
           
          DECLARE @sPrdShortName VARCHAR(64),          
                    @iId   BIGINT,           
                    @iNoOfProduct   BIGINT          
      
          SELECT @iNoOfProduct  = COUNT(*) FROM @tProductsInsurence          
     
          SET @iId = 1          
      
          SELECT '<ConfigurationProducts>'          
      
          SELECT '<Insurence>'          
          --IF @bIsInclusivePackage = 1  
    --BEGIN  
  -- SELECT '<Product Code="' + PrdShortName + '" Name="' + PrdName + '" dailyRate="' + CAST(ChgRate AS VARCHAR) + '" UOM="' + IsNull(dbo.getCodCode(BpdCodUOMId),'')   
  -- + '" GrossAmt ="' + CAST(BpdGrossAmt AS VARCHAR)  + '" MaxCharge="" SapId="' + BpdSapId + '" DisplayOrder="0" PromptQty="No" MinAge="" MaxAge="" PrdInfo="" IsInclusive="Yes" Rating="25"  HirePeriod="' + CAST(BpdHirePeriod AS VARCHAR) + '" >'    
  -- FROM Aurora.dbo.BookedProduct (NOLOCK), Aurora.dbo.SaleableProduct (NOLOCK), Aurora.dbo.Product (NOLOCK), Aurora.dbo.Charge (NOLOCK)  
  -- WHERE BpdRntId = @sRntId and BpdStatus = 'KK' AND BpdSapId = SapId AND SapPrdId = PrdId and IsNull(PrdIsVehicle,0) = 0 AND IsNull(PrdIsInclusive,0) = 1 AND ChgBpdid = BpdId  
    --END     
          WHILE @iNoOfProduct>=@iId          
          BEGIN          
               SET @sRetErr = NULL          
      
               SELECT       
                    @sPrdShortName = PrdShortName,       
                    @iHireItemId  = HireItemId       
               FROM       
                    @tProductsInsurence       
               WHERE       
                    ID = @iId          
        
               SET @iId = @iId + 1        
        
      IF @bIsInclusivePackage = 1   
      BEGIN  
       IF @sPrdShortName = 'BBPACK'  
			SET @sPrdShortName = 'BONUSPAC'  
       IF @sPrdShortName = 'BPPACK'  
			SET @sPrdShortName = 'SMARTPAC'    
       IF @sPrdShortName = 'MPPACK'  
			SET @sPrdShortName = 'PREMPAC'    
      END  
				
               EXEC WEBA_GetPriceForNonVehicleProduct          
                    @sRntId    = @sRntId,          
                    @sPrdShortName  = @sPrdShortName,          
                    @sUserId   = @sUsrCode,          
                    @iHireItemId  = @iHireItemId,          
                    @sRetErr   = @sRetErr OUTPUT          
          END    
     
                
          SELECT '</Insurence>'          
      
          INSERT INTO       
               @tProductsExtraHireItem(PrdShortName,PrdName,HireItemId)          
          SELECT       
         PrdShortName,       
               PrdName,       
               HreItmId       
          FROM       
               dbo.WebNonVehicleSetup (NOLOCK),       
               dbo.WebHireItems (NOLOCK),       
               dbo.Product (NOLOCK),       
               dbo.Class A (NOLOCK),       
               [Type] (NOLOCK),       
               Class B (NOLOCK)          
          WHERE       
               WebSetupId = HreItmWebSetupId       
               AND       
               HreItmPrdId = PrdId       
               AND       
               WebClaId = A.ClaId          
               AND       
               PrdTypId = TypId       
               AND       
               TypClaId = B.ClaId       
               AND       
               B.ClaCode NOT IN ('IN','FE','FC')          
               AND       
               WebCtyCode = @sCtyCode       
               AND       
               WebBrdCode = @sBrand       
               AND       
               A.ClaCode = @sClassCode     
     ORDER BY 
		WebHireItems.HreItmDispOrder
           
          SET @iNoOfProduct  = 0          
      
          SELECT @iNoOfProduct  = COUNT(*) FROM @tProductsExtraHireItem          
      
          SET @iId = 1          
      
          SELECT '<ExtraHireItems>'          
      
          WHILE @iNoOfProduct>=@iId          
          BEGIN          
               SET @sRetErr = NULL          
      
               SELECT       
                    @sPrdShortName = PrdShortName,       
                    @iHireItemId = HireItemId       
               FROM       
                    @tProductsExtraHireItem       
               WHERE       
                    ID = @iId      
          
               SET @iId = @iId + 1          
      
               EXEC WEBA_GetPriceForNonVehicleProduct          
                    @sRntId    = @sRntId,          
                    @sPrdShortName  = @sPrdShortName,          
                    @sUserId   = @sUsrCode,          
                    @iHireItemId  = @iHireItemId,          
                    @sRetErr   = @sRetErr OUTPUT       
      
          END          
      
          SELECT '</ExtraHireItems>'          
          
          INSERT INTO       
               @tProductsFerries(PrdShortName,PrdName,HireItemId)          
          SELECT       
               PrdShortName,       
               PrdName,       
               HreItmId       
          FROM       
               dbo.WebNonVehicleSetup (NOLOCK),       
               dbo.WebHireItems (NOLOCK),       
               dbo.Product (NOLOCK),       
               dbo.Class A (NOLOCK),       
               [Type] (NOLOCK),       
               Class B (NOLOCK)          
          WHERE       
               WebSetupId = HreItmWebSetupId       
               AND       
               HreItmPrdId = PrdId       
               AND       
               WebClaId = A.ClaId          
               AND       
               PrdTypId = TypId       
               AND       
               TypClaId = B.ClaId       
               AND       
               B.ClaCode  = 'FC'          
               AND       
               WebCtyCode = @sCtyCode       
               AND       
               WebBrdCode = @sBrand       
               AND       
               A.ClaCode = @sClassCode     
         ORDER BY 
		       WebHireItems.HreItmDispOrder     
           
          SET @iNoOfProduct  = 0          
      
          SELECT @iNoOfProduct  = COUNT(*) FROM @tProductsFerries          
      
          SET @iId = 1          
      
          SELECT '<FerryCrossing>'          
       
          WHILE @iNoOfProduct>=@iId          
          BEGIN          
                     
               SET @sRetErr = NULL          
        
               SELECT @sPrdShortName = PrdShortName, @iHireItemId  = HireItemId FROM @tProductsFerries WHERE ID = @iId          
        
               SET @iId = @iId + 1          
      
               EXEC WEBA_GetPriceForNonVehicleProduct          
                    @sRntId    = @sRntId,          
                    @sPrdShortName  = @sPrdShortName,          
                    @sUserId   = @sUsrCode,          
                    @iHireItemId  = @iHireItemId,          
                    @sRetErr   = @sRetErr OUTPUT       
          END          
      
          SELECT '</FerryCrossing>'          
      
          DECLARE @sPmtId  VARCHAR(64),          
                    @sToday  DATETIME,          
                    @sPrdCode VARCHAR(64)          
          
          SELECT        
               @sPmtId = PtmId       
          FROM       
               dbo.Paymentmethod (NOLOCK)       
          WHERE       
               dbo.getcodCode(PtmCodCtgyId) =  'Credit Card'          
           
          SELECT       
               @sPrdCode = PrdShortName       
          FROM       
               dbo.Product (NOLOCK)       
          WHERE       
               PrdId = @VehsPrdId          
      
          SELECT @sToday = CURRENT_TIMESTAMP          
          
            
          INSERT INTO       
               @tProductsSurcharge(PaymentName,PrdShortname)          
          SELECT PtmName, PtmPrdShortName  + 'R' FROM dbo.WebNonVehicleSetup,dbo.WebCardTypeSetup, dbo.PaymentMethod, dbo.Class (NOLOCK)    
		  WHERE WebSetupId = WbCrdTypSWebSetupId AND WbCrdTypSPtmId = PtmId  AND WebCtyCode = @sCtyCode AND WebBrdCode = @sBrand AND WebClaId = ClaId AND ClaCode = @sClassCode           
		  ORDER BY PtmName DESC
  
      
           
          SET @iNoOfProduct  = 0          
      
          SELECT @iNoOfProduct  = COUNT(*) FROM @tProductsSurcharge          
      
          SET @iId = 1          
                
          SELECT '<Surcharge>'          
          
          WHILE @iNoOfProduct>=@iId          
          BEGIN          
      
               SET @sRetErr = NULL          
                     
               SELECT       
                    @sPrdShortName = PrdShortName,       
                    @sPtmName = PaymentName       
               FROM       
              @tProductsSurcharge       
               WHERE       
                    ID = @iId          
      
               SET @iId = @iId + 1          
     IF dbo.IsSurchargeApplied(    
       NULL,  -- @sRntId  VARCHAR(64),    
       @sPmtId, -- @sPmtType VARCHAR(64),    
       @sToday, -- DATETIME,    
       @VehsPrdId, -- @sPrdId  VARCHAR(64), -- optional    
       @sCtyCode, -- VARCHAR(12), -- optional    
       @sPrdCode -- VARCHAR(64) -- optional    
         ) = 1  
               EXEC WEBA_GetPriceForNonVehicleProduct          
                    @sRntId    = @sRntId,          
                    @sPrdShortName  = @sPrdShortName,          
                    @sUserId   = @sUsrCode,          
                    @iHireItemId  = NULL,          
                    @mAmt    = 100,          
                    @iQty    = 1,          
                    @sIsCreditCard  = @sPtmName,          
                    @sRetErr   = @sRetErr OUTPUT          
    ELSE  
     SELECT '<Product Code="' + IsNull(@sPtmName,'') + '" SurchargePer="' + CAST(0 AS VARCHAR) + '%"/>'   
      
          END          
      
          SELECT '</Surcharge>'          
      
          SELECT '</ConfigurationProducts>'          
      
     ROLLBACK TRAN          
      
     RETURN      
END
GO


IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Create procedure [dbo.WEBP_GetUserCodeForRentalModify]'
GO
/*
Header line. Object: dbo.WEBP_GetUserCodeForRentalModify. Script date 6/10/2010 10:41:57 a.m.
*/
GO
CREATE PROC [dbo].[WEBP_GetUserCodeForRentalModify]   
 @sRntId AS VARCHAR(64),  
 @sUsrCode AS VARCHAR(24) OUTPUT,
 @sUsrId AS VARCHAR(64) OUTPUT  
AS  
BEGIN  
 SET NOCOUNT ON   
  
 DECLARE @sPkgid  VARCHAR(64),  
   @sComCode VARCHAR(64)  
   
 EXEC WEBA_GetDetailsFromRntId
		@sRntId = @sRntId,
		@sPrdId = NULL,
		@dCkoWhen = NULL,
		@dCkiWhen = NULL,
		@sCkoLocationCode = NULL,
		@sCkiLocationCode = NULL,
		@sAgnid = NULL,
		@sPkgid = @sPkgid OUTPUT,
		@sBooId = NULL,
		@dBooWhen = NULL,
		@nRentalNo = NULL
   
 SELECT @sComCode = dbo.fun_getCompany  
      (  
       NULL,  -- @sUserCode VARCHAR(64),  
       NULL,  -- @sLocCode VARCHAR(64),  
       NULL,  -- @sBrdCode VARCHAR(64),  
       NULL,  -- @sPrdId  VARCHAR(64),  
       @sPkgId,  -- VARCHAR(64),  
       NULL,  -- @sdummy1 VARCHAR(64),  
       NULL,  -- @sdummy2 VARCHAR(64),  
       NULL  -- @sdummy3 VARCHAR(64)  
      )  
   
 SELECT @sUsrCode = dbo.WEBP_getValueForCompanyORUserCode(@sComCode)  

 SELECT @sUsrId = UsrId FROM UserInfo (NOLOCK) WHERE UsrCode = @sUsrCode
  
 RETURN  
END
GO


IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Create procedure [dbo.WEBP_RetrieveBooking]'
GO
/*
Header line. Object: dbo.WEBP_RetrieveBooking. Script date 6/10/2010 10:41:57 a.m.
*/
GO
CREATE PROC [dbo].[WEBP_RetrieveBooking] -- WEBP_RetrieveBooking 'B3499463/1' , NULL
--DECLARE
	@sRentalNumber VARCHAR(64),
	@sBpdId VARCHAR(64),
	@sErrors VARCHAR(MAX) OUTPUT
AS
BEGIN
	SET DATEFORMAT DMY
	SET NOCOUNT ON

	DECLARE
	@sRntId	VARCHAR(64),
	@sBooId VARCHAR(64),
	@sBooNum	VARCHAR(64),
	@RntNum	VARCHAR(64),
	@RntStatus VARCHAR(10),
	@RntFollowUpWhen DATETIME,
	@sPickupLocCode VARCHAR(64)

	-- DEBUG
	--SELECT @sRentalNumber = 'B3499463/1' --  KK 'B3499461/1'
	-- DEBUG

	IF LTRIM(RTRIM(ISNULL(@sRentalNumber,''))) <> ''
		BEGIN
			-- Pick up info from Rental Number
			SET @sBooNum = Aurora.dbo.getsplitedData(@sRentalNumber, '/', 1)

			IF CHARINDEX('/', @sRentalNumber)= 0
				BEGIN
					SELECT @sErrors = 'Invalid rental number entered'
					RETURN
				END
			ELSE
				BEGIN
					SELECT @RntNum = Aurora.dbo.getsplitedData(@sRentalNumber, '/', 2)
				END

			SELECT TOP 1 
				@sRntId =RntId ,
				@RntStatus = RntStatus,
				@RntFollowUpWhen = RntFollowUpWhen,
				@sBooId = BooId,
				@sPickupLocCode = RntCkoLocCode
			FROM 
				Rental WITH (NOLOCK) INNER JOIN 
				Booking WITH (NOLOCK) ON RntBooId = BooId
			WHERE  
				BooNum = @sBooNum 
				AND 
				RntNum = @RntNum
		END
	ELSE
		BEGIN
			-- Pick up info from BpdId
			SELECT TOP 1 
				@sRntId =RntId ,
				@RntStatus = RntStatus,
				@RntFollowUpWhen = RntFollowUpWhen,
				@sBooId = BooId,
				@sPickupLocCode = RntCkoLocCode
			FROM 
				Rental WITH (NOLOCK) INNER JOIN 
				Booking WITH (NOLOCK) ON RntBooId = BooId INNER JOIN
				BookedProduct WITH (NOLOCK) ON BpdRntId = RntId
			WHERE  
				BpdId = @sBpdId
		END



	IF ISNULL(@sRntId,'') = ''
		BEGIN
			SELECT @sErrors = 'Invalid rental number entered'
			RETURN
		END

	-- validate the status, follow up date
	SELECT @RntStatus = LTRIM(RTRIM(UPPER(@RntStatus)))

	IF @RntStatus <> 'QN'
		BEGIN
			IF @RntStatus = 'KK'		
				BEGIN
					SELECT @sErrors = 'This quote has already been confirmed'
				END
			ELSE
				BEGIN 
					IF @RntStatus IN ('XX','XN')
						BEGIN
							SELECT @sErrors = 'This quote has already been cancelled'
						END
					ELSE
						BEGIN
							SELECT @sErrors = 'This rental is not a quote'
						END
				END
			RETURN
		END

	IF @RntFollowUpWhen < GETDATE()
		BEGIN
			SELECT @sErrors = 'This quote has expired'
			--RETURN
		END

	-- the rental number has passed status and validity checks, so now we should return the data!
	

	-- STUFF FROM GetBookingDetails
          
     DECLARE @sBrandName AS VARCHAR(128), @sProductName AS VARCHAR(64),         
     -- RKS: 05-OCT-2009 for extra driver fee        
     @sBrandCode VARCHAR(2), @sPrdId VARCHAR(64), @sClaId VARCHAR(64), @sVehicleCode VARCHAR(24)        
          
     SELECT    @sBrandName = Brand.BrdName ,          
               @sProductName = Product.PrdName,        
               -- RKS: 05-OCT-2009 for extra driver fee        
               @sBrandCode  = BrdCode,        
               @sPrdId   = PrdId,
			   @sVehicleCode = PrdShortName           
     FROM      Rental (NOLOCK) INNER JOIN             
               Product (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product.PrdId INNER JOIN            
               Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId INNER JOIN            
               Brand (NOLOCK) ON Package.PkgBrdCode = Brand.BrdCode            
     WHERE     (Rental.RntId = @sRntId)            
          
     SELECT @sClaId = ClaId FROM dbo.Product INNER JOIN dbo.Type ON PrdTypId = TypId        
       INNER JOIN dbo.Class ON TypClaId = ClaId AND PrdId = @sPrdId          
          
     SELECT '<RentalInfo>'                      
     SELECT                   
          @sBooNum AS BookingNumber,                
          Rental.RntId AS RentalId,                   
          CONVERT(VARCHAR, Rental.RntCkoWhen, 103) + ' ' + CONVERT(VARCHAR, Rental.RntCkoWhen, 108) AS PickupDateTime,                   
          CONVERT(VARCHAR, Rental.RntCkoWhen, 106) AS PickupDateForDisp,                   
          CONVERT(VARCHAR, Rental.RntCkiWhen, 103) + ' ' + CONVERT(VARCHAR, Rental.RntCkiWhen, 108) AS DropOffDateTime,                   
          CONVERT(VARCHAR, Rental.RntCkiWhen, 106) AS DropOffDateForDisp,                   
          RntCkoLocCode AS PickupLocCode,                  
          (SELECT LocName FROM dbo.Location (NOLOCK) WHERE LocCode = RntCkoLocCode) PickupLocName,                  
          RntCkiLocCode AS DropOffLocCode,                   
          (SELECT LocName FROM dbo.Location (NOLOCK) WHERE LocCode = RntCkiLocCode) DropOffLocName,                  
          dbo.getCountryForLocation(Rental.RntCkoLocCode, NULL, NULL) AS CountryCode,             
          @sBrandName + ' ' + @sProductName AS VehicleName,          
          @sProductName AS ProductName,     
          @sVehicleCode AS ProductCode,
          @sBrandName AS BrandName,    
		  @sBrandCode AS BrandCode,      
          ISNULL((SELECT AgnCode FROM Agent (NOLOCK) INNER JOIN Booking (NOLOCK) ON AgnId = BooAgnId WHERE RntBooId = BooId ) ,'') AS AgentCode,
          RntPkgId AS PackageId,
          DATEDIFF( d, CAST(CONVERT(VARCHAR, Rental.RntCkoWhen, 106) AS DATETIME), CAST(CONVERT(VARCHAR, Rental.RntCkiWhen, 106) AS DATETIME)) + 1 AS HirePeriod,        
--          ( SELECT     SUM(BpdGrossAmt)         
--            FROM       BookedProduct (NOLOCK)         
--            WHERE      BookedProduct.BpdPrdIsVehicle = 1 AND BookedProduct.BpdStatus = 'KK' AND BookedProduct.BpdRntId = @sRntId        
            ( SELECT     ISNULL(CAST(SUM(BpdGrossAmt) AS VARCHAR),'Paid to agent')        
              FROM       BookedProduct (NOLOCK)         
              WHERE      BookedProduct.BpdPrdIsVehicle = 1         
                         AND BookedProduct.BpdStatus = 'QN'         
                         AND BookedProduct.BpdRntId = @sRntId        
                         AND BpdIsCusCharge = 1        
          ) AS VehicleCharge,        
          (        
            SELECT  TOP 1 dbo.getcodcode(BookedProduct.BpdCodCurrId)        
            FROM    BookedProduct (NOLOCK)         
            WHERE   BookedProduct.BpdRntId = @sRntId        
          ) AS CurrencyCode,    
-- RKS : 11-May-2010
--        
--		 (    
--            SELECT TOP 1 ISNULL(PackageTermsAndConditions.PtcURL,'')    
--            FROM   Rental (NOLOCK) INNER JOIN    
--                   PackageTermsAndConditions (NOLOCK) ON Rental.RntPkgId = PackageTermsAndConditions.PtcPkgId INNER JOIN    
--                   Product (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product.PrdId INNER JOIN    
--                   [Type] (NOLOCK) ON Product.PrdTypId = Type.TypId AND PackageTermsAndConditions.PtcClaId = Type.TypClaId    
--            WHERE  Rental.RntId = @sRntId    
--                 AND     
--                   Rental.RntCkoWhen BETWEEN PackageTermsAndConditions.PtcFromDate AND PackageTermsAndConditions.PtcToDate    
--          ) AS PackageTandCURL,
		  dbo.WEBA_getTCLink(Rental.RntPkgId,Rental.RntPrdRequestedVehicleId,Rental.RntCkoWhen) AS PackageTandCURL,
		  ISNULL(RntArrivalConnectionRef,'') AS ArrivalRef,
          ISNULL(CONVERT(VARCHAR,RntArrivalConnectionWhen,103) + ' ' + CONVERT(VARCHAR,RntArrivalConnectionWhen,108),'') AS ArrivalDateTime,
          ISNULL(CONVERT(VARCHAR,RntDeptConnectionWhen,103) + ' ' + CONVERT(VARCHAR,RntArrivalConnectionWhen,108),'') AS DepartureDateTime,
          ISNULL(RntDeptConnectionRef,'') AS DepartureRef,
          -- Shoel : 14.6.10 : Change for Loyalty Card Number
          CASE ( SELECT TOP(1) 1 FROM Package (NOLOCK) WHERE Rental.RntPkgId = Package.PkgId AND ISNULL(Package.PkgRequiresLoyaltyCard, 0) = 1 ) 
               WHEN 1 THEN 'True'
               ELSE 'False'
          END AS LoyaltyCardNumberRequired
          -- Shoel : 14.6.10 : Change for Loyalty Card Number
     FROM                           
          Rental WITH (NOLOCK)                   
     WHERE                       
          Rental.RntId = @sRntId                  
     FOR XML AUTO                            
     SELECT '</RentalInfo>'                      
                      
     SELECT '<CustomerInfo>'                      
     DECLARE @CustomerList AS TABLE (TrvCusId VARCHAR(64))                  
     DELETE @CustomerList                  
                  
     INSERT INTO @CustomerList                  
     SELECT                       
     TrvCusId                  
     FROM                           
          Traveller (NOLOCK)                  
     WHERE                       
          TrvRntId = @sRntId                  
                  
     WHILE EXISTS(SELECT 1 FROM @CustomerList)                  
     BEGIN                  
          DECLARE @CusId AS VARCHAR(64)                        SELECT TOP 1 @CusId = TrvCusId FROM @CustomerList                  
                  
          EXEC WEBA_GetCustomerDetails             
               @sCusId=@CusId                  
                  
          DELETE @CustomerList WHERE TrvCusId = @CusId                  
     END                  
--     EXEC cust_get_CustomerSummary @ThisRental=1,@sBookId=@sBooId,@sRntID=@sRntId             
                   
     SELECT '</CustomerInfo>'                      
                        
     DECLARE @TempTable TABLE                     
     (                    
          PrdName VARCHAR(64)                    
          ,PrdShortName VARCHAR(8)             
          ,PrdInfo VARCHAR(MAX)                 
          ,ClaCode VARCHAR(12)                    
          ,BpdGrossAmt VARCHAR(15)                    
          ,BpdHirePeriod INT                    
          ,BpdCkoWhen VARCHAR(24)                    
          ,BpdCkiWhen VARCHAR(24)                    
          ,BpdQty INT                    
          ,BpdId VARCHAR(64)              
          ,BpdIsCusCharge VARCHAR(5)            
          ,SapId VARCHAR(64)          
          ,MinAge VARCHAR(12)          
          ,MaxAge VARCHAR(12)          
          ,UOMCode VARCHAR(12)          
          ,PrdIsInclusive VARCHAR(5)          
          ,NoteText VARCHAR(MAX)          
          ,DisplayOrder INT          
          ,Rating INT          
          ,Rate MONEY          
          ,MinCharge MONEY          
          ,MaxCharge MONEY          
          ,PromptForQuantity VARCHAR(5)          
          ,ProductHirePeriod INT          
          ,PrdId VARCHAR(64)    
		  ,GrossCalcPercentage varchar(24)        
     )                    
                    
     INSERT INTO                     
          @TempTable                    
     SELECT                         
          Product.PrdName,                     
          Product.PrdShortName,             
          dbo.fn_HtmlEncode(ISNULL(Product.PrdInfoURL,''),1) AS PrdInfo,                  
          Class.ClaCode,                     
          CONVERT(VARCHAR,BookedProduct.BpdGrossAmt,0),                    
          BookedProduct.BpdHirePeriod,                     
          CONVERT(VARCHAR,BookedProduct.BpdCkoWhen,103) + ' ' + CONVERT(VARCHAR,BookedProduct.BpdCkoWhen,108),                    
          CONVERT(VARCHAR,BookedProduct.BpdCkiWhen,103) + ' ' + CONVERT(VARCHAR,BookedProduct.BpdCkiWhen,108),                    
          BookedProduct.BpdQty,                
          BookedProduct.BpdId,          
          CASE ISNULL(BookedProduct.BpdIsCusCharge,0) WHEN 0 THEN 'false' ELSE 'true' END,          
          SapId,                
          ISNULL(dbo.getProductAttributeValue(SapId,'MINAGE'),0),           
          ISNULL(dbo.getProductAttributeValue(SapId,'MAXAGE'),0),          
          dbo.getCodCode(BpdCodUOMID),          
          CASE ISNULL(PrdIsInclusive,0) WHEN 0 THEN 'false' ELSE 'true' END,          
          CASE ISNULL(PrdIsInclusive,0) WHEN 0 THEN '' ELSE           
		  IsNull((		
			SELECT TOP 1 NteDesc FROm Aurora.dbo.Note (NOLOCK) WHERE NteVdrId = (SELECT TOP 1 Charge.ChgBaseId from Aurora.dbo.Charge  (NOLOCK) WHERE ChgBpdId = BpdId)
          ),'') END 
			AS NoteText,          
          (          
               SELECT DISTINCT           
               ISNULL(WebHireItems.HreItmDispOrder,0)          
               FROM                        
               Rental (NOLOCK) INNER JOIN          
               Product AS Product_1 WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product_1.PrdId INNER JOIN          
               Type (NOLOCK) ON Product_1.PrdTypId = Type.TypId INNER JOIN          
               WebHireItems (NOLOCK) INNER JOIN          
               Product AS Product_2 WITH (NOLOCK) ON WebHireItems.HreItmPrdId = Product_2.PrdId INNER JOIN          
               WebNonVehicleSetup (NOLOCK) ON WebHireItems.HreItmWebSetupId = WebNonVehicleSetup.WebSetupId ON Type.TypClaId = WebNonVehicleSetup.WebClaId INNER JOIN          
               Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId AND WebNonVehicleSetup.WebBrdCode = Package.PkgBrdCode AND WebNonVehicleSetup.WebCtyCode = Package.PkgCtyCode          
       WHERE               
               WebHireItems.HreItmPrdId = Product.PrdId          
               AND          
               Rental.RntId = @sRntId          
          ) AS DisplayOrder,          
        CASE WHEN PrdShortName = 'BONUSPAC' THEN (          
												   SELECT TOP 1 
												   ISNULL(WebHireItems.HreItmRating,0)          
												   FROM                   
												   Rental (NOLOCK) INNER JOIN          
												   Product AS Product_1 WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product_1.PrdId INNER JOIN          
												   Type (NOLOCK) ON Product_1.PrdTypId = Type.TypId INNER JOIN          
												   WebHireItems (NOLOCK) INNER JOIN          
												   Product AS Product_2 WITH (NOLOCK) ON WebHireItems.HreItmPrdId = Product_2.PrdId INNER JOIN          
												   WebNonVehicleSetup (NOLOCK) ON WebHireItems.HreItmWebSetupId = WebNonVehicleSetup.WebSetupId ON Type.TypClaId = WebNonVehicleSetup.WebClaId INNER JOIN          
												   Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId AND WebNonVehicleSetup.WebBrdCode = Package.PkgBrdCode AND WebNonVehicleSetup.WebCtyCode = Package.PkgCtyCode          
												   WHERE               
												   WebHireItems.HreItmPrdId = (SELECT top 1 Dum1.PrdId FROM dbo.Product Dum1 (NOLOCK) WHERE Dum1.PrdShortName = 'BBPACK')         
												   AND          
												   Rental.RntId = @sRntId          
											  )
			WHEN PrdShortName = 'SMARTPAC' THEN
											(          
											   SELECT TOP 1 
											   ISNULL(WebHireItems.HreItmRating,0)          
											   FROM                   
											   Rental (NOLOCK) INNER JOIN          
											   Product AS Product_1 WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product_1.PrdId INNER JOIN          
											   Type (NOLOCK) ON Product_1.PrdTypId = Type.TypId INNER JOIN          
											   WebHireItems (NOLOCK) INNER JOIN          
											   Product AS Product_2 WITH (NOLOCK) ON WebHireItems.HreItmPrdId = Product_2.PrdId INNER JOIN          
											   WebNonVehicleSetup (NOLOCK) ON WebHireItems.HreItmWebSetupId = WebNonVehicleSetup.WebSetupId ON Type.TypClaId = WebNonVehicleSetup.WebClaId INNER JOIN          
											   Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId AND WebNonVehicleSetup.WebBrdCode = Package.PkgBrdCode AND WebNonVehicleSetup.WebCtyCode = Package.PkgCtyCode          
											   WHERE               
											   WebHireItems.HreItmPrdId = (SELECT top 1 Dum1.PrdId FROM dbo.Product Dum1 (NOLOCK) WHERE Dum1.PrdShortName = 'BPPACK')         
											   AND          
											   Rental.RntId = @sRntId          
												)
			WHEN PrdShortName = 'PREMPAC' THEN 
											(          
											   SELECT TOP 1            
											   ISNULL(WebHireItems.HreItmRating,0)          
											   FROM                   
											   Rental (NOLOCK) INNER JOIN          
											   Product AS Product_1 WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product_1.PrdId INNER JOIN          
											   Type (NOLOCK) ON Product_1.PrdTypId = Type.TypId INNER JOIN          
											   WebHireItems (NOLOCK) INNER JOIN          
											   Product AS Product_2 WITH (NOLOCK) ON WebHireItems.HreItmPrdId = Product_2.PrdId INNER JOIN          
											   WebNonVehicleSetup (NOLOCK) ON WebHireItems.HreItmWebSetupId = WebNonVehicleSetup.WebSetupId ON Type.TypClaId = WebNonVehicleSetup.WebClaId INNER JOIN          
											   Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId AND WebNonVehicleSetup.WebBrdCode = Package.PkgBrdCode AND WebNonVehicleSetup.WebCtyCode = Package.PkgCtyCode          
											   WHERE               
											   WebHireItems.HreItmPrdId = (SELECT top 1 Dum1.PrdId FROM dbo.Product Dum1 (NOLOCK) WHERE Dum1.PrdShortName = 'MPPACK')         
											   AND          
											   Rental.RntId = @sRntId          
												)
			ELSE
				 (          
               SELECT TOP 1           
               ISNULL(WebHireItems.HreItmRating,0)          
               FROM                   
               Rental (NOLOCK) INNER JOIN          
               Product AS Product_1 WITH (NOLOCK) ON Rental.RntPrdRequestedVehicleId = Product_1.PrdId INNER JOIN          
               Type (NOLOCK) ON Product_1.PrdTypId = Type.TypId INNER JOIN          
               WebHireItems (NOLOCK) INNER JOIN          
               Product AS Product_2 WITH (NOLOCK) ON WebHireItems.HreItmPrdId = Product_2.PrdId INNER JOIN          
               WebNonVehicleSetup (NOLOCK) ON WebHireItems.HreItmWebSetupId = WebNonVehicleSetup.WebSetupId ON Type.TypClaId = WebNonVehicleSetup.WebClaId INNER JOIN          
               Package (NOLOCK) ON Rental.RntPkgId = Package.PkgId AND WebNonVehicleSetup.WebBrdCode = Package.PkgBrdCode AND WebNonVehicleSetup.WebCtyCode = Package.PkgCtyCode          
               WHERE               
               WebHireItems.HreItmPrdId = Product.PrdId          
               AND          
               Rental.RntId = @sRntId          
          ) END AS Rating,          
          (SELECT TOP 1 Charge.ChgRate FROM Charge WITH (NOLOCK) WHERE BookedProduct.BpdId = Charge.ChgBpdId AND ChgIsFoc = 0),          
          0,--(SELECT TOP 1 Charge.ChgMinCharge FROM Charge WITH (NOLOCK) WHERE BookedProduct.BpdId = Charge.ChgBpdId AND ChgIsFoc = 0),          
          (SELECT TOP 1 Charge.ChgMaxCharge FROM Charge WITH (NOLOCK) WHERE BookedProduct.BpdId = Charge.ChgBpdId AND ChgIsFoc = 0),          
          CASE WHEN ISNULL(dbo.getProductAttributeValue(SaleableProduct.SapId,'PROMPTQTY'),0) = 1 THEN 'true' ELSE 'false' END    ,          
          BookedProduct.BpdHirePeriod  ,        
          PrdId,    
    CASE WHEN dbo.getProductAttributeValue(BpdSapId,'CHGBALLPRD') = 1     
    THEN IsNull((select top 1 (100-PdsPercentage) from Charge (NOLOCK), chargediscount (NOLOCK), ProductRatediscount (NOLOCK) where chdpdsid = pdsid and chdchgid = chgid and ChgBpdId = BookedProduct.BpdId),0)     
    ELSE '' END as GrossCalcPercentage    
     FROM                             
          Booking WITH (NOLOCK) INNER JOIN                    
          Rental WITH (NOLOCK) ON Booking.BooID = Rental.RntBooID INNER JOIN                    
          BookedProduct WITH (NOLOCK) ON Rental.RntId = BookedProduct.BpdRntId INNER JOIN                    
          SaleableProduct WITH (NOLOCK) ON BookedProduct.BpdSapId = SaleableProduct.SapId INNER JOIN                    
          Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN                    
          Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN                    
          Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID /*INNER JOIN          
          Charge WITH (NOLOCK) ON BookedProduct.BpdId = Charge.ChgBpdId */          
     WHERE                         
          Booking.BooId = @sBooId                     
          AND          
		  BookedProduct.BpdRntId = @sRntId
		  AND           
          BookedProduct.BpdStatus = 'QN'                     
          AND                     
          /*Class.ClaCode IN ('HI', 'FC', 'IN', 'FL', 'MA', 'FE ', 'CR')                     
          AND*/                     
          Product.PrdIsVehicle = 0   
		  AND 1 = (CASE WHEN IsNull(dbo.getProductAttributeValue(Sapid, 'PROCVEH'),0)=1 AND BpdGrossAmt = 0 THEN 0
					   WHEN IsNull(dbo.getProductAttributeValue(Sapid, 'PROCVEH'),0)=1 AND BpdGrossAmt <> 0 THEN 1
						ELSE 1
					END)
		  
    
              
     SELECT '<InsuranceOptions>'           
        
     -- For ER0        
     IF EXISTS(SELECT 1                 
               FROM         
                    Product (NOLOCK) INNER JOIN         
                    @TempTable AS TEMPTABLE ON Product.PrdId = TEMPTABLE.PrdId INNER JOIN         
                    [Type] (NOLOCK) ON  PrdTypId = TypId               
               WHERE          
                    TypCode = 'BOND'        
                    AND        
                    TEMPTABLE.ClaCode = 'IN'     
                    AND  
                    Product.PrdShortName = 'BSTD'             
              )                  
     BEGIN        
           SELECT                    
                  'Standard bond' AS [Name]
                  ,'ER0' AS ShortName                 
                  ,ISNULL(PrdInfo,'') AS PrdInfo             
                  ,ISNULL(ClaCode ,'') AS Class              
                  ,ISNULL(BpdGrossAmt,'') AS GrossAmount              
                  ,ISNULL(BpdHirePeriod,'') AS HirePeriod              
                  ,ISNULL(BpdCkoWhen ,'') AS FromDate              
                  ,ISNULL(BpdCkiWhen ,'') AS ToDate        
                  ,CAST(ISNULL(BpdQty ,'0') AS INT) AS Quantity                             
                  ,ISNULL(BpdId,'') AS BpdId                
                  ,ISNULL(BpdIsCusCharge,'false') AS IsCustomerCharge          
                  ,ISNULL(SapId,'') AS SapId          
                  ,ISNULL(MinAge,'0') AS MinAge          
                  ,ISNULL(MaxAge,'0') AS MaxAge          
                  ,ISNULL(UOMCode,'') AS UOMCode          
                  ,ISNULL(PrdIsInclusive,'false') AS PrdIsInclusive          
                  ,ISNULL(NoteText,'') AS NoteText          
                  ,ISNULL(DisplayOrder,0) AS DisplayOrder          
                  ,ISNULL(Rating,0) AS Rating             
                  ,ISNULL(Rate,0) AS Rate             
                  ,ISNULL(MinCharge,0) AS MinCharge          
                  ,ISNULL(MaxCharge,0) AS MaxCharge          
                  ,ISNULL(PromptForQuantity,'false') AS PromptForQuantity          
                  ,ISNULL(ProductHirePeriod,0) AS ProductHirePeriod     
                  ,ISNULL(GrossCalcPercentage,'') AS GrossCalcPercentage           
           FROM               
                  @TempTable AS InsuranceOption              
           WHERE               
                  ClaCode = 'IN'              
           FOR XML AUTO         
     END        
--     ELSE        
--     BEGIN        
     -- For ER0        
           SELECT                    
                  ISNULL(PrdName ,'') AS Name              
                  ,ISNULL(PrdShortName,'') AS ShortName                 
                  ,ISNULL(PrdInfo,'') AS PrdInfo             
                  ,ISNULL(ClaCode ,'') AS Class              
                  ,ISNULL(BpdGrossAmt,'') AS GrossAmount              
                  ,ISNULL(BpdHirePeriod,'') AS HirePeriod              
                  ,ISNULL(BpdCkoWhen ,'') AS FromDate              
                  ,ISNULL(BpdCkiWhen ,'') AS ToDate              
                  ,CAST(ISNULL(BpdQty ,'0') AS INT) AS Quantity                           
                  ,ISNULL(BpdId,'') AS BpdId                
                  ,ISNULL(BpdIsCusCharge,'false') AS IsCustomerCharge          
                  ,ISNULL(SapId,'') AS SapId          
                  ,ISNULL(MinAge,'0') AS MinAge          
                  ,ISNULL(MaxAge,'0') AS MaxAge          
                  ,ISNULL(UOMCode,'') AS UOMCode          
                  ,ISNULL(PrdIsInclusive,'false') AS PrdIsInclusive          
                  ,ISNULL(NoteText,'') AS NoteText          
                  ,ISNULL(DisplayOrder,0) AS DisplayOrder          
                  ,ISNULL(Rating,0) AS Rating             
                  ,ISNULL(Rate,0) AS Rate             
                  ,ISNULL(MinCharge,0) AS MinCharge          
                  ,ISNULL(MaxCharge,0) AS MaxCharge          
                  ,ISNULL(PromptForQuantity,'false') AS PromptForQuantity          
                  ,ISNULL(ProductHirePeriod,0) AS ProductHirePeriod     
                  ,ISNULL(GrossCalcPercentage,'') AS GrossCalcPercentage           
           FROM               
                  @TempTable AS InsuranceOption              
           WHERE               
                  ClaCode = 'IN'              
           FOR XML AUTO                      
        
--     END        
        
     SELECT '</InsuranceOptions>'                      
         
                 
     SELECT '<ExtraHireItems>'                      
     SELECT                          
          ISNULL(PrdName ,'') AS Name                    
          ,ISNULL(PrdShortName,'') AS ShortName               
          ,ISNULL(PrdInfo,'') AS PrdInfo             
          ,ISNULL(ClaCode ,'') AS Class                    
          ,ISNULL(BpdGrossAmt,'') AS GrossAmount                    
          ,ISNULL(BpdHirePeriod,'') AS HirePeriod                    
          ,ISNULL(BpdCkoWhen ,'') AS FromDate                    
          ,ISNULL(BpdCkiWhen ,'') AS ToDate                    
          ,CAST(ISNULL(BpdQty ,'0') AS INT) AS Quantity                                    
          ,ISNULL(BpdId,'') AS BpdId                
          ,ISNULL(BpdIsCusCharge,'false') AS IsCustomerCharge          
          ,ISNULL(SapId,'') AS SapId          
          ,ISNULL(MinAge,'0') AS MinAge          
          ,ISNULL(MaxAge,'0') AS MaxAge          
          ,ISNULL(UOMCode,'') AS UOMCode          
          ,ISNULL(PrdIsInclusive,'false') AS PrdIsInclusive    
          ,ISNULL(NoteText,'') AS NoteText          
          ,ISNULL(DisplayOrder,0) AS DisplayOrder          
          ,ISNULL(Rating,0) AS Rating          
          ,ISNULL(Rate,0) AS Rate          
          ,ISNULL(MinCharge,0) AS MinCharge          
          ,ISNULL(MaxCharge,0) AS MaxCharge          
          ,ISNULL(PromptForQuantity,'false') AS PromptForQuantity          
          ,ISNULL(ProductHirePeriod,0) AS ProductHirePeriod    
          ,ISNULL(GrossCalcPercentage,'') AS GrossCalcPercentage          
     FROM                     
          @TempTable AS ExtraHireItem                    
     WHERE                     
          ClaCode NOT IN ('IN','FC')               
     FOR XML AUTO   


	SELECT                          
          ISNULL(PrdName ,'') AS Name                    
          ,ISNULL(PrdShortName,'') AS ShortName               
          ,ISNULL(PrdInfo,'') AS PrdInfo             
          ,ISNULL(ClaCode ,'') AS Class                    
          ,ISNULL(BpdGrossAmt,'') AS GrossAmount                    
          ,ISNULL(BpdHirePeriod,'') AS HirePeriod                    
          ,ISNULL(BpdCkoWhen ,'') AS FromDate                    
          ,ISNULL(BpdCkiWhen ,'') AS ToDate                    
          ,CAST(ISNULL(BpdQty ,'0') AS INT) AS Quantity                  
          ,ISNULL(BpdId,'') AS BpdId                
          ,ISNULL(BpdIsCusCharge,'false') AS IsCustomerCharge          
          ,ISNULL(SapId,'') AS SapId          
          ,ISNULL(MinAge,'0') AS MinAge          
          ,ISNULL(MaxAge,'0') AS MaxAge          
          ,ISNULL(UOMCode,'') AS UOMCode          
          ,ISNULL(PrdIsInclusive,'false') AS PrdIsInclusive          
          ,ISNULL(NoteText,'') AS NoteText          
          ,ISNULL(DisplayOrder,0) AS DisplayOrder          
          ,ISNULL(Rating,0) AS Rating          
          ,ISNULL(Rate,0) AS Rate          
          ,ISNULL(MinCharge,0) AS MinCharge          
          ,ISNULL(MaxCharge,0) AS MaxCharge          
          ,ISNULL(PromptForQuantity,'false') AS PromptForQuantity          
          ,ISNULL(ProductHirePeriod,0) AS ProductHirePeriod    
          ,ISNULL(GrossCalcPercentage,'') AS GrossCalcPercentage          
     FROM                     
          @TempTable AS ExtraHireItem, Charge (NOLOCK)                    
     WHERE                     
          ClaCode  = 'FC' AND ChgBpdId = ExtraHireItem.BpdId AND ChgBaseTableName <>'Prr'
     FOR XML AUTO   

	-- Ferry Crosing
	SELECT                          
          ISNULL(PrdName ,'') + ' - ' + isNull(dbo.getCodCode(PrrCodPrtId),'') AS Name                    
          ,ISNULL(PrdShortName,'') AS ShortName               
          ,ISNULL(PrdInfo,'') AS PrdInfo             
          ,ISNULL(ClaCode ,'') AS Class                    
          ,ISNULL(ChgGrossAmt,'') AS GrossAmount                    
          ,ISNULL(BpdHirePeriod,'') AS HirePeriod                    
          ,ISNULL(BpdCkoWhen ,'') AS FromDate                    
          ,ISNULL(BpdCkiWhen ,'') AS ToDate                    
          ,CAST(ISNULL(ChgQty ,'0') AS INT) AS Quantity                    
          ,ISNULL(BpdId,'') AS BpdId                
          ,ISNULL(BpdIsCusCharge,'false') AS IsCustomerCharge          
          ,ISNULL(SapId,'') AS SapId          
          ,ISNULL(MinAge,'0') AS MinAge          
          ,ISNULL(MaxAge,'0') AS MaxAge          
          ,ISNULL(UOMCode,'') AS UOMCode          
          ,ISNULL(PrdIsInclusive,'false') AS PrdIsInclusive          
          ,ISNULL(NoteText,'') AS NoteText          
          ,ISNULL(DisplayOrder,0) AS DisplayOrder          
          ,ISNULL(Rating,0) AS Rating          
          ,ISNULL(ChgRate,0) AS Rate          
          ,ISNULL(MinCharge,0) AS MinCharge          
          ,ISNULL(MaxCharge,0) AS MaxCharge          
          ,ISNULL(PromptForQuantity,'false') AS PromptForQuantity          
          ,ISNULL(ProductHirePeriod,0) AS ProductHirePeriod    
          ,ISNULL(GrossCalcPercentage,'') AS GrossCalcPercentage          
     FROM                     
          @TempTable AS ExtraHireItem, Charge (NOLOCK), PersonRate (NOLOCK)                    
     WHERE                     
          ClaCode  = 'FC' AND ChgBpdId = ExtraHireItem.BpdId AND ChgBaseTableName ='Prr' AND ChgBaseId = PrrId
     FOR XML AUTO
		                   
     SELECT '</ExtraHireItems>'                      
         
  -- get Extra driver fee Items if it's applied        
  -- RKS : 05-OCT-2009         
  -- START        
  DECLARE @sCtyCode  VARCHAR(12),        
    @iHireItemId BIGINT,        
    @sPrdShortName VARCHAR(24),        
    @sComCode  VARCHAR(3),        
    @sUsrCode  VARCHAR(64)        
        
   SELECT @sComCode = dbo.fun_getCompany                
                                   (                
                                        NULL,  --@sUserCode VARCHAR(64),                
                                        NULL,  --@sLocCode VARCHAR(64),                
                                        @sBrandCode,   --@sBrdCode VARCHAR(64),                
                                        NULL,  --@sPrdId  VARCHAR(64),                
										NULL,  --@sPkgId  VARCHAR(64),                
                                        NULL,  --@sdummy1 VARCHAR(64),                
                                        NULL,  --@sdummy2 VARCHAR(64),                
                                        NULL   --@sdummy3 VARCHAR(64)                
                                   )                
          
        SELECT @sUsrCode = dbo.WEBP_getValueForCompanyORUserCode(@sComCode)              
            
  SELECT @sCtyCode = dbo.getCountryForLocation(@sPickupLocCode,NULL,NULL)        
         
  SELECT          
     @sPrdShortname = PrdShortName,           
           @iHireItemId = HreItmId           
      FROM           
           dbo.WebNonVehicleSetup (NOLOCK),           
           dbo.WebHireItems (NOLOCK),           
           dbo.Product (NOLOCK),           
           dbo.Class A (NOLOCK),           
           [Type] (NOLOCK),           
           Class B (NOLOCK)              
      WHERE           
           WebSetupId = HreItmWebSetupId           
           AND           
           HreItmPrdId = PrdId           
           AND           
           WebClaId = A.ClaId              
           AND           
           PrdTypId = TypId           
           AND           
           TypClaId = B.ClaId           
           AND           
           B.ClaCode = 'FE'              
           AND           
           WebCtyCode = @sCtyCode        
           AND           
           WebBrdCode = @sBrandCode          
           AND           
           WebNonVehicleSetup.WebClaId = @sClaId        
     AND Exists(SELECT SapId FROM dbo.SaleableProduct (NOLOCK)         
     WHERE SapCtyCode = @sCtyCode AND SapPrdId = PrdId AND IsNull(dbo.getProductAttributeValue(SapID,'EXTRADB'),0)  = 1)        
           
  SELECT '<ExtraDriverFee>'        
  IF IsNull(@sPrdShortName,'')<>''        
  BEGIN        
   begin tran        
   EXEC WEBA_GetPriceForNonVehicleProduct              
      @sRntId   = @sRntId,              
      @sPrdShortName  = @sPrdShortName,              
      @sUserId  = @sUsrCode,              
      @iHireItemId = @iHireItemId,              
      @sRetErr  = NULL         
   rollback tran        
  END        
  SELECT '</ExtraDriverFee>'        
        
  -- get Extra driver fee Items if it's applied        
  -- RKS : 05-OCT-2009         
  -- END        
      
     -- Shoel . 13.10.9      
     -- this section is used for charge summary screen only      
     SELECT '<PaymentInfo>'      
     EXEC WEBA_getPaymentDetails @sRntId,1        
     SELECT '</PaymentInfo>'      
     -- Shoel . 13.10.9           

	-- STUFF FROM GetBookingDetails
	--SELECT @sErrors = ''

	RETURN 
END
GO


IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

IF EXISTS (SELECT * FROM #tmpErrors)
BEGIN
PRINT N'Rollback transaction'
ROLLBACK TRANSACTION
END
GO
IF @@TRANCOUNT>0 BEGIN
PRINT N'Commit transaction'
COMMIT TRANSACTION
END
GO
DROP TABLE #tmpErrors
GO

