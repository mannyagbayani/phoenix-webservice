ALTER PROC WEBP_GetBrandLookups  --  'NZ','THL','B'
 	 @sCtyCode AS VARCHAR(2) = ''
	,@sComCode AS VARCHAR(3) = ''
	,@sBrdCode AS VARCHAR(1) = ''
AS  
BEGIN

	SET NOCOUNT ON

	-- Set blank Params to Null
	IF LTRIM(RTRIM(@sCtyCode)) = ''
		SET @sCtyCode = NULL
	IF LTRIM(RTRIM(@sComCode)) = ''
		SET @sComCode = NULL
	IF LTRIM(RTRIM(@sBrdCode)) = ''
		SET @sBrdCode = NULL

	SELECT DISTINCT    
	 BrdCode "code"  
	 ,BrdName "name"  
	FROM           
		brand (NOLOCK) INNER JOIN
		LocationBrand (NOLOCK) ON Brand.BrdCode = LocationBrand.LbrBrdCode INNER JOIN
		Location (NOLOCK) ON LocationBrand.LbrLocCode = Location.LocCode INNER JOIN
		TownCity (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
	WHERE       
	 BrdIsActive = 1 
	 AND 
--	 BrdIsGeneric = 0
--	 AND   
	 BrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode
     AND
     BrdComCode LIKE ISNULL(@sComCode,'%') -- Company Code
	 AND 
	 TctCtyCode LIKE ISNULL(@sCtyCode, '%') -- Country Code
	FOR XML AUTO

	RETURN

END

GO

GRANT EXEC ON WEBP_GetBrandLookups TO PUBLIC
GO
