ALTER PROC WEBP_GetCountryVehicles --'NZ','KXS','X','SCARX'
 	 @sCtyCode AS VARCHAR(2) = ''
	,@sComCode AS VARCHAR(3) = ''
	,@sBrdCode AS VARCHAR(1) = ''
	,@sVehCode AS VARCHAR(64) = ''
AS

BEGIN
	
	-- Set blank Params to Null
	IF LTRIM(RTRIM(@sCtyCode)) = ''
		SET @sCtyCode = NULL
	IF LTRIM(RTRIM(@sComCode)) = ''
		SET @sComCode = NULL
	IF LTRIM(RTRIM(@sBrdCode)) = ''
		SET @sBrdCode = NULL
	IF LTRIM(RTRIM(@sVehCode)) = ''
		SET @sVehCode = NULL

	SELECT DISTINCT 
		SapCtyCode "countryCode",
		PrdBrdCode "brand", 
		'' + Class.ClaCode "type",
		brdname + ' ' +	PrdName "name", 
		SapCtyCode + Class.ClaCode + PrdBrdCode + '.' + PrdShortName "value",
		'' + ISNULL(patvalue,'0') "maxPax",
		'' + ISNULL(PrdNoOfAdult,'0') "maxAdults",
		'' + ISNULL(PrdNoOfChild,'0') "maxChildren",
		'' + ISNULL(PrdNoOfInfant,'0') "maxInfants"
	FROM         
		Product "vehicle" WITH (NOLOCK)
		INNER JOIN
		SaleableProduct "vehicles" WITH (NOLOCK) ON PrdId = SapPrdId 
		INNER JOIN
		[Type] WITH (NOLOCK) ON PrdTypId = Type.TypId 
		INNER JOIN
		Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID
		INNER JOIN
		Brand WITH (NOLOCK) ON PrdBrdCode = Brand.BrdCode
		INNER JOIN
		ProductAttribute WITH (NOLOCK) ON SapId = ProductAttribute.PatSapId 
		INNER JOIN
		Attribute WITH (NOLOCK) ON ProductAttribute.PatAttId = Attribute.AttId
	WHERE     
		PrdIsActive = 1 
		AND 
		PrdIsVehicle = 1 
		AND 
		Class.ClaIsVehicle = 1
		AND
		AttCode = 'MAXCUSTS'
		AND
		AttIsActive = 1
		AND
		SapIsBase=1
		AND
		SapCtyCode LIKE ISNULL(@sCtyCode,'%') -- CountryCode
		AND
		BrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode
		AND
		BrdComCode LIKE ISNULL(@sComCode,'%') -- Company Code
		AND
		PrdShortName LIKE ISNULL(@sVehCode,'%') -- Vechicle Code
	order by "countryCode"
	for xml auto

	RETURN

END

GO

GRANT EXEC ON WEBP_GetCountryVehicles TO PUBLIC
GO