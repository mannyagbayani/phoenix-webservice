ALTER PROC [dbo].[WEBP_GetVehicles]    
       @sBrdCode VARCHAR(1)    
     , @sCkoLoc VARCHAR(3)    
     , @sCkiLoc VARCHAR(3)    
     , @nAdults INT    
     , @nChildren INT    
     , @nInfants INT    
     , @bIsVan BIT    
AS    
    
BEGIN    
     SET NOCOUNT ON     
  
     SELECT DISTINCT 
          Product.PrdId, 
          Product.PrdShortName, 
          Product.PrdName
     FROM 
          Package WITH (NOLOCK) INNER JOIN
          PackageProduct WITH (NOLOCK) ON Package.PkgId = PackageProduct.PplPkgId INNER JOIN
          SaleableProduct WITH (NOLOCK) ON PackageProduct.PplSapId = SaleableProduct.SapId INNER JOIN
          Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN
          Type WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN
          Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID INNER JOIN
          LocationClass WITH (NOLOCK) ON Class.ClaID = LocationClass.LcsClaId INNER JOIN
          Location WITH (NOLOCK) ON LocationClass.LcsLocCode = Location.LocCode INNER JOIN
          LocationBrand ON Location.LocCode = LocationBrand.LbrLocCode AND Package.PkgBrdCode = LocationBrand.LbrBrdCode
     WHERE 
          Package.PkgBrdCode = @sBrdCode
          AND
          Package.PkgBookedToDate >= CURRENT_TIMESTAMP
          AND
          Package.PkgIsActive = 'Active' 
          AND
          Product.PrdIsVehicle = 1
          AND
          Product.PrdIsActive = 1
          AND
          ISNULL( Product.PrdNoOfAdult, 0) >= @nAdults
          AND 
          ISNULL( Product.PrdNoOfChild, 0) >= @nChildren
          AND
          ISNULL( Product.PrdNoOfInfant, 0) >= @nInfants
          AND 
          ISNULL( Product.PrdToAppearOnFS, 0) = 1
          AND
          Class.ClaCode = (SELECT CASE @bIsVan WHEN 1 THEN 'AV' WHEN 0 THEN 'AC' END AS ClaCode)
          AND
          Location.LocCode IN (@sCkoLoc, @sCkiLoc)
          AND
          Product.PrdWebEnabledDate <= CURRENT_TIMESTAMP
     ORDER BY 
          Product.PrdShortName
  
     RETURN    
END    
  