ALTER PROCEDURE [dbo].[PackageSetup_GetB2CInformations] -- 'C67509E3-7ACA-444E-9A03-54D86DEAD817' /*'13EFC623-2DE1-447F-8507-01C911BBC2A1'  */
     @PkgId   VARCHAR(64)    
AS    
BEGIN  
     SET NOCOUNT ON         
     SELECT    
          Package.PkgWebEnabledDate,    
          --     PgkTCREFurl,  -- Removed by Shoel on 19.5.9 as TC Refs are no longer stored in Package Table  
          Package.PgkINFOurl,    
          Package.PkgParentID,
          PK1.PkgCode AS PkgParentCode,
          PK1.PkgName AS PkgParentName
     FROM   
          Package WITH (NOLOCK)  
          LEFT OUTER JOIN Package PK1 (NOLOCK) ON PK1.PkgId = Package.PkgParentId
     WHERE   
          Package.PkgId = @PkgId    
    
    
    IF EXISTS  
     ( SELECT   
            P.PkgId    
       FROM   
            PackageSetupDomesticCountries (NOLOCK) P    
       INNER JOIN  
            Country (NOLOCK) C   
       ON   
            C.CtyCode = P.PkgCtyCode    
       WHERE   
            P.PkgId = @PkgId   
     )  
     BEGIN  
          SELECT   
               P.PkgSetupId,    
               C.CtyCode,     
               C.CtyCode + '-' + C.CtyName AS CtyName,    
               P.PkgId    
          FROM   
               PackageSetupDomesticCountries (NOLOCK) P    
               INNER JOIN  
               Country (NOLOCK) C   
               ON   
               C.CtyCode = P.PkgCtyCode    
          WHERE   
               P.PkgId = @PkgId    
     END  
     ELSE  
     BEGIN  
          -- Empty Row  
          SELECT '' AS PkgSetupId,  
     '' AS CtyCode,  
     '' AS CtyName,  
     @PkgId  
     END  
       
  
     -- Vehicle Class Data  
     SELECT   
          '' AS ClaID  
          ,'' AS Code  
          ,'' AS ClaCode  
     UNION   
     SELECT    
          ClaID,   
          ClaCode + ' - ' + ClaDesc AS Code,  
          ClaCode  
     FROM           
          Class WITH (NOLOCK)  
     WHERE       
          ClaIsVehicle = 1  
     ORDER BY   
          ClaCode  
  
     -- T&C Data  
     SELECT       
          PtcId,   
          PtcClaId,  
          PtcURL,  
          ClaCode,  
          PtcFromDate,  
          PtcToDate  
     FROM           
          PackageTermsAndConditions WITH (NOLOCK)  
          INNER JOIN   
          Class WITH (NOLOCK) ON ClaID = PtcClaId  
     WHERE       
          PtcPkgId = @PkgId --'0DF02FAF-7307-44F3-8760-424AECAA8893'  
          AND   
          PtcToDate >= CAST(CAST(GETDATE() AS INT) AS SMALLDATETIME)  
          AND   
          ClaID = PtcClaId  
     ORDER BY   
         ClaCode, PtcFromDate  

     -- Package Child stuff
     SELECT     
          PkgId, 
          PkgCode, 
          PkgName
     FROM         
          Package WITH (NOLOCK)
     WHERE     
          PkgParentID = @PkgId -- 'C67509E3-7ACA-444E-9A03-54D86DEAD817'

     RETURN  
  
END