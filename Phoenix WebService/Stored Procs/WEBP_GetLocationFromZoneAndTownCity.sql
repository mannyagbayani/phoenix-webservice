ALTER PROC WEBP_GetLocationFromZoneAndTownCity -- 'NZ','THL','P','CIF','AKL',1
	 @sCtyCode AS VARCHAR(2) = ''
	,@sComCode AS VARCHAR(3) = ''
	,@sBrdCode AS VARCHAR(1) = ''
	,@sZoneCode AS VARCHAR(3)
	,@sTctCode AS VARCHAR(3)
	,@bTestMode AS BIT = 0
AS 

BEGIN
	SET NOCOUNT ON

	-- Set blank Params to Null
	IF LTRIM(RTRIM(@sCtyCode)) = ''
		SET @sCtyCode = NULL
	IF LTRIM(RTRIM(@sComCode)) = ''
		SET @sComCode = NULL
	IF LTRIM(RTRIM(@sBrdCode)) = ''
		SET @sBrdCode = NULL
	
	IF @bTestMode = 0
		BEGIN
			SELECT DISTINCT 
				Location.LocCode
			FROM         
				Location WITH (NOLOCK) INNER JOIN
				LocationBrand  WITH (NOLOCK)ON Location.LocCode = LocationBrand.LbrLocCode INNER JOIN
				LocationZone WITH (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN
				TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
			WHERE     
				LTRIM(RTRIM(LocationZone.LocZoneCode)) = LTRIM(RTRIM(@sZoneCode))
				AND 
				LocationBrand.LbrBrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode
				AND 
				Location.LocIsActive = 1
				AND 
				ISNULL(Location.LocIsWebEnabled,0) = 1
				AND 
				Location.LocComCode LIKE ISNULL(@sComCode,'%') -- Company Code
				AND 
				TownCity.TctCtyCode LIKE ISNULL(@sCtyCode, '%') -- Country Code
				AND 
				ISNULL(Location.LocWebEnabledDate, GETDATE()) <= GETDATE()	
				AND
				TownCity.TctCode = 	@sTctCode	
		END
	ELSE
		BEGIN
			SELECT DISTINCT 
				Location.LocCode
			FROM         
				Location WITH (NOLOCK) INNER JOIN
				LocationBrand  WITH (NOLOCK)ON Location.LocCode = LocationBrand.LbrLocCode INNER JOIN
				LocationZone WITH (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN
				TownCity WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode
			WHERE     
				LTRIM(RTRIM(LocationZone.LocZoneCode)) = LTRIM(RTRIM(@sZoneCode))
				AND 
				LocationBrand.LbrBrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode
				AND 
				Location.LocIsActive = 1
--				AND 
--				ISNULL(Location.LocIsWebEnabled,0) = 1
				AND 
				Location.LocComCode LIKE ISNULL(@sComCode,'%') -- Company Code
				AND 
				TownCity.TctCtyCode LIKE ISNULL(@sCtyCode, '%') -- Country Code
--				AND 
--				ISNULL(Location.LocWebEnabledDate, GETDATE()) <= GETDATE()	
				AND
				TownCity.TctCode = 	@sTctCode		
		END
		
	RETURN
END	

