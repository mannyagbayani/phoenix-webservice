CREATE PROC Package_UpdateTermsAndConditions
	  @XmlData AS VARCHAR(MAX)
	, @PkgId AS VARCHAR(64)
	, @UserId AS VARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Test Section
	--DECLARE @XmlData varchar(max)
	--SET @XmlData ='
	--<TandCs>
	--	<TandC PtcId="6" PtcFromDate="1/04/2009" PtcToDate="1/04/2010" PtcClaId="75F44824-60F8-4CC6-BCEF-CBCAFD4DAEC4" PtcURL="http://www.exploremore.co.nz/terms-conditons-car-0910.aspx" />
	--	<TandC PtcId="5" PtcFromDate="1/04/2009" PtcToDate="1/04/2010" PtcClaId="A2D7C103-6FE5-46DE-B203-854C6FBA1483" PtcURL="http://www.exploremore.co.nz/terms-conditions-campervan-0910.aspx" />
	--	<TandC PtcFromDate="1/04/2011" PtcToDate="1/04/2012" PtcClaId="A2D7C103-6FE5-46DE-B203-854C6FBA1483" PtcURL="http://www.exploremore.co.nz/terms-conditions-campervan-1112.aspx" />
	--</TandCs>'
	--
	--DECLARE @UserId VARCHAR(64)
	--DECLARE @PkgId AS VARCHAR(64)
	--
	--
	--SELECT @PkgId = '0DF02FAF-7307-44F3-8760-424AECAA8893'
	--SELECT @UserId = UsrId from USERINFO WHERE UsrCode = 'sp7'
	-- Test Section


DECLARE @idoc INT

	EXEC sp_xml_preparedocument @idoc OUTPUT, @XmlData

	DECLARE @PassedData TABLE 
	(
		  PtcId  BIGINT
		, PtcFromDate SMALLDATETIME
		, PtcToDate SMALLDATETIME
		, PtcClaId VARCHAR(64)
		, PtcURL VARCHAR(256)
	)
	
	INSERT INTO @PassedData
	SELECT *
	FROM       
		OPENXML (@idoc, '/TandCs/TandC',1)
    WITH (	
		  PtcId  BIGINT
		, PtcFromDate SMALLDATETIME
		, PtcToDate SMALLDATETIME
		, PtcClaId VARCHAR(64)
		, PtcURL VARCHAR(256)
		  )



	DECLARE   @PtcId  BIGINT
			, @PtcFromDate SMALLDATETIME
			, @PtcToDate SMALLDATETIME
			, @PtcClaId VARCHAR(64)
			, @PtcURL VARCHAR(256)


	DECLARE CURSOR1 CURSOR
	FOR 
	SELECT    PtcId  
			, PtcFromDate 
			, PtcToDate 
			, PtcClaId 
			, PtcURL 
	FROM  @PassedData

	OPEN CURSOR1
	FETCH NEXT FROM CURSOR1
	INTO	  @PtcId  
			, @PtcFromDate 
			, @PtcToDate 
			, @PtcClaId 
			, @PtcURL



	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF @PtcId IS NULL
			BEGIN
				-- NEW RECORD :)
				INSERT INTO PackageTermsAndConditions WITH (ROWLOCK)
				(
					  PtcPkgId
					, PtcFromDate 
					, PtcToDate 
					, PtcClaId 
					, PtcURL 
					, AddUsrId
					, AddDateTime
				)
				VALUES
				(
					  @PkgId
					, @PtcFromDate 
					, @PtcToDate 
					, @PtcClaId 
					, @PtcURL
					, @UserId
					, CURRENT_TIMESTAMP
				)
			END
		ELSE
			BEGIN
				UPDATE PackageTermsAndConditions WITH (ROWLOCK)
				SET
					  PtcFromDate = @PtcFromDate
					, PtcToDate = @PtcToDate
					, PtcClaId = @PtcClaId
					, PtcURL = @PtcURL
					, ModUsrId = @UserId
					, ModDateTime = CURRENT_TIMESTAMP
				WHERE
					PtcId = @PtcId
					AND
					PtcPkgId = @PkgId
			END




		FETCH NEXT FROM CURSOR1
		INTO	  @PtcId  
				, @PtcFromDate 
				, @PtcToDate 
				, @PtcClaId 
				, @PtcURL
	END

	CLOSE CURSOR1
	DEALLOCATE CURSOR1


	DECLARE @sError AS VARCHAR(100)
	IF @@ERROR <> 0
		SET @sError = 'ERROR/Error Updating Fleet Package Terms and Conditions'

	IF @sError IS NOT NULL AND @sError <> ''
		SELECT @sError
	ELSE
		SELECT 'GEN046/' + dbo.getErrorString('GEN046', NULL, NULL, NULL, NULL, NULL, NULL)


	RETURN
END
