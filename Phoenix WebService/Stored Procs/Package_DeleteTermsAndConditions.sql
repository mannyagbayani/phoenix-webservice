ALTER PROC [dbo].[Package_DeleteTermsAndConditions]
	  @XmlData AS VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON
	
	-- Test Section
	--DECLARE @XmlData varchar(max)
	--SET @XmlData ='
	--<TandCs>
	--	<TandC PtcId="6" PtcFromDate="1/04/2009" PtcToDate="1/04/2010" PtcClaId="75F44824-60F8-4CC6-BCEF-CBCAFD4DAEC4" PtcURL="http://www.exploremore.co.nz/terms-conditons-car-0910.aspx" />
	--	<TandC PtcId="5" PtcFromDate="1/04/2009" PtcToDate="1/04/2010" PtcClaId="A2D7C103-6FE5-46DE-B203-854C6FBA1483" PtcURL="http://www.exploremore.co.nz/terms-conditions-campervan-0910.aspx" />
	--	<TandC PtcFromDate="1/04/2011" PtcToDate="1/04/2012" PtcClaId="A2D7C103-6FE5-46DE-B203-854C6FBA1483" PtcURL="http://www.exploremore.co.nz/terms-conditions-campervan-1112.aspx" />
	--</TandCs>'
	--
	--DECLARE @UserId VARCHAR(64)
	--DECLARE @PkgId AS VARCHAR(64)
	--
	--
	--SELECT @PkgId = '0DF02FAF-7307-44F3-8760-424AECAA8893'
	--SELECT @UserId = UsrId from USERINFO WHERE UsrCode = 'sp7'
	-- Test Section


DECLARE @idoc INT

	EXEC sp_xml_preparedocument @idoc OUTPUT, @XmlData

	DECLARE @PassedData TABLE 
	(
		  PtcId  BIGINT
	)
	
	INSERT INTO @PassedData
	SELECT *
	FROM       
		OPENXML (@idoc, '/TandCs/TandC',1)
    WITH (	
		  PtcId  BIGINT
		  )

    DELETE 
         PackageTermsAndConditions
    WHERE
         PackageTermsAndConditions.PtcId IN (SELECT PtcId FROM @PassedData)

	DECLARE @sError AS VARCHAR(100)
	IF @@ERROR <> 0
		SET @sError = 'ERROR/Error Deleting Fleet Package Terms and Conditions'

	IF @sError IS NOT NULL AND @sError <> ''
		SELECT @sError
	ELSE
		SELECT 'GEN047/' + dbo.getErrorString('GEN047', NULL, NULL, NULL, NULL, NULL, NULL)

	RETURN
END
