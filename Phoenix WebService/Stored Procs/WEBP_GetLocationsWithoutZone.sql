ALTER PROC [dbo].[WEBP_GetLocationsWithoutZone] --  'NZ','KXS','X'    
   @sCtyCode AS VARCHAR(2) = ''    
   ,@sComCode AS VARCHAR(3) = ''    
   ,@sBrdCode AS VARCHAR(1) = ''    
   --,@sVehCode AS VARCHAR(64) = ''    
AS    
    
BEGIN    
 SET NOCOUNT ON    
    
 -- Set blank Params to Null    
 IF LTRIM(RTRIM(@sCtyCode)) = ''    
  SET @sCtyCode = NULL    
 IF LTRIM(RTRIM(@sComCode)) = ''    
  SET @sComCode = NULL    
 IF LTRIM(RTRIM(@sBrdCode)) = ''    
  SET @sBrdCode = NULL    
-- IF LTRIM(RTRIM(@sVehCode)) = ''    
--  SET @sVehCode = NULL      
      
 DECLARE @DataSet1 TABLE      
 (      
  CompanyList VARCHAR(128),    
  CtyCode VARCHAR(12),       
  CtyName VARCHAR(64),       
  TctCode VARCHAR(12),       
  TctName VARCHAR(64),      
  LocZoneCode VARCHAR(3),       
  LocZoneName VARCHAR(64),       
  LocCode VARCHAR(12),       
  LocName VARCHAR(64),       
  AddAddress1 VARCHAR(64),       
  AddAddress2 VARCHAR(64),       
  AddAddress3 VARCHAR(64),       
  PhnNumber VARCHAR(64),       
 -- CodCode VARCHAR(64),      
  BrandList VARCHAR(64),      
  [Type] VARCHAR(64)      
 )      
       
 INSERT INTO @DataSet1      
       
       
       
 --SELECT DISTINCT       
 -- NULL AS CompanyList,    
 -- Country.CtyCode, Country.CtyName,       
 -- TownCity.TctCode, TownCity.TctName,       
 -- LocationZone.LocZoneCode, LocationZone.LocZoneName,       
 -- Location.LocCode, Location.LocName,       
 -- AddressDetails.AddAddress1, AddressDetails.AddAddress2, AddressDetails.AddAddress3, PhoneNumber.PhnNumber,      
 -- NULL AS BrandList,      
 -- NULL AS [Type]      
 --     
 --    
 --FROM             
 -- TownCity (NOLOCK) INNER JOIN    
 --    Country (NOLOCK) ON TownCity.TctCtyCode = Country.CtyCode INNER JOIN    
 -- Location (NOLOCK) ON Location.LocTctCode = TownCity.TctCode LEFT OUTER JOIN    
 -- LocationZone (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN    
 -- AddressDetails (NOLOCK) ON AddressDetails.AddPrntID = Location.LocCode AND AddressDetails.AddPrntTableName = 'Location' INNER JOIN    
 -- PhoneNumber (NOLOCK) ON PhoneNumber.PhnPrntId = Location.LocCode AND PhoneNumber.PhnPrntTableName = 'Location' INNER JOIN    
 -- Code (NOLOCK) AS Code_1 ON PhoneNumber.PhnCodPhoneId = Code_1.CodId INNER JOIN    
 -- Code (NOLOCK) AS Code_2 ON AddressDetails.AddCodAddressId = Code_2.CodId     
 --WHERE         
 -- (TownCity.TctIsActive = 1)     
 -- AND     
 -- (Country.CtyIsActive = 1)     
 -- AND     
 -- (Location.LocIsActive = 1)     
 -- AND     
 -- (Code_1.CodCode = 'Phone')     
 -- AND     
 --    (Code_2.CodCode = 'Physical')    
 -- AND    
 -- CtyCode LIKE ISNULL(@sCtyCode,'%') -- CountryCode    
    
    
 SELECT DISTINCT     
  NULL AS CompanyList,     
  Country.CtyCode,     
  Country.CtyName,     
  TownCity.TctCode,     
  TownCity.TctName,     
  LocationZone.LocZoneCode,     
  LocationZone.LocZoneName,     
  Location.LocCode,     
  Location.LocName,     
  ISNULL(AddressDetails.AddAddress1,''),     
  ISNULL(AddressDetails.AddAddress2,''),     
  ISNULL(AddressDetails.AddAddress3,''),     
  ISNULL(PhoneNumber.PhnNumber,''),     
  NULL AS BrandList,     
  NULL AS [Type]    
 FROM             
  Brand WITH (NOLOCK) INNER JOIN    
  LocationBrand WITH (NOLOCK) ON Brand.BrdCode = LocationBrand.LbrBrdCode RIGHT OUTER JOIN    
  TownCity WITH (NOLOCK) INNER JOIN    
  Country WITH (NOLOCK) ON TownCity.TctCtyCode = Country.CtyCode INNER JOIN    
  Location WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode LEFT OUTER JOIN    
  LocationZone WITH (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN    
  AddressDetails WITH (NOLOCK) ON AddressDetails.AddPrntID = Location.LocCode AND     
  AddressDetails.AddPrntTableName = 'Location' INNER JOIN    
  PhoneNumber WITH (NOLOCK) ON PhoneNumber.PhnPrntId = Location.LocCode AND     
  PhoneNumber.PhnPrntTableName = 'Location' INNER JOIN    
  Code AS Code_1 WITH (NOLOCK) ON PhoneNumber.PhnCodPhoneId = Code_1.CodId INNER JOIN    
  Code AS Code_2 WITH (NOLOCK) ON AddressDetails.AddCodAddressId = Code_2.CodId ON     
  LocationBrand.LbrLocCode = Location.LocCode    AND     
  Code_1.CodCode = 'Phone'     
  AND     
  Code_2.CodCode = 'Physical'       
 WHERE         
  TownCity.TctIsActive = 1     
  AND     
  Country.CtyIsActive = 1     
  AND     
  Location.LocIsActive = 1     
  AND     
  Country.CtyCode LIKE ISNULL(@sCtyCode, '%') -- country code    
  AND    
  BrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode    
  AND    
  BrdComCode LIKE ISNULL(@sComCode,'%') -- Company Code    
 -- AND    
 -- PrdShortName LIKE ISNULL(@sVehCode,'%') -- Vechicle Code    
    
    
 ORDER BY Country.CtyCode, TownCity.TctCode, LocationZone.LocZoneCode, Location.LocCode    
    
    
    
       
 DECLARE @LocCode AS VARCHAR(12), @CtyCode AS VARCHAR(12)      
       
 DECLARE CURSOR1 CURSOR      
 FOR       
 SELECT DISTINCT LocCode , CtyCode      
 FROM  @DataSet1      
       
 OPEN CURSOR1      
 FETCH NEXT FROM CURSOR1      
 INTO @LocCode , @CtyCode      
       
 WHILE @@FETCH_STATUS = 0      
 BEGIN      
  DECLARE @BrandList AS VARCHAR(64)      
   ,@TypeList AS VARCHAR(64)    
   ,@CompanyList AS VARCHAR(128)    
    
  SELECT  @BrandList = '' -- initialising it      
   ,@TypeList = ''    
   ,@CompanyList = ''     
    
  DECLARE @DummyTable TABLE (LbrBrdCode VARCHAR(1))    
     INSERT INTO @DummyTable    
  SELECT DISTINCT LbrBrdCode     
  FROM LocationBrand (NOLOCK)      
  WHERE LbrLocCode = @LocCode      
  ORDER BY LbrBrdCode    
    
  SELECT @BrandList = @BrandList + LbrBrdCode + ' '      
  FROM @DummyTable     
    
  DELETE @DummyTable    
    
  SELECT @TypeList = @TypeList + Class.ClaCode + ' '      
  FROM LocationClass (NOLOCK) INNER JOIN    
    Class (NOLOCK) ON LocationClass.LcsClaId = Class.ClaID    
  WHERE     LocationClass.LcsLocCode = @LocCode    
  ORDER BY Class.ClaCode    
    
 -- ONLY DO IF COMPANY LIST NOT ALREADY SET!!!    
  DECLARE @TempComList AS VARCHAR(128)    
  SELECT @TempComList = CompanyList FROM @DataSet1 WHERE CtyCode = @CtyCode    
    
  IF @TempComList IS NULL    
  BEGIN    
  DECLARE @TempTable1 TABLE (LocComCode VARCHAR(3))    
    
  INSERT INTO @TempTable1    
  SELECT DISTINCT Location.LocComCode    
  FROM Location (NOLOCK) INNER JOIN    
    TownCity (NOLOCK) ON Location.LocTctCode = TownCity.TctCode    
  WHERE  TownCity.TctCtyCode = @CtyCode AND @CtyCode LIKE ISNULL(@sCtyCode,'%')    
  AND LocComCode LIKE ISNULL(@sComCode,'%')    
  ORDER BY LocComCode     
      
  SELECT @CompanyList = @CompanyList + LocComCode + ' '    
  FROM @TempTable1    
    
  DELETE @TempTable1 -- CLEAR OLD STUFF    
      
  END    
    
       
  SELECT  @BrandList = RTRIM(@BrandList)      
   ,@TypeList = RTRIM(@TypeList)      
   ,@CompanyList = RTRIM(@CompanyList)    
       
  IF @BrandList <> ''      
  BEGIN      
   UPDATE @DataSet1      
   SET [Type] = @TypeList      
   WHERE LocCode = @LocCode      
  END      
    
  IF @TypeList <> ''    
  BEGIN    
    IF CHARINDEX('BK',@TypeList) <> 0 AND CHARINDEX('AC',@TypeList) <> 0 AND CHARINDEX('AV',@TypeList) <> 0    
  SET @TypeList = 'ALL'    
        
    UPDATE @DataSet1      
    SET BrandList = @BrandList      
    WHERE LocCode = @LocCode       
  END    
    
  IF @CompanyList <> ''    
  BEGIN    
   UPDATE @DataSet1      
   SET CompanyList = @CompanyList      
   WHERE CtyCode = @CtyCode      
  END    
       
  FETCH NEXT FROM CURSOR1      
  INTO @LocCode  , @CtyCode    
 END      
       
 CLOSE CURSOR1      
 DEALLOCATE CURSOR1      
       
       
       
 -- this gets the <city> nodes in country      
       
 SELECT  DISTINCT      
  country.CtyCode "code"      
  --,'' "companyList"      
  ,ISNULL(companyList,'') AS companyList    
   ,TctCode "code"      
  ,ISNULL([Type],'') AS  "type"      
  ,TctName "name"      
  ,ISNULL(AddAddress1,'') "address1"      
  ,ISNULL(AddAddress2,'') "address2"      
  ,ISNULL(PhnNumber,'') "phoneNumber"      
  ,ISNULL(BrandList,'') "brandList"      
 FROM      
  @DataSet1 "city"      
  INNER JOIN country (NOLOCK) ON city.CtyCode = Country.CtyCode      
 WHERE      
  LocZoneCode IS NULL      
 FOR XML AUTO      
    
    
RETURN    
    
END    