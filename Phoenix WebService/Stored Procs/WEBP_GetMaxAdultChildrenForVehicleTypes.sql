ALTER PROC WEBP_GetMaxAdultChildrenForVehicleTypes --   'NZ','THL','B','2BBXS'
 	 @sCtyCode AS VARCHAR(2) = ''
	,@sComCode AS VARCHAR(3) = ''
	,@sBrdCode AS VARCHAR(1) = ''
	,@sVehCode AS VARCHAR(64) = ''
AS  

BEGIN

	SET NOCOUNT ON

	-- Set blank Params to Null
	IF LTRIM(RTRIM(@sCtyCode)) = ''
		SET @sCtyCode = NULL
	IF LTRIM(RTRIM(@sComCode)) = ''
		SET @sComCode = NULL
	IF LTRIM(RTRIM(@sBrdCode)) = ''
		SET @sBrdCode = NULL
	IF LTRIM(RTRIM(@sVehCode)) = ''
		SET @sVehCode = NULL  

	SELECT DISTINCT   
	 CtyCode,  
	 ClaCode AS "type",    
	 ISNULL(MAX(Product.PrdNoOfAdult),0) AS "maxAdult",   
	 ISNULL(MAX(Product.PrdNoOfChild),0) AS "maxChildren"  
	FROM           
	 Country "travelers" (NOLOCK) INNER JOIN  
		SaleableProduct (NOLOCK) ON CtyCode = SaleableProduct.SapCtyCode INNER JOIN  
		Product (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN  
		Brand (NOLOCK) ON BrdCode = PrdBrdCode INNER JOIN
		[Type] (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN  
		Class "vehicle" (NOLOCK) ON Type.TypClaId = ClaID  
	WHERE       
	 Product.PrdIsVehicle = 1   
	 AND   
	 Product.PrdIsActive = 1   
	 AND   
	 Product.PrdIsPrimary = 1  
	 AND
	 PrdBrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode
     AND
     PrdShortName LIKE ISNULL(@sVehCode,'%') -- Vechicle Code
     AND
     SapCtyCode LIKE ISNULL(@sCtyCode, '%') -- Country Code
	 AND
     BrdComCode LIKE ISNULL(@sComCode,'%') -- Company Code
	GROUP BY   
	 ClaCode, CtyCode  
	  
	FOR XML AUTO

	RETURN

END

GO
GRANT EXEC ON WEBP_GetMaxAdultChildrenForVehicleTypes   TO PUBLIC
GO
