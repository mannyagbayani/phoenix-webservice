ALTER PROCEDURE [dbo].[Package_SearchPackage]   
(  
 @ComCode AS varchar(64),  
 @PkgId AS varchar(64) = NULL,  
 @PkgCode AS varchar(10) = NULL,  
 @PkgName AS varchar(64) = NULL,  
 @PkgBookedFromDate AS datetime = NULL,  
 @PkgBookedToDate AS datetime = NULL,   
 @PkgTravelFromDate AS datetime = NULL,   
 @PkgTravelToDate AS datetime = NULL,   
 @PkgBrdCode AS varchar(1) = NULL,  
 @PkgCodTypId AS varchar(64) = NULL,  
 @PkgCtyCode AS varchar(12) = NULL,  
 @PkgIsActive AS varchar(12) = NULL,  
 @PkgSapId AS varchar(64) = NULL,
 @PkgIsInclusive AS BIT = 0,
 @PkgIsWebEnabled AS BIT = 0 
)  
AS  
  
SELECT   
 Package.*   
FROM   
 Package WITH (NOLOCK)  
 INNER JOIN Brand WITH (NOLOCK) ON Brand.BrdCode = Package.PkgBrdCode  
 INNER JOIN Company WITH (NOLOCK) ON Company.ComCode = Brand.BrdComCode  
 LEFT JOIN   
 (  
  SELECT   
   PackageProduct.PplPkgId AS PkgId  
  FROM   
   PackageProduct WITH (NOLOCK)  
   INNER JOIN SaleableProduct WITH (NOLOCK) ON SaleableProduct.SapId = PackageProduct.PplSapId  
   INNER JOIN Product WITH (NOLOCK) ON Product.PrdId = SaleableProduct.SapPrdId  
   INNER JOIN Brand WITH (NOLOCK) ON Brand.BrdCode = Product.PrdBrdCode  
   INNER JOIN Company WITH (NOLOCK) ON Company.ComCode = Brand.BrdComCode   
  WHERE  
   Company.ComCode = @ComCode  
   AND SaleableProduct.SapId = @PkgSapId  
  GROUP BY  
   PackageProduct.PplPkgId  
 ) AS SapSearch ON SapSearch.PkgId = Package.PkgId   
WHERE  
 Company.ComCode = @ComCode  
 AND (@PkgId IS NULL OR Package.PkgId = @PkgId)  
 AND (@PkgCode IS NULL OR Package.PkgCode LIKE (@PkgCode + '%'))  
 AND (@PkgName IS NULL OR Package.PkgName LIKE ('%' + @PkgName + '%'))  
 AND (@PkgBookedFromDate IS NULL OR Package.PkgBookedToDate >= @PkgBookedFromDate)  
 AND (@PkgBookedToDate IS NULL OR Package.PkgBookedFromDate <= @PkgBookedToDate)  
 AND (@PkgTravelFromDate IS NULL OR Package.PkgTravelToDate >= @PkgTravelFromDate)  
 AND (@PkgTravelToDate IS NULL OR Package.PkgTravelFromDate <= @PkgTravelToDate)  
 AND (@PkgBrdCode IS NULL OR Package.PkgBrdCode = @PkgBrdCode)  
 AND (@PkgCodTypId IS NULL OR Package.PkgCodTypId = @PkgCodTypId)  
 AND (@PkgCtyCode IS NULL OR Package.PkgCtyCode = @PkgCtyCode)  
 AND (@PkgIsActive IS NULL OR Package.PkgIsActive = @PkgIsActive)  
 AND (@PkgSapId IS NULL OR SapSearch.PkgId IS NOT NULL)  
 AND ISNULL(PkgWebEnabledDate,(SELECT CASE @PkgIsWebEnabled WHEN 0 THEN CURRENT_TIMESTAMP WHEN 1 THEN PkgWebEnabledDate END)) IS NOT NULL
 AND ISNULL(PkgParentID,(SELECT CASE @PkgIsInclusive WHEN 0 THEN '' WHEN 1 THEN PkgParentID END)) IS NOT NULL
ORDER BY  
 Package.PkgCode  
   