ALTER PROCEDURE [dbo].[Product_GetProduct]   
(  
 @ComCode AS varchar(64),  
 @PrdId AS varchar(64) = NULL,  
 @PrdTypId AS varchar(64) = NULL,  
 @PrdBrdCode AS varchar(1) = NULL,  
 @PrdShortName AS varchar(8) = NULL,  
 @PrdName AS varchar(64) = NULL,  
 @PrdIsActive AS bit = NULL,  
 @FtrId0 AS varchar(64) = NULL,  
 @FtrId1 AS varchar(64) = NULL,  
 @FtrId2 AS varchar(64) = NULL,  
 @FtrId3 AS varchar(64) = NULL,  
 @FtrId4 AS varchar(64) = NULL,
 @WebEnabledSelection AS INT = 0  -- 0 Means Irrespective of Web Enabled Date, 1 Means Web Enabled Vehicles, 2 Means Web Enabled Non-Vehicles, 3 Meand Web Enabled Vehicles and Non-Vehicles
)   
AS  
  
DECLARE @FeatureCount AS int  
SET @FeatureCount =   
 CASE WHEN @FtrId0 IS NOT NULL THEN 1 ELSE 0 END  
 + CASE WHEN @FtrId1 IS NOT NULL THEN 1 ELSE 0 END  
 + CASE WHEN @FtrId2 IS NOT NULL THEN 1 ELSE 0 END  
 + CASE WHEN @FtrId3 IS NOT NULL THEN 1 ELSE 0 END  
 + CASE WHEN @FtrId4 IS NOT NULL THEN 1 ELSE 0 END  

 -- Phoenix Change by Shoel
 DECLARE @WebVehicleSelection AS VARCHAR(10)
 SELECT @WebVehicleSelection = CASE @WebEnabledSelection WHEN 1 THEN '1' WHEN 2 THEN '0' ELSE '0,1' END
 -- Phoenix Change by Shoel
  
  
SELECT   
 Product.*   
INTO   
 #temp  
FROM   
 Product WITH (NOLOCK)  
 INNER JOIN Brand WITH (NOLOCK)  
  ON Brand.BrdCode = Product.PrdBrdCode  
 LEFT JOIN  
 (  
  SELECT   
   ProductFeature.PftPrdId AS PrdId,  
   COUNT (ProductFeature.PftFtrId) AS [Count]  
  FROM  
   ProductFeature WITH (NOLOCK)  
  WHERE  
   ProductFeature.PftFtrId IN (@FtrId0, @FtrId1, @FtrId2, @FtrId3, @FtrId4)  
  GROUP BY  
   ProductFeature.PftPrdId  
 ) AS ProductFeatureCount ON ProductFeatureCount.PrdId = Product.PrdId  
WHERE  
 Brand.BrdComCode = @ComCode  
 AND (@PrdId IS NULL OR Product.PrdId = @PrdId)  
 AND (@PrdTypId IS NULL OR Product.PrdTypId = @PrdTypId)  
 AND (@PrdBrdCode IS NULL OR Product.PrdBrdCode = @PrdBrdCode)  
 AND (@PrdShortName IS NULL OR Product.PrdShortName LIKE @PrdShortName + '%')  
 AND (@PrdName IS NULL OR Product.PrdName LIKE @PrdName + '%')  
 AND (@PrdIsActive IS NULL OR Product.PrdIsActive = @PrdIsActive)  
 AND (@FeatureCount = 0 OR ISNULL (ProductFeatureCount.[Count], 0) = @FeatureCount)  
 -- Phoenix Change by Shoel
 AND ISNULL(PrdWebEnabledDate,(SELECT CASE @WebEnabledSelection WHEN 0 THEN CAST(CAST(CURRENT_TIMESTAMP AS INT) AS DATETIME) ELSE PrdWebEnabledDate END )) IS NOT NULL
 AND CHARINDEX(CAST(PrdIsVehicle AS VARCHAR),@WebVehicleSelection) <> 0
 -- Phoenix Change by Shoel
ORDER BY   
 Product.PrdShortName  
  
SELECT * FROM #temp  
  
SELECT   
 ProductFeature.*  
FROM  
 #temp  
 INNER JOIN ProductFeature WITH (NOLOCK) ON ProductFeature.PftPrdId = #temp.PrdId  
 INNER JOIN Feature WITH (NOLOCK) ON Feature.FtrId = ProductFeature.PftFtrId  
ORDER BY   
 #temp.PrdShortName,  
 Feature.FtrOrder,  
 Feature.FtrDesc  
   
DROP TABLE #temp   
  
-- Added by Shoel : for Product Expiry   
SELECT   
 NonAvailabilityProfile.*  
FROM   
 NonAvailabilityProfile WITH (NOLOCK)  
WHERE  
 NonAvailabilityProfile.NapPrdId = @PrdId  
-- Added by Shoel : for Product Expiry  