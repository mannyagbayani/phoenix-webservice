ALTER PROC [dbo].[WEBP_GetLocationsWithZone]   -- 'NZ','THL','B','2BBXS'  
   @sCtyCode AS VARCHAR(2) = ''  
   ,@sComCode AS VARCHAR(3) = ''  
   ,@sBrdCode AS VARCHAR(1) = ''  
   -- ,@sVehCode AS VARCHAR(64) = ''  
AS    
  
BEGIN  
 SET NOCOUNT ON  
  
 -- Set blank Params to Null  
 IF LTRIM(RTRIM(@sCtyCode)) = ''  
  SET @sCtyCode = NULL  
 IF LTRIM(RTRIM(@sComCode)) = ''  
  SET @sComCode = NULL  
 IF LTRIM(RTRIM(@sBrdCode)) = ''  
  SET @sBrdCode = NULL  
-- IF LTRIM(RTRIM(@sVehCode)) = ''  
--  SET @sVehCode = NULL    
    
 DECLARE @DataSet1 TABLE    
 (    
  CtyCode VARCHAR(12),     
  CtyName VARCHAR(64),     
  TctCode VARCHAR(12),     
  TctName VARCHAR(64),    
  LocZoneCode VARCHAR(3),     
  LocZoneName VARCHAR(64),     
  LocCode VARCHAR(12),     
  LocName VARCHAR(64),     
  AddAddress1 VARCHAR(64),     
  AddAddress2 VARCHAR(64),     
  AddAddress3 VARCHAR(64),     
  PhnNumber VARCHAR(64),     
 -- CodCode VARCHAR(64),    
  BrandList VARCHAR(64),    
  ZoneBrandList VARCHAR(64),    
  [Type] VARCHAR(64)    
 )    
     
 INSERT INTO @DataSet1    
     
     
     
-- SELECT DISTINCT     
--  Country.CtyCode, Country.CtyName,     
--  TownCity.TctCode, TownCity.TctName,     
--  LocationZone.LocZoneCode, LocationZone.LocZoneName,     
--  Location.LocCode, Location.LocName,     
--  AddressDetails.AddAddress1, AddressDetails.AddAddress2, AddressDetails.AddAddress3, PhoneNumber.PhnNumber,    
--  NULL AS BrandList,    
--  NULL AS ZoneBrandList,    
--  NULL AS [Type]    
-- FROM             
--  Code INNER JOIN    
--  TownCity INNER JOIN    
--  Country ON TownCity.TctCtyCode = Country.CtyCode INNER JOIN    
--  Location ON Location.LocTctCode = TownCity.TctCode LEFT OUTER JOIN    
--  LocationZone ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN    
--  AddressDetails ON AddressDetails.AddPrntID = Location.LocCode AND AddressDetails.AddPrntTableName = 'Location' INNER JOIN    
--  PhoneNumber ON PhoneNumber.PhnPrntId = Location.LocCode AND PhoneNumber.PhnPrntTableName = 'Location' ON     
--  Code.CodId = Location.LocCodTypId INNER JOIN    
--  Code AS Code_1 ON PhoneNumber.PhnCodPhoneId = Code_1.CodId INNER JOIN    
--  Code AS Code_2 ON AddressDetails.AddCodAddressId = Code_2.CodId    
-- WHERE     (TownCity.TctIsActive = 1) AND (Country.CtyIsActive = 1) AND (Location.LocIsActive = 1) AND (Code_1.CodCode = 'Phone') AND     
--        (Code_2.CodCode = 'Physical')    
-- AND Code.CodCode IN ('Branch','Thrifty')    
  
  
     SELECT DISTINCT   
          Country.CtyCode, 
          Country.CtyName, 
          TownCity.TctCode, 
          TownCity.TctName, 
          LocationZone.LocZoneCode,   
          LocationZone.LocZoneName, 
          Location.LocCode, 
          Location.LocName, 
          ISNULL(AddressDetails.AddAddress1,''), 
          ISNULL(AddressDetails.AddAddress2,''), 
          ISNULL(AddressDetails.AddAddress3,''), 
          ISNULL(PhoneNumber.PhnNumber,''), 
          NULL AS BrandList, 
          NULL AS ZoneBrandList, 
          NULL AS Type  
     FROM         
          TownCity WITH (NOLOCK) INNER JOIN
          Country WITH (NOLOCK) ON TownCity.TctCtyCode = Country.CtyCode INNER JOIN
          Location WITH (NOLOCK) ON Location.LocTctCode = TownCity.TctCode LEFT OUTER JOIN
          LocationZone WITH (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN
          LocationBrand WITH (NOLOCK) ON Location.LocCode = LocationBrand.LbrLocCode INNER JOIN
          Brand WITH (NOLOCK) ON LocationBrand.LbrBrdCode = Brand.BrdCode LEFT OUTER JOIN
          Code AS Code_2 WITH (NOLOCK) RIGHT OUTER JOIN
          AddressDetails WITH (NOLOCK) ON Code_2.CodId = AddressDetails.AddCodAddressId ON 
          AddressDetails.AddPrntID = Location.LocCode AND AddressDetails.AddPrntTableName = 'Location' LEFT OUTER JOIN
          Code AS Code_1 WITH (NOLOCK) RIGHT OUTER JOIN
          PhoneNumber WITH (NOLOCK) ON Code_1.CodId = PhoneNumber.PhnCodPhoneId ON 
          PhoneNumber.PhnPrntId = Location.LocCode 
               AND PhoneNumber.PhnPrntTableName = 'Location' 
               AND ISNULL(Code_1.CodCode, 'Phone') = 'Phone' 
               AND ISNULL(Code_2.CodCode, 'Physical') = 'Physical'
     WHERE     
          TownCity.TctIsActive = 1 
          AND 
          Country.CtyIsActive = 1 
          AND 
          Location.LocIsActive = 1 
          AND 
          BrdCode LIKE ISNULL(@sBrdCode,'%') -- BrandCode  
          AND 
          BrdComCode LIKE ISNULL(@sComCode,'%') -- Company Code  
          AND 
          TctCtyCode LIKE ISNULL(@sCtyCode, '%') -- Country Code  
          --AND  LocZoneCode IS NOT NULL AND LocZoneName IS NOT NULL    
     ORDER BY 
          Country.CtyCode, 
          TownCity.TctCode, 
          LocationZone.LocZoneCode, 
          Location.LocCode    
     
     
 DECLARE @LocCode AS VARCHAR(12)    
     
 DECLARE CURSOR1 CURSOR    
 FOR     
 SELECT DISTINCT LocCode     
 FROM  @DataSet1    
     
 OPEN CURSOR1    
 FETCH NEXT FROM CURSOR1    
 INTO @LocCode    
     
 WHILE @@FETCH_STATUS = 0    
 BEGIN    
  DECLARE @BrandList AS VARCHAR(64)    
  SET @BrandList = '' -- initialising it    
     
  DECLARE @DummyTable TABLE (LbrBrdCode VARCHAR(1))  
     INSERT INTO @DummyTable  
  SELECT DISTINCT LbrBrdCode   
  FROM LocationBrand (NOLOCK)    
  WHERE LbrLocCode = @LocCode    
  ORDER BY LbrBrdCode  
  
  SELECT @BrandList = @BrandList + LbrBrdCode + ' '    
  FROM @DummyTable   
  
  DELETE @DummyTable  
     
  SET @BrandList = RTRIM(@BrandList)    
     
  IF @BrandList <> ''    
  BEGIN    
   UPDATE @DataSet1    
   SET BrandList = @BrandList    
   WHERE LocCode = @LocCode    
  END    
     
   DECLARE @ZoneBrandList AS VARCHAR(64)    
     ,@TypeList AS VARCHAR(64)  
     ,@LocZoneCode AS VARCHAR(12)    
     ,@TctCode AS VARCHAR(12)    
  
  SELECT @ZoneBrandList = '' --initializing it    
   ,@TypeList = ''  
  
  SELECT     
   @LocZoneCode = LocationZone.LocZoneCode,     
   @TctCode = TownCity.TctCode    
  FROM             
   LocationZone (NOLOCK) INNER JOIN    
   Location (NOLOCK) ON LocationZone.LocZoneid = Location.LocZoneid INNER JOIN    
   TownCity (NOLOCK) ON Location.LocTctCode = TownCity.TctCode    
  WHERE         
   Location.LocCode = @LocCode    
  
  SELECT @TypeList = @TypeList + Class.ClaCode + ' '    
  FROM LocationClass (NOLOCK) INNER JOIN  
    Class (NOLOCK) ON LocationClass.LcsClaId = Class.ClaID  
  WHERE     LocationClass.LcsLocCode = @LocCode  
  ORDER BY Class.ClaCode  
     
      
  INSERT INTO @DummyTable  
  SELECT DISTINCT LbrBrdCode   
  FROM         LocationBrand (NOLOCK) INNER JOIN    
        Location (NOLOCK) ON LocationBrand.LbrLocCode = Location.LocCode INNER JOIN    
        LocationZone (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN    
        TownCity (NOLOCK) ON Location.LocTctCode = TownCity.TctCode    
  WHERE     (LocationZone.LocZoneCode = @LocZoneCode) AND (TownCity.TctCode = @TctCode)    
  ORDER BY LbrBrdCode  
  
  SELECT @ZoneBrandList = @ZoneBrandList + LbrBrdCode + ' '    
  FROM @DummyTable   
  
  DELETE @DummyTable  
  
  
     
  --SELECT @ZoneBrandList = @ZoneBrandList + LbrBrdCode + ' '    
--  FROM         LocationBrand (NOLOCK) INNER JOIN    
--        Location (NOLOCK) ON LocationBrand.LbrLocCode = Location.LocCode INNER JOIN    
--        LocationZone (NOLOCK) ON Location.LocZoneid = LocationZone.LocZoneid INNER JOIN    
--        TownCity (NOLOCK) ON Location.LocTctCode = TownCity.TctCode    
--  WHERE     (LocationZone.LocZoneCode = @LocZoneCode) AND (TownCity.TctCode = @TctCode)    
     
  SELECT @ZoneBrandList = RTRIM(@ZoneBrandList)    
   ,@TypeList = RTRIM(@TypeList)  
     
  IF @ZoneBrandList <> ''    
  BEGIN    
   UPDATE @DataSet1    
   SET ZoneBrandList = @ZoneBrandList    
   WHERE LocCode = @LocCode    
  END    
  
  IF @TypeList <> ''    
  BEGIN    
   IF CHARINDEX('BK',@TypeList) <> 0 AND CHARINDEX('AC',@TypeList) <> 0 AND CHARINDEX('AV',@TypeList) <> 0  
      SET @TypeList = 'ALL'  
  
   UPDATE @DataSet1    
   SET [Type] = @TypeList    
   WHERE LocCode = @LocCode    
  END    
     
  FETCH NEXT FROM CURSOR1    
  INTO @LocCode    
 END    
     
 CLOSE CURSOR1    
 DEALLOCATE CURSOR1    
     
     
 SELECT DISTINCT    
  CtyCode     
  ,TctCode     
  ,ISNULL([Type],'') AS "Type"  
  ,TctName     
  ,LocZoneCode    
  ,LocZoneName    
  ,LocCode    
  ,LocName    
  ,ISNULL(AddAddress1,'') "Address1"    
  ,ISNULL(AddAddress2,'') "Address2"    
  ,ISNULL(PhnNumber,'') "PhoneNumber"    
  ,ISNULL(BrandList,'') "BrandList"    
  ,ISNULL(ZoneBrandList,'') "ZoneBrandList"    
 FROM    
  @DataSet1 "city"    
 WHERE    
  LocZoneCode IS NOT NULL    
     
 RETURN   
  
END  
  