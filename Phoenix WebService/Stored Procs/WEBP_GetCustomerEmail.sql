ALTER PROC WEBP_GetCustomerEmail  
@RentalId AS VARCHAR(64)  
AS  
BEGIN  
	 SET NOCOUNT ON  
	 SELECT       
		'' + CusFirstName AS "FIRSTNAME"  
	  , '' + CusLastName AS "LASTNAME"  
	  , '' + PhnEmailAddress AS "EMAIL"  
	 FROM           
	  Customer  
	  INNER JOIN  
			PhoneNumber ON dbo.PhoneNumber.PhnPrntId = dbo.Customer.CusId   
	  INNER JOIN  
			Traveller ON dbo.Customer.CusId = dbo.Traveller.TrvCusId  
	 WHERE       
	  dbo.PhoneNumber.PhnPrntTableName = 'Customer'  
	  AND   
	  dbo.Traveller.TrvRntId = @RentalId  
	  AND   
	  ISNULL(dbo.PhoneNumber.PhnEmailAddress,'') <> ''  
	 FOR XML AUTO,ELEMENTS  
 	 RETURN
END