USE [Aurora]਍ഀ
GO਍ഀ
/****** Object:  StoredProcedure [dbo].[WEBP_GetAgentList]    Script Date: 04/23/2009 14:38:55 ******/਍ഀ
SET ANSI_NULLS ON਍ഀ
GO਍ഀ
SET QUOTED_IDENTIFIER ON਍ഀ
GO਍ഀ
ALTER PROC [dbo].[WEBP_GetAgentList]  --'NZ','KXS','X','SCARX'਍ഀ
 	 @sCtyCode AS VARCHAR(2) = ''਍ഀ
	,@sComCode AS VARCHAR(3) = ''਍ഀ
	,@sBrdCode AS VARCHAR(1) = ''਍ഀ
	,@sVehCode AS VARCHAR(64) = ''਍ഀ
AS  ਍ഀ
DECLARE @tblAgentClass TABLE  ਍ഀ
(  ਍ഀ
 AgnCode VARCHAR(64)  ਍ഀ
 ,ClaCode VARCHAR(64)  ਍ഀ
)   ਍ഀ
BEGIN  ਍ഀ
਍ഀ
	-- Set blank Params to Null਍ഀ
	IF LTRIM(RTRIM(@sCtyCode)) = ''਍ഀ
		SET @sCtyCode = NULL਍ഀ
	IF LTRIM(RTRIM(@sComCode)) = ''਍ഀ
		SET @sComCode = NULL਍ഀ
	IF LTRIM(RTRIM(@sBrdCode)) = ''਍ഀ
		SET @sBrdCode = NULL਍ഀ
	IF LTRIM(RTRIM(@sVehCode)) = ''਍ഀ
		SET @sVehCode = NULL  ਍ഀ
਍ഀ
	INSERT INTO @tblAgentClass  ਍ഀ
	 SELECT DISTINCT   ਍ഀ
	  Agent.AgnCode  ਍ഀ
	  , Class.ClaCode  ਍ഀ
	 FROM           ਍ഀ
	  Agent WITH (NOLOCK) INNER JOIN  ਍ഀ
-- Filter for picking up Affiliates only਍ഀ
	  AgentType ON Agent.AgnAtyId = dbo.AgentType.AtyId INNER JOIN਍ഀ
	  AgentPackage WITH (NOLOCK) ON Agent.AgnId = AgentPackage.ApkAgnId INNER JOIN  ਍ഀ
	  Package WITH (NOLOCK) ON AgentPackage.ApkPkgId = Package.PkgId INNER JOIN  ਍ഀ
	  PackageProduct WITH (NOLOCK) ON Package.PkgId = PackageProduct.PplPkgId INNER JOIN  ਍ഀ
	  SaleableProduct WITH (NOLOCK) ON PackageProduct.PplSapId = SaleableProduct.SapId INNER JOIN  ਍ഀ
	  Product WITH (NOLOCK) ON SaleableProduct.SapPrdId = Product.PrdId INNER JOIN  ਍ഀ
	  [Type] WITH (NOLOCK) ON Product.PrdTypId = Type.TypId INNER JOIN  ਍ഀ
	  Class WITH (NOLOCK) ON Type.TypClaId = Class.ClaID INNER JOIN ਍ഀ
	  Brand WITH (NOLOCK) ON BrdCode = PrdBrdCode਍ഀ
	 WHERE       ਍ഀ
	  AgentType.AtyIsActive = 1 AND ਍ഀ
      AgentType.AtyDesc = 'Affiliate' AND਍ഀ
	  Package.PkgBookedToDate >= GETDATE() AND   ਍ഀ
	  Package.PkgTravelToDate >= GETDATE() AND   ਍ഀ
	  GETDATE() >= Package.PkgBookedFromDate AND   ਍ഀ
	  GETDATE() >= Package.PkgTravelFromDate AND   ਍ഀ
	  Package.PkgIsActive = 'Active' AND   ਍ഀ
	  Product.PrdIsActive = 1 AND   ਍ഀ
	  Product.PrdIsVehicle = 1 AND   ਍ഀ
	  Agent.AgnIsActive = 1 AND   ਍ഀ
	  Class.ClaCode IN ('AC', 'AV')  ਍ഀ
	  AND SapCtyCode LIKE ISNULL(@sCtyCode, '%')਍ഀ
	  AND BrdCode LIKE ISNULL(@sBrdCode, '%')਍ഀ
	  AND PrdShortName LIKE ISNULL(@sVehCode, '%')਍ഀ
	  AND BrdComCode LIKE ISNULL(@sComCode, '%')਍ഀ
	 ORDER BY Agent.AgnCode  ਍ഀ
	  ਍ഀ
	DECLARE @tblAgentClass2 TABLE  ਍ഀ
	(  ਍ഀ
	 AgnCode VARCHAR(64)  ਍ഀ
	 ,ClaCode VARCHAR(64)  ਍ഀ
	)  ਍ഀ
	  ਍ഀ
	  ਍ഀ
	  ਍ഀ
	--cursor  ਍ഀ
	DECLARE @AgnCode AS VARCHAR(64)  ਍ഀ
	  ਍ഀ
	DECLARE CURSOR1 CURSOR  ਍ഀ
	FOR   ਍ഀ
	SELECT DISTINCT AgnCode   ਍ഀ
	FROM  @tblAgentClass  ਍ഀ
	  ਍ഀ
	OPEN CURSOR1  ਍ഀ
	FETCH NEXT FROM CURSOR1  ਍ഀ
	INTO @AgnCode  ਍ഀ
	  ਍ഀ
	WHILE @@FETCH_STATUS = 0  ਍ഀ
	BEGIN  ਍ഀ
	 DECLARE @ClaCodeList AS VARCHAR(64)  ਍ഀ
	 SET @ClaCodeList = '' -- initialising it  ਍ഀ
	  ਍ഀ
	 SELECT @ClaCodeList = @ClaCodeList + ClaCode + ' '  ਍ഀ
	 FROM @tblAgentClass  ਍ഀ
	 WHERE AgnCode = @AgnCode  ਍ഀ
	  ਍ഀ
	 SET @ClaCodeList = RTRIM(@ClaCodeList)  ਍ഀ
	  ਍ഀ
	 IF CHARINDEX('AV',@ClaCodeList) > 0 AND CHARINDEX('AC',@ClaCodeList) > 0  ਍ഀ
	 BEGIN  ਍ഀ
	  SET @ClaCodeList = 'both'  ਍ഀ
	 END  ਍ഀ
	  ਍ഀ
	 INSERT INTO @tblAgentClass2  ਍ഀ
	 VALUES (@AgnCode,@ClaCodeList)  ਍ഀ
	  ਍ഀ
	 FETCH NEXT FROM CURSOR1  ਍ഀ
	 INTO @AgnCode  ਍ഀ
	END  ਍ഀ
	  ਍ഀ
	CLOSE CURSOR1  ਍ഀ
	DEALLOCATE CURSOR1  ਍ഀ
	  ਍ഀ
	SELECT  ਍ഀ
	  AgnCode "code"  ਍ഀ
	 ,ClaCode "disp"  ਍ഀ
	FROM  ਍ഀ
	 @tblAgentClass2 "agent"  ਍ഀ
	FOR XML AUTO  ਍ഀ
	   ਍ഀ
	  ਍ഀ
	RETURN਍ഀ
਍ഀ
END਍ഀ
਍ഀ
