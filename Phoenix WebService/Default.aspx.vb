﻿'' REVISIONS
'' NOV172010 - MIA - ADDED FILLWITHTESTDATA AND NEW CONFIG KEY FOR MY TESTING

Imports System.Xml
Imports Aurora.Common

Partial Class _Default
    Inherits System.Web.UI.Page

    Const DATE_FORMAT As String = "dd-MMM-yyyy"
    Const TIME_FORMAT As String = "HH:mm"

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim oResult As XmlDocument

        Session("Brand") = cmbBrand.SelectedValue
        'Session("Company") = cmbCompanyCode.SelectedValue
        Session("Country") = cmbCountryCode.SelectedValue
        Session("CODate") = CDate(txtCheckOutDate.Text)
        Session("CIDate") = CDate(txtCheckInDate.Text)
        Session("COZone") = txtCheckOutZoneCode.Text
        Session("CIZone") = txtCheckInZoneCode.Text
        'Session("COTct") = txtCheckOutTownCityCode.Text
        'Session("CITct") = txtCheckInTownCityCode.Text
        Session("IsVan") = chkIsVan.Checked
        Session("IsTest") = chkIsTestMode.Checked

        If txtNoOfChildren.Text.Trim().Equals(String.Empty) Then
            txtNoOfChildren.Text = "0"
        End If

        'If txtNoOfInfants.Text.Trim().Equals(String.Empty) Then
        '    txtNoOfInfants.Text = "0"
        'End If

        oResult = oService.GetAvailability(cmbBrand.SelectedValue, cmbCountryCode.SelectedValue, txtVehicleCode.Text, _
                                           txtCheckOutZoneCode.Text, CDate(txtCheckOutDate.Text), _
                                           txtCheckInZoneCode.Text, CDate(txtCheckInDate.Text), _
                                           CInt(txtNumberOfAdults.Text), CInt(txtNoOfChildren.Text), _
                                           txtAgentCode.Text, txtPackageCode.Text, _
                                           chkIsVan.Checked, chkIsBestBuy.Checked, txtCountryOfResidence.Text, False, _
                                           chkIsTestMode.Checked, True, "")

        '' ltrVehicleList.Text = ProcessVehicleList(oResult)
        LitResultSet.Text = ProcessVehicleList(oResult)



    End Sub

    Function ProcessVehicleList(ByVal VehicleXML As XmlDocument) As String
        Dim sbHTMLOutput As StringBuilder = New StringBuilder
        Dim sFlexCode As String = String.Empty

        Dim sColor As String

        sbHTMLOutput.Append("<TABLE>")
        For Each oVehicleNode As XmlNode In VehicleXML.DocumentElement.SelectNodes("AvailableRate")
            sbHTMLOutput.Append("   <TR>")
            sbHTMLOutput.Append("       <TD>")

            sbHTMLOutput.Append("           <TABLE style=""border:solid 3px Black;width:100%"">")

            ' Non-available
            If oVehicleNode.SelectSingleNode("Errors") IsNot Nothing _
               AndAlso _
               ( _
                ( _
                    oVehicleNode.SelectSingleNode("Errors/BlockingRuleMessage") IsNot Nothing _
                    AndAlso _
                    oVehicleNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Allow").Value <> "" _
                ) _
                OrElse _
                ( _
                    oVehicleNode.SelectSingleNode("Errors/ErrorMessage") IsNot Nothing _
                    AndAlso _
                    oVehicleNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Allow").Value <> "" _
                ) _
               ) _
            Then
                sColor = "Salmon"

                ' Non-Available vehicles
                If oVehicleNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Allow").Value = "true" Then
                    For Each oChargeNode As XmlNode In oVehicleNode.SelectNodes("Charge/Det[@IsVeh='1']")
                        sbHTMLOutput.Append("               <TR>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Vehicle Code : " & oChargeNode.Attributes("PrdCode").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Vehicle Name : " & oChargeNode.Attributes("PrdName").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Brand : " & oVehicleNode.SelectSingleNode("Package").Attributes("BrandCode").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Price : " & oChargeNode.Attributes("CurrCode").Value & " " & oChargeNode.Attributes("Price").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Product Info : " & oChargeNode.Attributes("ProductInfo").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        'sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Rate : " & oChargeNode.Attributes("Rate").Value & " per " & oChargeNode.Attributes("UOM").Value & " x " & oChargeNode.Attributes("Qty").Value & oChargeNode.Attributes("UOM").Value & "(s)")
                        'sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("               </TR>")
                        sbHTMLOutput.Append("               <TR>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """  colspan=""5"">")
                        sbHTMLOutput.Append(oVehicleNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Message").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("               </TR>")

                        Try
                            If Not String.IsNullOrEmpty(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerText) Then
                                sbHTMLOutput.Append("<TR><TD colspan=""5"" style=""background-color:LightYellow"" >" & XmlEncode(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerXml) & "</TD></TR>")
                            End If
                        Catch ex As Exception

                        End Try

                        If oChargeNode.Attributes("FlxNum") IsNot Nothing Then
                            sFlexCode = oChargeNode.Attributes("FlxNum").Value
                        End If

                        ' look at RateBands
                        If oChargeNode.SelectNodes("RateBands/Band") IsNot Nothing Then
                            For Each oRateBand As XmlNode In oChargeNode.SelectNodes("RateBands/Band")
                                sbHTMLOutput.Append("               <TR>")
                                sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ colspan=""3"">Rate Band From : " & CDate(oRateBand.Attributes("From").Value).ToString("dd/MM/yyyy") & _
                                                                           " To : " & CDate(oRateBand.Attributes("To").Value).ToString("dd/MM/yyyy"))
                                sbHTMLOutput.Append("                   </TD>")
                                sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ colspan=""2"">Amount : " & oRateBand.Attributes("Amount").Value & " = " & _
                                                                            oRateBand.Attributes("BaseRate").Value & " (Base Rate) x " & _
                                                                            oRateBand.Attributes("HirePeriod").Value & " (Hire Period)")
                                sbHTMLOutput.Append("                   </TD>")
                                sbHTMLOutput.Append("               </TR>")
                            Next
                        Else
                            sbHTMLOutput.Append("               <TR>")
                            sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ colspan=""3"">Price : " & oChargeNode.Attributes("Price").Value)
                            sbHTMLOutput.Append("                   </TD>")
                            sbHTMLOutput.Append("               </TR>")
                        End If

                        ' look at RateBands

                    Next
                Else
                    For Each oChargeNode As XmlNode In oVehicleNode.SelectNodes("Charge/Det[@IsVeh='1']")


                        sbHTMLOutput.Append("               <TR>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Vehicle Code : " & oChargeNode.Attributes("PrdCode").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:" & sColor & """ >Brand : " & oVehicleNode.SelectSingleNode("Package").Attributes("BrandCode").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("                   <TD colspan=""3"" style=""background-color:" & sColor & """ >Vehicle Name : " & oChargeNode.Attributes("PrdName").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("               </TR>")
                        sbHTMLOutput.Append("               <TR>")
                        If oVehicleNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Allow").Value = "false" Then
                            sbHTMLOutput.Append("                   <TD colspan=""5"" style=""background-color:" & sColor & """ >" & oVehicleNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Message").Value)
                            sbHTMLOutput.Append("                   </TD>")
                        ElseIf oVehicleNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Allow").Value = "true" Then
                            sbHTMLOutput.Append("                   <TD colspan=""5"" style=""background-color:" & sColor & """ >T & C<BR/>" & oVehicleNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Message").Value)
                            sbHTMLOutput.Append("                   </TD>")
                        Else
                            sbHTMLOutput.Append("                   <TD colspan=""5""  style=""background-color:" & sColor & """ >BLOCKING RULE<BR/>" & oVehicleNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Message").Value)
                            sbHTMLOutput.Append("                   </TD>")
                        End If
                        sbHTMLOutput.Append("               </TR>")

                        Try
                            If Not String.IsNullOrEmpty(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerText) Then
                                sbHTMLOutput.Append("<TR><TD colspan=""5"" style=""background-color:LightYellow"" >" & XmlEncode(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerXml) & "</TD></TR>")
                            End If
                        Catch ex As Exception

                        End Try


                    Next
                End If



            Else

                ' Vehicle Details
                For Each oChargeNode As XmlNode In oVehicleNode.SelectNodes("Charge/Det[@IsVeh='1']")
                    sbHTMLOutput.Append("               <TR>")
                    sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" >Vehicle Code : " & oChargeNode.Attributes("PrdCode").Value)
                    sbHTMLOutput.Append("                   </TD>")
                    sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" >Vehicle Name : " & oChargeNode.Attributes("PrdName").Value)
                    sbHTMLOutput.Append("                   </TD>")
                    sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" >Brand : " & oVehicleNode.SelectSingleNode("Package").Attributes("BrandCode").Value)
                    sbHTMLOutput.Append("                   </TD>")
                    If Not oChargeNode.Attributes("CurrCode") Is Nothing Then
                        If Not oChargeNode.Attributes("Price") Is Nothing Then
                            sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" >Price : " & oChargeNode.Attributes("CurrCode").Value & " " & oChargeNode.Attributes("Price").Value)
                            sbHTMLOutput.Append("                   </TD>")
                        End If
                    End If

                    If Not oChargeNode.Attributes("ProductInfo") Is Nothing Then
                        sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" >Product Info : " & oChargeNode.Attributes("ProductInfo").Value)
                        sbHTMLOutput.Append("                   </TD>")
                    End If


                    sbHTMLOutput.Append("               </TR>")

                    Try
                        If Not String.IsNullOrEmpty(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerText) Then
                            sbHTMLOutput.Append("<TR><TD colspan=""5"" style=""background-color:LightYellow"" >" & XmlEncode(oVehicleNode.SelectSingleNode("ThriftyDebugInfo").InnerXml) & "</TD></TR>")
                        End If
                    Catch ex As Exception

                    End Try

                    If oChargeNode.Attributes("FlxNum") IsNot Nothing Then
                        sFlexCode = oChargeNode.Attributes("FlxNum").Value
                    End If

                    ' look at RateBands
                    If oChargeNode.SelectNodes("RateBands/Band") IsNot Nothing Then
                        For Each oRateBand As XmlNode In oChargeNode.SelectNodes("RateBands/Band")
                            sbHTMLOutput.Append("               <TR>")
                            sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" colspan=""3"">Rate Band From : " & CDate(oRateBand.Attributes("From").Value).ToString("dd/MM/yyyy") & _
                                                                       " To : " & CDate(oRateBand.Attributes("To").Value).ToString("dd/MM/yyyy"))
                            sbHTMLOutput.Append("                   </TD>")
                            sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" colspan=""2"">Amount : " & oRateBand.Attributes("Amount").Value & " = " & _
                                                                        oRateBand.Attributes("BaseRate").Value & " (Base Rate) x " & _
                                                                        oRateBand.Attributes("HirePeriod").Value & " (Hire Period)")
                            sbHTMLOutput.Append("                   </TD>")
                            sbHTMLOutput.Append("               </TR>")
                        Next
                    Else
                        sbHTMLOutput.Append("               <TR>")
                        sbHTMLOutput.Append("                   <TD style=""background-color:AliceBlue"" colspan=""3"">Price : " & oChargeNode.Attributes("Price").Value)
                        sbHTMLOutput.Append("                   </TD>")
                        sbHTMLOutput.Append("               </TR>")
                    End If

                    ' look at RateBands

                Next
                ' Vehicle Details
            End If



            ' non vehicle details
            For Each oChargeNode As XmlNode In oVehicleNode.SelectNodes("Charge/Det[@IsVeh='0']")
                sbHTMLOutput.Append("               <TR>")
                sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"">Product Code : " & oChargeNode.Attributes("PrdCode").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"">Product Name : " & oChargeNode.Attributes("PrdName").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"">Brand : " & oVehicleNode.SelectSingleNode("Package").Attributes("BrandCode").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"">Price : " & oChargeNode.Attributes("CurrCode").Value & " " & oChargeNode.Attributes("Price").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"" >Product Info : " & oChargeNode.Attributes("ProductInfo").Value)
                sbHTMLOutput.Append("                   </TD>")
                'sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"">Rate : " & oChargeNode.Attributes("Rate").Value & " per " & oChargeNode.Attributes("UOM").Value & " x " & oChargeNode.Attributes("Qty").Value & oChargeNode.Attributes("UOM").Value & "(s)")
                'sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("               </TR>")


                ' look at RateBands
                For Each oRateBand As XmlNode In oChargeNode.SelectNodes("RateBands/Band")
                    sbHTMLOutput.Append("               <TR>")
                    sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"" colspan=""3"">Rate Band From : " & CDate(oRateBand.Attributes("From").Value).ToString("dd/MM/yyyy") & _
                                                               " To : " & CDate(oRateBand.Attributes("To").Value).ToString("dd/MM/yyyy"))
                    sbHTMLOutput.Append("                   </TD>")
                    sbHTMLOutput.Append("                   <TD style=""background-color:LightGreen"" colspan=""2"">Amount : " & oRateBand.Attributes("Amount").Value & " = " & _
                                                                oRateBand.Attributes("BaseRate").Value & " (Base Rate) x " & _
                                                                oRateBand.Attributes("HirePeriod").Value & " (Hire Period)")
                    sbHTMLOutput.Append("                   </TD>")
                    sbHTMLOutput.Append("               </TR>")
                Next
                ' look at RateBands

            Next
            ' non vehicle details

            If oVehicleNode.SelectSingleNode("Package").Attributes.Count > 1 Then
                sbHTMLOutput.Append("               <TR>")
                sbHTMLOutput.Append("                   <TD style=""background-color:BlanchedAlmond"">")
                sbHTMLOutput.Append("                       <A HREF='javascript:window.location = ""PopUpDetails.aspx?AvpId=" & oVehicleNode.SelectSingleNode("Package").Attributes("AvpId").Value & """;'>")
                If oVehicleNode.SelectSingleNode("Package").Attributes("PkgCode").Value.ToUpper().Contains("FLEX") Then
                    sbHTMLOutput.Append("Package Code : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgCode").Value & " | " & sFlexCode)
                Else
                    sbHTMLOutput.Append("Package Code : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgCode").Value)
                End If
                sbHTMLOutput.Append("                       </A>")
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:BlanchedAlmond"">Package Desc : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgDesc").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                   <TD style=""background-color:BlanchedAlmond"" colspan=""3"">Package Comment : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgComment").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                </TR>")
                sbHTMLOutput.Append("               <TR>")
                sbHTMLOutput.Append("                   <TD style=""background-color:BlanchedAlmond"" colspan=""5"">Package T&amp;C URL  : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgTCURL").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("               </TR>")
                sbHTMLOutput.Append("               <TR>")
                sbHTMLOutput.Append("                   <TD style=""background-color:BlanchedAlmond"" colspan=""5"">Package Info URL  : " & oVehicleNode.SelectSingleNode("Package").Attributes("PkgInfoURL").Value)
                sbHTMLOutput.Append("                   </TD>")
                sbHTMLOutput.Append("                </TR>")
            End If





            sbHTMLOutput.Append("           </TABLE>")

            sbHTMLOutput.Append("       </TD>")
            sbHTMLOutput.Append("   </TR>")
        Next

        ' Errors
        sbHTMLOutput.Append("   <TR>")
        sbHTMLOutput.Append("       <TD>")
        sbHTMLOutput.Append("ERRORS ENCOUNTERED : -")

        For Each oErrorNode As XmlNode In VehicleXML.DocumentElement.SelectNodes("Errors/Vehicle")
            sbHTMLOutput.Append("<br/>" & oErrorNode.Attributes("PrdShortName").Value & " - " & oErrorNode.Attributes("ErrorMessage").Value)
        Next
        For Each oErrorNode As XmlNode In VehicleXML.DocumentElement.SelectNodes("Errors/Location")
            sbHTMLOutput.Append("<br/>" & oErrorNode.Attributes("ErrorMessage").Value)
        Next
        For Each oErrorNode As XmlNode In VehicleXML.DocumentElement.SelectNodes("Errors/Error")
            sbHTMLOutput.Append("<br/>" & oErrorNode.Attributes("ErrorMessage").Value)
        Next
        sbHTMLOutput.Append("       </TD>")
        sbHTMLOutput.Append("   </TR>")

        sbHTMLOutput.Append("</TABLE>")
        Return sbHTMLOutput.ToString()
    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ows As New PhoenixWebService
        Dim xmlForceTest As New XmlDocument
        xmlForceTest.LoadXml( _
                      "<Data>" & _
                      "   <RentalDetails>" & _
                      "       <AddBookingNumber/>" & _
                      "       <AvpId/>" & _
                      "       <AgentReference>AGENTREF1234</AgentReference>" & _
                      "       <QuantityRequested>1</QuantityRequested>" & _
                      "       <Status>KK</Status>" & _
                      "       <CheckOutDateTime>" & txtCheckOutDate.Text & "</CheckOutDateTime>" & _
                      "       <CheckOutLocationCode>" & txtCheckOutZoneCode.Text.Trim().ToUpper() & "</CheckOutLocationCode>" & _
                      "       <CheckInDateTime>" & txtCheckInDate.Text & "</CheckInDateTime>" & _
                      "       <CheckInLocationCode>" & txtCheckInZoneCode.Text.Trim().ToUpper() & "</CheckInLocationCode>" & _
                      "       <ProductList/>" & _
                      "       <CountryCode>" & cmbCountryCode.SelectedValue.Trim().ToUpper() & "</CountryCode>" & _
                      "       <Note>This is a note to be added to the booking</Note>" & _
                      "       <UserCode>B2CNZ</UserCode>" & _
                      "       <ConfirmationHeaderText/>" & _
                      "       <ConfirmationFooterText/>" & _
                      "       <GetConfXML>False</GetConfXML>" & _
                      "       <ProductShortName>" & txtVehicleCode.Text.Trim().ToUpper() & "</ProductShortName>" & _
                      "       <AgentCode>" & txtAgentCode.Text.Trim().ToUpper() & "</AgentCode>" & _
                      "       <PackageCode>" & txtPackageCode.Text.Trim().ToUpper() & "</PackageCode>" & _
                      "       <NoOfAdults>" & txtNumberOfAdults.Text.Trim().ToUpper() & "</NoOfAdults>" & _
                      "       <NoOfChildren>" & txtNoOfChildren.Text.Trim().ToUpper() & "</NoOfChildren>" & _
                      "       <BrandCode>" & cmbBrand.SelectedValue.Trim().ToUpper() & "</BrandCode>" & _
                      "       <IsInclusivePackage>False</IsInclusivePackage>" & _
                      "       <IsTestMode>" & chkIsTestMode.Checked & "</IsTestMode>" & _
                      "       <IsRequestBooking>True</IsRequestBooking>" & _
                      "       <IsVan>" & chkIsVan.Checked & "</IsVan>" & _
                      "   </RentalDetails>" & _
                      "   <CustomerData>" & _
                      "       <Title>Mr.</Title>" & _
                      "       <FirstName>Shoel</FirstName>" & _
                      "       <LastName>Palli</LastName>" & _
                      "       <Email>shoel.palli@thlonline.com</Email>" & _
                      "       <PhoneNumber>+6493364237</PhoneNumber>" & _
                      "       <CountryOfResidence>NZ</CountryOfResidence>" & _
                      "   </CustomerData>" & _
                      "   <CreditCardDetails>" & _
                      "       <CardType>VISA</CardType>" & _
                      "       <CreditCardNumber>1111-1111-1111-1111</CreditCardNumber>" & _
                      "       <CreditCardName>V ISA</CreditCardName>" & _
                      "       <ApprovalRef>123</ApprovalRef>" & _
                      "       <ExpiryDate>12/12</ExpiryDate>" & _
                      "       <PaymentAmount>1000.00</PaymentAmount>" & _
                      "       <PaymentSurchargeAmount>2.00</PaymentSurchargeAmount>" & _
                      "   </CreditCardDetails>" & _
                      "   <DPSData>" & _
                      "       <DPSAuthCode>DPSAUTH</DPSAuthCode>" & _
                      "       <DPSTxnRef>DPSTXNREF</DPSTxnRef>" & _
                      "   </DPSData>" & _
                      "</Data>")

        Dim xmlRet As XmlNode

        Dim xmltemp As New XmlDocument

        xmltemp.LoadXml("<data xmlns=''><RentalDetails><AddBookingNumber /><AvpId /><AgentReference /><QuantityRequested>1</QuantityRequested><Status>WL</Status><CheckOutDateTime>02-Feb-2010 08:00</CheckOutDateTime><CheckOutLocationCode>ADL</CheckOutLocationCode><CheckInDateTime>24-Feb-2010 15:00</CheckInDateTime><CheckInLocationCode>ADL</CheckInLocationCode><ProductList /><CountryCode>AU</CountryCode><Note>test(This model is not available to this location,)</Note><UserCode>B2CAU</UserCode><ConfirmationHeaderText /><ConfirmationFooterText /><IsVan>True</IsVan><GetConfXML>False</GetConfXML><ProductShortName>2BBXS</ProductShortName><AgentCode>B2CAU</AgentCode><PackageCode /><NoOfAdults>1</NoOfAdults><NoOfChildren>0</NoOfChildren><BrandCode>b</BrandCode><IsInclusivePackage>False</IsInclusivePackage><IsTestMode>False</IsTestMode><IsRequestBooking>True</IsRequestBooking></RentalDetails><CustomerData><Title>Mr</Title><FirstName>test</FirstName><LastName>test</LastName><Email>denis.ng@thlonline.com</Email><PhoneNumber>+1111111</PhoneNumber><CountryOfResidence>DK</CountryOfResidence><PreferredModeOfCommunication>PHONE</PreferredModeOfCommunication></CustomerData></data>")

        'xmlRet = ows.ForceCreateBooking(xmlForceTest)
        xmlRet = ows.ForceCreateBooking(xmltemp)

        ''ltrForceResult.Text = Server.HtmlEncode(xmlRet.OuterXml)
        LitResultSet.Text = Server.HtmlEncode(xmlRet.OuterXml)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CBool(IIf(Request("FillDefaults") Is Nothing, 0, Request("FillDefaults"))) AndAlso Not IsPostBack Then
            txtAgentCode.Text = "B2CNZ"
            ''txtCheckOutDate.Text = Now.Date.AddDays(15).AddHours(11).ToString(DATE_FORMAT & " " & TIME_FORMAT)
            ''txtCheckInDate.Text = Now.Date.AddDays(35).AddHours(11).ToString(DATE_FORMAT & " " & TIME_FORMAT)
            txtCheckOutDate.Text = "01-Apr-2013 10:00"
            txtCheckInDate.Text = "30-Apr-2013 10:00"

            txtCountryOfResidence.Text = "NZ"
            txtNumberOfAdults.Text = 1
            txtNoOfChildren.Text = 0
            txtCheckOutZoneCode.Text = "AKL"
            txtCheckInZoneCode.Text = "AKL"
            cmbBrand.SelectedValue = "B"
            cmbCountryCode.SelectedValue = "NZ"
            chkIsBestBuy.Checked = True
            chkIsTestMode.Checked = False
            chkIsVan.Checked = True
        End If

        ''REV:MIA NOV172010
        If Not String.IsNullOrEmpty(ConfigurationManager.AppSettings("displayTestValue")) Then
            Dim displayTestValue As String = ConfigurationManager.AppSettings("displayTestValue")
            If displayTestValue.Equals("Y") And Not IsPostBack Then
                FillWithTestData()
            End If
        End If

    End Sub

    Public Function XmlEncode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39, 37, 58, 63}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, Chr(aryChars(i)), "&#" & aryChars(i) & ";")
        Next
        XmlEncode = strText
    End Function


#Region "manny's testing REV:MIA NOV172010"
    Sub FillWithTestData()
        txtAgentCode.Text = "b2cnz"
        txtCheckOutDate.Text = "01-Apr-2013 11:00" ''Now.Date.AddDays(1).AddHours(11).ToString(DATE_FORMAT & " " & TIME_FORMAT)
        txtCheckInDate.Text = "30-Apr-2013 11:00" ''Now.Date.AddDays(11).AddHours(11).ToString(DATE_FORMAT & " " & TIME_FORMAT)
        txtCountryOfResidence.Text = "NZ"
        txtNumberOfAdults.Text = 1
        txtNoOfChildren.Text = 0
        txtCheckOutZoneCode.Text = "AKL"
        txtCheckInZoneCode.Text = "AKL"
        cmbBrand.SelectedValue = "B"
        cmbCountryCode.SelectedValue = "NZ"
        chkIsBestBuy.Checked = True
        chkIsTestMode.Checked = False
        chkIsVan.Checked = True
        txtVehicleCode.Text = "2bb"
        ''txtVehicleCode.Text = "2BMA"
    End Sub
#End Region

    Protected Sub alternateAvailability_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles alternateAvailability.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim oResult As XmlDocument
        Dim foundDataIn As String = ""
        Dim foundDataout As String = ""
        Dim reachcallback As Boolean = False
        Dim warningmessage As String = ""

        oService.VehicleInfo.VehicleCode = txtVehicleCode.Text
        oService.VehicleInfo.CountryCode = cmbCountryCode.SelectedValue
        oService.VehicleInfo.Brand = cmbBrand.SelectedValue
        oService.VehicleInfo.CheckOutZoneCode = txtCheckOutZoneCode.Text
        oService.VehicleInfo.CheckOutDateTime = CDate(txtCheckOutDate.Text)
        oService.VehicleInfo.CheckInZoneCode = txtCheckInZoneCode.Text
        oService.VehicleInfo.CheckInDateTime = CDate(txtCheckInDate.Text)
        oService.VehicleInfo.AgentCode = txtAgentCode.Text
        oService.VehicleInfo.PackageCode = txtPackageCode.Text
        oService.VehicleInfo.IsVan = chkIsVan.Checked
        oService.VehicleInfo.IsTestMode = chkIsTestMode.Checked

        oService.Passenger.CountryOfResidence = txtCountryOfResidence.Text
        oService.Passenger.IsBestBuy = chkIsBestBuy.Checked
        oService.Passenger.NoOfAdults = CInt(txtNumberOfAdults.Text)
        oService.Passenger.NoOfChildren = CInt(txtNoOfChildren.Text)
        oService.VehicleInfo.IsGross = chkIsGross.Checked
        Dim aryCriteria(cblCriteria.Items.Count - 1) As Integer
        Dim newCriteria As String
        For Each item As ListItem In cblCriteria.Items
            If item.Selected Then
                ''aryCriteria(item.Value) = item.Value
                newCriteria = newCriteria & item.Value & ","
            Else
                ''aryCriteria(item.Value) = -1
                ''newCriteria = newCriteria & "-1,"
                'If (item.Value = 7) Then
                '    newCriteria = newCriteria & item.Value & ","
                'End If
            End If
        Next

        If newCriteria = "" Then newCriteria = "1"
        If newCriteria.Substring(newCriteria.Length - 1) = "," Then
            newCriteria = newCriteria.Remove(newCriteria.Length - 1)
        End If

        If (newCriteria = "7") Then
            LitResultSet.Text = "<br/><h1>Please select criteria below</h1>"
            Exit Sub
        End If

        ''ReDim Preserve aryCriteria(aryCriteria.Length - 1)
        ''Array.Sort(aryCriteria)
        ''Array.Reverse(aryCriteria)
        ''oResult = oService.GetAlternateAvailability(oService.VehicleInfo, oService.Passenger, aryCriteria)

        ''aryCriteria = New Integer() {6, 1, 2, 3, 4, 5, 0}

        Try
            Dim server As String = Request.ServerVariables("HTTP_HOST")
            Dim virtualDirectory As String = Request.ServerVariables("URL").ToUpper ''.Remove("/Default.aspx").Replace("/", "").Replace(" ", "%20")
            virtualDirectory = virtualDirectory.Remove(virtualDirectory.IndexOf("/default.aspx".ToUpper)).Replace("/", "").Replace(" ", "%20")

            Dim sbURL As New StringBuilder
            With sbURL
                .Append("http://")
                'If server.Contains("localhost") Then
                '    .Append(server)
                'End If
                .Append(server)
                .Append("/")
                .Append(virtualDirectory)
                .Append("/")
                .Append("service.asmx/GetAlternateAvailability?")
                .AppendFormat("{0}={1}", "&VehicleCode", txtVehicleCode.Text)
                .AppendFormat("{0}={1}", "&CountryCode", cmbCountryCode.SelectedValue)
                .AppendFormat("{0}={1}", "&Brand", cmbBrand.SelectedValue)
                .AppendFormat("{0}={1}", "&CheckOutZoneCode", txtCheckOutZoneCode.Text)
                .AppendFormat("{0}={1}", "&CheckOutDateTime", txtCheckOutDate.Text)
                .AppendFormat("{0}={1}", "&CheckInZoneCode", txtCheckInZoneCode.Text)
                .AppendFormat("{0}={1}", "&CheckInDateTime", txtCheckInDate.Text)
                .AppendFormat("{0}={1}", "&AgentCode", txtAgentCode.Text)
                .AppendFormat("{0}={1}", "&PackageCode", txtPackageCode.Text)
                .AppendFormat("{0}={1}", "&IsVan", chkIsVan.Checked)
                .AppendFormat("{0}={1}", "&NumberOfAdults", CInt(txtNumberOfAdults.Text))
                .AppendFormat("{0}={1}", "&NoOfChildren", CInt(txtNoOfChildren.Text))
                .AppendFormat("{0}={1}", "&isBestBuy", chkIsBestBuy.Checked)
                .AppendFormat("{0}={1}", "&countryOfResidence", txtCountryOfResidence.Text)
                .AppendFormat("{0}={1}", "&DisplayMode", newCriteria)
                .AppendFormat("{0}={1}", "&Istestmode", chkIsTestMode.Checked)
                .AppendFormat("{0}={1}", "&IsGross", chkIsGross.Checked)

            End With
            Urllabel.InnerText = sbURL.ToString

        Catch ex As Exception
            Urllabel.InnerText = "(Ignore )Test Page error: " & ex.Message
        End Try

       
        oResult = oService.GetAlternateAvailability(txtVehicleCode.Text, _
                                                           cmbCountryCode.SelectedValue, _
                                                           cmbBrand.SelectedValue, _
                                                           txtCheckOutZoneCode.Text, _
                                                           CDate(txtCheckOutDate.Text), _
                                                           txtCheckInZoneCode.Text, _
                                                           CDate(txtCheckInDate.Text), _
                                                           txtAgentCode.Text, _
                                                           txtPackageCode.Text, _
                                                           chkIsVan.Checked, _
                                                           CInt(txtNumberOfAdults.Text), _
                                                           CInt(txtNoOfChildren.Text), _
                                                           chkIsBestBuy.Checked, _
                                                           txtCountryOfResidence.Text, _
                                                           newCriteria, chkIsTestMode.Checked, chkIsGross.Checked)




        alternateAvailabilityLiteral.Text = ProcessAlternateVehicleList(oResult)

    End Sub

    Function ProcessAlternateVehicleList(ByVal vehicle As XmlDocument) As String

        ''SaveXml(vehicle)
        Dim sb As New StringBuilder

        Try
            If vehicle.ChildNodes.Count = 0 Then
                Return "Please check your data in the test page"
            End If
            If vehicle.SelectSingleNode("data").ChildNodes.Count - 1 >= 0 Then


                Dim availablerate As XmlElement = Nothing
                For Each availablerate In vehicle.SelectSingleNode("data").ChildNodes
                    sb.Append("<table border='1' width='100%;'>")
                    For Each content As XmlElement In availablerate.ChildNodes
                        If (content.Name = "Package") Then

                            If Not availablerate.Attributes("searchPattern") Is Nothing AndAlso Not availablerate.Attributes("rqGuid") Is Nothing Then
                                sb.AppendFormat("<tr><td width='150px;'>SearchPattern/Guid</td><td><b>{0}</b></td></tr>", availablerate.Attributes("searchPattern").InnerText & " / " & availablerate.Attributes("rqGuid").InnerText)
                            Else
                                sb.AppendFormat("<tr><td width='150px;'>SearchPattern/Guid</td><td><b>{0}</b></td></tr>", "? / ?")
                            End If

                            If Not content.Attributes("BrandCode") Is Nothing Then
                                sb.AppendFormat("<tr><td>BrandCode</td><td>{0}</td></tr>", content.Attributes("BrandCode").InnerText)
                            Else
                                sb.AppendFormat("<tr><td>BrandCode</td><td>{0}</td></tr>", "?")
                            End If

                            If Not content.Attributes("AvpId") Is Nothing Then
                                sb.AppendFormat("<tr><td>AvpId</td><td>{0}</td></tr>", content.Attributes("AvpId").InnerText)
                            Else
                                sb.AppendFormat("<tr><td>AvpId</td><td>{0}</td></tr>", "?")
                            End If
                        End If


                        If (content.Name = "Charge") Then
                            Dim sbtrace As New StringBuilder


                            If Not availablerate.Attributes("searchPattern") Is Nothing Then
                                System.Diagnostics.Debug.WriteLine(availablerate.Attributes("searchPattern").InnerText)
                            End If



                            If content.SelectSingleNode("Det[@IsVeh='1']").ChildNodes(0).ChildNodes.Count - 1 <> -1 Then
                                If Not content.SelectSingleNode("Det[@IsVeh='1']").ChildNodes(0).ChildNodes(0).ChildNodes(0) Is Nothing Then
                                    sbtrace.Append("<table border='1' width='100%;'")
                                    sbtrace.Append("<tr> <td width='175px;'>Code</td> <td width='175px;'>Name</td> <td width='175px;'>Pickup Date</td> <td width='500px;'>DropOff Date</td></tr>")
                                    sbtrace.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> </tr></table>", _
                                                     content.SelectSingleNode("Det[@IsVeh='1']").Attributes("PrdCode").InnerText, _
                                                     content.SelectSingleNode("Det[@IsVeh='1']").Attributes("PrdName").InnerText, _
                                                     content.SelectSingleNode("Det[@IsVeh='1']").SelectSingleNode("RateBands/Bands/Band").Attributes("From").InnerText, _
                                                     content.SelectSingleNode("Det[@IsVeh='1']").SelectSingleNode("RateBands/Bands/Band").Attributes("To").InnerText)

                                    sb.AppendFormat("<tr><td valign='top' style='color:Blue;'>{0}</td><td>{1}</td></tr>", String.Concat(content.SelectSingleNode("Det[@IsVeh='1']").Attributes("CurrCode").InnerText, " ", content.SelectSingleNode("Det[@IsVeh='1']").Attributes("Price").InnerText), sbtrace.ToString)
                                End If
                            End If



                        End If


                        If (content.Name = "Errors") Then

                            If content.HasChildNodes Then
                                If (Not content.ChildNodes(0).Attributes("Message") Is Nothing) Or (Not content.ChildNodes(1).Attributes("Message") Is Nothing) Then
                                    If content.ChildNodes(0).Attributes("Message").InnerText <> "" Or content.ChildNodes(1).Attributes("Message").InnerText <> "" Then
                                        Dim sbError As New StringBuilder
                                        sbError.Append("<table border='1' width='100%'>")
                                        sbError.AppendFormat("<tr><td width='175px;'>Blocking Error</td><td width='850px;'  style='color:Red;' >{0}</td></tr>", content.ChildNodes(0).Attributes("Message").InnerText)
                                        sbError.AppendFormat("<tr><td width='175px;'>Other Error</td><td width='850px;'  style='color:Red;' >{0}</td></tr>", content.ChildNodes(1).Attributes("Message").InnerText)
                                        sbError.Append("</table>")
                                        sb.AppendFormat("<tr><td></td><td>{0}</td></tr>", sbError.ToString)
                                    End If
                                End If
                            End If


                            Dim tracecount As Integer = 0
                            ''If vehicle.SelectSingleNode("data/Traces[@searchPattern='MoveForward']").childnodes.Count
                            If Not vehicle.SelectSingleNode("data/Traces") Is Nothing Then
                                Dim searchpattern As String = availablerate.Attributes("searchPattern").InnerText
                                tracecount = vehicle.SelectSingleNode("data/Traces[@searchPattern='" & searchpattern & "']").ChildNodes.Count - 1
                                If tracecount <> -1 Then
                                    Dim sbtraceActivity As New StringBuilder
                                    sbtraceActivity.Append("<table border='1' width='100%'>")
                                    sbtraceActivity.Append("<tr> <td width='175px;'>Loop Counter</td> <td width='175px;'>PickupDate/Loc</td> <td width='175pxpx;'>DropDate/Loc</td> <td width='500px;'>Messages</td></tr>")

                                    Dim loopctr As String = ""
                                    Dim PickupDateLoc As String = ""
                                    Dim DropupDateLoc As String = ""
                                    Dim warningmessage As String = ""
                                    For Each myTrace As XmlElement In vehicle.SelectSingleNode("data/Traces[@searchPattern='" & searchpattern & "']").ChildNodes
                                        If Not String.IsNullOrEmpty(myTrace.Attributes("Message").InnerText) Then
                                            Dim activity As String() = myTrace.Attributes("Message").InnerText.Trim.Split("|")
                                            If (activity.Length - 1 = 3) Then
                                                loopctr = activity(0)
                                                PickupDateLoc = activity(1).Trim
                                                DropupDateLoc = activity(2).Trim
                                                warningmessage = activity(3).Trim

                                                sbtraceActivity.AppendFormat("<tr> <td>{0}</td> <td>{1}</td> <td>{2}</td> <td>{3}</td> </tr>", _
                                                                 loopctr, _
                                                                 PickupDateLoc, _
                                                                 DropupDateLoc, _
                                                                warningmessage)

                                            End If
                                        End If
                                    Next
                                    sb.AppendFormat("<tr><td valign='top'>Trace</td><td>{0}</td></tr></table>", sbtraceActivity.ToString)
                                End If
                            End If
                            ''Dim tracesNode As XmlElement = availablerate("Traces"

                        End If
                    Next

                    sb.Append("</table>")
                Next

            End If



        Catch ex As Exception
            Return "Exception thrown..please try again.." & ex.Message & vbCrLf & ex.StackTrace
        End Try
        Return sb.ToString ''ProcessVehicleList(vehicle)
    End Function

    Function AlternateVehicleError(ByVal vehicle As XmlDocument) As String

        Dim sbError As New StringBuilder
        sbError.Append("<ul>")
        sbError.AppendFormat("<li>{0}", "Blocking Error: " & vehicle.SelectSingleNode("data/AvailableRate/Errors").ChildNodes(0).Attributes("Message").InnerText)
        sbError.AppendFormat("<li>{0})", "Error         : " & vehicle.SelectSingleNode("data/AvailableRate/Errors").ChildNodes(1).Attributes("Message").InnerText)
        sbError.Append("</ul>")

        Dim traces As XmlNode
        Dim sbTrace As New StringBuilder

        sbTrace.Append("<table border='1'>")
        sbTrace.AppendFormat("<tr><td>Counter</td><td>Pickup Date</td><td>DropOff Date</td><td>Message</td>")
        If Not vehicle.SelectSingleNode("data/Traces") Is Nothing Then
            traces = vehicle.SelectSingleNode("data/Traces")
            For Each tracenode As XmlElement In traces
                ''sbTrace.AppendFormat("<li>{0}", tracenode.Attributes("Message").InnerText)
                Dim tempMessages() As String = tracenode.Attributes("Message").InnerText.Split("|")
                Dim counter As String = tempMessages(0).ToUpper.Trim
                Dim pkDate As String = tempMessages(1).ToUpper.Trim
                Dim dpDate As String = tempMessages(2).ToUpper.Trim
                Dim finalmsg As String = tempMessages(3).ToUpper.Trim
                sbTrace.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", counter, pkDate, dpDate, finalmsg)
            Next

        End If
        sbTrace.Append("</table>")


        Dim sb As New StringBuilder
        sb.Append("<table border='1'>")
        sb.AppendFormat("<tr><td>BrandCode</td><td>{0}</td></tr>", vehicle.SelectSingleNode("data/AvailableRate/Package").Attributes(0).InnerText)
        sb.AppendFormat("<tr><td>Product Code</td><td>{0}</td></tr>", vehicle.SelectSingleNode("data/AvailableRate/Charge/Det").Attributes("PrdCode").InnerText)
        sb.AppendFormat("<tr><td>Product Name</td><td>{0}</td></tr>", vehicle.SelectSingleNode("data/AvailableRate/Charge/Det").Attributes("PrdName").InnerText)
        sb.AppendFormat("<tr><td>Errors</td><td>{0}</td></tr>", sbError.ToString)
        sb.AppendFormat("<tr><td>TracesActivity</td><td>{0}</td></tr>", sbTrace.ToString)
        sb.Append("</table>")



        Return sb.ToString
    End Function


    Sub SaveXml(ByVal vehicleDoc As XmlDocument)
        Try
            Dim filename As String = vehicleDoc.SelectSingleNode("data/AvailableRate").Attributes("rqGuid").InnerText & ".xml"
            Dim xmlpath As String = Server.MapPath("xml") & "\" & filename
            vehicleDoc.Save(xmlpath)
            System.Diagnostics.Process.Start(xmlpath)
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        'LiteralThrifty.Text = "Alive :<b> " & oService.IsThriftyAlive() & "</b></br>URL : " _
        '                                                                       & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityURL") & "?soap_method=VehLocDetailsNotif&pickup=&account=&token=&vehicle=&promotion=&subaccount=" _
        '                                                                       & "</br>Timeout: " & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityTimeOutCheck") & " ms"


        LitResultSet.Text = "Alive :<b> " & oService.IsThriftyAlive() & "</b></br>URL : " _
                                                                               & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityURL") & "?soap_method=VehLocDetailsNotif&pickup=&account=&token=&vehicle=&promotion=&subaccount=" _
                                                                               & "</br>Timeout: " & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityTimeOutCheck") & " ms"
    End Sub


#Region "Centralized GetAvailability"

    Protected Sub btnGetAvailabilityv2_Click(sender As Object, e As System.EventArgs) Handles btnGetAvailabilityv2.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim oResult As XmlDocument

        Session("Brand") = cmbBrand.SelectedValue
        'Session("Company") = cmbCompanyCode.SelectedValue
        Session("Country") = cmbCountryCode.SelectedValue
        Session("CODate") = CDate(txtCheckOutDate.Text)
        Session("CIDate") = CDate(txtCheckInDate.Text)
        Session("COZone") = txtCheckOutZoneCode.Text
        Session("CIZone") = txtCheckInZoneCode.Text
        'Session("COTct") = txtCheckOutTownCityCode.Text
        'Session("CITct") = txtCheckInTownCityCode.Text
        Session("IsVan") = chkIsVan.Checked
        Session("IsTest") = chkIsTestMode.Checked

        If txtNoOfChildren.Text.Trim().Equals(String.Empty) Then
            txtNoOfChildren.Text = "0"
        End If

        'If txtNoOfInfants.Text.Trim().Equals(String.Empty) Then
        '    txtNoOfInfants.Text = "0"
        'End If

        'oResult = oService.GetAvailabilityV2(cmbBrand.SelectedValue, cmbCountryCode.SelectedValue, txtVehicleCode.Text, _
        '                                   txtCheckOutZoneCode.Text, CDate(txtCheckOutDate.Text), _
        '                                   txtCheckInZoneCode.Text, CDate(txtCheckInDate.Text), _
        '                                   CInt(txtNumberOfAdults.Text), CInt(txtNoOfChildren.Text), _
        '                                   txtAgentCode.Text, txtPackageCode.Text, _
        '                                   chkIsVan.Checked, chkIsBestBuy.Checked, txtCountryOfResidence.Text, False, _
        '                                   chkIsTestMode.Checked, _
        '                                   IIf(chkIsAurora.Checked = True, 0, 1), _
        '                                   chkisQuickAvail.Checked)

        If (chkIsAurora.Checked = True) Then
            If (oResult.OuterXml.Contains("ERROR") = True) Then
                GetAvailabilityV2Error(oResult)
            Else
                GetAvailabilityV2(oResult)
            End If

        Else

            LitResultSet.Text = ProcessVehicleList(oResult)
        End If



    End Sub

    Sub GetAvailabilityV2(xml As XmlDocument)

        Dim sb As New StringBuilder
        sb.Append("<table border='1'><tr><td>DVASS Number</td><td>Out</td><td>In</td><td>#</td></tr>")

        Dim xquery As String = "Data/AvailableVehicle/"
        Dim dvassnumber As String = xml.SelectSingleNode(xquery & "PrdId").InnerText
        Dim out As String = xml.SelectSingleNode(xquery & "CkoLocation").InnerText
        Dim outdate As String = xml.SelectSingleNode(xquery & "CkoDate").InnerText
        Dim ins As String = xml.SelectSingleNode(xquery & "CkiLocation").InnerText
        Dim insdate As String = xml.SelectSingleNode(xquery & "CkiDate").InnerText
        Dim vehnum As String = xml.SelectSingleNode(xquery & "NumberOfVehicles").InnerText

        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>", dvassnumber, out & "</br>" & outdate, ins & "</br>" & insdate, vehnum)
        sb.Append("</table>")

        LitResultSet.Text = sb.ToString
    End Sub

    Sub GetAvailabilityV2Error(xml As XmlDocument)

        Dim sb As New StringBuilder
        sb.Append("<table border='1'><tr><td>Status</td><td>Result</td><td>Message</td></tr>")

        Dim xquery As String = "Data/"
        Dim status As String = xml.SelectSingleNode(xquery & "Status").InnerText
        Dim result As String = xml.SelectSingleNode(xquery & "Result").InnerText
        Dim mesge As String = xml.SelectSingleNode(xquery & "Message").InnerText

        sb.AppendFormat("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>", status, result, mesge)
        sb.Append("</table>")

        LitResultSet.Text = sb.ToString
    End Sub

#End Region

    Protected Sub btnGetAvailabilityInGrid_Click(sender As Object, e As System.EventArgs) Handles btnGetAvailabilityInGrid.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim oResult As XmlDocument

        
        If txtNoOfChildren.Text.Trim().Equals(String.Empty) Then
            txtNoOfChildren.Text = "0"
        End If

        
        oResult = oService.GetAvailabilitiesBasedOnDays(cmbBrand.SelectedValue, _
                                                        cmbCountryCode.SelectedValue, _
                                                        txtVehicleCode.Text, _
                                                        txtCheckOutZoneCode.Text, _
                                                        CDate(txtCheckOutDate.Text), _
                                                        txtCheckInZoneCode.Text, _
                                                        CDate(txtCheckInDate.Text), _
                                                        CInt(txtNumberOfAdults.Text), _
                                                        CInt(txtNoOfChildren.Text), _
                                                        txtAgentCode.Text, _
                                                        txtPackageCode.Text, _
                                                        chkIsVan.Checked, _
                                                        chkIsBestBuy.Checked, _
                                                        txtCountryOfResidence.Text, _
                                                        False, _
                                                        chkIsTestMode.Checked)


        LitResultSet.Text = AvailabilityBasedOnDays(oResult)

    End Sub

    Function AvailabilityBasedOnDays(xml As XmlDocument) As String

        If xml Is Nothing Then Return "No result"

        Dim vehiclecode As String = xml.SelectSingleNode("data/vehiclecode").Attributes("code").Value
        Dim locfrom As String = xml.SelectSingleNode("data/vehiclecode").Attributes("from").Value
        Dim locto As String = xml.SelectSingleNode("data/vehiclecode").Attributes("to").Value
        Dim origDatefrom As String = xml.SelectSingleNode("data/vehiclecode").Attributes("origDateFrom").Value
        Dim origDateto As String = xml.SelectSingleNode("data/vehiclecode").Attributes("origDateTo").Value
        Dim noofDays As String = xml.SelectSingleNode("data/vehiclecode").Attributes("noofdays").Value


        Dim availabilitiesNodes As XmlNodeList = xml.SelectNodes("data/vehiclecode/availability")


        Dim datefrom As String = String.Empty
        Dim dateto As String = String.Empty
        Dim mycolor As String = String.Empty
        Dim errormsg As String = "nil"
        Dim noofvehicle As String = "nil"
        Dim avail As String = String.Empty

        Dim sb As New StringBuilder
        sb.Append("<table border='1' width='100%;'>")
        sb.AppendFormat("<tr><td>Vehicle:</td><td>{0}</td></tr>", vehiclecode)
        sb.AppendFormat("<tr><td>From:</td><td>{0}</td></tr>", locfrom)
        sb.AppendFormat("<tr><td>To:</td><td>{0}</td></tr>", locto)
        sb.AppendFormat("<tr><td>DateFrom</td><td>{0}</td></tr>", origDatefrom)
        sb.AppendFormat("<tr><td>DateTo</td><td>{0}</td></tr>", origDateto)
        sb.AppendFormat("<tr><td>Days to compute (-/+)</td><td>{0}</td></tr>", noofDays)
        sb.Append("<tr><td>CKO-Out</td><td>CKO-In</td><td>#</td><td>message</td><td>Status</td></tr>")

        For Each item As XmlNode In availabilitiesNodes
            datefrom = item.Attributes("fr").Value
            dateto = item.Attributes("fr").Value
            ''mycolor = item.Attributes("bgcolor").Value
            If (Not item.Attributes("rule") Is Nothing) Then
                errormsg = item.Attributes("rule").Value
            End If
            If (Not item.Attributes("no") Is Nothing) Then
                noofvehicle = item.Attributes("no").Value
            End If

            avail = item.InnerText

            sb.AppendFormat("<tr><td width='150px;'>{0}</td><td width='150px;'>{1}</td><td <td width='50px;'>{2}</td><td <td width='250px;'>{3}</td><td style='background-color:{4}; width:50px;'>{5}</td></tr>", datefrom, dateto, noofvehicle, errormsg, mycolor, avail)

        Next
        sb.Append("</table>")

        Return sb.ToString
    End Function

    Protected Sub CreateAndSendButton_Click(sender As Object, e As System.EventArgs) Handles CreateAndSendButton.Click
        If String.IsNullOrEmpty(RentalIDTextBox.Text) Then Exit Sub
        If RentalIDTextBox.Text.Contains("-") = False Then Exit Sub

        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim oResult As XmlDocument = New XmlDocument
        Dim result As String = oService.CreateAndSendconfirmation(RentalIDTextBox.Text)
        ''oResult.LoadXml(oService.CreateAndSendconfirmation(RentalIDTextBox.Text))
    End Sub

    'Protected Sub BookingWithExtraHireItemAndPaymentButton_Click(sender As Object, e As System.EventArgs) Handles BookingWithExtraHireItemAndPaymentButton.Click
    '    'Dim oService As PhoenixWebService = New PhoenixWebService
    '    'Dim oinput As XmlDocument = New XmlDocument
    '    'oinput.LoadXml(HttpUtility.HtmlEncode(BookingWithExtraHireItemAndPaymentTextBox.Text))
    '    'Dim oResult As XmlDocument = New XmlDocument
    '    'oResult = oService.CreateBookingWithExtraHireItemAndPayment(oinput)

    'End Sub
End Class
