﻿Imports System.Xml
Imports PhoenixWebService

Partial Class PopUpDetails
    Inherits System.Web.UI.Page

    Const CONFIRMATION_XSL_PATH As String = "xsl/confreport.xsl"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadEHIDetails(Request("AvpId"))
            btnBookNow.Attributes.Add("onclick", "ParsePageData('" & txtXMLDATA.ClientID & "');")
        End If
        
    End Sub

    Sub LoadEHIDetails(ByVal AvpId As String, Optional ByVal bIsGross As Boolean = True)
        Dim xmlEHIDetails As XmlDocument
        Dim oService As PhoenixWebService = New PhoenixWebService
        xmlEHIDetails = oService.GetInsuranceAndExtraHireItems(AvpId, bIsGross, False)

        Dim sbHTMLTable As StringBuilder = New StringBuilder

        Dim oLookup As Hashtable = New Hashtable

        With sbHTMLTable

            .Append("<TABLE id='tblMain'>")

            ' #######################  INSURANCE OPTIONS #########################

            .Append("    <TR id='dummyRow1'>")
            .Append("        <TD colspan='4'>")
            .Append("INSURANCE OPTIONS")
            .Append("        </TD>")
            .Append("    </TR>")

            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/Insurence/Product")
                .Append("   <TR>")
                .Append("       <TD>")

                Dim sbNotDisplay As StringBuilder = New StringBuilder
                Dim sbDisplayWhen As StringBuilder = New StringBuilder
                If oProductNode.SelectNodes("RequiredActions/Action") IsNot Nothing Then
                    For Each oActionNode As XmlNode In oProductNode.SelectNodes("RequiredActions/Action")
                        Select Case oActionNode.Attributes("code").Value.ToUpper()
                            Case "NOTDISPLAY"
                                sbNotDisplay.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                            Case "DISPLAYWHEN"
                                sbDisplayWhen.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                        End Select
                    Next
                End If

                If Not String.IsNullOrEmpty(sbDisplayWhen.ToString()) Then
                    For Each sDependency As String In sbDisplayWhen.ToString().Substring(0, sbDisplayWhen.ToString().Length - 1).Split(","c)
                        If oLookup.ContainsKey("chk" & sDependency) Then
                            oLookup("chk" & sDependency) = oLookup("chk" & sDependency) & "," & oProductNode.Attributes("Code").Value.ToUpper()
                        Else
                            oLookup.Add("chk" & sDependency, oProductNode.Attributes("Code").Value.ToUpper())
                        End If
                    Next
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "');""  style='display:none;'/>")
                Else
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "');""  />")
                End If


                .Append("       </TD>")
                .Append("       <TD>")

                If Not String.IsNullOrEmpty(sbDisplayWhen.ToString()) Then
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%;display:none;"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                Else
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                End If

                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='500px'>Name : ")
                .Append(oProductNode.Attributes("Name").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Daily Rate : ")
                .Append(oProductNode.Attributes("dailyRate").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Max Charge : ")
                .Append(oProductNode.Attributes("MaxCharge").Value)
                .Append("                   </TD>")
                .Append("               </TR>")

                If oProductNode.SelectSingleNode("Note") IsNot Nothing Then
                    .Append("           <TR>")
                    .Append("               <TD colspan='4'>Note : <BR/>")
                    .Append(oProductNode.SelectSingleNode("Note").Attributes("NteDesc").Value.Replace(vbNewLine, "<BR/>"))
                    .Append("               </TD>")
                    .Append("           </TR>")
                End If

                .Append("           </TABLE>")

                .Append("       </TD>")
                .Append("   </TR>")
            Next

            ' #######################  ExtraHireItems #########################

            .Append("    <TR id='dummyRow2'>")
            .Append("        <TD colspan='4'>")
            .Append("EXTRA HIRE ITEMS")
            .Append("        </TD>")
            .Append("    </TR>")
            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/ExtraHireItems/Product")
                .Append("   <TR>")
                .Append("       <TD>")

                Dim sbNotDisplay As StringBuilder = New StringBuilder
                Dim sbDisplayWhen As StringBuilder = New StringBuilder
                If oProductNode.SelectNodes("RequiredActions/Action") IsNot Nothing Then
                    For Each oActionNode As XmlNode In oProductNode.SelectNodes("RequiredActions/Action")
                        Select Case oActionNode.Attributes("code").Value.ToUpper()
                            Case "NOTDISPLAY"
                                sbNotDisplay.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                            Case "DISPLAYWHEN"
                                sbDisplayWhen.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                        End Select
                    Next
                End If
                If oProductNode.Attributes("PromptQty").Value.ToUpper() = "YES" Then
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='txt" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='text' style=""width:15px;"" />")
                Else
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "','" & sbDisplayWhen.ToString() & "');"" />")
                End If

                .Append("       </TD>")
                .Append("       <TD>")
                .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='500px'>Name : ")
                .Append(oProductNode.Attributes("Name").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Daily Rate : ")
                .Append(oProductNode.Attributes("dailyRate").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Max Charge : ")
                .Append(oProductNode.Attributes("MaxCharge").Value)
                .Append("                   </TD>")

                .Append("               </TR>")
                .Append("           </TABLE>")
                .Append("       </TD>")
                .Append("   </TR>")

            Next

            ' #######################  Ferry Crossing #########################
            If xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/FerryCrossing/Product") IsNot Nothing Then

                .Append("    <TR id='dummyRow3'>")
                .Append("        <TD colspan='4'>")
                .Append("FERRY CROSSING")
                .Append("        </TD>")
                .Append("    </TR>")

                For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/FerryCrossing/Product")

                    Dim bHasChildRates As Boolean = False
                    ' check for child nodes
                    If oProductNode.SelectNodes("StartOn/PersonRate") IsNot Nothing AndAlso oProductNode.SelectNodes("StartOn/PersonRate").Count > 0 Then
                        bHasChildRates = True
                    End If

                    .Append("   <TR>")
                    .Append("       <TD>")
                    If bHasChildRates Then
                        .Append("           <INPUT  PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""ShowFerryDetails();"" />")
                    Else
                        .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox'  onclick=""ShowFerryDetails1();"" />")
                    End If

                    .Append("       </TD>")
                    .Append("       <TD>")
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                    .Append("               <TR>")
                    .Append("                   <TD width='150px'>Code : ")
                    .Append(oProductNode.Attributes("Code").Value)
                    .Append("                   </TD>")
                    .Append("                   <TD width='500px'>Name : ")
                    .Append(oProductNode.Attributes("Name").Value)
                    .Append("                   </TD>")

                    If oProductNode.Attributes("dailyRate") IsNot Nothing Then
                        .Append("                   <TD width='150px'>Daily Rate : ")
                        .Append(oProductNode.Attributes("dailyRate").Value)
                        .Append("                   </TD>")
                    End If

                    If oProductNode.Attributes("MaxCharge") IsNot Nothing Then
                        .Append("                   <TD width='150px'>Max Charge : ")
                        .Append(oProductNode.Attributes("MaxCharge").Value)
                        .Append("                   </TD>")
                    End If

                    If bHasChildRates Then
                        .Append("               </TR>")
                        .Append("               <TR>")
                        .Append("                   <TD colspan='4'>")
                        ' Child products
                        For Each oStartOn As XmlNode In oProductNode.SelectNodes("StartOn")


                            .Append("           <TABLE style=""border:solid 3px Black;width:100%;display:none"">")
                            .Append("               <TR>")

                            .Append("                   <TD>Crossing Date : ")
                            .Append("                       <INPUT TYPE='text' style=""width:75px;"" value='" & oStartOn.Attributes("dateBegin").Value & "' />")
                            .Append("                   </TD>")

                            .Append("                   <TD>")
                            .Append("                       <TABLE>")
                            For Each oPersonRateNode As XmlNode In oStartOn.SelectNodes("PersonRate")
                                .Append("                           <TR>")

                                .Append("                               <TD>Passenger Type : ")
                                .Append(oPersonRateNode.Attributes("PaxType").Value)
                                .Append("                               </TD>")

                                .Append("                               <TD>Rate : ")
                                .Append(oPersonRateNode.Attributes("PrrRate").Value)
                                .Append("                               </TD>")

                                .Append("                               <TD>No. of Passengers : ")
                                .Append("                                   <INPUT PrrId='" & oPersonRateNode.Attributes("PrrId").Value & "' PaxType='" & oPersonRateNode.Attributes("PaxType").Value & "' SapId='" & oPersonRateNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='text' style=""width:15px;"" />")
                                .Append("                           </TD>")

                                .Append("                       </TR>")
                            Next
                            .Append("                       </TABLE>")
                            .Append("                   </TD>")

                            .Append("               </TR>")
                            .Append("           </TABLE>")

                        Next
                        .Append("                   </TD>")
                        .Append("               </TR>")
                    Else
                        .Append("               </TR>")
                        .Append("               <TR style=""display:none;"">")
                        .Append("                   <TD colspan='4'>Crossing Date : ")
                        .Append("                       <INPUT TYPE='text' style=""width:75px;"" value='' />")
                        .Append("                   </TD>")

                        .Append("                   <TD>")
                        .Append("                   </TD>")

                        .Append("               </TR>")
                    End If


                    .Append("           </TABLE>")
                    .Append("       </TD>")
                    .Append("   </TR>")

                Next
            End If

            ' #######################  Credit Card Surcharge #########################
            .Append("    <TR id='dummyRow4'>")
            .Append("        <TD colspan='4'>")
            .Append("CREDIT CARD SURCHARGE")
            .Append("        </TD>")
            .Append("    </TR>")
            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/Surcharge/Product")
                .Append("   <TR>")
                .Append("       <TD>")
                .Append("       </TD>")
                .Append("       <TD>")
                .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD colspan='3'>Surcharge Percentage : ")
                .Append(oProductNode.Attributes("SurchargePer").Value)
                .Append("                   </TD>")

                .Append("               </TR>")
                .Append("           </TABLE>")
                .Append("       </TD>")
                .Append("   </TR>")

            Next


            .Append("</TABLE>")

        End With

        Dim sHTML As String = sbHTMLTable.ToString()

        For Each sKey As Object In oLookup.Keys
            sHTML = sHTML.Replace("id='" & sKey & "'", " id='" & sKey & "' dependentProductList='" & oLookup(sKey) & "' ")
        Next


        ltrExtraHireItems.Text = sHTML
    End Sub



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim xmlData As New XmlDocument
        xmlData.LoadXml(txtXMLDATA.Text)
        Dim result As XmlDocument
        result = oService.CreateBookingWithExtraHireItemAndPayment(xmlData)
        'result = oService.ForceCreateBooking(xmlData)
        'result = oService.AddUpdateCustomerData(xmlData)
        'result = oService.ModifyBooking(xmlData)

        txtXMLDATA.Text = ""
        txtXMLDATA.Text = result.OuterXml

        Dim transform As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform()
        transform.Load(Server.MapPath(CONFIRMATION_XSL_PATH))

        Dim confirmationXml As XmlDocument = New XmlDocument
        If result.SelectSingleNode("Data/Confirmation/ConfXML") IsNot Nothing Then
            confirmationXml.LoadXml(result.SelectSingleNode("Data/Confirmation/ConfXML").InnerXml)
            Dim confirmationHtml As String
            confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText.Replace("..", Request.ApplicationPath)

            Using writer As System.IO.StringWriter = New System.IO.StringWriter
                transform.Transform(confirmationXml, Nothing, writer, Nothing)
                confirmationHtml = Server.HtmlDecode(writer.ToString())
            End Using

            confirmationXmlLiteral.Text = confirmationHtml
        End If
        
    End Sub

    Protected Sub btnBookNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBookNow.Click
        Dim sCkoLoc As String = "" 'WebServiceData.GetLocation(Session("COZone"), Session("Brand"), IIf(CBool(Session("IsVan")), "AV", "AC"), Session("Country"))
        Dim sCkiLoc As String = "" 'WebServiceData.GetLocation(Session("CIZone"), Session("Brand"), IIf(CBool(Session("IsVan")), "AV", "AC"), Session("Country"))
        
        '"<ProductList>" & _
        '                                      "<Product SapId='7A3B7C2F-4F77-458F-BE77-5A9BFC272695' Qty='1' PrdShortName='FERRPAX' UsageOn='10/06/2009'>" & _
        '                                          "<PersonProduct name='Adult' Qty='3'/>" & _
        '                                          "<PersonProduct name='Child' Qty='2'/>" & _
        '                                          "<PersonProduct name='Infant' Qty='1'/>" & _
        '                                      "</Product>" & _
        '                                      "<Product SapId='94697627-AE78-459A-B756-B157D2D31DA7' Qty='1' PrdShortName='TABLE'/>" & _
        '                                  "</ProductList>" & _
        txtXMLDATA.Text = "<Data>" & _
                              "<RentalDetails>" & _
                                  "<IsRequestBooking>True</IsRequestBooking>" & _
                                  "<AddBookingNumber></AddBookingNumber>" & _
                                  "<AvpId>" & Request("AvpId") & "</AvpId>" & _
                                  "<AgentReference>AGENTREF1234</AgentReference>" & _
                                  "<QuantityRequested>1</QuantityRequested>" & _
                                  "<Status>WL</Status>" & _
                                  "<CheckOutDateTime>" & CDate(Session("CODate")).ToString("dd/MM/yyyy HH:mm") & "</CheckOutDateTime>" & _
                                  "<CheckOutLocationCode>" & sCkoLoc & "</CheckOutLocationCode>" & _
                                  "<CheckInDateTime>" & CDate(Session("CIDate")).ToString("dd/MM/yyyy HH:mm") & "</CheckInDateTime>" & _
                                  "<CheckInLocationCode>" & sCkiLoc & "</CheckInLocationCode>" & _
                                    txtXMLDATA.Text & _
                                  "<CountryCode>" & Session("Country") & "</CountryCode>" & _
                                  "<Note>This is a note to be added to the booking</Note>" & _
                                  "<UserCode>B2CNZ</UserCode>" & _
                                  "<ConfirmationHeaderText>Header Text Goes Here</ConfirmationHeaderText>" & _
                                  "<ConfirmationFooterText>Footer Text Goes Here</ConfirmationFooterText>" & _
                                  "<GetConfXML>True</GetConfXML>" & _
                              "</RentalDetails>" & _
                              "<CustomerData>" & _
                                  "<Title>Mr.</Title>" & _
                                  "<FirstName>Shoel</FirstName>" & _
                                  "<LastName>Palli</LastName>" & _
                                  "<Email>" & txtEmail.Text & "</Email>" & _
                                  "<PhoneNumber>+6493364237</PhoneNumber>" & _
                                  "<LoyaltyCardNumber>" & txtLoyaltyCardNumber.Text.Trim() & "</LoyaltyCardNumber>" & _
                             "</CustomerData>" & _
                              "<CreditCardDetails>" & _
                                  "<CardType>VISA</CardType>" & _
                                  "<CreditCardNumber>1111-1111-1111-1111</CreditCardNumber>" & _
                                  "<CreditCardName>V ISA</CreditCardName>" & _
                                  "<ApprovalRef>123</ApprovalRef>" & _
                                  "<ExpiryDate>12/12</ExpiryDate>" & _
                                  "<PaymentAmount>1000.00</PaymentAmount>" & _
                                  "<PaymentSurchargeAmount>2.00</PaymentSurchargeAmount>" & _
                              "</CreditCardDetails>" & _
                              "<DPSData>" & _
                                  "<DPSAuthCode>DPSAUTH</DPSAuthCode>" & _
                                  "<DPSTxnRef>DPSTXNREF</DPSTxnRef>" & _
                              "</DPSData>" & _
                          "</Data>"
    End Sub

End Class
