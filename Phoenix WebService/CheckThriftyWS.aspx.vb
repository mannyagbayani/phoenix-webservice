﻿
Partial Class CheckThriftyWS
    Inherits System.Web.UI.Page

    Protected Sub CheckThriftyWS_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim sb As New StringBuilder
        sb.Append("Thrifty WebService Status</br>")
        sb.Append("---------------------------------------------</br>")
        sb.Append("Alive: " & oService.IsThriftyAlive())
        sb.Append("</br>URL: " & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityURL") & "?soap_method=VehLocDetailsNotif&pickup=&account=&token=&vehicle=&promotion=&subaccount=")
        sb.Append("</br>Timeout: " & System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityTimeOutCheck") & " ms")
        Literal1.Text = sb.ToString

    End Sub
End Class
