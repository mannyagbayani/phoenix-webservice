﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PopUpDetails1.aspx.vb" Inherits="PopUpDetails1"
    Debug="true" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>

    <script language="javascript" type="text/javascript">
    
    String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };

    
    function OptionsValidate(sListNotDisplay)
    {
        var chkCurrent = event.srcElement;
        
        var sNotDisplayArray = sListNotDisplay.split(",");
        var sDisplayWhenArray;
        
        for(var i = 0; i < sNotDisplayArray.length; i++)
        {
            var tableToHide = document.getElementById("tbl"+sNotDisplayArray[i]);
            var chkToHide = document.getElementById("chk"+sNotDisplayArray[i]);
            
            if(tableToHide)
            {
                if (chkCurrent.checked)
                {
                    tableToHide.style.display = "none";
                    chkToHide.style.display = "none";
                }
                else
                {
                    tableToHide.style.display = "block";
                    chkToHide.style.display = "block";
                }
            }
        }
        
        if (chkCurrent.attributes["dependentProductList"])
        {
            sDisplayWhenArray = chkCurrent.attributes["dependentProductList"].value.split(",");
        
            for(var i = 0; i < sDisplayWhenArray.length; i++)
            {
                var tableToHide = document.getElementById("tbl"+sDisplayWhenArray[i]);
                var chkToHide = document.getElementById("chk"+sDisplayWhenArray[i]);
                
                if(tableToHide)
                {
                    if (chkCurrent.checked)
                    {
                        tableToHide.style.display = "block";
                        chkToHide.style.display = "block";
                    }
                    else
                    {
                        tableToHide.style.display = "none";
                        chkToHide.style.display = "none";
                    }
                }
            }        
        }
    }
    
    function HideShowDependentProducts(sList)
    {
        var chkCurrent = event.srcElement;
        
        var sDisplayArray = sList.split(",");
        for(var i = 0; i < sDisplayArray.length; i++)
        {
            var tableToHide = document.getElementById("tbl"+sDisplayArray[i]);
            var chkToHide = document.getElementById("chk"+sDisplayArray[i]);
            
            if(tableToHide)
            {
                if (chkCurrent.checked)
                {
                    tableToHide.style.display = "block";
                    chkToHide.style.display = "block";
                }
                else
                {
                    tableToHide.style.display = "none";
                    chkToHide.style.display = "none";
                }
            }
        }
        
    }
    
    
    function ShowFerryDetails()
    {
        var chkItem = event.srcElement;
        var tblChildItems = chkItem.parentElement.nextSibling.firstChild.firstChild.childNodes(1).firstChild.firstChild;
        if (tblChildItems)
        {
            if (chkItem.checked)
            {
                tblChildItems.style.display="block";
            }
            else
            {
                tblChildItems.style.display="none";
            }
        }
    }
    
    function ShowFerryDetails1()
    {
        var chkItem = event.srcElement;
        var tblChildItems = chkItem.parentElement.nextSibling.firstChild.firstChild.childNodes(1);
        if (tblChildItems)
        {
            if (chkItem.checked)
            {
                tblChildItems.style.display="block";
            }
            else
            {
                tblChildItems.style.display="none";
            }
        }
    }
    
    function ParsePageData(txtClientXMLId)
    {
        var txtClientXML = document.getElementById(txtClientXMLId);
        
        var tblMain = document.getElementById("tblMain");
        
        var sProductList;
        sProductList = "<AddProductList>" ;
        
        var bLookingForFerryProducts = false;
                
        for (var i = 0 ; i < tblMain.children[0].children.length ; i++)
        {
            var row = tblMain.children[0].children[i];
            if (
                    (row.innerText.trim() != "INSURANCE OPTIONS")
                      &&
                    (row.innerText.trim() != "EXTRA HIRE ITEMS")
                      &&
                    (row.innerText.trim() != "FERRY CROSSING")
                      &&
                    (row.innerText.trim() != "CREDIT CARD SURCHARGE")
                      &&
                    (row.innerText.trim() != "")
                      &&
                    (tblMain.children[0].children[i].children[0].children.length > 0)
                      &&
                    (tblMain.children[0].children[i].children[0].outerHTML.indexOf("Ferry")==-1)
                )
            {
                if(tblMain.children[0].children[i].children[0].children[0].type=="checkbox")
                {
                    if(tblMain.children[0].children[i].children[0].children[0].checked)
                    {
                        if(tblMain.children[0].children[i].children[0].children[0].attributes["SapId"])
                        sProductList += "<Product SapId='" 
                                    + tblMain.children[0].children[i].children[0].children[0].attributes["SapId"].value 
                                    + "' Qty='1' PrdShortName='" 
                                    + tblMain.children[0].children[i].children[0].children[0].attributes["PrdShortName"].value 
                                    + "'/>" ;        
                    }
                    
                    
                }
                else
                {
                    if(tblMain.children[0].children[i].children[0].children[0].value.trim() != "")
                    {
                        if(tblMain.children[0].children[i].children[0].children[0].attributes["SapId"])
                        sProductList += "<Product SapId='" 
                                    + tblMain.children[0].children[i].children[0].children[0].attributes["SapId"].value 
                                    + "' Qty='" 
                                    + tblMain.children[0].children[i].children[0].children[0].value
                                    + "' PrdShortName='" 
                                    + tblMain.children[0].children[i].children[0].children[0].attributes["PrdShortName"].value 
                                    + "'/>" ;  
                    }
                }
                
                
            }
            
            // Ferry Stuff
            if(row.innerText.trim() == "FERRY CROSSING")
            {
                bLookingForFerryProducts = true;
            }
            if(row.innerText.trim() == "CREDIT CARD SURCHARGE")
            {
                bLookingForFerryProducts = false;
            }
            
            if( (bLookingForFerryProducts) && (row.innerText.trim() != "FERRY CROSSING"))
            {
                if(tblMain.children[0].children[i].children[0].children[0].type=="checkbox")
                {
                    if(tblMain.children[0].children[i].children[0].children[0].checked)
                    {
                        if (tblMain.children[0].children[i].children[0].children[0].attributes["SapId"]) //FERRVEH
                        {
                            sProductList += "<Product SapId='" 
                                         + tblMain.children[0].children[i].children[0].children[0].attributes["SapId"].value 
                                         + "' Qty='1' UsageOn='" 
                                         + tblMain.children[0].children[i].children[0].children[0].parentElement.nextSibling.firstChild.firstChild.childNodes(1).firstChild.children[0].value
                                         + "' PrdShortName='" 
                                         + tblMain.children[0].children[i].children[0].children[0].attributes["PrdShortName"].value 
                                         + "'/>" ;   
                        }
                        else // FERRY PAX
                        {
                            sProductList += "<Product Qty='1' PrdShortName='" + tblMain.children[0].children[i].children[0].children[0].attributes["PrdShortName"].value + "' UsageOn='" 
                            // Crossing Date
                            //                                              table      tbody       row 1       td           table        tbody        tr           td        datetextbox
                            if(tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[0].children[0])
                            {
                                sProductList += tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[0].children[0].value + "' SapId='"
                            }

                            sProductList += tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[1].children[0].children[0].children[0].children[2].children[0].attributes["SapId"].value + "' >"                        
                            
                            for(var j=0;j<tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[1].children[0].children[0].children.length;j++)
                            {
                                //                                              table      tbody       row 1       td           table        tbody        tr           td         table     tbody         tr         td            input
                                if(tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[1].children[0].children[0].children[j].children[2].children[0])
                                {
                                    sProductList += "<PersonProduct name='" + tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[1].children[0].children[0].children[j].children[2].children[0].attributes["PaxType"].value + "' Qty='"
                                    sProductList += tblMain.children[0].children[i].children[1].children[0].children[0].children[1].children[0].children[0].children[0].children[0].children[1].children[0].children[0].children[j].children[2].children[0].value + "' />"
                                }
                            }
                            
                            sProductList += "</Product>"
                        }
                    }
                }
            }
            
        }
        
        sProductList += "</AddProductList>" ;
        
        txtClientXML.value = sProductList;
        
    }
    
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    Customer Email
                </td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" MaxLength="64"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:Literal ID="ltrExtraHireItems" runat="server"></asp:Literal>
        <hr />
        <asp:Button ID="btnBookNow" Text="Add Now" runat="server" />
        <hr />
        <asp:TextBox ID="txtXMLDATA" runat="server" TextMode="MultiLine" Width="100%" Height="500px"></asp:TextBox>
        <hr />
        <asp:Button ID="btnSubmit" Text="Submit" runat="server" />
        <asp:Panel ID="pnlMain" runat="server" CssClass="pnlMain">
            <asp:Literal ID="confirmationXmlLiteral" runat="server" />
        </asp:Panel>
    </div>
    <asp:HyperLink ID="lnkCheckOut" runat="server" Text="Check Out"></asp:HyperLink>
    </form>
</body>
</html>
