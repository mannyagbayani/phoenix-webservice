﻿Imports System.Xml
Imports PhoenixWebService

Partial Class PopUpDetails1
    Inherits System.Web.UI.Page

    Const CONFIRMATION_XSL_PATH As String = "xsl/confreport.xsl"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ltrExtraHireItems.Text = LoadEHIDetails(Request("RentalId"))
            btnBookNow.Attributes.Add("onclick", "ParsePageData('" & txtXMLDATA.ClientID & "');")

            lnkCheckOut.NavigateUrl = "SelfCheckOut1.aspx?CountryCode=" & Request("CountryCode") & "&RentalId=" & Request("RentalId")
        End If

    End Sub

    Function LoadEHIDetails(ByVal RentalId As String) As String
        Dim xmlEHIDetails As XmlDocument
        Dim oService As PhoenixWebService = New PhoenixWebService
        xmlEHIDetails = oService.GetInsuranceAndExtraHireItemsForRental(RentalId)

        Dim sbHTMLTable As StringBuilder = New StringBuilder

        Dim oLookup As Hashtable = New Hashtable

        With sbHTMLTable

            .Append("<TABLE id='tblMain'>")

            ' #######################  INSURANCE OPTIONS #########################

            .Append("    <TR id='dummyRow1'>")
            .Append("        <TD colspan='4'>")
            .Append("INSURANCE OPTIONS")
            .Append("        </TD>")
            .Append("    </TR>")

            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/Insurence/Product")
                .Append("   <TR>")
                .Append("       <TD>")

                Dim sbNotDisplay As StringBuilder = New StringBuilder
                Dim sbDisplayWhen As StringBuilder = New StringBuilder
                If oProductNode.SelectNodes("RequiredActions/Action") IsNot Nothing Then
                    For Each oActionNode As XmlNode In oProductNode.SelectNodes("RequiredActions/Action")
                        Select Case oActionNode.Attributes("code").Value.ToUpper()
                            Case "NOTDISPLAY"
                                sbNotDisplay.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                            Case "DISPLAYWHEN"
                                sbDisplayWhen.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                        End Select
                    Next
                End If

                If Not String.IsNullOrEmpty(sbDisplayWhen.ToString()) Then
                    For Each sDependency As String In sbDisplayWhen.ToString().Substring(0, sbDisplayWhen.ToString().Length - 1).Split(","c)
                        If oLookup.ContainsKey("chk" & sDependency) Then
                            oLookup("chk" & sDependency) = oLookup("chk" & sDependency) & "," & oProductNode.Attributes("Code").Value.ToUpper()
                        Else
                            oLookup.Add("chk" & sDependency, oProductNode.Attributes("Code").Value.ToUpper())
                        End If
                    Next
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "');""  style='display:none;'/>")
                Else
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "');""  />")
                End If


                .Append("       </TD>")
                .Append("       <TD>")

                If Not String.IsNullOrEmpty(sbDisplayWhen.ToString()) Then
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%;display:none;"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                Else
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                End If

                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='500px'>Name : ")
                .Append(oProductNode.Attributes("Name").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Daily Rate : ")
                .Append(oProductNode.Attributes("dailyRate").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Max Charge : ")
                .Append(oProductNode.Attributes("MaxCharge").Value)
                .Append("                   </TD>")
                .Append("               </TR>")

                If oProductNode.SelectSingleNode("Note") IsNot Nothing Then
                    .Append("           <TR>")
                    .Append("               <TD colspan='4'>Note : <BR/>")
                    .Append(oProductNode.SelectSingleNode("Note").Attributes("NteDesc").Value.Replace(vbNewLine, "<BR/>"))
                    .Append("               </TD>")
                    .Append("           </TR>")
                End If

                .Append("           </TABLE>")

                .Append("       </TD>")
                .Append("   </TR>")
            Next

            ' #######################  ExtraHireItems #########################

            .Append("    <TR id='dummyRow2'>")
            .Append("        <TD colspan='4'>")
            .Append("EXTRA HIRE ITEMS")
            .Append("        </TD>")
            .Append("    </TR>")
            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/ExtraHireItems/Product")
                .Append("   <TR>")
                .Append("       <TD>")

                Dim sbNotDisplay As StringBuilder = New StringBuilder
                Dim sbDisplayWhen As StringBuilder = New StringBuilder
                If oProductNode.SelectNodes("RequiredActions/Action") IsNot Nothing Then
                    For Each oActionNode As XmlNode In oProductNode.SelectNodes("RequiredActions/Action")
                        Select Case oActionNode.Attributes("code").Value.ToUpper()
                            Case "NOTDISPLAY"
                                sbNotDisplay.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                            Case "DISPLAYWHEN"
                                sbDisplayWhen.Append(oActionNode.Attributes("PrdCode").Value.ToUpper() & ",")
                        End Select
                    Next
                End If
                If oProductNode.Attributes("PromptQty").Value.ToUpper() = "YES" Then
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='txt" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='text' style=""width:15px;"" />")
                Else
                    .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""OptionsValidate('" & sbNotDisplay.ToString() & "','" & sbDisplayWhen.ToString() & "');"" />")
                End If

                .Append("       </TD>")
                .Append("       <TD>")
                .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='500px'>Name : ")
                .Append(oProductNode.Attributes("Name").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Daily Rate : ")
                .Append(oProductNode.Attributes("dailyRate").Value)
                .Append("                   </TD>")
                .Append("                   <TD width='150px'>Max Charge : ")
                .Append(oProductNode.Attributes("MaxCharge").Value)
                .Append("                   </TD>")

                .Append("               </TR>")
                .Append("           </TABLE>")
                .Append("       </TD>")
                .Append("   </TR>")

            Next

            ' #######################  Ferry Crossing #########################
            If xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/FerryCrossing/Product") IsNot Nothing Then

                .Append("    <TR id='dummyRow3'>")
                .Append("        <TD colspan='4'>")
                .Append("FERRY CROSSING")
                .Append("        </TD>")
                .Append("    </TR>")

                For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/FerryCrossing/Product")

                    Dim bHasChildRates As Boolean = False
                    ' check for child nodes
                    If oProductNode.SelectNodes("StartOn/PersonRate") IsNot Nothing AndAlso oProductNode.SelectNodes("StartOn/PersonRate").Count > 0 Then
                        bHasChildRates = True
                    End If

                    .Append("   <TR>")
                    .Append("       <TD>")
                    If bHasChildRates Then
                        .Append("           <INPUT  PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox' onclick=""ShowFerryDetails();"" />")
                    Else
                        .Append("           <INPUT SapId='" & oProductNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' id='chk" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='checkbox'  onclick=""ShowFerryDetails1();"" />")
                    End If

                    .Append("       </TD>")
                    .Append("       <TD>")
                    .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                    .Append("               <TR>")
                    .Append("                   <TD width='150px'>Code : ")
                    .Append(oProductNode.Attributes("Code").Value)
                    .Append("                   </TD>")
                    .Append("                   <TD width='500px'>Name : ")
                    .Append(oProductNode.Attributes("Name").Value)
                    .Append("                   </TD>")

                    If oProductNode.Attributes("dailyRate") IsNot Nothing Then
                        .Append("                   <TD width='150px'>Daily Rate : ")
                        .Append(oProductNode.Attributes("dailyRate").Value)
                        .Append("                   </TD>")
                    End If

                    If oProductNode.Attributes("MaxCharge") IsNot Nothing Then
                        .Append("                   <TD width='150px'>Max Charge : ")
                        .Append(oProductNode.Attributes("MaxCharge").Value)
                        .Append("                   </TD>")
                    End If

                    If bHasChildRates Then
                        .Append("               </TR>")
                        .Append("               <TR>")
                        .Append("                   <TD colspan='4'>")
                        ' Child products
                        For Each oStartOn As XmlNode In oProductNode.SelectNodes("StartOn")


                            .Append("           <TABLE style=""border:solid 3px Black;width:100%;display:none"">")
                            .Append("               <TR>")

                            .Append("                   <TD>Crossing Date : ")
                            .Append("                       <INPUT TYPE='text' style=""width:75px;"" value='" & oStartOn.Attributes("dateBegin").Value & "' />")
                            .Append("                   </TD>")

                            .Append("                   <TD>")
                            .Append("                       <TABLE>")
                            For Each oPersonRateNode As XmlNode In oStartOn.SelectNodes("PersonRate")
                                .Append("                           <TR>")

                                .Append("                               <TD>Passenger Type : ")
                                .Append(oPersonRateNode.Attributes("PaxType").Value)
                                .Append("                               </TD>")

                                .Append("                               <TD>Rate : ")
                                .Append(oPersonRateNode.Attributes("PrrRate").Value)
                                .Append("                               </TD>")

                                .Append("                               <TD>No. of Passengers : ")
                                .Append("                                   <INPUT PaxType='" & oPersonRateNode.Attributes("PaxType").Value & "' SapId='" & oPersonRateNode.Attributes("SapId").Value & "' PrdShortName='" & oProductNode.Attributes("Code").Value.ToUpper() & "' TYPE='text' style=""width:15px;"" />")
                                .Append("                           </TD>")

                                .Append("                       </TR>")
                            Next
                            .Append("                       </TABLE>")
                            .Append("                   </TD>")

                            .Append("               </TR>")
                            .Append("           </TABLE>")

                        Next
                        .Append("                   </TD>")
                        .Append("               </TR>")
                    Else
                        .Append("               </TR>")
                        .Append("               <TR style=""display:none;"">")
                        .Append("                   <TD colspan='4'>Crossing Date : ")
                        .Append("                       <INPUT TYPE='text' style=""width:75px;"" value='' />")
                        .Append("                   </TD>")

                        .Append("                   <TD>")
                        .Append("                   </TD>")

                        .Append("               </TR>")
                    End If


                    .Append("           </TABLE>")
                    .Append("       </TD>")
                    .Append("   </TR>")

                Next
            End If

            ' #######################  Credit Card Surcharge #########################
            .Append("    <TR id='dummyRow4'>")
            .Append("        <TD colspan='4'>")
            .Append("CREDIT CARD SURCHARGE")
            .Append("        </TD>")
            .Append("    </TR>")
            For Each oProductNode As XmlNode In xmlEHIDetails.DocumentElement.SelectNodes("ConfigurationProducts/Surcharge/Product")
                .Append("   <TR>")
                .Append("       <TD>")
                .Append("       </TD>")
                .Append("       <TD>")
                .Append("           <TABLE style=""border:solid 3px Black;width:100%"" id='tbl" & oProductNode.Attributes("Code").Value.ToUpper() & "'>")
                .Append("               <TR>")
                .Append("                   <TD width='150px'>Code : ")
                .Append(oProductNode.Attributes("Code").Value)
                .Append("                   </TD>")
                .Append("                   <TD colspan='3'>Surcharge Percentage : ")
                .Append(oProductNode.Attributes("SurchargePer").Value)
                .Append("                   </TD>")

                .Append("               </TR>")
                .Append("           </TABLE>")
                .Append("       </TD>")
                .Append("   </TR>")

            Next


            .Append("</TABLE>")

        End With

        Dim sHTML As String = sbHTMLTable.ToString()

        For Each sKey As Object In oLookup.Keys
            sHTML = sHTML.Replace("id='" & sKey & "'", " id='" & sKey & "' dependentProductList='" & oLookup(sKey) & "' ")
        Next


        Return sHTML
    End Function



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim oService As PhoenixWebService = New PhoenixWebService
        Dim xmlData As New XmlDocument
        xmlData.LoadXml(txtXMLDATA.Text)
        Dim result As XmlDocument
        'result = oService.AddUpdateCustomerData(xmlData)
        'result = oService.AddPayment(xmlData)
        result = oService.CreateBookingWithExtraHireItemAndPayment(xmlData)

        txtXMLDATA.Text = ""
        txtXMLDATA.Text = result.OuterXml

        'Dim transform As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform()
        'transform.Load(Server.MapPath(CONFIRMATION_XSL_PATH))

        'Dim confirmationXml As XmlDocument = New XmlDocument
        'If result.SelectSingleNode("Data/Confirmation/ConfXML") IsNot Nothing Then
        '    confirmationXml.LoadXml(result.SelectSingleNode("Data/Confirmation/ConfXML").InnerXml)
        '    Dim confirmationHtml As String
        '    confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText.Replace("..", Request.ApplicationPath)

        '    Using writer As System.IO.StringWriter = New System.IO.StringWriter
        '        transform.Transform(confirmationXml, Nothing, writer, Nothing)
        '        confirmationHtml = Server.HtmlDecode(writer.ToString())
        '    End Using

        '    confirmationXmlLiteral.Text = confirmationHtml
        'End If

    End Sub

    Protected Sub btnBookNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBookNow.Click
        Dim sCkoLoc As String = "" 'WebServiceData.GetLocation(Session("COZone"), Session("Brand"), IIf(CBool(Session("IsVan")), "AV", "AC"), Session("Country"))
        Dim sCkiLoc As String = "" 'WebServiceData.GetLocation(Session("CIZone"), Session("Brand"), IIf(CBool(Session("IsVan")), "AV", "AC"), Session("Country"))

        '<Data>
        '   <RentalDetails>
        '       <RentalId>W3414099-1</RentalId>
        '       <AddProductList>
        '           <Product SapId='03D96C9B-12AB-447D-97A8-69EE2AE846BB' Qty='1' PrdShortName='MPPACK'/>
        '           <Product SapId='94697627-AE78-459A-B756-B157D2D31DA7' Qty='1' PrdShortName='TABLE'/>
        '           <Product SapId='BF3698E3-5B7A-4690-A413-4C65CF134190' Qty='2' PrdShortName='CHAIR'/>
        '           <Product SapId='6FAE9635-C1B6-4D34-9F1F-065905D7EECC' Qty='1' PrdShortName='CHAINS'/>
        '           <Product SapId='BE5F7CB4-9CC4-4852-B3D9-AC1940EE9AC8' Qty='1' PrdShortName='HEATER'/>
        '           <Product Qty='1' PrdShortName='FERRPAX' UsageOn='01/04/2009' SapId='7A3B7C2F-4F77-458F-BE77-5A9BFC272695' >
        '               <PersonProduct name='Adult' Qty='1' />
        '               <PersonProduct name='Child' Qty='0' />
        '               <PersonProduct name='Infant' Qty='0' />
        '           </Product>
        '       </AddProductList>
        '       <RemoveProductList>
        '           <Product BpdId='ZZZZ' />
        '           <Product BpdId='ZZZZ' />
        '       </RemoveProductList>
        '       <UserCode>SP7</UserCode>
        '   </RentalDetails>
        '</Data>

        txtXMLDATA.Text = "<Data>" & _
                              "<RentalDetails>" & _
                                  "<RentalId>" & Request("RentalId") & "</RentalId>" & _
                                  txtXMLDATA.Text & _
                                  "<!--" & _
                                  "<RemoveProductList>" & _
                                  " <Product BpdId='123' />" & _
                                  "</RemoveProductList>" & _
                                  "-->" & _
                                  "<UserCode>B2C" & Request("CountryCode") & "</UserCode>" & _
                              "</RentalDetails>" & _
                          "</Data>"
    End Sub

End Class
