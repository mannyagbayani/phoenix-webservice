﻿Imports System.Xml

Partial Class SelfCheckout1
    Inherits System.Web.UI.Page

    Property CountryCode() As String
        Get
            Return CStr(ViewState("Country_Code"))
        End Get
        Set(ByVal value As String)
            ViewState("Country_Code") = value
        End Set
    End Property

    Property RentalId() As String
        Get
            Return CStr(ViewState("Rental_Id"))
        End Get
        Set(ByVal value As String)
            ViewState("Rental_Id") = value
        End Set
    End Property

    Public Function XmlEncode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, Chr(aryChars(i)), "&#" & aryChars(i) & ";")
        Next
        XmlEncode = strText
    End Function





    Protected Sub btnPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayment.Click
        Dim oWebService As New PhoenixWebService
        Dim xmlInput As New XmlDocument
        xmlInput.LoadXml(txaInputXML.Text)
        Dim xmlResult As New XmlDocument

        xmlResult = oWebService.AddPayment(xmlInput)
        'xmlResult = oWebService.AddUpdateCustomerData(xmlInput)
        'xmlResult = oWebService.ForceCreateBooking(xmlInput)

        ''xmlResult = oWebService.ForceCreateBooking(xmlInput)

        txaPaymentOutput.Text = xmlResult.OuterXml

        If xmlResult.DocumentElement.SelectSingleNode("RentalAgreementHTMLData") IsNot Nothing Then
            ltrRentalAgreement.Text = Server.HtmlDecode(xmlResult.DocumentElement.SelectSingleNode("RentalAgreementHTMLData").InnerText)
        Else
            ltrRentalAgreement.Text = xmlResult.OuterXml
        End If


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CountryCode = Request("CountryCode")
            RentalId = Request("RentalId")

            Dim sbXMLInput As New StringBuilder

            With sbXMLInput
                .Append("<Data>" & vbNewLine)

                .Append("   <CustomerInfo>" & vbNewLine)
                .Append("       <Customer>" & vbNewLine)
                .Append("           <CustomerDetails>" & vbNewLine)
                .Append("               <!-- START common for all customers -->" & vbNewLine)
                .Append("               <CusId>5284F42BE19D49E5B0681246B369696D</CusId>" & vbNewLine)

                .Append("               <FirstName>Philippe</FirstName>" & vbNewLine)
                .Append("               <LastName>Seiler</LastName>" & vbNewLine)
                .Append("               <Title>Mr.</Title>" & vbNewLine)

                .Append("               <LicenceNum>LIC 123</LicenceNum>" & vbNewLine)
                .Append("               <ExpiryDate>12/12/2012</ExpiryDate>" & vbNewLine)
                .Append("               <IssingAuthority>Licensor</IssingAuthority>" & vbNewLine)

                .Append("               <DOB>12/12/1952</DOB>" & vbNewLine)
                .Append("               <!-- END common for all customers -->" & vbNewLine)

                .Append("               <!-- START only for primary hirer-->" & vbNewLine)
                .Append("               <PhDayCtyCode>64</PhDayCtyCode>" & vbNewLine)
                .Append("               <PhDayAreaCode>9</PhDayAreaCode>" & vbNewLine)
                .Append("               <PhDayNumber>3364235</PhDayNumber>" & vbNewLine)

                .Append("               <MobileCtyCode>64</MobileCtyCode>" & vbNewLine)
                .Append("               <MobileAreaCode>21</MobileAreaCode>" & vbNewLine)
                .Append("               <MobileNumber>2426400</MobileNumber>" & vbNewLine)

                .Append("               <Email>test.me@thl.com</Email>" & vbNewLine)

                .Append("           </CustomerDetails>" & vbNewLine)
                .Append("           <PhysicalAddress>" & vbNewLine)
                .Append("               <Street>123 Somestreet</Street>" & vbNewLine)
                .Append("               <CityTown>SomeCityTown</CityTown>" & vbNewLine)
                .Append("               <Country>NZ</Country>" & vbNewLine)
                .Append("               <!-- END only for primary hirer-->" & vbNewLine)
                .Append("           </PhysicalAddress>" & vbNewLine)
                .Append("       </Customer>" & vbNewLine)
                .Append("   </CustomerInfo>" & vbNewLine)


                .Append("   <RentalDetails>" & vbNewLine)
                .Append("       <AddProductList>" & vbNewLine)
                .Append("           <Product SapId='F6E8123C-9666-411F-B102-464231E03ECE' Qty='1' PrdShortName='EXTRADB'/>" & vbNewLine)
                .Append("       </AddProductList>" & vbNewLine)
                .Append("       <RentalId>" & RentalId & "</RentalId>" & vbNewLine)
                .Append("       <CountryCode>" & CountryCode & "</CountryCode>" & vbNewLine)
                .Append("       <UserCode>B2C" & CountryCode & "</UserCode>" & vbNewLine)
                .Append("   </RentalDetails>" & vbNewLine)
                .Append("   <CreditCardDetails>" & vbNewLine)
                .Append("       <CardType>VISA</CardType>" & vbNewLine)
                .Append("       <CreditCardNumber>1111-1111-1111-1111</CreditCardNumber>" & vbNewLine)
                .Append("       <CreditCardName>V ISA</CreditCardName>" & vbNewLine)
                .Append("       <ApprovalRef>123.</ApprovalRef>" & vbNewLine)
                .Append("       <ExpiryDate>12/12</ExpiryDate>" & vbNewLine)
                .Append("       <PaymentAmount>1000.00</PaymentAmount>" & vbNewLine)
                .Append("       <PaymentSurchargeAmount>2.00</PaymentSurchargeAmount>" & vbNewLine)
                .Append("   </CreditCardDetails>" & vbNewLine)
                .Append("   <DPSData>" & vbNewLine)
                .Append("       <DPSAuthCode>DPSAUTH</DPSAuthCode>" & vbNewLine)
                .Append("       <DPSTxnRef>DPSTXNREF</DPSTxnRef>" & vbNewLine)
                .Append("   </DPSData>" & vbNewLine)
                .Append("	<AddUpdateCustomerData>True</AddUpdateCustomerData>" & vbNewLine)
                .Append("	<AddPayment>True</AddPayment>" & vbNewLine)
                .Append("   <DoCheckOut>True</DoCheckOut>" & vbNewLine)
                .Append("   <PrintRentalAgreement>True</PrintRentalAgreement>" & vbNewLine)
                .Append("</Data>")
            End With

            txaInputXML.Text = sbXMLInput.ToString()

        End If
    End Sub
End Class
