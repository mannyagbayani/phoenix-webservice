﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SelfCheckout1.aspx.vb" Inherits="SelfCheckout1"
    Debug="true" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <%--<div style="background-color: AliceBlue">
        <hr />
        Input : Get Booking Info
        <hr />
        <table>
            <tr>
                <td>
                    Booking Number
                </td>
                <td>
                    <asp:TextBox ID="txtBookingNumber" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Customer First Name
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Customer Last Name
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Check-out Location
                </td>
                <td>
                    <asp:TextBox ID="txtCheckoutLocation" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Check-out Date
                </td>
                <td>
                    <asp:TextBox ID="txtCheckoutDate" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button Text="Search" runat="server" ID="btnSearch" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <hr />
        Result : Get Booking Info
        <table>
            <tr>
                <td>
                    <asp:Literal runat="server" ID="ltrResultXML"></asp:Literal>
                </td>
            </tr>
        </table>
        <hr />
        <table>
            <tr>
                <td>
                    <asp:Literal runat="server" ID="ltrResultTable"></asp:Literal>
                </td>
            </tr>
        </table>
        <hr />
    </div>--%>
    <div style="background-color: Honeydew">
        <hr />
        Input : Add Payment with Check Out and Print Rental Agreement
        <hr />
        <table>
            <tr>
                <td>
                    <asp:TextBox ID="txaInputXML" runat="server" TextMode="MultiLine" Width="500px" Height="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnPayment" runat="server" Text="Pay Me!" />
                </td>
            </tr>
            <tr>
                <td>
                    Result : Add Payment with Check Out and Print Rental Agreement
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txaPaymentOutput" runat="server" TextMode="MultiLine" Width="500px"
                        Height="200px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Literal ID="ltrRentalAgreement" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
