﻿Imports Microsoft.VisualBasic

Namespace WS.AlternateAvailability

    Public Class Pattern

        Public Shared Function CachingPattern(ByVal bypass As Boolean, ByVal criterionKey As String) As Object

            Dim cachedResult As Object = System.Web.HttpContext.Current.Cache(criterionKey)
            If cachedResult Is Nothing Or bypass Then
                Select Case criterionKey
                    Case "IsMovedForward"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsMovedForward")
                    Case "IsMoveBackWard"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsMoveBackWard")
                    Case "IsPickUpBased"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsPickUpBased")
                    Case "IsDropOffBased"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsDropOffBased")
                    Case "IsSwitchLocation"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsSwitchLocation")
                    Case "MoveableDays"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("MoveableDays")
                    Case "ActivityTrace"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("ActivityTrace")
                    Case "IsOnewaySwitchLocation"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsOnewaySwitchLocation")
                    Case "IsOnewaySwitchLocationOnPickup"
                        cachedResult = System.Configuration.ConfigurationManager.AppSettings("IsOnewaySwitchLocationOnPickup")
                End Select
                System.Web.HttpContext.Current.Cache.Insert(criterionKey, cachedResult, Nothing, DateTime.Now.AddDays(1), TimeSpan.Zero)
            End If
            Return cachedResult
        End Function

        Public Shared Function InfoCachingPattern(ByVal criterionKey As String, ByVal objInput As Object) As Object
            Dim cachedResult As Object = System.Web.HttpContext.Current.Cache(criterionKey)
            If Not objInput Is Nothing Then
                If cachedResult Is Nothing Then
                    Select Case criterionKey
                        Case "VehicleErrorResponse"
                            cachedResult = objInput
                        Case "CriteriaCollationDocument"
                            cachedResult = objInput
                        Case Else
                            cachedResult = objInput
                    End Select
                    System.Web.HttpContext.Current.Cache.Insert(criterionKey, cachedResult, Nothing, DateTime.Now.AddDays(1), TimeSpan.Zero)
                Else
                    '' System.Web.HttpContext.Current.Cache.Remove(criterionKey)
                End If
            End If
            Return cachedResult
        End Function

        Public Shared Function IsCachedExist(ByVal criterion As String) As Boolean
            Return IIf(System.Web.HttpContext.Current.Cache(criterion) Is Nothing, False, True)
        End Function

        Public Shared Sub DeleteOldCachedAndReInsert(ByVal criterion As String, ByVal objInput As Object)
            If IsCachedExist(criterion) Then
                System.Web.HttpContext.Current.Cache.Remove(criterion)
                System.Web.HttpContext.Current.Cache.Insert(criterion, objInput, Nothing, DateTime.Now.AddDays(1), TimeSpan.Zero)
            End If
        End Sub

        Public Shared Sub CacheCount(ByVal criterion As String)
            If IsCachedExist(criterion) Then
                Dim objectCache As ArrayList = CType(System.Web.HttpContext.Current.Cache(criterion), ArrayList)
                '' System.Diagnostics.Debug.WriteLine(criterion & " has " & objectCache.Count - 1 & " items")
            End If
        End Sub

        Shared Sub ClearCachePerSession(ByVal criterion As String)
            If String.IsNullOrEmpty(criterion) Then Exit Sub
            If IsCachedExist(criterion) Then
                System.Web.HttpContext.Current.Cache.Remove(criterion)
            End If
        End Sub
    End Class


End Namespace