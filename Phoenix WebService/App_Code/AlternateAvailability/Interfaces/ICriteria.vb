﻿Imports Microsoft.VisualBasic
Namespace WS.AlternateAvailability

    Interface ICriteria
        ReadOnly Property IsMovedForward As Boolean
        ReadOnly Property IsMoveBackWard As Boolean
        ReadOnly Property IsSwitchLocation As Boolean
        ReadOnly Property IsPickUpBased As Boolean
        ReadOnly Property IsDropOffBased As Boolean
        ReadOnly Property MoveableDays As Integer
        ReadOnly Property ActivityTrace As Boolean
        ReadOnly Property IsOnewaySwitchLocation As Boolean
        ReadOnly Property IsOnewaySwitchLocationOnPickup As Boolean

    End Interface


End Namespace