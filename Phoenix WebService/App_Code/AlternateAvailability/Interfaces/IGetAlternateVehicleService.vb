﻿Imports Microsoft.VisualBasic
Imports WS.AlternateAvailability.Model
Namespace WS.AlternateAvailability

    Public Interface IGetAlternateVehicleService
        Property getAvailabilityErrors As Hashtable
        Property TimeStampRequest As String

        Function IsAlternateVehicleStatusAvailable(ByVal vehicleInfo As VehicleInformation, Optional ByRef warningmessage As String = "", Optional ByVal displayMode As String = "") As Boolean
        Function AlternateAvailabilityRS(ByVal vehicleInfo As VehicleInformation, ByVal passenger As PassengerInformation) As System.Xml.XmlDocument
        Function GetAlternateVehicleAvailability(ByVal vehicleInfo As VehicleInformation, _
                                                 ByVal passenger As PassengerInformation, _
                                                 ByVal DisplayMode As String, _
                                                 Optional ByRef warningmessage As String = "") As System.Xml.XmlDocument
        Function ErrorMessage(ByVal message As String) As String
        Function ErrorXMLMessage(ByVal vehicleCode As String, _
                                 ByVal messages As ArrayList, _
                                 ByVal messagestraces As ArrayList) As String
        Function messagesTraceActivity(ByVal messages As ArrayList) As String
        Sub LogInformation(ByVal functionname As String, ByVal TextToLog As String)
        Sub AddAlternateVehicleResponses(ByVal ParamArray Messages As String())
        Function GetAlternateVehicleResponses(ByVal ParamArray BookingInfo As String()) As String
        Function ExecuteSearchCriteria(ByVal vehicleInfo As VehicleInformation, _
                                       ByVal passenger As PassengerInformation, _
                                       ByVal criteria As Integer, _
                                       ByRef AlternateVehicle As System.Xml.XmlDocument, _
                                       Optional ByRef warningmessage As String = "", _
                                       Optional ByRef messageErrors As ArrayList = Nothing, _
                                       Optional ByRef messagesActivity As ArrayList = Nothing) As Boolean
        Sub AddSearchCriteriaResult(ByVal alternateVehicle As System.Xml.XmlDocument, ByVal displaymode As String)
        Function GetSearchCriteriaResult() As String

    End Interface


End Namespace