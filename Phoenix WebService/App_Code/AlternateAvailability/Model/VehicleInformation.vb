﻿Imports Microsoft.VisualBasic
Namespace WS.AlternateAvailability.Model
    Public Class VehicleInformation


        Private _CountryCode As String
        Private _Brand As String
        Private _CheckOutZoneCode As String
        Private _CheckOutDateTime As DateTime
        Private _CheckInZoneCode As String
        Private _CheckInDateTime As DateTime
        Private _AgentCode As String
        Private _PackageCode As String
        Private _IsVan As Boolean
        Private _VehicleCode As String
        Private _isTestMode As Boolean
        Private _isGross As Boolean
        Private _isAuroraRequest As Boolean


#Region "Properties"

        Property IsAuroraRequest As String
            Get
                Return _isAuroraRequest
            End Get
            Set(ByVal value As String)
                _isAuroraRequest = value
            End Set
        End Property


        Property VehicleCode As String
            Get
                Return _VehicleCode
            End Get
            Set(ByVal value As String)
                _VehicleCode = value
            End Set
        End Property

        Property Brand As String
            Get
                Return _Brand
            End Get
            Set(ByVal value As String)
                _Brand = value
            End Set
        End Property

        Property CheckOutZoneCode As String
            Get
                Return _CheckOutZoneCode
            End Get
            Set(ByVal value As String)
                _CheckOutZoneCode = value
            End Set
        End Property

        Property CheckOutDateTime As DateTime
            Get
                Return _CheckOutDateTime
            End Get
            Set(ByVal value As DateTime)
                _CheckOutDateTime = value
            End Set
        End Property

        Property CheckInDateTime As DateTime
            Get
                Return _CheckInDateTime
            End Get
            Set(ByVal value As DateTime)
                _CheckInDateTime = value
            End Set
        End Property

        Property CheckInZoneCode As String
            Get
                Return _CheckInZoneCode
            End Get
            Set(ByVal value As String)
                _CheckInZoneCode = value
            End Set
        End Property

        Property AgentCode As String
            Get
                Return _AgentCode
            End Get
            Set(ByVal value As String)
                _AgentCode = value
            End Set
        End Property

        Property PackageCode As String
            Get
                Return _PackageCode
            End Get
            Set(ByVal value As String)
                _PackageCode = value
            End Set
        End Property

        Property CountryCode As String
            Get
                Return _CountryCode
            End Get
            Set(ByVal value As String)
                _CountryCode = value
            End Set
        End Property

        Property IsVan As Boolean
            Get
                Return _IsVan
            End Get
            Set(ByVal value As Boolean)
                _IsVan = value
            End Set
        End Property

        Property IsTestMode As Boolean
            Get
                Return _isTestMode
            End Get
            Set(ByVal value As Boolean)
                _isTestMode = value
            End Set
        End Property

        Property IsGross As Boolean
            Get
                Return _isGross
            End Get
            Set(ByVal value As Boolean)
                _isGross = value
            End Set
        End Property



#End Region


        Sub New(ByVal argVehicleCode As String, _
                ByVal argCountryCode As String, _
                ByVal argBrand As String, _
                ByVal argCheckOutZoneCode As String, _
                ByVal argCheckOutDateTime As DateTime, _
                ByVal argCheckInZoneCode As String, _
                ByVal argCheckInDateTime As DateTime, _
                ByVal argAgentCode As String, _
                ByVal argPackageCode As String, _
                ByVal argIsVan As Boolean, _
                ByVal argIsTestMode As Boolean, _
                ByVal argIsGross As Boolean)

            _Brand = Brand
            _CountryCode = argCountryCode
            _CheckOutZoneCode = argCheckOutZoneCode
            _CheckOutDateTime = argCheckOutDateTime
            _CheckInZoneCode = argCheckInZoneCode
            _CheckInDateTime = argCheckInDateTime
            _AgentCode = argAgentCode
            _PackageCode = argPackageCode
            _IsVan = argIsVan
            _VehicleCode = argVehicleCode
            _isTestMode = argIsTestMode
            _isGross = argIsGross
        End Sub

        Sub New()

        End Sub
    End Class
End Namespace