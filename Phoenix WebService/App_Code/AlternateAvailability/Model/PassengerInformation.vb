﻿Imports Microsoft.VisualBasic
Namespace WS.AlternateAvailability.Model

    Public Class PassengerInformation
        Private _NoOfAdults As Integer
        Private _NoOfChildren As Integer
        Private _CountryOfResidence As String
        Private _isBestBuy As Boolean

        Public Property IsBestBuy As Boolean
            Get
                Return _isBestBuy
            End Get
            Set(ByVal value As Boolean)
                _isBestBuy = value
            End Set
        End Property

        Public Property NoOfAdults As Integer
            Get
                Return _NoOfAdults
            End Get
            Set(ByVal value As Integer)
                _NoOfAdults = value
            End Set
        End Property

        Public Property NoOfChildren As Integer
            Get
                Return _NoOfChildren
            End Get
            Set(ByVal value As Integer)
                _NoOfChildren = value
            End Set
        End Property

        Public Property CountryOfResidence As String
            Get
                Return _CountryOfResidence
            End Get
            Set(ByVal value As String)
                _CountryOfResidence = value
            End Set
        End Property

        Sub New(ByVal argNoOfAdults As Integer, _
                ByVal argNoOfChildren As Integer, _
                ByVal argCountryOfResidence As String, _
                ByVal argIsBestBuy As Boolean)

            _NoOfAdults = argNoOfAdults
            _NoOfChildren = argNoOfChildren
            _CountryOfResidence = argCountryOfResidence
            _isBestBuy = argIsBestBuy

        End Sub

        Sub New()

        End Sub
    End Class
End Namespace