﻿
Imports Microsoft.VisualBasic
Imports WS.AlternateAvailability
Imports System.Data
Imports WebServiceData
Imports System.Xml


Public Class GetAlternateVehicleService
    Implements ICriteria
    Implements IGetAlternateVehicleService



#Region "Enum"
    Public Enum AlternateAvailability
        MoveForward = 1    ''•	+ number of day search      – ex: looks out 1-10 days from initial pick up date to find avail 
        MoveBackWard = 2   ''•	- number day search         – ex: looks back 1-10 days from initial pick up to find avail
        SwitchLocation = 3 ''•	  Reverse direction         – ex: swap the pick up and drop off locations and do a search for same dates 
        PickUpBased = 4    ''•	  Must pick up on that date - ex: if hire request is initally 10 days, 
        DropOffBased = 5   ''•	  Must drop off on that date –ex: move pick up day 1 day forward and query for 1 days less than original request
        NormalBased = 0

        ''rev:mia March 3 2011
        '' •	If original request is for one way hire then display
        '' o	Reverse direction
        '' o	One way hire based on pick up location
        OnewaySwitchLocation = 6
        OnewaySwitchLocationOnPickup = 7


    End Enum
#End Region

#Region "Constant"
    Const CRITERIA_VEHICLE_REQUEST As String = "VehicleErrorResponse_"
    Const CRITERIA_COLLATION_REQUEST As String = "CriteriaCollationDocument_"
#End Region

#Region "ICriteria Interface"

    Public ReadOnly Property ActivityTrace As Boolean Implements WS.AlternateAvailability.ICriteria.ActivityTrace
        Get
            Return Pattern.CachingPattern(False, "ActivityTrace")
        End Get
    End Property

    Public Property TimeStampRequest As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.TimeStampRequest
        Get
            Return System.Web.HttpContext.Current.Session("TimeStampRequest")
        End Get
        Set(ByVal value As String)
            System.Web.HttpContext.Current.Session("TimeStampRequest") = value
        End Set
    End Property

    Public ReadOnly Property IsDropOffBased As Boolean Implements ICriteria.IsDropOffBased
        Get
            Return Pattern.CachingPattern(False, "IsDropOffBased")
        End Get
    End Property

    Public ReadOnly Property IsMoveBackWard As Boolean Implements ICriteria.IsMoveBackWard
        Get
            Return Pattern.CachingPattern(False, "IsMoveBackWard")
        End Get
    End Property

    Public ReadOnly Property IsMovedForward As Boolean Implements ICriteria.IsMovedForward
        Get
            Return Pattern.CachingPattern(False, "IsMovedForward")
        End Get
    End Property

    Public ReadOnly Property IsPickUpBased As Boolean Implements ICriteria.IsPickUpBased
        Get
            Return Pattern.CachingPattern(False, "IsPickUpBased")
        End Get
    End Property

    Public ReadOnly Property IsSwitchLocation As Boolean Implements ICriteria.IsSwitchLocation
        Get
            Return Pattern.CachingPattern(False, "IsPickUpBased")
        End Get
    End Property

    Public ReadOnly Property MoveableDays As Integer Implements ICriteria.MoveableDays
        Get
            Return Pattern.CachingPattern(False, "MoveableDays")
        End Get
    End Property

    Public ReadOnly Property IsOnewaySwitchLocation As Boolean Implements WS.AlternateAvailability.ICriteria.IsOnewaySwitchLocation
        Get
            Return Pattern.CachingPattern(False, "IsOnewaySwitchLocation")
        End Get
    End Property

    Public ReadOnly Property IsOnewaySwitchLocationOnPickup As Boolean Implements WS.AlternateAvailability.ICriteria.IsOnewaySwitchLocationOnPickup
        Get
            Return Pattern.CachingPattern(False, "IsOnewaySwitchLocationOnPickup")
        End Get
    End Property
#End Region


    ''' <summary>
    ''' Unused property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
#Region "    IGetAlternateVehicleService Property"
    Public Property getAvailabilityErrors As System.Collections.Hashtable Implements WS.AlternateAvailability.IGetAlternateVehicleService.getAvailabilityErrors
        Get

        End Get
        Set(ByVal value As System.Collections.Hashtable)

        End Set
    End Property
#End Region

#Region "IGetAlternateVehicleService Functions"

    Public Function AlternateAvailabilityRS(ByVal vehicleInfo As WS.AlternateAvailability.Model.VehicleInformation, _
                                            ByVal passenger As WS.AlternateAvailability.Model.PassengerInformation) As System.Xml.XmlDocument Implements WS.AlternateAvailability.IGetAlternateVehicleService.AlternateAvailabilityRS

        Dim Brand As String = vehicleInfo.Brand
        Dim CountryCode As String = vehicleInfo.CountryCode
        Dim VehicleCode As String = vehicleInfo.VehicleCode
        Dim CheckOutZoneCode As String = vehicleInfo.CheckOutZoneCode
        Dim CheckOutDateTime As DateTime = vehicleInfo.CheckOutDateTime
        Dim CheckInZoneCode As String = vehicleInfo.CheckInZoneCode
        Dim CheckInDateTime As DateTime = vehicleInfo.CheckInDateTime
        Dim AgentCode As String = vehicleInfo.AgentCode
        Dim PackageCode As String = vehicleInfo.PackageCode
        Dim IsVan As Boolean = vehicleInfo.IsVan
        Dim IsTestMode As Boolean = vehicleInfo.IsTestMode
        Dim IsAuroraRequest As Boolean = vehicleInfo.IsAuroraRequest

        Dim NoOfAdults As Int16 = passenger.NoOfAdults
        Dim NoOfChildren As Int16 = passenger.NoOfChildren
        Dim IsBestBuy As Boolean = passenger.IsBestBuy
        Dim CountryOfResidence As String = passenger.CountryOfResidence
        Dim IsGross As Boolean = vehicleInfo.IsGross

        LogInformation("AlternateAvailabilityRS", MessageInformationFormat("AlternateAvailabilityRS GetAvailability", "Started", "Calling..."))
        Dim availabilityXML As New XmlDocument

        Try
            If (IsAuroraRequest = True) Then
                Brand = Brand & "|FromAurora"
            End If
            Dim service As New PhoenixWebService
            availabilityXML = service.GetAvailability(Brand, _
                                      CountryCode, _
                                      VehicleCode, _
                                      CheckOutZoneCode, _
                                      CheckOutDateTime, _
                                      CheckInZoneCode, _
                                      CheckInDateTime, _
                                      NoOfAdults, _
                                      NoOfChildren, _
                                      AgentCode, _
                                      PackageCode, _
                                      IsVan, _
                                      IsBestBuy, _
                                      CountryOfResidence, _
                                      IsTestMode, _
                                      IsGross, _
                                      True, _
                                      "") ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct

            LogInformation("AlternateAvailabilityRS", _
                           MessageInformationFormat("AlternateAvailabilityRS GetAvailability", "RESPONSE", availabilityXML.OuterXml))
        Catch ex As Exception
            LogInformation("AlternateAvailabilityRS", MessageInformationFormat("AlternateAvailabilityRS GetAvailability ", "Exception", ex.Message + ", " + ex.Source))
        End Try
        Return availabilityXML
    End Function


    Private _timestampRequest As String


    Public Function GetAlternateVehicleAvailability(ByVal vehicleInfo As WS.AlternateAvailability.Model.VehicleInformation, _
                                                    ByVal passenger As WS.AlternateAvailability.Model.PassengerInformation, _
                                                    ByVal DisplayMode As String, _
                                                    Optional ByRef warningmessage As String = "") As System.Xml.XmlDocument Implements WS.AlternateAvailability.IGetAlternateVehicleService.GetAlternateVehicleAvailability


        Dim Brand As String = vehicleInfo.Brand
        Dim CountryCode As String = vehicleInfo.CountryCode
        Dim VehicleCode As String = vehicleInfo.VehicleCode
        Dim CheckOutZoneCode As String = vehicleInfo.CheckOutZoneCode
        Dim CheckOutDateTime As DateTime = vehicleInfo.CheckOutDateTime
        Dim CheckInZoneCode As String = vehicleInfo.CheckInZoneCode
        Dim CheckInDateTime As DateTime = vehicleInfo.CheckInDateTime
        Dim AgentCode As String = vehicleInfo.AgentCode
        Dim PackageCode As String = vehicleInfo.PackageCode
        Dim IsVan As Boolean = vehicleInfo.IsVan
        Dim Istestmode As Boolean = vehicleInfo.IsTestMode
        Dim IsAuroraRequest As Boolean = vehicleInfo.IsAuroraRequest

        Dim NoOfAdults As Int16 = passenger.NoOfAdults
        Dim NoOfChildren As Int16 = passenger.NoOfChildren
        Dim IsBestBuy As Boolean = passenger.IsBestBuy
        Dim CountryOfResidence As String = passenger.CountryOfResidence
        Dim messageErrors As New ArrayList
        Dim messagesActivity As New ArrayList

        Dim AlternateVehicleStatusAvailable As Nullable(Of Boolean)
        Dim alternateVehicleDoc As New XmlDocument

       

        Dim vehicleInfoClone As WS.AlternateAvailability.Model.VehicleInformation = vehicleInfo
        Try
            Pattern.ClearCachePerSession(TimeStampRequest)
            TimeStampRequest = ""
            TimeStampRequest = Guid.NewGuid.ToString
        Catch ex As Exception
            '' TimeStampRequest = Guid.NewGuid.ToString
        End Try

        LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability RQ", _
                                                                                       "VehicleCode", IIf(String.IsNullOrEmpty(VehicleCode), "NIL", VehicleCode), _
                                                                                       "CountryCode", IIf(String.IsNullOrEmpty(CountryCode), "NIL", CountryCode), _
                                                                                       "Brand", IIf(String.IsNullOrEmpty(Brand), "NIL", Brand), _
                                                                                       "CheckOutZoneCode", IIf(String.IsNullOrEmpty(CheckOutZoneCode), "NIL", CheckOutZoneCode), _
                                                                                       "CheckOutDateTime", IIf(String.IsNullOrEmpty(CheckOutDateTime), "NIL", CheckOutDateTime), _
                                                                                       "CheckInZoneCode", IIf(String.IsNullOrEmpty(CheckInZoneCode), "NIL", CheckInZoneCode), _
                                                                                       "CheckInDateTime", IIf(String.IsNullOrEmpty(CheckInDateTime), "NIL", CheckInDateTime), _
                                                                                       "AgentCode", IIf(String.IsNullOrEmpty(AgentCode), "NIL", AgentCode), _
                                                                                       "PackageCode", IIf(String.IsNullOrEmpty(PackageCode), "NIL", PackageCode), _
                                                                                       "IsVan", IIf(String.IsNullOrEmpty(IsVan), "NIL", IsVan), _
                                                                                       "DisplayMode", DisplayMode, _
                                                                                       "IsAuroraRequest", IsAuroraRequest))


      
        Try

            ''step #1 - get the vehicle status
            AlternateVehicleStatusAvailable = False ''IsAlternateVehicleStatusAvailable(vehicleInfo, warningmessage)

            If (Not String.IsNullOrEmpty(warningmessage)) Then
                '' System.Diagnostics.Debug.WriteLine("WARNING: " & warningmessage)
                messageErrors.Add(ErrorMessage(warningmessage))
                messagesActivity.Add("Initial Call|" & CheckOutDateTime.ToShortDateString & "-" & CheckOutZoneCode & "|" & CheckInDateTime.ToShortDateString & "-" & CheckInZoneCode & "|" & warningmessage)
            End If
            ''check if warning message
            ''AlternateVehicleStatusAvailable = False
            ''step #2 - check the vehicle status, it its available then call the GETAVAILABILITY function
            If (AlternateVehicleStatusAvailable) Then
                alternateVehicleDoc = AlternateAvailabilityRS(vehicleInfo, passenger)
                AddSearchCriteriaResult(alternateVehicleDoc, "Available (No Pattern invoke)")
            Else

                If Not String.IsNullOrEmpty(MoveableDays) Then
                    Dim daysToSearch As Integer = CInt(MoveableDays)
                    Dim ctr As Integer = 1

                    Dim MoveForwardCriteria As Boolean = False
                    Dim MoveBackWardCriteria As Boolean = False
                    Dim SwitchLocationCriteria As Boolean = False
                    Dim PickUpBasedCriteria As Boolean = False
                    Dim DropOffBasedCriteria As Boolean = False
                    Dim NormalBasedCriteria As Boolean = False
                    Dim OnewaySwitchLocationCriteria As Boolean = False

                    Dim displayModeArray() As String = DisplayMode.Split(",")
                    For Each mode As String In displayModeArray
                        '' For Each indx As Integer In DisplayMode
                        Select Case mode ''indx
                            Case AlternateAvailability.MoveForward And IsMovedForward
                                MoveForwardCriteria = True
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "Forward", " IsAllowed", IsMovedForward))

                            Case AlternateAvailability.MoveBackWard And IsMoveBackWard
                                MoveBackWardCriteria = True
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "Backward", " IsAllowed", IsMoveBackWard))

                            Case AlternateAvailability.SwitchLocation And IsSwitchLocation
                                SwitchLocationCriteria = True
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "SwitchLocation", " IsAllowed", IsSwitchLocation))

                            Case AlternateAvailability.PickUpBased And IsPickUpBased
                                PickUpBasedCriteria = True
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "PickUpBase", " IsAllowed", IsPickUpBased))

                            Case AlternateAvailability.DropOffBased And IsDropOffBased
                                DropOffBasedCriteria = True
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "DropOffBased", " IsAllowed", IsDropOffBased))

                            Case AlternateAvailability.NormalBased
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "normal"))
                                NormalBasedCriteria = True

                                ''rev:mia March 3 2011
                            Case AlternateAvailability.OnewaySwitchLocation And IsOnewaySwitchLocation
                                LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability ", "DisplayMode", "OnewaySwitchLocation and OnewaySwitchLocationOnPickup"))
                                OnewaySwitchLocationCriteria = True
                        End Select
                        ''Next
                    Next


                    Dim booleanResultCriteria As Boolean

                    ''rev:mia March 3 2011
                    ''Based on this feedback we would like to include this additional request included 
                    ''in the web service call for alternative availability for reverse direction & one way hire under the following circumstances
                    '•	If original request is for one way hire then display
                    'o	Reverse direction
                    'o	One way hire based on pick up location
                    '•	If original request is for a return hire eg AKL-AKL don’t do this alternative availability request.

                    ''rev:mia March 14 2011 - output sequence should vary in the input of displaymode
                    For Each mode As String In displayModeArray
                        ''For Each indx As Integer In DisplayMode

                        alternateVehicleDoc = New XmlDocument
                        warningmessage = String.Empty

                        vehicleInfo.CheckOutDateTime = CheckOutDateTime
                        vehicleInfo.CheckInDateTime = CheckInDateTime
                        vehicleInfo.CheckOutZoneCode = CheckOutZoneCode
                        vehicleInfo.CheckInZoneCode = CheckInZoneCode

                        Select Case mode ''indx

                            Case AlternateAvailability.MoveForward And MoveForwardCriteria
                                If MoveForwardCriteria Then

                                    booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.MoveForward, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    AddSearchCriteriaResult(alternateVehicleDoc, "MoveForward")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If

                            Case AlternateAvailability.MoveBackWard And MoveBackWardCriteria
                                If MoveBackWardCriteria Then

                                    booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.MoveBackWard, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    AddSearchCriteriaResult(alternateVehicleDoc, "MoveBackWard")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If


                            Case AlternateAvailability.SwitchLocation And SwitchLocationCriteria
                                If SwitchLocationCriteria Then

                                    booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.SwitchLocation, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    AddSearchCriteriaResult(alternateVehicleDoc, "SwitchLocation")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If


                            Case AlternateAvailability.PickUpBased And PickUpBasedCriteria
                                If PickUpBasedCriteria Then

                                    booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.PickUpBased, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    AddSearchCriteriaResult(alternateVehicleDoc, "PickUpBased")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If



                            Case AlternateAvailability.DropOffBased And DropOffBasedCriteria
                                If DropOffBasedCriteria Then


                                    booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.DropOffBased, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    AddSearchCriteriaResult(alternateVehicleDoc, "DropOffBased")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If

                            Case AlternateAvailability.NormalBased And NormalBasedCriteria
                                If NormalBasedCriteria Then

                                    ''no longer needed
                                    ''booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.NormalBased, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                    ''AddSearchCriteriaResult(alternateVehicleDoc, "NormalBased")

                                    messagesActivity = New ArrayList
                                    messageErrors = New ArrayList
                                End If

                            Case AlternateAvailability.OnewaySwitchLocation And OnewaySwitchLocationCriteria

                                If OnewaySwitchLocationCriteria Then


                                    If IsOneWay(CheckOutZoneCode, CheckInZoneCode) Then

                                        ''if its one way display reverse direction
                                        ''---no longer needed---
                                        '' booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.OnewaySwitchLocation, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                        ''AddSearchCriteriaResult(alternateVehicleDoc, "OnewaySwitchLocation")

                                        vehicleInfo.CheckOutDateTime = CheckOutDateTime
                                        vehicleInfo.CheckInDateTime = CheckInDateTime
                                        vehicleInfo.CheckOutZoneCode = CheckOutZoneCode
                                        vehicleInfo.CheckInZoneCode = CheckInZoneCode

                                        messagesActivity = New ArrayList
                                        messageErrors = New ArrayList

                                        alternateVehicleDoc = New XmlDocument
                                        warningmessage = String.Empty


                                        ''and o	One way hire based on pick up location
                                        booleanResultCriteria = ExecuteSearchCriteria(vehicleInfo, passenger, AlternateAvailability.OnewaySwitchLocationOnPickup, alternateVehicleDoc, warningmessage, messageErrors, messagesActivity)
                                        AddSearchCriteriaResult(alternateVehicleDoc, "OnewaySwitchLocationOnPickup")

                                        messagesActivity = New ArrayList
                                        messageErrors = New ArrayList
                                    Else
                                        messageErrors.Add(ErrorMessage("Not a valid oneway rental"))
                                        messagesActivity.Add("1" & "|" & vehicleInfo.CheckOutDateTime.ToShortDateString & "-" & vehicleInfo.CheckOutZoneCode.ToUpper & "|" & vehicleInfo.CheckInDateTime.ToShortDateString & "-" & vehicleInfo.CheckInZoneCode.ToUpper & "| Not a valid oneway rental")
                                        alternateVehicleDoc.LoadXml(ErrorXMLMessage(vehicleInfo.VehicleCode, messageErrors, messagesActivity))
                                        AddSearchCriteriaResult(alternateVehicleDoc, "OnewaySwitchLocation")

                                        messagesActivity = New ArrayList
                                        messageErrors = New ArrayList
                                    End If
                                End If

                        End Select

                    Next

                    alternateVehicleDoc.LoadXml(GetSearchCriteriaResult())

                End If

            End If ''If (AlternateVehicleStatusAvailable) Then


        Catch ex As Exception
            LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability EXCEPTION", "message", ex.Message, "stack", ex.StackTrace))
        End Try
        ''System.Diagnostics.Debug.WriteLine(alternateVehicleDoc.OuterXml)
        LogInformation("GetAlternateVehicleAvailability", MessageInformationFormat("GetAlternateVehicleAvailability RS", "XML", alternateVehicleDoc.OuterXml))
        Return alternateVehicleDoc
    End Function

    Public Function IsAlternateVehicleStatusAvailable(ByVal vehicleInfo As WS.AlternateAvailability.Model.VehicleInformation, Optional ByRef warningmessage As String = "", Optional ByVal displayMode As String = "") As Boolean Implements WS.AlternateAvailability.IGetAlternateVehicleService.IsAlternateVehicleStatusAvailable
        LogInformation("IsAlternateVehicleStatusAvailable", MessageInformationFormat("IsAlternateVehicleStatusAvailable GetVehicleStatus", "Started", "Calling..."))
        Dim vehicleStatus As String

        Try
            Dim Brand As String = vehicleInfo.Brand
            Dim CountryCode As String = vehicleInfo.CountryCode
            Dim VehicleCode As String = vehicleInfo.VehicleCode
            Dim CheckOutZoneCode As String = vehicleInfo.CheckOutZoneCode
            Dim CheckOutDateTime As DateTime = vehicleInfo.CheckOutDateTime
            Dim CheckInZoneCode As String = vehicleInfo.CheckInZoneCode
            Dim CheckInDateTime As DateTime = vehicleInfo.CheckInDateTime
            Dim AgentCode As String = vehicleInfo.AgentCode
            Dim PackageCode As String = vehicleInfo.PackageCode
            Dim IsVan As Boolean = vehicleInfo.IsVan
            Dim IsTestMode As Boolean = vehicleInfo.IsTestMode

            Dim service As New PhoenixWebService

            ''rev:mia april 13 2011 - addtion of IsAlternateServiceCall
            IsAlternateServiceCall = "YES"

            vehicleStatus = service.GetVehicleStatus(VehicleCode, _
                                                CountryCode, _
                                                Brand, _
                                                CheckOutZoneCode, _
                                                CheckOutDateTime, _
                                                CheckInZoneCode, _
                                                CheckInDateTime, _
                                                AgentCode, _
                                                PackageCode, _
                                                IsVan)

            LogInformation("IsAlternateVehicleStatusAvailable", MessageInformationFormat("IsAlternateVehicleStatusAvailable GetVehicleStatus", "result", vehicleStatus))
            Dim message As String = GetAlternateVehicleResponses(CheckOutZoneCode, CheckOutDateTime, CheckInZoneCode, CheckInDateTime, displayMode)

            If Not String.IsNullOrEmpty(message) And vehicleStatus = "N" Then
                Dim criteriaName As String = String.Concat(CRITERIA_VEHICLE_REQUEST, TimeStampRequest)
                Pattern.CacheCount(criteriaName)
                Throw New Exception(message)
            End If

        Catch ex As Exception
            vehicleStatus = "N"
            warningmessage = ex.Message
            LogInformation("IsAlternateVehicleStatusAvailable", MessageInformationFormat("AlternateAvailabilityRS IsAlternateVehicleStatusAvailable ", "Exception", ex.Message + ", " + ex.Source))
        End Try




        Return IIf(vehicleStatus.ToUpper.Equals("Y"), True, False)
    End Function



#End Region

#Region "Error Messages"
    Public Function messagesTraceActivity(ByVal messages As System.Collections.ArrayList) As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.messagesTraceActivity
        Dim sb As New StringBuilder
        Dim txtMessage As String = String.Empty
        sb.Append("<Traces>")
        For i As Integer = 0 To messages.Count - 1
            txtMessage = IIf(messages(i).ToString.Contains("'") = True, SanitizedBlockingrules(messages(i).ToString), messages(i).ToString)
            sb.AppendFormat("<TraceMessage  Message='{0}' />", txtMessage.ToUpper)
        Next
        sb.Append("</Traces>")
        Return sb.ToString
    End Function

    Public Function ErrorXMLMessage(ByVal vehicleCode As String, ByVal messages As System.Collections.ArrayList, ByVal messagestraces As System.Collections.ArrayList) As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.ErrorXMLMessage
        Dim sb As New StringBuilder
        Dim txtMessage As String = String.Empty
        Dim start As Integer = 0
        Dim arylist() As String = Nothing

        Dim dview As New DataView

        dview = GetSingleVehicleInfo(vehicleCode)
        sb.AppendFormat("<data><AvailableRate><Package BrandCode='{0}'/>", dview(0)("PrdBrdCode").ToString)
        sb.AppendFormat("<Charge><Det IsVeh='1' PrdCode='{0}' PrdName='{1}'> <RateBands /> </Det></Charge>", dview(0)("PrdShortName").ToString, dview(0)("PrdName").ToString)
        sb.Append("<Errors>")
        Dim isblocking As Boolean = False
        For i As Integer = 0 To messages.Count - 1
            txtMessage = IIf(messages(i).ToString.Contains("'") = True, SanitizedBlockingrules(messages(i).ToString), messages(i).ToString)

            If messages(i).indexof("BLOCKINGRULE") > 0 And Not isblocking Then
                isblocking = True
                sb.Append(txtMessage)
                sb.Append("<ErrorMessage Allow='' Message='' />")
                Exit For
            ElseIf messages(i).indexof("BLOCKINGRULE") < 0 Then
                sb.Append("<BlockingRuleMessage Allow='' Message='' />")
                sb.Append(txtMessage)
                Exit For
            End If
        Next
        sb.Append("</Errors></AvailableRate>" & messagesTraceActivity(messagestraces) & "</data>")
        Return sb.ToString
    End Function

    Private Function SanitizedBlockingrules(textMessage As String) As String
        If (textMessage.Contains("'") = False) Then Return textMessage
        If (textMessage.Split("'").Length = 5) Then Return textMessage

        Const errormessage As String = "Message="
        Dim origText As String = String.Empty
        Dim txttosanitized As String = String.Empty
        Try


        
        If (textMessage.Contains(errormessage) = False And textMessage.Contains("<") = False And textMessage.Contains("/>") = False) Then
            txttosanitized = textMessage
                origText = txttosanitized
            Else

                Dim start As Integer = textMessage.IndexOf(errormessage)
                txttosanitized = textMessage.Substring(start + (errormessage.Length + 1))

                Dim stopped As Integer = txttosanitized.LastIndexOf(">")
                txttosanitized = txttosanitized.Substring(0, stopped - 2)
                origText = txttosanitized

        End If

            If (Not String.IsNullOrEmpty(txttosanitized)) Then
                txttosanitized = txttosanitized.Replace("'", "").Replace("""", "").Replace("&", "").Replace("\", "")
            End If

            Return textMessage.Replace(origText, txttosanitized)

        Catch ex As Exception
            If (textMessage.Contains(errormessage) = False And textMessage.Contains("<") = False And textMessage.Contains("/>") = False) Then
                Return "Error encountered when parsing the traceMessages"
            Else
                Return "<ErrorMessage Allow='' Message='Error encountered when parsing the traceMessages and blocking rules' />"
            End If

        End Try


    End Function

    Public Function ErrorMessage(ByVal message As String) As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.ErrorMessage
        If (message.IndexOf("BLOCKINGRULE") = 0) Then
            Return "<BlockingRuleMessage Allow='' Message='" & message & "'/>"
        Else
            Return "<ErrorMessage Allow='' Message='" & message & "'/>"
        End If
    End Function
#End Region

#Region "Helper"
    Public Sub LogInformation(ByVal functionName As String, ByVal TextToLog As String) Implements WS.AlternateAvailability.IGetAlternateVehicleService.LogInformation
        Aurora.Common.Logging.LogInformation(vbCrLf & "GetAlternateVehicleService", String.Concat(vbCrLf, vbCrLf, MessageInformationFormat(functionName, TextToLog)))
    End Sub

    Private Function MessageInformationFormat(ByVal methodName As String, ByVal ParamArray strmessage As String()) As String
        Dim sb As New StringBuilder
        Dim skipfirstHeaderInlog As Boolean = False

        Try
            If (strmessage.Length = 1) Then
                skipfirstHeaderInlog = True
            End If
        Catch ex As Exception
            skipfirstHeaderInlog = False
        End Try


        If (Not skipfirstHeaderInlog) Then
            sb.AppendFormat("{0}{1}{2}{3}{4}{5}", vbCrLf, vbCrLf, New String("-", 30), "STARTING", New String("-", 30), vbCrLf)
            sb.Append(methodName.ToUpper)
            sb.AppendFormat("{0}{1}{2}", vbCrLf, New String("-", 68), vbCrLf & vbCrLf)

        End If
        
        Dim ctr As Integer = 0
        For Each strvalue As String In strmessage
            If Not String.IsNullOrEmpty(strvalue) Then
                strvalue = strvalue.Trim
                strvalue = strvalue.PadRight(30)
                If ctr = 0 Then
                    sb.AppendFormat("{0}:", strvalue)
                    ctr = ctr + 1
                Else
                    sb.AppendFormat("{0}{1}", strvalue, vbCrLf)
                    ctr = 0
                End If
            End If
        Next
        If (Not skipfirstHeaderInlog) Then
            ''sb.AppendFormat("{0}{1}{2}", vbCrLf, "----------------END-------------", vbCrLf & vbCrLf)
            sb.AppendFormat("{0}{1}{2}{3}{4}{5}{6}{7}", vbCrLf, vbCrLf, New String("-", 30), "END HERE", New String("-", 30), vbCrLf, vbCrLf, vbCrLf)
        End If

        Return sb.ToString
    End Function

    Private Function IsOneWay(ByVal [from] As String, ByVal [to] As String) As Boolean
        Return Not String.Equals([from].ToUpper.Trim, [to].ToUpper.Trim)
    End Function



#End Region


    Public Function GetAlternateVehicleResponses(ByVal ParamArray BookingInfo As String()) As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.GetAlternateVehicleResponses
        Dim sb As New StringBuilder()

        ''FORMAT OF THE XML WILL BE
        ''<Responses timeRequest='áaa'>
        ''<FleetCode>M</FleetCode>
        ''<AvailStatus>Y</AvailStatus>
        ''<Brand></Brand>
        ''<VehicleName></VehicleName>
        ''<VehicleCode></VehicleCode>
        ''<Messages></Messages>
        ''<CheckoutLoc></CheckoutLoc>
        ''<CheckoutDate></CheckoutDate>
        ''<CheckInLoc></CheckInLoc>
        ''<CheckInDate></CheckINtDate>
        ''</Responses>

        ''pickup loc
        ''pickup dte
        ''drop   loc
        ''drop   dte
        Dim timerequest As String = ""
        Dim pickupLoc As String = ""
        Dim pickupdte As String = ""
        Dim dropLoc As String = ""
        Dim dropdte As String = ""

        Dim pickupLocElem As String = ""
        Dim pickupdteElem As String = ""
        Dim dropLocElem As String = ""
        Dim dropdteElem As String = ""


        Dim errsarray As ArrayList = Nothing
        Dim criteriaName As String = String.Concat(CRITERIA_VEHICLE_REQUEST, TimeStampRequest)

        If Pattern.IsCachedExist(criteriaName) Then
            errsarray = CType(Pattern.InfoCachingPattern(criteriaName, Nothing), ArrayList)
        End If

        Pattern.CacheCount(criteriaName)

        If errsarray Is Nothing Then Return String.Empty
        sb.Append("<VehicleResponses>")

        Dim temp As String = ""
        Dim arycount As Integer = errsarray.Count - 1
        Dim substringToReplace As String = ""
        Dim origsubstring As String = ""
        Dim displaymode As String = ""
        If BookingInfo.Length - 1 = 4 Then
            If Not String.IsNullOrEmpty(BookingInfo(4)) Then
                displaymode = BookingInfo(4)
            End If
        End If

        Dim testctr As Integer = 0
        Dim newlist As New ArrayList

        For i As Integer = 0 To errsarray.Count - 1
            substringToReplace = errsarray(i).ToString.Substring(0, errsarray(i).ToString.IndexOf("<FleetCode>"))

            ''rev:mia jan 25 2011--exactly 4 years here in NZ
            ''ATTRIBUTE HAS NOT BEEN ADDED
            If substringToReplace.IndexOf("searchPattern") = -1 Then
                temp = errsarray(i).ToString.Replace(substringToReplace, "<Responses searchPattern='" & displaymode & "' timeRequest='" & TimeStampRequest & "' sort='" & i.ToString & "'>")
            Else
                temp = errsarray(i).ToString
            End If


            newlist.Add(temp)
            sb.Append(temp)

            System.Diagnostics.Debug.WriteLine(temp)


        Next

        Pattern.DeleteOldCachedAndReInsert(criteriaName, newlist)
        sb.Append("</VehicleResponses>")



        Dim tempXmldocument As New XmlDocument
        Try
            tempXmldocument.LoadXml(sb.ToString)
        Catch ex As Exception

        End Try



        Dim fleetcode As String = ""
        Dim availstatus As String = ""
        Dim msg As String = ""

        If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses").Attributes("sort") Is Nothing Then
            If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']") Is Nothing Then

                If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/FleetCode") Is Nothing Then
                    If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/FleetCode") Is Nothing Then
                        fleetcode = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/FleetCode").InnerText
                    End If

                End If

                If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/AvailStatus") Is Nothing Then
                    If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/AvailStatus") Is Nothing Then
                        availstatus = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/AvailStatus").InnerText
                    End If

                End If

                If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/Messages") Is Nothing Then
                    If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/Messages") Is Nothing Then
                        msg = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']/Messages").InnerText
                    End If

                End If

                If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']").Attributes("timeRequest") Is Nothing Then
                    If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']") Is Nothing Then
                        timerequest = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & TimeStampRequest & "' and @searchPattern='" & displaymode & "']").Attributes("timeRequest").Value
                    End If
                End If


            End If


            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/CheckoutLoc") Is Nothing Then
            '    pickupLocElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & timestampRequest & "']/CheckoutLoc").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/CheckoutDate") Is Nothing Then
            '    pickupdteElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & timestampRequest & "']/CheckoutDate").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/CheckInLoc") Is Nothing Then
            '    dropLocElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & timestampRequest & "']/CheckInLoc").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "']/CheckInDate") Is Nothing Then
            '    dropdteElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses[@sort='" & arycount & "' and  @timeRequest='" & timestampRequest & "']/CheckInDate").InnerText.TrimEnd
            'End If

        Else

            If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/FleetCode") Is Nothing Then
                fleetcode = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/FleetCode").InnerText
            End If

            If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/AvailStatus") Is Nothing Then
                availstatus = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/AvailStatus").InnerText
            End If

            If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/Messages") Is Nothing Then
                msg = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/Messages").InnerText
            End If

            If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses").Attributes("timeRequest") Is Nothing Then
                timerequest = tempXmldocument.SelectSingleNode("VehicleResponses/Responses").Attributes("timeRequest").Value
            End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckoutLoc") Is Nothing Then
            '    pickupLocElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckoutLoc").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckoutDate") Is Nothing Then
            '    pickupdteElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckoutDate").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckInLoc") Is Nothing Then
            '    dropLocElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckInLoc").InnerText.TrimEnd
            'End If

            'If Not tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckInDate") Is Nothing Then
            '    dropdteElem = tempXmldocument.SelectSingleNode("VehicleResponses/Responses/CheckInDate").InnerText.TrimEnd
            'End If

        End If


        Dim ctr As Integer = 0
        For Each bookInfo As String In BookingInfo
            If ctr = 0 Then pickupLoc = bookInfo.TrimEnd
            If ctr = 1 Then pickupdte = bookInfo.TrimEnd
            If ctr = 2 Then dropLoc = bookInfo.TrimEnd
            If ctr = 3 Then dropdte = bookInfo.TrimEnd
            ctr = ctr + 1
        Next


        If timerequest.Equals(TimeStampRequest) Then
            If (fleetcode = "R" And availstatus = "N") Then
                Return msg
            End If

        End If


        Return String.Empty

    End Function

    Public Function ExecuteSearchCriteria(ByVal vehicleInfo As WS.AlternateAvailability.Model.VehicleInformation, _
                                          ByVal passenger As WS.AlternateAvailability.Model.PassengerInformation, _
                                          ByVal criteria As Integer, _
                                          ByRef AlternateVehicle As System.Xml.XmlDocument, _
                                          Optional ByRef warningmessage As String = "", _
                                          Optional ByRef messageErrors As ArrayList = Nothing, _
                                          Optional ByRef messagesActivity As ArrayList = Nothing) As Boolean Implements WS.AlternateAvailability.IGetAlternateVehicleService.ExecuteSearchCriteria

        Dim hasFoundAvailability As Boolean = False
        ''System.Diagnostics.Debug.WriteLine("--------------------------------------")

        Dim AlternateVehicleStatusAvailable As Boolean
        Dim daysToSearch As Integer = CInt(MoveableDays)
        Dim ctr As Integer = 1
        Dim displaymode As String = ""
        While ctr <= daysToSearch
            Select Case criteria

                Case AlternateAvailability.MoveForward And IsMovedForward

                    vehicleInfo.CheckOutDateTime = vehicleInfo.CheckOutDateTime.AddDays(1)
                    vehicleInfo.CheckInDateTime = vehicleInfo.CheckInDateTime.AddDays(1)
                    displaymode = "MoveForward"
                Case AlternateAvailability.MoveBackWard And IsMoveBackWard

                    vehicleInfo.CheckOutDateTime = vehicleInfo.CheckOutDateTime.AddDays(-1)
                    vehicleInfo.CheckInDateTime = vehicleInfo.CheckInDateTime.AddDays(-1)
                    displaymode = "MoveBackWard"
                Case AlternateAvailability.PickUpBased And IsPickUpBased

                    vehicleInfo.CheckInDateTime = vehicleInfo.CheckInDateTime.AddDays(-1)
                    displaymode = "PickUpBased"
                Case AlternateAvailability.DropOffBased And IsDropOffBased

                    vehicleInfo.CheckOutDateTime = vehicleInfo.CheckOutDateTime.AddDays(1)
                    displaymode = "DropOffBased"
                Case AlternateAvailability.SwitchLocation And IsSwitchLocation

                    Dim [out] As String = vehicleInfo.CheckOutZoneCode.ToUpper
                    Dim [in] As String = vehicleInfo.CheckInZoneCode.ToUpper

                    vehicleInfo.CheckOutZoneCode = [in]
                    vehicleInfo.CheckInZoneCode = [out]

                    displaymode = "SwitchLocation"

                Case AlternateAvailability.NormalBased

                    vehicleInfo.CheckOutDateTime = vehicleInfo.CheckOutDateTime
                    vehicleInfo.CheckInDateTime = vehicleInfo.CheckInDateTime
                    vehicleInfo.CheckInZoneCode = vehicleInfo.CheckInZoneCode
                    vehicleInfo.CheckOutZoneCode = vehicleInfo.CheckOutZoneCode

                Case AlternateAvailability.OnewaySwitchLocation And IsOnewaySwitchLocation

                    Dim [out] As String = vehicleInfo.CheckOutZoneCode.ToUpper
                    Dim [in] As String = vehicleInfo.CheckInZoneCode.ToUpper

                    vehicleInfo.CheckOutZoneCode = [in]
                    vehicleInfo.CheckInZoneCode = [out]

                    displaymode = "OnewaySwitchLocation"

                Case AlternateAvailability.OnewaySwitchLocationOnPickup And IsOnewaySwitchLocationOnPickup

                    Dim [out] As String = vehicleInfo.CheckOutZoneCode.ToUpper


                    vehicleInfo.CheckOutZoneCode = [out]
                    vehicleInfo.CheckInZoneCode = [out]

                    displaymode = "OnewaySwitchLocationOnPickup"
            End Select

            warningmessage = String.Empty

            ''if its overlapping ...exit 
            If vehicleInfo.CheckOutDateTime.ToShortDateString = vehicleInfo.CheckInDateTime.ToShortDateString Then
                messagesActivity.Add(ctr & "|" & vehicleInfo.CheckOutDateTime.ToShortDateString & "-" & vehicleInfo.CheckOutZoneCode & "|" & vehicleInfo.CheckInDateTime.ToShortDateString & "-" & vehicleInfo.CheckInZoneCode & "|DATE OVERLAPPED")
                Exit While
            End If
            AlternateVehicleStatusAvailable = IsAlternateVehicleStatusAvailable(vehicleInfo, warningmessage, displaymode)
            'System.Diagnostics.Debug.WriteLine(displaymode & "> counter: " + ctr.ToString + vbCrLf + _
            '                                              "alternateCheckOutDateTime: " + vehicleInfo.CheckOutDateTime.ToShortDateString + _
            '                                              ", alternateCheckInDateTime: " + vehicleInfo.CheckInDateTime.ToShortDateString + vbCrLf + _
            '                                              "Warning: " & warningmessage & vbCrLf & _
            '                                              "AlternateVehicleStatusAvailable: " & AlternateVehicleStatusAvailable & vbCrLf)

            ''found so get the availability
            If (AlternateVehicleStatusAvailable) Then
                messagesActivity.Add(ctr & "|" & vehicleInfo.CheckOutDateTime.ToShortDateString & "-" & vehicleInfo.CheckOutZoneCode & "|" & vehicleInfo.CheckInDateTime.ToShortDateString & "-" & vehicleInfo.CheckInZoneCode & "|FOUND")
                hasFoundAvailability = AlternateVehicleStatusAvailable
                AlternateVehicle = AlternateAvailabilityRS(vehicleInfo, passenger)
                Exit While
            Else
                messageErrors.Add(ErrorMessage(warningmessage))
                messagesActivity.Add(ctr & "|" & vehicleInfo.CheckOutDateTime.ToShortDateString & "-" & vehicleInfo.CheckOutZoneCode & "|" & vehicleInfo.CheckInDateTime.ToShortDateString & "-" & vehicleInfo.CheckInZoneCode & "|" & warningmessage)

                ''no need to loop several times for switch locations
                If (criteria = AlternateAvailability.SwitchLocation) Or _
                    (criteria = AlternateAvailability.NormalBased) Or _
                    (criteria = AlternateAvailability.OnewaySwitchLocation) Or _
                    (criteria = AlternateAvailability.OnewaySwitchLocationOnPickup) Then
                    Exit While
                End If
            End If

            ctr = ctr + 1

        End While

        ''System.Diagnostics.Debug.WriteLine("--------------------------------------")
        ''empty so get the empty result set and throws to the calling module
        If Not hasFoundAvailability Then
            ''notify the calling program that
            ''it reach the checking of alternate availability

            If AlternateVehicle.OuterXml = "" Or messageErrors.Count - 1 > 0 Then
                AlternateVehicle.LoadXml(ErrorXMLMessage(vehicleInfo.VehicleCode, messageErrors, messagesActivity))
            End If
        Else
            '' sb.Append("</Errors></AvailableRate>" & messagesTraceActivity(messagestraces) & "</data>")
            If ActivityTrace Then
                Dim temp As String = AlternateVehicle.OuterXml.Replace("</data>", messagesTraceActivity(messagesActivity) & "</data>")
                AlternateVehicle.LoadXml(temp)
            End If
        End If

        Return hasFoundAvailability
    End Function

    Public Sub AddSearchCriteriaResult(ByVal alternateVehicle As System.Xml.XmlDocument, ByVal displaymode As String) Implements WS.AlternateAvailability.IGetAlternateVehicleService.AddSearchCriteriaResult
        Dim sb As New StringBuilder
        Dim attMode As XmlAttribute = alternateVehicle.CreateAttribute("searchPattern")
        attMode.Value = displaymode
        alternateVehicle.SelectSingleNode("data/AvailableRate").Attributes.Append(attMode)

        Dim attrqGuid As XmlAttribute = alternateVehicle.CreateAttribute("rqGuid")
        attrqGuid.Value = TimeStampRequest
        alternateVehicle.SelectSingleNode("data/AvailableRate").Attributes.Append(attrqGuid)

        sb.Append(alternateVehicle.SelectSingleNode("data/AvailableRate").OuterXml)

        If ActivityTrace AndAlso Not alternateVehicle.SelectSingleNode("data/Traces") Is Nothing Then
            alternateVehicle.SelectSingleNode("data/Traces").Attributes.Append(attMode)
            sb.Append(alternateVehicle.SelectSingleNode("data/Traces").OuterXml)
        End If

        Dim CriteriaCollationDocument As New ArrayList
        Dim criteriaName As String = String.Concat(CRITERIA_COLLATION_REQUEST, TimeStampRequest)
        With sb

            If Not Pattern.IsCachedExist(criteriaName) Then
                CriteriaCollationDocument.Add(.ToString)
                Pattern.InfoCachingPattern(criteriaName, CriteriaCollationDocument)
            Else
                CriteriaCollationDocument = CType(Pattern.InfoCachingPattern(criteriaName, CriteriaCollationDocument), ArrayList)
                CriteriaCollationDocument.Add(.ToString)
                Pattern.DeleteOldCachedAndReInsert(criteriaName, CriteriaCollationDocument)
            End If
        End With

        Pattern.CacheCount(criteriaName)
    End Sub

    Public Function GetSearchCriteriaResult() As String Implements WS.AlternateAvailability.IGetAlternateVehicleService.GetSearchCriteriaResult
        Dim sb As New StringBuilder
        Dim criteriaName As String = String.Concat(CRITERIA_COLLATION_REQUEST, TimeStampRequest)

        With sb
            .Append("<data>")
            If Pattern.IsCachedExist(criteriaName) Then
                Dim CriteriaCollationDocument As New ArrayList
                CriteriaCollationDocument = CType(Pattern.InfoCachingPattern(criteriaName, CriteriaCollationDocument), ArrayList)
                For i As Integer = 0 To CriteriaCollationDocument.Count - 1
                    .AppendFormat(CriteriaCollationDocument(i))
                Next
            End If
            .Append("</data>")
        End With
        Pattern.CacheCount(criteriaName)
        Return sb.ToString
    End Function

    Public Sub AddAlternateVehicleResponses(ByVal ParamArray Messages() As String) Implements WS.AlternateAvailability.IGetAlternateVehicleService.AddAlternateVehicleResponses

        Try
            If IsAlternateServiceCall.Equals("NO") Then Exit Sub

            ''FORMAT OF THE XML WILL BE
            ''<Responses timeRequest='aaaaaa'>
            ''<FleetCode>M</FleetCode>
            ''<AvailStatus>Y</AvailStatus>
            ''<Brand></Brand>
            ''<VehicleName></VehicleName>
            ''<VehicleCode></VehicleCode>
            ''<Messages></Messages>
            ''<CheckoutLoc></CheckoutLoc>
            ''<CheckoutDate></CheckoutDate>
            ''<CheckInLoc></CheckInLoc>
            ''<CheckInDate></CheckINtDate>
            ''<guid></guid>
            ''</Responses>



            Dim ctr As Integer = 0
            Dim sb As New StringBuilder

            If String.IsNullOrEmpty(TimeStampRequest) Then
                TimeStampRequest = Guid.NewGuid.ToString
                System.Diagnostics.Debug.WriteLine("another timestamp: " & TimeStampRequest)
            End If

            With sb
                .AppendFormat("<Responses timeRequest='{0}'>", TimeStampRequest)
                For Each strValue As String In Messages
                    strValue = strValue.Trim.ToUpper
                    If ctr = 0 Then .AppendFormat("<FleetCode>{0}</FleetCode>", strValue)
                    If ctr = 1 Then .AppendFormat("<AvailStatus>{0}</AvailStatus>", strValue)
                    If ctr = 2 Then .AppendFormat("<Brand>{0}</Brand>", strValue)
                    If ctr = 3 Then .AppendFormat("<VehicleName>{0}</VehicleName>", strValue)
                    If ctr = 4 Then .AppendFormat("<VehicleCode>{0}</VehicleCode>", strValue)
                    If ctr = 5 Then .AppendFormat("<Messages>{0}</Messages>", strValue)
                    'If ctr = 6 Then .AppendFormat("<CheckoutLoc>{0}</CheckoutLoc>", strValue)
                    'If ctr = 7 Then .AppendFormat("<CheckoutDate>{0}</CheckoutDate>", strValue)
                    'If ctr = 8 Then .AppendFormat("<CheckInLoc>{0}</CheckInLoc>", strValue)
                    'If ctr = 9 Then .AppendFormat("<CheckInDate>{0}</CheckInDate>", strValue)

                    ctr = ctr + 1
                Next
                .Append("</Responses>")


                Dim criteriaName As String = String.Concat(CRITERIA_VEHICLE_REQUEST, TimeStampRequest)

                Dim vehicleresponses As New ArrayList
                vehicleresponses.Add(.ToString)

                If Not Pattern.IsCachedExist(criteriaName) Then
                    Pattern.InfoCachingPattern(criteriaName, vehicleresponses)
                Else
                    vehicleresponses = CType(Pattern.InfoCachingPattern(criteriaName, vehicleresponses), ArrayList)
                    vehicleresponses.Add(.ToString)
                    Pattern.DeleteOldCachedAndReInsert(criteriaName, vehicleresponses)
                End If
                Pattern.CacheCount(criteriaName)
            End With

        Catch ex As Exception
            ''just trap possible errors
        End Try
      
    End Sub


    Private _IsAlternateServiceCall As String
    Private Property IsAlternateServiceCall As String
        Get
            If String.IsNullOrEmpty(_IsAlternateServiceCall) Then
                _IsAlternateServiceCall = "NO"
            End If
            Return _IsAlternateServiceCall.ToUpper
        End Get
        Set(ByVal value As String)
            _IsAlternateServiceCall = value
        End Set
    End Property


#Region "rev:mia Nov.26 2012 - Helper for AlternateVehicle inside Aurora"


    'Private ReadOnly Property GetSkipPriceConfigValue As Boolean
    '    Get
    '        Return CBool(ConfigurationManager.AppSettings("SkipPriceCalculation").ToString)
    '    End Get
    'End Property

    'Private ReadOnly Property IsAuroraRequest As Boolean
    '    Get



    '        'Dim originalstring As String = System.Web.HttpContext.Current.Request.Url.OriginalString
    '        'LogInformation("AlternateAvailabilityRQ", MessageInformationFormat("AlternateAvailabilityRQ IsAuroraRequest ", originalstring))


    '        ''can be considered as request that came from aurora
    '        ''Return IIf(originalstring.Contains("BookingProcess.aspx".ToLower) = True, True, False)

    '        'Dim channelrequest As String = System.Web.HttpContext.Current.Request.ServerVariables("HTTP_HOST").ToLower
    '        'Dim channelDomain As String = System.Web.HttpContext.Current.Request.ServerVariables("URL").ToLower


    '        'LogInformation("AlternateAvailabilityRQ", MessageInformationFormat("AlternateAvailabilityRQ IsAuroraRequest ", channelrequest, channelDomain))
    '        'Dim arrChannelRequest As ArrayList = New ArrayList
    '        'arrChannelRequest.Add("localhost")
    '        'arrChannelRequest.Add("tdc-aur-001")
    '        'arrChannelRequest.Add("pap-app-001")
    '        'arrChannelRequest.Add("pap-app-002")
    '        'arrChannelRequest.Add("pap-app-003")
    '        'arrChannelRequest.Add("cor-dev-001")

    '        'Dim fromvalidAuroraSystem As Boolean = False
    '        'Dim isFromAurora As Boolean = False
    '        'For Each item As String In arrChannelRequest
    '        '    If (channelrequest.Contains(item)) Then
    '        '        fromvalidAuroraSystem = True
    '        '        Exit For
    '        '    End If
    '        'Next


    '        'If fromvalidAuroraSystem Then
    '        '    ''this came from live or test environment
    '        '    If (channelDomain.Contains("aurora") = True Or channelDomain.Contains("oslo") = True) Then
    '        '        ''alternate availability without price will take over
    '        '        isFromAurora = True
    '        '    ElseIf (channelDomain.Contains("http://tdc-aur-001/Aurora/Web/Booking/BookingProcess.aspx") = True) Then
    '        '        isFromAurora = True

    '        '        ''testing in pap-app-002
    '        '    ElseIf (channelDomain.Contains("Phoenix webservicev2".ToLower) = True) Then
    '        '        isFromAurora = False
    '        '        ''testing in local
    '        '    ElseIf (channelDomain.Contains("Phoenix webservice".ToLower) = True) Then
    '        '        isFromAurora = False

    '        '    Else
    '        '        isFromAurora = False
    '        '    End If
    '        'End If

    '        ' ''if isFromAurora = false then this came from b2c channel
    '        ' ''so normal alternate availability will trigger
    '        ' ''otherwise, alternate availability without price will take over
    '        'Return isFromAurora
    '    End Get
    'End Property


#End Region
End Class
