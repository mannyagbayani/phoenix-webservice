﻿Imports Microsoft.VisualBasic
Imports System.Xml
Namespace WS.CentralizedAvailability
    Public Interface IDvassInformation
        Property NumberOfVehicle As Integer
        Function GetResult(ByVal CountryCode As String, _
                                      ByVal ProductId As String, _
                                      ByVal CheckOutLocationCode As String, _
                                      ByVal CheckOutDate As Date, _
                                      ByVal CheckInLocationCode As String, _
                                      ByVal CheckInDate As Date, _
                                      ByRef loggingmessages As String) As XmlDocument


        Function IsWSDvassAvailable _
                                           (ByVal iCountry As Integer, _
                                            ByVal sProdId As String, _
                                            ByVal sLocId_Fr As String, _
                                            ByVal dDate_Fr As Date, _
                                            ByVal sLocId_To As String, _
                                            ByVal dDate_To As Date, _
                                            ByVal NumberOfVehicleTarget As Integer, _
                                            ByRef NumberOfVehicle As Integer, _
                                            ByRef sReturnMessage As String, _
                                            ByRef loggingmessages As String) As Boolean
    End Interface

End Namespace




