﻿Imports Microsoft.VisualBasic
Imports System.Xml

Namespace WS.CentralizedAvailability
    Public Interface IGetCentralizedAvailability
        Function GetAvailability(ByVal Brand As String, _
                                ByVal CountryCode As String, ByVal VehicleCode As String, _
                                ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As DateTime, _
                                ByVal CheckInZoneCode As String, ByVal CheckInDateTime As DateTime, _
                                ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, _
                                ByVal AgentCode As String, ByVal PackageCode As String, _
                                ByVal IsVan As Boolean, ByVal IsBestBuy As Boolean, _
                                ByVal CountryOfResidence As String, ByVal IsTestMode As Boolean, ByVal IsGross As Boolean) As XmlDocument



        Function WSexecute(AvailCode As String, _
                              ByVal CountryCode As String, _
                              ByVal ProductId As String, _
                              ByVal CheckOutLocationCode As String, _
                              ByVal CheckOutDate As Date, _
                              ByVal CheckInLocationCode As String, _
                              ByVal CheckInDate As Date, _
                              ByVal ProductShortName As String, _
                              ByVal NoOfVehiclesLookUp As Integer, _
                              ByVal AgentId As String, _
                              ByVal BrdCode As String, _
                              ByVal isQuickAvail As Boolean, _
                              ByRef loggingmessages As String) As String


        Function GetFleetStatus(ByVal ProductShortName As String, _
                                  ByVal CheckOutLocationCode As String, _
                                  ByVal CheckOutDate As DateTime, _
                                  ByVal CheckInLocationCode As String, _
                                  ByVal CheckInDate As DateTime, _
                                  ByVal AgentCode As String, _
                                  ByVal PackageCode As String, _
                                  ByRef ErrorMessage As String, _
                                  ByRef DvassSequenceNumber As Int64) As Char


        'Function GetWSDvassXmlResult(ByVal CountryCode As String, _
        '                               ByVal ProductId As String, _
        '                               ByVal CheckOutLocationCode As String, _
        '                               ByVal CheckOutDate As Date, _
        '                               ByVal CheckInLocationCode As String, _
        '                               ByVal CheckInDate As Date, _
        '                               ByVal NumberOfVehicleTarget As Integer) As XmlDocument


        'Function GetDVASSXmlResult _
        '                                (ByVal iCountry As Integer, _
        '                                 ByVal sProdId As String, _
        '                                 ByVal sLocId_Fr As String, _
        '                                 ByVal dDate_Fr As Date, _
        '                                 ByVal sLocId_To As String, _
        '                                 ByVal dDate_To As Date, _
        '                                 ByVal NumberOfVehicleTarget As Integer _
        '                                 ) As String


        Function GetAvailabilityV2(ByVal Brand As String, _
                                      ByVal CountryCode As String, _
                                      ByVal VehicleCode As String, _
                                      ByVal CheckOutZoneCode As String, _
                                      ByVal CheckOutDateTime As DateTime, _
                                      ByVal CheckInZoneCode As String, _
                                      ByVal CheckInDateTime As DateTime, _
                                      ByVal NoOfAdults As Int16, _
                                      ByVal NoOfChildren As Int16, _
                                      ByVal AgentCode As String, _
                                      ByVal PackageCode As String, _
                                      ByVal IsVan As Boolean, _
                                      ByVal IsBestBuy As Boolean, _
                                      ByVal CountryOfResidence As String, _
                                      ByVal IsTestMode As Boolean, _
                                      ByVal IsGross As Boolean, _
                                      ByVal ChannelRequest As Integer, _
                                      ByVal IsQuickAvail As Boolean) As XmlDocument

    End Interface



End Namespace
