﻿Imports Microsoft.VisualBasic
Imports System.Xml

Namespace WS.CentralizedAvailability


    Public Interface IVehicleList
        Function GetAvailabilitiesBasedOnDays(ByVal Brand As String, _
                               ByVal CountryCode As String, ByVal VehicleCode As String, _
                               ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As DateTime, _
                               ByVal CheckInZoneCode As String, ByVal CheckInDateTime As DateTime, _
                               ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, _
                               ByVal AgentCode As String, ByVal PackageCode As String, _
                               ByVal IsVan As Boolean, ByVal IsBestBuy As Boolean, _
                               ByVal CountryOfResidence As String, ByVal IsTestMode As Boolean, ByVal IsGross As Boolean) As XmlDocument


        Function GetFleetStatus(ByVal ProductShortName As String, _
                                  ByVal CheckOutLocationCode As String, _
                                  ByVal CheckOutDate As DateTime, _
                                  ByVal CheckInLocationCode As String, _
                                  ByVal CheckInDate As DateTime, _
                                  ByVal AgentCode As String, _
                                  ByVal PackageCode As String, _
                                  ByRef ErrorMessage As String, _
                                  ByRef DvassSequenceNumber As Int64) As Char

        Property AvailabilityInDays As Integer
    End Interface

End Namespace


