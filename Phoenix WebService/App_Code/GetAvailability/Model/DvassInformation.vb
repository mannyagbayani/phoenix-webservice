﻿Imports Microsoft.VisualBasic
Imports Aurora.Booking.Data
Imports Aurora.Booking.Services
Imports WS.CentralizedAvailability
Imports System.Xml
Imports COMDVASSMgrLib
Imports Aurora.Reservations.Services
Imports Aurora.Common

Namespace WS.CentralizedAvailability
    Public Class DvassInformation
        Implements IDvassInformation



        Private _NumberOfVehicle As Integer

        Sub New()
            NumberOfVehicle = 1
        End Sub

        Sub New(numberOfVehicleRequired As Integer)
            NumberOfVehicle = numberOfVehicleRequired
            ''get only 20 numbers of vehicle
            If (NumberOfVehicle > 20) Then NumberOfVehicle = 20
        End Sub

        Public Function GetResult(CountryCode As String, _
                                  ProductId As String, _
                                  CheckOutLocationCode As String, _
                                  CheckOutDate As Date, _
                                  CheckInLocationCode As String, _
                                  CheckInDate As Date, _
                                  ByRef loggingmessages As String) As System.Xml.XmlDocument Implements IDvassInformation.GetResult


            Dim sReturnedMessage As String = String.Empty
            Dim xmlReturnMessage As XmlDocument = New XmlDocument

            Dim bResult As Boolean
            Dim iCountry As Integer
            Dim NumberOfVehicleReturned As Integer = 0

            Dim sb As New StringBuilder
            ''sb.Append(loggingmessages)

            Dim sContent As String = ""

            Try

                iCountry = IIf(CountryCode = "AU", 0, 1)
                sb.Append("************************" & vbCrLf)
                sb.Append("4.Calling GetResult ".ToUpper & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("FUNCTION PARAMETERS " & vbCrLf)
                sb.Append("iCountry             : " & iCountry & vbCrLf)
                sb.Append("ProductId            : " & ProductId & vbCrLf)
                sb.Append("CheckOutLocationCode : " & CheckOutLocationCode & vbCrLf)
                sb.Append("CheckOutDate         : " & CheckOutDate.ToString & vbCrLf)
                sb.Append("CheckInLocationCode  : " & CheckInLocationCode & vbCrLf)
                sb.Append("CheckInDate          : " & CheckInDate.ToString & vbCrLf)
                sb.Append("ProductShortName     : " & ProductId & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


                loggingmessages = String.Concat(loggingmessages, sb.ToString)



                bResult = IsWSDvassAvailable(iCountry, _
                                              ProductId, _
                                              CheckOutLocationCode, _
                                              CheckOutDate, _
                                              CheckInLocationCode, _
                                               CheckInDate, _
                                              NumberOfVehicle, _
                                              NumberOfVehicleReturned, _
                                              sReturnedMessage, _
                                              loggingmessages)


                If (bResult) Then
                    xmlReturnMessage.LoadXml(sReturnedMessage)
                Else
                    Throw New Exception(sReturnedMessage)
                End If

            Catch ex As Exception

                ''we need to check that a valid xml is being return 
                ''even if there are some errors
                Dim errormsgthrow As String = ex.Message
                If (errormsgthrow.Contains("<Data>") = False) Then
                    sReturnedMessage = "<Data>" & _
                                          "<Status>ERROR</Status>" & _
                                          "<Result>False</Result>" & _
                                          "<Message>" & ex.Message & " - " & ex.StackTrace.Replace("\", "").Replace("&", "") & "</Message>" & _
                                          "<MannyNote>Error message was thrown somewhere else.We need to check that a valid xml is being return even if there are some errors. </MannyNote>" & _
                                       "</Data>"

                End If


                
                xmlReturnMessage.LoadXml(sReturnedMessage)


                sb = New StringBuilder
                sb.Append("************************" & vbCrLf)
                sb.Append("Exception  caught in GetResult       : ".ToUpper & xmlReturnMessage.OuterXml & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                loggingmessages = String.Concat(loggingmessages, sb.ToString)

            End Try

            ''Logging.LogInformation("getresult", loggingmessages)

            Return xmlReturnMessage
        End Function

        Public Property NumberOfVehicle As Integer Implements IDvassInformation.NumberOfVehicle
            Get
                Return _NumberOfVehicle
            End Get
            Set(value As Integer)
                _NumberOfVehicle = value
            End Set
        End Property



#Region "Private Properties"
        Private Overloads Shared Function convertDVASSDateToVBDate(ByVal pDate As Long) As Date
            Return DateAdd(DateInterval.Day, pDate, New Date(2000, 1, 1))
        End Function
#End Region

        Public Function IsWSDvassAvailable(iCountry As Integer, _
                                           sProdId As String, _
                                           sLocId_Fr As String, _
                                           dDate_Fr As Date, _
                                           sLocId_To As String, _
                                           dDate_To As Date, _
                                           NumberOfVehicleTarget As Integer, _
                                           ByRef NumberOfVehicle As Integer, _
                                           ByRef sReturnMessage As String, _
                                           ByRef loggingmessages As String) As Boolean Implements IDvassInformation.IsWSDvassAvailable

            Dim objDVASSMgr As New CDVASSMgr
            Dim pStatus As comStatus = Nothing
            Dim oDateFr As COMDVASSMgrLib.comDateTime = Nothing
            Dim oDateTo As COMDVASSMgrLib.comDateTime = Nothing

            Const DVASS_NOT_STARTED As String = "DVASS is not started."
            Const DVASS_NOT_AVAILABLE As String = "DVASS is not available."
            Const AVAILABLE_XML As String = "<AvailableVehicle><NumberOfVehicles>{0}</NumberOfVehicles></AvailableVehicle>"

            Dim oRes As Array = Nothing


            Dim iPnmons As Long
            Dim iPdate As Long
            Dim iPtime As Long
            Dim iPin As Long
            Dim i As Long = 0



            If (objDVASSMgr Is Nothing) Then

                With pStatus
                    .status = 2
                    .msg = DVASS_NOT_STARTED
                    .detail = 900
                End With

                Throw New Exception(DVASS_NOT_STARTED)

            End If

            If Not (AuroraDvass.IsDVASSAvailable(iCountry)) Then
                With pStatus
                    .status = 2
                    .msg = DVASS_NOT_AVAILABLE
                    .detail = 900
                End With

                Throw New Exception(DVASS_NOT_AVAILABLE)

            End If ''ENDIF FOR dvassAvailable() = False 



            Dim iRetNoOfResults As Long
            Dim pstarttime As Long = 1
            Dim pendtime As Long = 1



            oDateFr = AuroraDvass.ConvertDateTimeToDVASSDateTime(dDate_Fr, pstarttime)
            oDateTo = AuroraDvass.ConvertDateTimeToDVASSDateTime(dDate_To, pendtime)


            With pStatus
                .status = 0
                .msg = String.Empty
                .detail = 0
            End With

            Dim dPriority As Double = 1
            Dim iVisibility As Long = 10
            Dim iAlternative_Flag As Long = 0

            ''make result equal to expected numbes of vehicle
            Dim iMaxResult_Flag As Long = NumberOfVehicleTarget

            IsWSDvassAvailable = False

            Dim sb As New StringBuilder
            sb.Append(loggingmessages)


            Try

                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("5.Calling AuroraDvass.IsWSDvassAvailable " & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("6.Calling DVASSMgr.GetStatus " & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("DVASS.GetStatus PARAMETERS " & vbCrLf)
                sb.Append("iCountry             : " & iCountry & vbCrLf)
                sb.Append("iPnmons              : " & iPnmons & vbCrLf)
                sb.Append("iPdate               : " & iPdate & vbCrLf)
                sb.Append("iPtime               : " & iPtime & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                Call objDVASSMgr.GetStatus(iCountry, iPnmons, iPdate, iPtime, iPin, pStatus)

                With pStatus
                    If (.detail <> 0) Then
                        loggingmessages = sb.ToString
                        Throw New Exception(.msg)
                    Else
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                        sb.Append("NOTE: DVASS status is " & pStatus.msg & vbCrLf)
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                    End If

                End With




                Try

                    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                    sb.Append("************************" & vbCrLf)
                    sb.Append("7.Calling DVASS.AvailQuery " & vbCrLf)
                    sb.Append("************************" & vbCrLf)
                    sb.Append("FUNCTION PARAMETERS " & vbCrLf)
                    sb.Append("iCountry                               (icountry)            : " & iCountry & vbCrLf)
                    sb.Append("sProdId                                (prodid)              : " & sProdId & vbCrLf)
                    sb.Append("NumberOfVehicleTarget                  (nvehicles)           : " & NumberOfVehicleTarget & vbCrLf)
                    sb.Append("CandidateVehicle                       (candidate_vehicles)  : " & "" & vbCrLf)
                    sb.Append("sLocId_Fr                              (locid_fr)            : " & sLocId_Fr & vbCrLf)
                    sb.Append("oDateFr                                (pdat_fr)             : " & oDateFr.d & "-" & oDateFr.t & vbCrLf)
                    sb.Append("sLocId_To                              (locid_to)            : " & sLocId_To & vbCrLf)
                    sb.Append("oDateTo                                (pdat_to)             : " & oDateTo.d & "-" & oDateTo.t & vbCrLf)
                    sb.Append("dPriority                              (priority)            : " & dPriority.ToString & vbCrLf)
                    sb.Append("iVisibility                            (vis_flag)            : " & iVisibility.ToString & vbCrLf)
                    sb.Append("iAlternative_Flag                      (alterns_flag)        : " & iAlternative_Flag.ToString & vbCrLf)
                    sb.Append("iMaxResult_Flag                        (max_nresults)        : " & iMaxResult_Flag.ToString & vbCrLf)
                    sb.Append("iRetNoOfResults                        (pnresults)           : " & iRetNoOfResults.ToString & vbCrLf)
                    ''sb.Append("pstat                                  (pstat)               : " & pStatus.detail & "/" & pStatus.msg & "/" & pStatus.status & vbCrLf)
                    sb.Append("************************" & vbCrLf)
                    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                Catch ex As Exception
                    ''do nothing
                End Try



                Call objDVASSMgr.AvailQuery(iCountry, _
                                            sProdId, _
                                            1, _
                                            String.Empty, _
                                            sLocId_Fr, _
                                            oDateFr, _
                                            sLocId_To, _
                                            oDateTo, _
                                            dPriority, _
                                            iVisibility, _
                                            iAlternative_Flag, _
                                            iMaxResult_Flag, _
                                            iRetNoOfResults, _
                                            pStatus, _
                                            oRes)


                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("DVASS.AvailQuery RESULT" & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.Append("iCountry                               (icountry)            : " & iCountry & vbCrLf)
                sb.Append("sProdId                                (prodid)              : " & sProdId & vbCrLf)
                sb.Append("NumberOfVehicleTarget                  (nvehicles)           : " & NumberOfVehicleTarget & vbCrLf)
                sb.Append("CandidateVehicle                       (candidate_vehicles)  : " & "" & vbCrLf)
                sb.Append("sLocId_Fr                              (locid_fr)            : " & sLocId_Fr & vbCrLf)
                sb.Append("oDateFr                                (pdat_fr)             : " & oDateFr.d.ToString & "-" & oDateFr.t.ToString & vbCrLf)
                sb.Append("sLocId_To                              (locid_to)            : " & sLocId_To & vbCrLf)
                sb.Append("oDateTo                                (pdat_to)             : " & oDateTo.d.ToString & "-" & oDateTo.t.ToString & vbCrLf)
                sb.Append("dPriority                              (priority)            : " & dPriority.ToString & vbCrLf)
                sb.Append("iVisibility                            (vis_flag)            : " & iVisibility.ToString & vbCrLf)
                sb.Append("iAlternative_Flag                      (alterns_flag)        : " & iAlternative_Flag.ToString & vbCrLf)
                sb.Append("iMaxResult_Flag                        (max_nresults)        : " & iMaxResult_Flag.ToString & vbCrLf)
                sb.Append("iRetNoOfResults                        (pnresults)           : " & iRetNoOfResults.ToString & vbCrLf)
                ''sb.Append("pstat                                  (pstat)               : " & pStatus.detail & "/" & pStatus.msg & "/" & pStatus.status & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


                Dim xmlSbstring As New StringBuilder
                Dim strTemp As String = String.Empty

                If (dDate_Fr = dDate_To) Then
                    NumberOfVehicle = CInt(CStr(oRes(7, 0)))
                    xmlSbstring.AppendFormat(AVAILABLE_XML, NumberOfVehicle)
                Else
                    For lngResultIndex As Integer = 0 To iRetNoOfResults - 1

                        strTemp = String.Concat("<AvailableVehicle><PrdId>", CStr(oRes(0, lngResultIndex)), _
                                  "</PrdId><CkoLocation>", CStr(oRes(1, lngResultIndex)), _
                                  "</CkoLocation><CkoDate>", convertDVASSDateToVBDate(CLng(oRes(2, lngResultIndex))), _
                                  "</CkoDate><CkiLocation>", CStr(oRes(3, lngResultIndex)), _
                                  "</CkiLocation><CkiDate>", convertDVASSDateToVBDate(CLng(oRes(4, lngResultIndex))), _
                                  "</CkiDate><Preference>", CStr(oRes(5, lngResultIndex)), _
                                  "</Preference><Cost>", CStr(oRes(6, lngResultIndex)), _
                                  "</Cost><NumberOfVehicles>", CStr(oRes(7, lngResultIndex)), _
                                  "</NumberOfVehicles><ReducesRelocationCount>", CStr(oRes(8, lngResultIndex)), _
                                  "</ReducesRelocationCount></AvailableVehicle>")

                        xmlSbstring.Append(strTemp)

                    Next
                End If


                If iRetNoOfResults > 0 Then
                    IsWSDvassAvailable = True
                    sReturnMessage = String.Concat("<Data>", xmlSbstring.ToString, "</Data>")

                Else
                    sReturnMessage = String.Empty
                    IsWSDvassAvailable = False
                End If

                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                sb.Append("NOTE: NumberOfVehicle of vehicle return by AvailQuery is " & NumberOfVehicle & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


            Catch ex As Exception


                IsWSDvassAvailable = False


                sReturnMessage = "<Data>" & _
                                           "<Status>ERROR</Status>" & _
                                           "<Result>False</Result>" & _
                                           "<Message>" & ex.Message & " - " & ex.StackTrace.Replace("\", "").Replace("&", "") & "</Message>" & _
                                        "</Data>"

                sb.Append("************************" & vbCrLf)
                sb.Append("Exception  caught in IsWSDvassAvailable    : ".ToUpper & sReturnMessage & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)



            End Try

            loggingmessages = sb.ToString

            ''Logging.LogInformation("IsWSDvassAvailable", loggingmessages)

            Return IsWSDvassAvailable
        End Function
    End Class

End Namespace
