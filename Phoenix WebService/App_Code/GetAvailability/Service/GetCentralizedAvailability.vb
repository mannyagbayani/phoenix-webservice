﻿Imports Microsoft.VisualBasic
Imports COMDVASSMgrLib
Imports Aurora.Reservations.Services
Imports Aurora.Common
Imports PhoenixWebService
Imports System.Xml

Namespace WS.CentralizedAvailability

    Public Class GetCentralizedAvailability
        Implements CentralizedAvailability.IGetCentralizedAvailability

        Public Function GetAvailability(ByVal Brand As String, ByVal CountryCode As String, ByVal VehicleCode As String, ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As Date, ByVal CheckInZoneCode As String, ByVal CheckInDateTime As Date, ByVal NoOfAdults As Short, ByVal NoOfChildren As Short, ByVal AgentCode As String, ByVal PackageCode As String, ByVal IsVan As Boolean, ByVal IsBestBuy As Boolean, ByVal CountryOfResidence As String, ByVal IsTestMode As Boolean, ByVal IsGross As Boolean) As System.Xml.XmlDocument Implements IGetCentralizedAvailability.GetAvailability
            Return (New PhoenixWebService).GetAvailability(Brand, _
                                      CountryCode, _
                                      VehicleCode, _
                                      CheckOutZoneCode, _
                                      CheckOutDateTime, _
                                      CheckInZoneCode, _
                                      CheckInDateTime, _
                                      NoOfAdults, _
                                      NoOfChildren, _
                                      AgentCode, _
                                      PackageCode, _
                                      IsVan, _
                                      IsBestBuy, _
                                      CountryOfResidence, _
                                      IsTestMode, _
                                      IsGross, _
           True, _
           "")
        End Function

        Public Function GetAvailabilityV2(Brand As String, CountryCode As String, VehicleCode As String, CheckOutZoneCode As String, CheckOutDateTime As Date, CheckInZoneCode As String, CheckInDateTime As Date, NoOfAdults As Short, NoOfChildren As Short, AgentCode As String, PackageCode As String, IsVan As Boolean, IsBestBuy As Boolean, CountryOfResidence As String, IsTestMode As Boolean, IsGross As Boolean, ChannelRequest As Integer, IsQuickAvail As Boolean) As System.Xml.XmlDocument Implements IGetCentralizedAvailability.GetAvailabilityV2
            Dim ErrorMessage As String = ""
            Dim DvassNumber As Long

            Const FUNCTION_NAME As String = "GetCentralizedAvailability.GetAvailabilityV2"
            Dim xmlAvailability As New XmlDocument
            Dim result As String = String.Empty
            Dim errormsgthrow As String = String.Empty
            Dim sb As New StringBuilder
            Dim loggingmessages As String = String.Empty

            Try

            
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                sb.Append("************************************************ S T A R T *********************************************************" & vbCrLf)
            sb.Append("Rev:MIA NOV.22 2012: GetAvailabilityV2 stories in everyday request. Have fun tracing".ToUpper & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.Append("1.Calling GetAvailabilityV2 ".ToUpper & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.Append("INPUT PARAMETERS" & vbCrLf)
            sb.Append("Brand                : " & Brand & vbCrLf)
            sb.Append("VehicleCode          : " & VehicleCode & vbCrLf)
            sb.Append("CountryCode          : " & CountryCode & vbCrLf)
            sb.Append("CheckOutZoneCode     : " & CheckOutZoneCode & vbCrLf)
            sb.Append("CheckOutDateTime     : " & CheckOutDateTime & vbCrLf)
            sb.Append("CheckInZoneCode      : " & CheckInZoneCode & vbCrLf)
            sb.Append("CheckInDateTime      : " & CheckInDateTime & vbCrLf)
            sb.Append("NoOfAdults           : " & NoOfAdults & vbCrLf)
            sb.Append("NoOfChildren         : " & NoOfChildren & vbCrLf)
            sb.Append("AgentCode            : " & AgentCode & vbCrLf)
            sb.Append("PackageCode          : " & PackageCode & vbCrLf)
            sb.Append("IsVan                : " & IsVan & vbCrLf)
            sb.Append("IsBestBuy            : " & IsBestBuy & vbCrLf)
            sb.Append("CountryOfResidence   : " & CountryOfResidence & vbCrLf)
            sb.Append("IsTestMode           : " & IsTestMode & vbCrLf)
            sb.Append("IsGross              : " & IsGross & vbCrLf)
            sb.Append("ChannelRequest       : " & ChannelRequest & vbCrLf)
            sb.Append("IsQuickAvail         : " & IsQuickAvail & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


            ''channelRequest coding
            ''0 = request came from aurora
            ''1 = request came from b2c
            ''3 = request came from b2b

            ''all channelrequest that is greater than zero will go to the 
            ''old GetAavailability. 
            If (ChannelRequest > 0) Then
                sb.Append("NOTE: all channelrequest that is GREATER THAN ZERO will go to OLD GetAvailability." & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


                sb.Append("************************" & vbCrLf)
                sb.Append("2.Calling GetAvailability ".ToUpper & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)



                    xmlAvailability = GetAvailability(Brand, _
                                          CountryCode, _
                                          VehicleCode, _
                                          CheckOutZoneCode, _
                                          CheckOutDateTime, _
                                          CheckInZoneCode, _
                                          CheckInDateTime, _
                                          NoOfAdults, _
                                          NoOfChildren, _
                                          AgentCode, _
                                          PackageCode, _
                                          IsVan, _
                                          IsBestBuy, _
                                          CountryOfResidence, _
                                          IsTestMode, _
                                          IsGross)

                    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                    sb.Append("*********************************************************************************************************" & vbCrLf)
                    sb.Append("NOTE: GetAvailabilityV2 final xml:  ".ToUpper & vbCrLf)
                    sb.Append("*********************************************************************************************************" & vbCrLf)
                    sb.Append(vbCrLf & xmlAvailability.OuterXml & vbCrLf & vbCrLf)
                    sb.Append("************************************************ E N D *********************************************************" & vbCrLf)
                    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                    Logging.LogInformation(FUNCTION_NAME, sb.ToString)
                    Return xmlAvailability
            End If
            sb.Append("NOTE: all channelrequest that is EQUAL TO zero will go to the NEW GetAvailability." & vbCrLf)
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

            sb.Append("************************" & vbCrLf)
            sb.Append("2.Calling GetFleetStatus ".ToUpper & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                Dim avail As Char = GetFleetStatus(VehicleCode, _
                                                   CheckOutZoneCode, _
                                                   CheckOutDateTime, _
                                                   CheckInZoneCode, _
                                                   CheckInDateTime, _
                                                   AgentCode, _
                                                   PackageCode, _
                                                   ErrorMessage, _
                                                   DvassNumber)

            sb.Append("NOTE: output parameters of GetFleetStatus." & vbCrLf)
            sb.Append("Avail                : " & avail & vbCrLf)
            sb.Append("ErrorMessage         : " & ErrorMessage & vbCrLf)
            sb.Append("DvassNumber          : " & DvassNumber & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)


            Dim defaultavail As String = ConfigurationManager.AppSettings("availCodeDefault").ToString
            If (Not String.IsNullOrEmpty(defaultavail) And String.IsNullOrEmpty(avail)) Then
                sb.Append("NOTE: [Avail] returns empty. Program will now use the default Avail from Web.config which is [" & defaultavail & "]" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                avail = defaultavail
            End If

            Dim NoOfVehiclesLookUp As Integer = 20
            If (IsQuickAvail = True) Then
                sb.Append("NOTE: we are not interested in the error message if the request is for Quick Availability so lets remove them " & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                ErrorMessage = ""
                NoOfVehiclesLookUp = 1
                    avail = "R"
                    sb.Append("NOTE: If its Quick Availability, we need to set the NoOfVehiclesLookUp = 1 and Avail = 'R'" & vbCrLf)
            End If

                loggingmessages = sb.ToString


            If (Not String.IsNullOrEmpty(avail)) Then
                result = WSexecute(avail, _
                                CountryCode, _
                                DvassNumber.ToString(), _
                                CheckOutZoneCode, _
                                CheckOutDateTime, _
                                CheckInZoneCode, _
                                CheckInDateTime, _
                                VehicleCode, _
                                NoOfVehiclesLookUp, _
                                AgentCode, _
                                Brand, _
                                IsQuickAvail, _
                                loggingmessages)

                Try
                    xmlAvailability.LoadXml(result)
                Catch ex As Exception

                    errormsgthrow = ex.Message
                    If (errormsgthrow.Contains("<Data>") = False) Then
                        result = "<Data>" & _
                                              "<Status>ERROR</Status>" & _
                                              "<Result>False</Result>" & _
                                              "<Message>" & ex.Message & " - " & ex.StackTrace.Replace("\", "").Replace("&", "") & "</Message>" & _
                                              "<MannyNote>Error message was thrown somewhere else.We need to check that a valid xml is being return even if there are some errors. </MannyNote>" & _
                                           "</Data>"

                 
                    End If

                    sb = New StringBuilder
                    sb.Append("************************" & vbCrLf)
                        sb.Append("Exception #2 caught in GetAvailabilityV2. Function will return this XML    : ".ToUpper & result & vbCrLf)
                    sb.Append("************************" & vbCrLf)

                    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                        sb.Append("************************************************ E N D *********************************************************" & vbCrLf)


                    xmlAvailability.LoadXml(result)
                    loggingmessages = String.Concat(loggingmessages, sb.ToString)

                    Logging.LogInformation(FUNCTION_NAME & " WITH ERROR:", loggingmessages)
                    Return xmlAvailability

                End Try

                sb = New StringBuilder
                    sb.Append(loggingmessages)

                    If (result.Contains("ERROR") = False) Then

                        ''rev:mia Nov.22, 2012 - THIS IS FINAL XML THAT WILL BE RENDER IN AURORA CALL
                        ''                     - I'm going to attend oathtaking ceremony in one tree hill college
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                        sb.Append("*********************************************************************************************************" & vbCrLf)
                        sb.Append("NOTE: GetAvailabilityV2 final xml:  ".ToUpper & vbCrLf)
                        sb.Append("*********************************************************************************************************" & vbCrLf)
                        sb.Append(vbCrLf & result & vbCrLf & vbCrLf)
                        sb.Append("************************************************ E N D *********************************************************" & vbCrLf)
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                        loggingmessages = sb.ToString

                        ''Logging.LogInformation("v2 - a", loggingmessages)
                    Else

                        sb.Append("************************" & vbCrLf)
                        sb.Append("Exception #2  caught in GetAvailabilityV2. Function will return this XML   : ".ToUpper & result & vbCrLf)
                        sb.Append("************************" & vbCrLf)
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
                        sb.Append("************************************************ E N D *********************************************************" & vbCrLf)

                        xmlAvailability.LoadXml(result)
                        loggingmessages = sb.ToString

                        Logging.LogInformation(FUNCTION_NAME & " WITH ERROR:", loggingmessages)
                        Return xmlAvailability

                    End If

                End If

            Catch ex As Exception

                errormsgthrow = ex.Message
                If (errormsgthrow.Contains("<Data>") = False) Then
                    result = "<Data>" & _
                                          "<Status>ERROR</Status>" & _
                                          "<Result>False</Result>" & _
                                          "<Message>" & ex.Message & " - " & ex.StackTrace.Replace("\", "").Replace("&", "") & "</Message>" & _
                                          "<MannyNote>Error message was thrown somewhere else.We need to check that a valid xml is being return even if there are some errors. </MannyNote>" & _
                                       "</Data>"

                    xmlAvailability.LoadXml(result)
                    loggingmessages = String.Concat(loggingmessages, xmlAvailability.OuterXml)
                End If
            End Try

            Logging.LogInformation(FUNCTION_NAME, loggingmessages)
            Return xmlAvailability

        End Function

        

        Public Function GetFleetStatus(ProductShortName As String, CheckOutLocationCode As String, CheckOutDate As Date, CheckInLocationCode As String, CheckInDate As Date, AgentCode As String, PackageCode As String, ByRef ErrorMessage As String, ByRef DvassSequenceNumber As Long) As Char Implements IGetCentralizedAvailability.GetFleetStatus
            Return (New PhoenixWebService).GetFleetStatus(ProductShortName, _
                                             CheckOutLocationCode, _
                                             CheckOutDate, _
                                             CheckInLocationCode, _
                                             CheckInDate, _
                                             AgentCode, _
                                             PackageCode, _
                                             ErrorMessage, _
                                             DvassSequenceNumber)
        End Function

        Public Function GetAndSkipBlockingRuleForFleetStatus(ProductShortName As String, CheckOutLocationCode As String, CheckOutDate As Date, CheckInLocationCode As String, CheckInDate As Date, AgentCode As String, PackageCode As String, ByRef ErrorMessage As String, ByRef DvassSequenceNumber As Long) As Char
            Return WebServiceData.GetAndSkipBlockingRuleForFleetStatus(ProductShortName, _
                                             CheckOutLocationCode, _
                                             CheckOutDate, _
                                             CheckInLocationCode, _
                                             CheckInDate, _
                                             AgentCode, _
                                             PackageCode, _
                                             ErrorMessage, _
                                             DvassSequenceNumber)
        End Function


        Public Function WSexecute(AvailCode As String, _
                                  CountryCode As String, _
                                  ProductId As String, _
                                  CheckOutLocationCode As String, _
                                  CheckOutDate As Date, _
                                  CheckInLocationCode As String, _
                                  CheckInDate As Date, _
                                  ProductShortName As String, _
                                  NoOfVehiclesLookUp As Integer, _
                                  AgentId As String, _
                                  BrdCode As String, _
                                  isQuickAvail As Boolean, _
                                  ByRef loggingmessages As String) As String Implements IGetCentralizedAvailability.WSexecute
            Dim result As String = String.Empty
            Const NOT_VALID As String = "Warning: Not valid Availability Code"""

            Dim sb As New StringBuilder
            sb.Append("************************" & vbCrLf)
            sb.Append("3.Calling WSexecute ".ToUpper & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.Append("FUNCTION PARAMETERS" & vbCrLf)
            sb.Append("CountryCode          : " & CountryCode & vbCrLf)
            sb.Append("DvassSequenceNumber  : " & ProductId & vbCrLf)
            sb.Append("CheckOutLocationCode : " & CheckOutLocationCode & vbCrLf)
            sb.Append("CheckOutDate         : " & CheckOutDate.ToString & vbCrLf)
            sb.Append("CheckInLocationCode  : " & CheckInLocationCode & vbCrLf)
            sb.Append("CheckInDate          : " & CheckInDate.ToString & vbCrLf)
            sb.Append("ProductShortName     : " & ProductShortName & vbCrLf)
            sb.Append("NoOfVehiclesLookUp   : " & NoOfVehiclesLookUp & vbCrLf)
            sb.Append("AgentId              : " & AgentId & vbCrLf)
            sb.Append("BrdCode              : " & BrdCode & vbCrLf)
            sb.Append("************************" & vbCrLf)
            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)




            Try
                Select Case AvailCode

                    Case "R" ''call dvass

                        sb.Append("NOTE: Inside select Statement case: 'R' DVASS :" & vbCrLf)
                        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                        loggingmessages = String.Concat(loggingmessages, sb.ToString)
                        sb = New StringBuilder


                        Dim dvassInformation As New DvassInformation(NoOfVehiclesLookUp)



                        Dim xmlResult As New XmlDocument
                        xmlResult.LoadXml(dvassInformation.GetResult(CountryCode, _
                                                                      ProductId, _
                                                                      CheckOutLocationCode, _
                                                                      CheckOutDate, _
                                                                      CheckInLocationCode, _
                                                                      CheckInDate, _
                                                                      loggingmessages).OuterXml)


                        result = xmlResult.OuterXml


                    Case "E"  ''call Eurocar

                    Case "F"  ''Free Cell

                        sb.Append("Inside select Statement case: 'F':Free Sale " & vbCrLf)
                        result = "Y"

                    Case "X"  ''Stop Cell

                        sb.Append("Inside select Statement case: 'X':Stop Sale " & vbCrLf)
                        result = "N"

                    Case "M"  ''On Request

                        sb.Append("Inside select Statement case: 'M':On Request " & vbCrLf)
                        result = "N"

                    Case "Y"  ''available

                        sb.Append("Inside select Statement case: 'Y':Available " & vbCrLf)
                        result = "Y"

                    Case Else  ''other

                        sb.Append(":Else Part " & vbCrLf)
                        result = NOT_VALID

                End Select

            Catch ex As Exception

                ''we need to check that a valid xml is being return 
                ''even if there are some errors
                Dim errormsgthrow As String = ex.Message
                If (errormsgthrow.Contains("<Data>") = False) Then
                    result = "<Data>" & _
                                          "<Status>ERROR</Status>" & _
                                          "<Result>False</Result>" & _
                                          "<Message>" & ex.Message & " - " & ex.StackTrace.Replace("\", "").Replace("&", "") & "</Message>" & _
                                          "<MannyNote>Error message was thrown somewhere else.We need to check that a valid xml is being return even if there are some errors. </MannyNote>" & _
                                       "</Data>"

                  End If

                sb = New StringBuilder
                sb.Append(loggingmessages)

                sb.Append("************************" & vbCrLf)
                sb.Append("Exception  caught in WSExecute        : ".ToUpper & result & vbCrLf)
                sb.Append("************************" & vbCrLf)
                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)

                loggingmessages = String.Concat(loggingmessages, sb.ToString)

                
            End Try

            ''Logging.LogInformation("WsExecute", loggingmessages)
            Return result
        End Function

#Region "Private Properties"
        Private Overloads Shared Function convertDVASSDateToVBDate(ByVal pDate As Long) As Date
            Return DateAdd(DateInterval.Day, pDate, New Date(2000, 1, 1))
        End Function
#End Region


    End Class

End Namespace
