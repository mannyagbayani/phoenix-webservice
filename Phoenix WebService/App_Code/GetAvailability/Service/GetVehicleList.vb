﻿Imports Microsoft.VisualBasic
Imports System.Xml
Imports WS.Caching

Namespace WS.CentralizedAvailability

    Public Class GetVehicleList
        Implements IVehicleList

        Private _AvailabilityInDays As Integer
        Dim centralizedAvail As GetCentralizedAvailability

        Sub New(checknoOfDays As Integer)
            _AvailabilityInDays = checknoOfDays
            centralizedAvail = New GetCentralizedAvailability
        End Sub

        Public Property AvailabilityInDays As Integer Implements IVehicleList.AvailabilityInDays
            Get
                Return _AvailabilityInDays
            End Get
            Set(value As Integer)
                _AvailabilityInDays = value
            End Set
        End Property

        Public Function GetAvailabilitiesBasedOnDays(Brand As String, _
                                        CountryCode As String, _
                                        VehicleCode As String, _
                                        CheckOutZoneCode As String, _
                                        CheckOutDateTime As Date, _
                                        CheckInZoneCode As String, _
                                        CheckInDateTime As Date, _
                                        NoOfAdults As Short, _
                                        NoOfChildren As Short, _
                                        AgentCode As String, _
                                        PackageCode As String, _
                                        IsVan As Boolean, _
                                        IsBestBuy As Boolean, _
                                        CountryOfResidence As String, _
                                        IsTestMode As Boolean, _
                                        IsGross As Boolean) As System.Xml.XmlDocument Implements IVehicleList.GetAvailabilitiesBasedOnDays


            Dim sbkey As New StringBuilder
            Dim xmlAvailability As New XmlDocument

            If (Not String.IsNullOrEmpty(Brand)) Then
                sbkey.AppendFormat("BRD_{0}_", Brand)
            End If

            If (Not String.IsNullOrEmpty(CountryCode)) Then
                sbkey.AppendFormat("CTY_{0}_", CountryCode)
            End If

            If (Not String.IsNullOrEmpty(VehicleCode)) Then
                sbkey.AppendFormat("VEHICLE_{0}_", VehicleCode)
            End If


            If (Not String.IsNullOrEmpty(CheckOutZoneCode)) Then
                sbkey.AppendFormat("CKO_{0}_", CheckOutZoneCode)
            End If

            If (Not String.IsNullOrEmpty(CheckOutDateTime)) Then
                sbkey.AppendFormat("CKODATE_{0}_", CheckOutDateTime.ToShortDateString.Replace("/", ""))
            End If

            If (Not String.IsNullOrEmpty(CheckInZoneCode)) Then
                sbkey.AppendFormat("CKI_{0}_", CheckInZoneCode)
            End If

            If (Not String.IsNullOrEmpty(CheckInDateTime)) Then
                sbkey.AppendFormat("CKIDATE_{0}_", CheckInDateTime.ToShortDateString.Replace("/", ""))
            End If

            If (NoOfAdults > 0) Then
                sbkey.AppendFormat("ADLT_{0}_", NoOfAdults)
            End If

            If (NoOfChildren > 0) Then
                sbkey.AppendFormat("CHLD_{0}_", NoOfChildren)
            End If

            If (Not String.IsNullOrEmpty(AgentCode)) Then
                sbkey.AppendFormat("AGENT_{0}_", AgentCode)
            End If

            If (Not String.IsNullOrEmpty(PackageCode)) Then
                sbkey.AppendFormat("PKG_{0}_", PackageCode)
            End If

            If (IsVan = True) Then
                sbkey.AppendFormat("VAN_{0}_", True)
            End If

            If (IsBestBuy = True) Then
                sbkey.AppendFormat("BESTBUY_{0}_", IsBestBuy)
            End If

            If (IsTestMode = True) Then
                sbkey.AppendFormat("TEST_{0}_", IsTestMode)
            End If

            If (IsGross = True) Then
                sbkey.AppendFormat("GROSS_{0}_", IsGross)
            End If

            If (Not String.IsNullOrEmpty(CountryOfResidence)) Then
                sbkey.AppendFormat("CTYRES_{0}_", CountryOfResidence)
            End If

            Try
                If (Not ConfigurationManager.AppSettings("cachealways_auroravehiclegrid") Is Nothing) Then
                    If (ConfigurationManager.AppSettings("cachealways_auroravehiclegrid").ToString = "true") Then
                        sbkey.Append("CACHEALWAYS")
                    Else
                        sbkey.Append("NOCACHE")
                    End If
                End If
            Catch ex As Exception
                Helper.LogInformation("GetAvailabilitiesBasedOnDays", "ConfigurationManager.AppSettings(cachealways_auroravehiclegrid) dont exist in web.config")
            End Try
            

            Dim key As String = sbkey.ToString
            Dim _cacheservice As New CacheService(Nothing, CachingObjectType.strings)
            If _cacheservice.isExist(key) Then
                xmlAvailability.LoadXml(_cacheservice.Retrieve(key))
                Return xmlAvailability
            End If

            Const XML_HEADER As String = "<data><vehiclecode code='{0}' from='{1}' to='{2}' noofdays='{3}' origDateFrom='{4}' origDateTo='{5}' dayIncrement='0'>"
            Dim loggingmessages As String = String.Empty
            Dim ErrorMessage As String = String.Empty
            Dim result As String = String.Empty

            Dim errormsgthrow As String = String.Empty
            Dim sb As New StringBuilder

            Dim availabilityXML As StringBuilder = New StringBuilder
            ''availabilityXML.AppendFormat("<data><vehiclecode code='{0}' from='{1}' to='{2}' noofdays='{3}' origDateFrom='{4}' origDateTo='{5}'>", VehicleCode, CheckOutZoneCode, CheckInZoneCode, AvailabilityInDays, CheckOutDateTime, CheckInDateTime)
            availabilityXML.AppendFormat(XML_HEADER, VehicleCode, CheckOutZoneCode, CheckInZoneCode, AvailabilityInDays, CheckOutDateTime, CheckInDateTime)
            Dim DvassNumber As Long

            '' Dim counter As Integer = 0
            Dim avail As Char = String.Empty
            Dim dateFrom As DateTime = Convert.ToDateTime(CheckOutDateTime).AddDays(-AvailabilityInDays)
            Dim EndingDateTo As Date = Convert.ToDateTime(CheckInDateTime).AddDays(AvailabilityInDays)

            If (dateFrom < DateTime.Now) Then
                Helper.LogInformation("GetAvailabilitiesBasedOnDays", Helper.MessageInformationFormat("GetAvailabilitiesBasedOnDays", "Starting date is less than current date", dateFrom, " use the current date and start from here ", DateTime.Now))
                dateFrom = String.Concat(DateTime.Now.ToString("dd/MMM/yyyy"), " ", dateFrom.ToShortTimeString) '' DateTime.Now.ToString("dd/MMM/yyyy 08:00:00")
            End If
            ''Helper.LogInformation("GetAvailabilitiesBasedOnDays", Helper.MessageInformationFormat("GetAvailabilitiesBasedOnDays - original date ", "CheckOutDateTime", CheckOutDateTime, "CheckInDateTime", CheckInDateTime))


            Do While EndingDateTo >= dateFrom
                If (dateFrom > EndingDateTo) Then Exit Do
                avail = GetFleetStatus(VehicleCode, _
                                                       CheckOutZoneCode, _
                                                       dateFrom, _
                                                       CheckInZoneCode, _
                                                       dateFrom, _
                                                       AgentCode, _
                                                       PackageCode, _
                                                       ErrorMessage, _
                                                       DvassNumber)

                Helper.LogInformation("GetAvailabilitiesBasedOnDays", BuildAvaibilityXML(dateFrom.ToString("dd/MM/yyyy"), _
                                                                               String.Empty, _
                                                                               CheckOutZoneCode, _
                                                                               CheckInZoneCode, _
                                                                               Nothing, _
                                                                               ErrorOrWarningMessage:=ErrorMessage))
                If avail.Equals("R"c) = True Then
                    result = centralizedAvail.WSexecute(avail, _
                             CountryCode, _
                             DvassNumber.ToString(), _
                             CheckOutZoneCode, _
                             dateFrom, _
                             CheckInZoneCode, _
                             dateFrom, _
                             VehicleCode, _
                             1, _
                             AgentCode, _
                             Brand, _
                             False, _
                             loggingmessages)

                    ''Helper.LogInformation("GetAvailabilitiesBasedOnDays", Helper.MessageInformationFormat("GetAvailabilitiesBasedOnDays", "centralizedAvail.WSexecute", result))

                    availabilityXML.AppendFormat(ExtractAvailabilityFromDvass(result, _
                                                                              dateFrom.ToString("dd/MM/yyyy"), _
                                                                              String.Empty, _
                                                                              CheckOutZoneCode, _
                                                                              CheckInZoneCode, _
                                                                              avail, _
                                                                              ErrorOrWarningMessage:=ErrorMessage))
                Else
                    ''check first for TANDC or BLOCKINGRULE
                    If (Not String.IsNullOrEmpty(ErrorMessage)) Then

                        If (ErrorMessage.Contains("BLOCKINGRULE") = True) Then

                            availabilityXML.AppendFormat("{0}", BuildAvaibilityXML(dateFrom.ToString("dd/MM/yyyy"), _
                                                                                   String.Empty, _
                                                                                   CheckOutZoneCode, _
                                                                                   CheckInZoneCode, _
                                                                                   Nothing, _
                                                                                   ErrorOrWarningMessage:=ErrorMessage))

                        ElseIf (ErrorMessage.Contains("TANDC") = True And avail.Equals("Y"c) = True) Then
                            availabilityXML.AppendFormat("{0}", BuildAvaibilityXML(dateFrom.ToString("dd/MM/yyyy"), _
                                                                           String.Empty, _
                                                                           CheckOutZoneCode, _
                                                                           CheckInZoneCode, _
                                                                           "Y", _
                                                                           ErrorOrWarningMessage:=ErrorMessage))
                        ElseIf (ErrorMessage.Contains("TANDC") = True And avail.Equals("N"c) = True) Then
                            availabilityXML.AppendFormat("{0}", BuildAvaibilityXML(dateFrom.ToString("dd/MM/yyyy"), _
                                                                               String.Empty, _
                                                                               CheckOutZoneCode, _
                                                                               CheckInZoneCode, _
                                                                               "N", _
                                                                               ErrorOrWarningMessage:=ErrorMessage))
                        End If

                    Else
                        availabilityXML.AppendFormat("{0}", BuildAvaibilityXML(dateFrom.ToString("dd/MM/yyyy"), _
                                                                           String.Empty, _
                                                                           CheckOutZoneCode, _
                                                                           CheckInZoneCode, _
                                                                           IIf(avail = Nothing, "X", avail), _
                                                                           ErrorOrWarningMessage:=ErrorMessage))
                    End If '' If (Not String.IsNullOrEmpty(ErrorMessage)) Then
                    
                End If






                ''Helper.LogInformation("GetAvailabilitiesBasedOnDays", Helper.MessageInformationFormat("GetAvailabilitiesBasedOnDays", "counter", counter, "CheckOutDateTime", dateFrom, "CheckInDateTime", dateFrom, "Avail", avail))
                ''counter = counter + 1
                dateFrom = Convert.ToDateTime(dateFrom).AddDays(1)
            Loop

            availabilityXML.Append("</vehiclecode></data>")

            Helper.LogInformation("GetAvailabilitiesBasedOnDays", Helper.MessageInformationFormat("GetAvailabilitiesBasedOnDays XML", "availabilityXML", availabilityXML.ToString))
            _cacheservice.Add(key, availabilityXML.ToString)

            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(availabilityXML.ToString)


            Return xmldoc

        End Function

        Public Function GetFleetStatus(ProductShortName As String, _
                                       CheckOutLocationCode As String, _
                                       CheckOutDate As Date, _
                                       CheckInLocationCode As String, _
                                       CheckInDate As Date, _
                                       AgentCode As String, _
                                       PackageCode As String, _
                                       ByRef ErrorMessage As String, _
                                       ByRef DvassSequenceNumber As Long) As Char Implements IVehicleList.GetFleetStatus

            Return centralizedAvail.GetAndSkipBlockingRuleForFleetStatus(ProductShortName, _
                                             CheckOutLocationCode, _
                                             CheckOutDate, _
                                             CheckInLocationCode, _
                                             CheckInDate, _
                                             AgentCode, _
                                             PackageCode, _
                                             ErrorMessage, _
                                             DvassSequenceNumber)
        End Function

        Private Function BuildAvaibilityXML(datefrom As String, _
                                            dateto As String, _
                                            locfrom As String, _
                                            locto As String, _
                                            avail As String, _
                                            Optional ErrorOrWarningMessage As String = "" _
                                            ) As String
            Dim sb As New StringBuilder
            Dim bgcolor As String = ""
            Const FINAL_XML As String = "<availability  fr='{0}'>{1}</availability>"
            Const FINAL_XML_WITH_ERROR As String = "<availability  fr='{0}' rule='{1}'>{2}</availability>"
            If (Not String.IsNullOrEmpty(ErrorOrWarningMessage)) Then

                If (ErrorOrWarningMessage.Contains("BLOCKINGRULE") = True) Then
                    Return sb.AppendFormat(FINAL_XML_WITH_ERROR, datefrom, SanitizedBlockingrules(ErrorOrWarningMessage), Nothing).ToString
                ElseIf (ErrorOrWarningMessage.Contains("TANDC") = True) Then
                    Return sb.AppendFormat(FINAL_XML_WITH_ERROR, datefrom, SanitizedBlockingrules(ErrorOrWarningMessage), avail).ToString
                Else
                    Return sb.AppendFormat(FINAL_XML, datefrom, avail).ToString
                End If

            Else
                Return sb.AppendFormat(FINAL_XML, datefrom, avail).ToString
            End If

        End Function

        Private Function ExtractAvailabilityFromDvass(xml As String, _
                                                      datefrom As String, _
                                                      dateto As String, _
                                                      locfrom As String, _
                                                      locto As String, _
                                                      avail As String, _
                                                      Optional origDateFrom As String = "", _
                                                      Optional origDateTo As String = "", _
                                                      Optional ErrorOrWarningMessage As String = "") As String
            Dim xmldoc As New XmlDocument
            Dim noOfAvailable As String = "0"
            Dim bgcolor As String = "#ccffcc"

            Const XPATH As String = "Data/AvailableVehicle/NumberOfVehicles"
            Const ERROR_XML As String = "<availability fr='{0}'  rule='{1}' >{2}</availability>"
            Const FINAL_XML As String = "<availability fr='{0}'  no='{1}' rule='{2}'>{3}</availability>"

            Try
                xmldoc.LoadXml(xml)
                If (xmldoc.SelectSingleNode(XPATH) Is Nothing) Then
                    avail = "N"
                Else
                    noOfAvailable = xmldoc.SelectSingleNode(XPATH).InnerText
                    If (Not String.IsNullOrEmpty(noOfAvailable)) Then
                        avail = IIf(CInt(noOfAvailable) > 0, "Y", "N")
                    Else
                        avail = "N"
                        noOfAvailable = "0"
                    End If
                End If

            Catch ex As Exception
                Dim errorText As String = ex.Message.Replace("/", "").Replace("'", "").Replace("""", "") & " " & ex.StackTrace.Replace("/", "").Replace("'", "").Replace("""", "")
                Return (New StringBuilder).AppendFormat(ERROR_XML, datefrom, errorText, avail).ToString

            End Try

            Return (New StringBuilder).AppendFormat(FINAL_XML, datefrom, _
                                                    noOfAvailable, _
                                                    IIf(Not String.IsNullOrEmpty(ErrorOrWarningMessage), _
                                                        SanitizedBlockingrules(ErrorOrWarningMessage), _
                                                        String.Empty), _
                                                    avail).ToString
        End Function

        Private Function SanitizedBlockingrules(textMessage As String) As String

            If (textMessage.Contains("&") = False And textMessage.Contains("'") = False And textMessage.Contains("\") = False And textMessage.Contains("/") = False And textMessage.Contains("""") = False) Then Return textMessage
            Dim txttosanitized As String = String.Empty

            Try
                txttosanitized = textMessage.Replace("'", "").Replace("""", "").Replace("&", "").Replace("\", "").Replace("/", "")
            Catch ex As Exception
                txttosanitized = "Error: SanitizedBlockingrules"
            End Try

            Return txttosanitized
        End Function
    End Class

End Namespace