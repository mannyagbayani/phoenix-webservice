﻿'/////////////////////////////////////////////////////////////////////////////////////////////////////////
'//THE WEBSERVICE for B2C                                                                               //
'/////////////////////////////////////////////////////////////////////////////////////////////////////////
'//VERSION HIST                                                                                         //
'//                                                                                                     //
'//REVISION DATE        USER                    COMMENTS                                                //       
'//-----------------------------------------------------------------------------------------------------//
'// 1       5.3.9       Shoel                   Created - Contains master xml + conf sending code!      //
'// 2       20.4.9      Shoel                   Best buy functionality added, aspx page added           //
'// 3       21.4.9      Shoel                   Best buy now uses tctcode + zone code to get loc cod    //
'// 4       22.4.9      Shoel                   Experimental stuff to add Extra Hire Items now included //
'//                                             Fix for confirmation sending not adding history         //
'// 5       20.5.9      Shoel                   Massive re-write for GetAvailability                    //
'// 6       9.6.9       Shoel                   Fix in place for missing error list items               //
'// 7       17.6.9      Shoel                   Fix for spurious availability results added             //
'// 8       19.6.9      Shoel                   ForceCreateBooking - new webmethod to create            //
'//                                             on-req bookings added                                   //
'// 9       23.6.9      Shoel                   GetAvailability now puts non-avail vehicles in the      //
'//                                             general data alongside valid vehicle data, it includes  //
'//                                             xml nodes that mark the problem as general avail check  //
'//                                             or Blocking rule                                        //
'// 10      23.6.9      Shoel                   change to output xml format in GetAvailability          //
'// 11      24.6.9      Shoel                   GetAvailability - Error node attribute 'Allow'          //
'//                                             now contains false/true                                 //
'// 12      24.6.9      Shoel                   GetAvailability - Errors are now of type Generic/T&C    //
'// 13      25.6.9      Shoel                   GetAvailability - Unavaliable messages standardised     //
'// 14      25.6.9      Shoel                   GetAvailability - bug with multi brand search that led  //
'//                                             to unnecessary calls after location test failed,        //
'//                                             has been fixed. Error list logic improved               //
'// 15      26.6.9      Shoel                   VEHICLE RATES UNAVAILABLE message moved to web.config   //
'// 16      26.6.9      Shoel                   CountryCode also used to validate locations in GetAvail //
'// 17      8.7.9       Shoel                   Better quality Blocking rule messages are now sent out  //
'// 18      14.7.9      Shoel                   GetInsuranceAndExtraHireItems now returns the details   //
'//                                             of the Inclusive package as well                        //
'// 19      16.7.9      Shoel                   Blocking rule message display logic cleaned up          //
'// 20      16.7.9      Shoel                   Problems with FerryVeh and FerryPax are now fixed       //
'// 21      16.7.9      Shoel                   Bug which caused GetInsuranceAndExtraHireItems to fail  //
'//                                             for packages that had no inclusive version is now fixed //
'// 22      17.7.9      Shoel                   Phenomenal Logging capabilities now included!           //
'// 23      21.7.9      Shoel                   Rental history now appears on one line                  //
'// 24      29.7.9      Shoel                   Self Checkout makes its debut!                          //
'// 25      4.8.9       Shoel                   Fixed a problem noticed when testing for US - Apollo    //
'//                                             Program assumed presense of atleast one location that   // 
'//                                             was not mapped to a zone, this is now fixed!            //
'// 26      6.8.9       Shoel                   When looking for a specific vehicle's avail, if unavail //
'//                                             the vehicle requested now appears at the top of the list//
'//                                             Also, vehicles blocked or not available give a          //
'//                                             "Limited Availability" message instead of the           //
'//                                             "Vehicle Rates Unavailable" message                     //
'//                                             Also added some input validation on LocZoneCode and     // 
'//                                             VehicleCode                                             //
'// 27      7.8.9       Shoel                   Different requests can now be differentiated in the log //
'// 28      20.8.9      Shoel                   Now includes all the Self Checkout stuff!               //
'// 29      21.8.9      Shoel                   AIMS and DVASS calls in Self Checkout now moved to the  //
'//                                             Aurora Webservice                                       //
'//                                             Also logging capabilities of the SCO part are improved  //
'// 30      24.8.9      Shoel                   Numerous bug fixes, now supports checkout of crosshires //
'// 31      24.8.9      Shoel                   If the rental has a product that requires a serial no.  //
'//                                             the success xml will return a node <Note> with that info//
'// 32      25.8.9      Shoel                   Now notes in the addextra hire item xml will be added   //
'//                                             to the rental                                           //
'// 33      25.8.9      Shoel                   Error messages for UsageOn and Qty are now improved     //
'// 34      28.8.9      Shoel                   Customer info update/customer add now included          //
'// 35      28.8.9      Shoel                   Thrifty vehicles on request now shown on request        //
'// 36      1.9.9       Shoel                   Try catch blocks added to the Selfcheckout webmethods   //
'//                                             to guarantee a return xml                               //
'// 37      2.9.9       Shoel                   Web methods can now throw exceptions that contain       //
'//                                             messages to be passed on to the frontend                //
'// 38      2.9.9       Shoel                   In Self Checkout get booking info, input data is now    //
'//                                             tested for SQL code                                     //
'// 39      11.9.9      Shoel                   WL/KB bookings in phoenix now add payment data into an  //
'//                                             encrypted note                                          //
'// 40      11.9.9      Shoel                   SCO - AddCustData and AddPayment are now seperated out  //
'// 41      11.9.9      Shoel                   SCO - Payment type R/B?? can now be specified           //
'// 42      17.9.9      Shoel                   DVASS errors, Blockin Rule messages now come with price //
'// 43      18.9.9      Shoel                   GetRelevantXML now returns Survey, MailingList flags    //
'//                                             AddUpdateCustomerData now also adds in the extra driver //
'//                                             fees for the rental                                     //
'// 44      21.9.9      Shoel                   Change 42 Undone :)                                     //
'// 45      22.9.9      Shoel                   Unavailble message now to be sent to frontend           //
'// 46      28.9.9      Shoel                   Country of residence is now saved with the bookings     //
'// 47      28.9.9      Shoel                   Preferred Mode of Comm is now saved with the bookings   //
'// 48      1.10.9      Shoel                   Massive change to structure of master xml for branches  //
'// 49      5.10.9      Shoel                   AddUpdateCustomerData now allows deletion of EXTRADB    //
'// 50      6.10.9      Shoel                   Error list in GetAvail now always has brand code        //
'//                                             +Aurora User Code for fleetstatus check is now read from//
'//                                             web.config                                              //
'//                                             +Dvass sequence number is now used for dvass calls      //
'// 51      7.10.9      Shoel                   Vehicle Rates Unavailable now dominates other messages  //
'// 52      9.10.9      Shoel                   Availability price list is now sorted                   //
'// 53      15.10.9     Shoel                   Delete T Processed BPDS now called between addition of  //
'//                                             extra hire items!                                       //
'// 54      16.10.9     Shoel                   Even if CI/CO locs are invalid, messages will appear for//
'//                                             each vehicle in the selected brand(s)                   //
'// 55      19.10.9     Shoel                   1. Thrifty stuff for cars                               //
'//                                             2. CodeId lookups removed, its read from the web.config //
'//                                             3. misc changes                                         //
'// 56      27.10.9     Shoel                   added enormous hardcoding for Thrifty stuff for cars    //
'// 57      28.10.9     Shoel                   date format thing fixed :) + ltd avail errors appear    //
'//                                             before other errors in getavailablility                 //
'// 58      9.11.9      Shoel                   fix for AIRNZ UserCode problem                          //
'// 59      11.11.9     Shoel                   Added a "ping" method to test the health of the WS      //
'// 60      3.12.9      Shoel                   GetVehicles proc now uses check out date and check in   //
'//                                             date to validate against non-availability table         //
'// 61      8.1.10      Shoel                   Credit Card data is now encrypted and logged/sent to    //
'//                                             aurora                                                  //
'// 62      20.1.10     Shoel                   Thrifty error prefix problem now fixed                  //
'// 63      21.1.10     Shoel                   Thrifty WS interface integration completed              //
'// 64      27.1.10     Shoel                   Added new MACCOM prefix and fleet status                //
'// 65      5.3.10      Shoel                   Drivers licences that dont have expiry dates now get    //
'//                                             stored as  Ckidate + 2months and a note is added        //
'// 66      19.5.10     Shoel                   thrifty errors now overwrite any others and appear as a //
'//                                             T&C if safe (configurable) or as a blocking rule        //
'// 67      11.6.10     Shoel                   Initial work for NRMA Loyalty Card Num integration      //
'//                                             + all dlls refreshed and synced with Aurora versions    //
'// 68      14.6.10     Shoel                   More loyalty card stuff added, now saved as part of     //
'//                                             createbookingwithpayment                                //
'// 69      7.7.10      Shoel                   Now forcecreation without CC info wont cause exceptions //
'// 70      14.4.10     Guess who?              Added the ArrivalAtBranchDateTime stuff, hope someone   //
'//                                             uses it...                                              //
'// 71      9.8.10      Guess who?              Modify quote stuff is all here, just needs testing...   //
'// 72      24.8.10     Guess who?              RetrieveBooking tweaked to get all data even if the     //
'//                                             quote had expired or the vehicle is unavailable         //
'// 73      24.8.10     Guess who?              CreateBookingWithExtraHireItemAndPayment now doesnt     //
'//                                             call dvass for request bookings                         //
'// 74      31.8.10     Guess who?              Added new method to retrieve list of EHI & Insurance    //
'//                                             GetInsuranceAndExtraHireItemsForRentalQuote.            //
'// 75      10.9.10     Me again                Massive changes for making ferryveh, ferrypax work again//
'//                                             changes to GetInsuranceAndExtraHireItemsForRentalQuote  //
'//                                             as well as existing create booking, add product stuff   //
'// 76      14.9.10     Me again                Fixed a bug with adding of FerrVeh product, that always //
'//                                             used the cko date instead of usageon date specified     //
'// 77      17.10.10    MANNY                   adding of new AlternateVehicleAvailability              //
'// 78      25.03.11    MANNY                   changing DisplayMode from date to string                //
'// 79      12.04.11    MANNY                   addition of thrifty WEBSERVICE Checking                 //
'// 80      25.05.12    RKS                     Changes for Mighty Drop-off details                     //
'/////////////////////////////////////////////////////////////////////////////////////////////////////////

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml
Imports System.Xml.XPath
Imports System.Data
Imports PhoenixWebService
Imports WebServiceData
Imports System.Configuration
Imports System.Net
Imports System.IO

Imports WS.CentralizedAvailability
Imports WS.FlexiAvailability
Imports WS.RentalAgreement
Imports WS.Printer


<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class PhoenixWebService
    Inherits System.Web.Services.WebService

#Region "GLOBAL CONSTANTS"
    Const PROGRAM_NAME As String = "PHOENIX_WEBSERVICE"
    Const DATE_FORMAT As String = "dd/MM/yyyy"
    Const TIME_FORMAT As String = "HH:mm"
    Const SUCCESS_STRING As String = "SUCCESS"
    Const APP_NAME As String = "WEB"
    Dim DEBUG_MODE As Boolean = CBool(ConfigurationSettings.AppSettings("DebugMode"))
    Dim ENABLE_LOGGING As Boolean = CBool(ConfigurationSettings.AppSettings("EnableLogging"))
    Dim ALPHANUMERIC_REGEX As String = ConfigurationSettings.AppSettings("ALPHANUMERICREGEX")
    Dim LOCATIONZONE_REGEX As String = ConfigurationSettings.AppSettings("LOCATIONZONEREGEX")
    Dim PUBLIC_ERROR_MESSAGE As String = ConfigurationSettings.AppSettings("PUBLICERRORMESSAGE")
    Dim MESSAGE_FLAG As String = "MESSAGE"
    Dim SQL_CODE_SYMBOL_LIST As String() = ConfigurationSettings.AppSettings("SQLCodeSymbolList").Split(","c)
    Dim INVALID_CHECK_OUT_LOCATION As String = ConfigurationSettings.AppSettings("INVALIDCHECKOUTLOCATION")
    Dim INVALID_CHECK_IN_LOCATION As String = ConfigurationSettings.AppSettings("INVALIDCHECKINLOCATION")
    Dim INVALID_CHECK_OUT_AND_INVALID_CHECK_IN_LOCATION As String = ConfigurationSettings.AppSettings("INVALIDCHECKOUTINLOCATION")

    Dim NOTE_CODE_ID As String = ConfigurationSettings.AppSettings("NOTECODETYPEID")
    Dim NOTE_AUDIENCE_INTERNAL As String = ConfigurationSettings.AppSettings("NOTEAUDIENCEINTERNAL")
    Dim NOTE_AUDIENCE_CUSTOMER As String = ConfigurationSettings.AppSettings("NOTEAUDIENCECUSTOMER")
    Dim CARD_CODE_ID As String = ConfigurationSettings.AppSettings("CARDCODEID")
    Dim WEB_CODE_ID As String = ConfigurationSettings.AppSettings("WEBCODEID")
    Dim RENTAL_CODE_ID As String = ConfigurationSettings.AppSettings("RENTALCODEID")
    Dim LICENSE_WITH_NO_EXPIRY_DATE_NOTE_TEXT As String = ConfigurationSettings.AppSettings("NOEXPIRYLICENSENOTE")
    Dim TOKEN_LICENSE_NUMBER As String = ConfigurationSettings.AppSettings("LICENSENUMBERTOKEN")
    Dim TOKEN_EXPIRY_DATE As String = ConfigurationSettings.AppSettings("EXPIRYDATETOKEN")

    Dim THRIFTY_OVERWRITE_ERROR_SECTION_WITH_THRIFTY_ERRORS As Boolean = ConfigurationSettings.AppSettings("OverwriteErrorSectionWithThriftyError")
    Dim THRIFTY_DISPLAY_THRIFTY_ERRORS As Boolean = ConfigurationSettings.AppSettings("DisplayThriftyErrors")
    Dim THRIFTY_DISPLAY_SAFE_THRIFTY_ERRORS_ONLY As Boolean = ConfigurationSettings.AppSettings("UseSafeThriftyErrorsOnly")
    Dim THRIFTY_SAFE_THRIFTY_ERROR_CODE_LIST As String() = ConfigurationSettings.AppSettings("SafeThriftyErrorCodeList").Split(","c)
#End Region

    Dim RequestIdentifier As String

#Region "Master XML Document"
    <WebMethod(Description:="This method returns the master data used to create the b2c frontend pages")> _
    Public Function GenerateMasterXML(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String, ByVal VehicleCode As String) As System.Xml.XmlDocument
        LogInformation("GenerateMasterXML: RQ: CountryCode=""" & CountryCode & """, CompanyCode=""" & CompanyCode & """, BrandCode=""" & BrandCode & """, VehicleCode=""" & VehicleCode & """")

        Dim xmlMain As XmlDocument = New XmlDocument
        xmlMain.LoadXml("<root/>")

        Dim xmlCountryCompany, xmlCountryVehicles, xmlLocationsWithoutZone, xmlAgents, xmlBrandLookups, xmlTravelers As XmlDocument

        Try
            LogInformation("Starting:GetCountryCompanyList")
            xmlCountryCompany = WebServiceData.GetCountryCompanyList(CountryCode, CompanyCode, BrandCode)
            LogInformation("Done:GetCountryCompanyList")

            LogInformation("Starting:GetCountryVehicles")
            xmlCountryVehicles = WebServiceData.GetCountryVehicles(CountryCode, CompanyCode, BrandCode, VehicleCode)
            LogInformation("Done:GetCountryVehicles")

            LogInformation("Starting:GetLocationsWithoutZone")
            xmlLocationsWithoutZone = WebServiceData.GetLocationsWithoutZone(CountryCode, CompanyCode, BrandCode)
            LogInformation("Done:GetLocationsWithoutZone")

            LogInformation("Starting:GetAgentList")
            xmlAgents = WebServiceData.GetAgentList(CountryCode, CompanyCode, BrandCode, VehicleCode)
            LogInformation("Done:GetAgentList")

            LogInformation("Starting:GetBrandLookup")
            xmlBrandLookups = WebServiceData.GetBrandLookup(CountryCode, CompanyCode, BrandCode)
            LogInformation("Done:GetBrandLookup")

            LogInformation("Starting:GetMaxAdultChildrenCount")
            xmlTravelers = WebServiceData.GetMaxAdultChildrenCount(CountryCode, CompanyCode, BrandCode, VehicleCode)
            LogInformation("Done:GetMaxAdultChildrenCount")

            Dim dtLocationsWithZone As System.Data.DataTable
            LogInformation("Starting:GetLocationsWithZone")
            dtLocationsWithZone = WebServiceData.GetLocationsWithZone(CountryCode, CompanyCode, BrandCode)
            LogInformation("Done:GetLocationsWithZone")

            ' Get Country List
            Dim sbCountryList As StringBuilder = New StringBuilder
            sbCountryList.Append("<root>")

            For Each oCountryNode As XmlNode In xmlCountryCompany.DocumentElement.SelectNodes("country") 'xmlLocationsWithoutZone.DocumentElement.SelectNodes("country")
                ' get shell country node
                sbCountryList.Append(oCountryNode.OuterXml.Replace("/>", ">"))

                Dim sCountryCode As String = oCountryNode.Attributes("code").Value()

                ' Add in the Location without zone data
                Dim oLocationsWithoutZones As XmlNode
                oLocationsWithoutZones = xmlLocationsWithoutZone.DocumentElement.SelectSingleNode("country [@code='" & sCountryCode & "']")

                sbCountryList.Append("<locations>")

                ' Locations without zones
                'If oLocationsWithoutZones IsNot Nothing Then
                '    sbCountryList.Append(oLocationsWithoutZones.InnerXml)
                'End If
                ' Locations with zones
                sbCountryList.Append(CreateXMLForLocationsWithZone(dtLocationsWithZone, sCountryCode, oLocationsWithoutZones))
                sbCountryList.Append("</locations>")

                ' Operating Hours
                LogInformation("Starting:GetOperationHours")
                sbCountryList.Append(WebServiceData.GetOperationHours(sCountryCode).DocumentElement.InnerXml)
                LogInformation("Done:GetOperationHours")

                ' add in vehicles'
                Dim oBrandClass As XmlNodeList
                oBrandClass = xmlCountryVehicles.DocumentElement.SelectNodes("brandclass [@countryCode='" & sCountryCode & "']")
                sbCountryList.Append("<vehicles>")
                For Each oBrandClassNode As XmlNode In oBrandClass
                    oBrandClassNode.Attributes.Remove(oBrandClassNode.Attributes("countryCode"))
                    sbCountryList.Append(oBrandClassNode.OuterXml)
                Next
                sbCountryList.Append("</vehicles>")

                ' add travelers info
                Dim oTravelerNode As XmlNode
                oTravelerNode = xmlTravelers.DocumentElement.SelectSingleNode("travelers [@CtyCode='" & sCountryCode & "']")
                oTravelerNode.Attributes.RemoveAll()
                sbCountryList.Append(oTravelerNode.OuterXml)

                sbCountryList.Append("</country>")
            Next

            ' add agents
            sbCountryList.Append("<agents>")
            sbCountryList.Append(xmlAgents.DocumentElement.InnerXml)
            sbCountryList.Append("</agents>")

            ' add brand lookups

            sbCountryList.Append("<brandlookups>")
            sbCountryList.Append(xmlBrandLookups.DocumentElement.InnerXml)
            sbCountryList.Append("</brandlookups>")

            ' add timestamp
            sbCountryList.Append("<timeStamp creationDate=""" & Now.Date.ToString(DATE_FORMAT) & """ creationTime=""" & Now.ToString(TIME_FORMAT) & """ />")

            sbCountryList.Append("</root>")

            xmlMain.LoadXml(sbCountryList.ToString())

        Catch ex As Exception
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            xmlMain = GetErrorXML(ex)
        End Try

        LogInformation("GenerateMasterXML: RS:" & xmlMain.OuterXml)

        Return xmlMain

    End Function

    Function CreateXMLForLocationsWithZone(ByVal LocationTable As DataTable, ByVal CountryCode As String, ByVal LocationsWithoutZoneXML As XmlNode) As String
        Dim sbLocationsWithZone As StringBuilder = New StringBuilder
        Dim dvLocationView As DataView = New DataView(LocationTable)
        dvLocationView.RowFilter = "CtyCode='" & CountryCode & "'"

        Dim dtCountrySpecificLocations As DataTable = dvLocationView.ToTable()

        Dim dtTctList As DataTable = dvLocationView.ToTable(True, "TctCode", "TctName")

        For Each oTctRow As DataRow In dtTctList.Rows

            Dim sTctCode, sTctName As String

            sTctCode = oTctRow("TctCode")
            sTctName = oTctRow("TctName")

            With sbLocationsWithZone
                .Append("<city code=""" & sTctCode & """ name=""" & sTctName & """>")

                If LocationsWithoutZoneXML IsNot Nothing Then
                    Dim xmlLocations As XmlNode
                    xmlLocations = LocationsWithoutZoneXML.SelectSingleNode("city[@code='" & sTctCode & "']")
                    If xmlLocations IsNot Nothing Then
                        .Append(xmlLocations.InnerXml)
                        xmlLocations.ParentNode.RemoveChild(xmlLocations)
                    End If
                End If

                .Append("<zones>")

                dvLocationView.RowFilter = "CtyCode='" & CountryCode & "'" & " AND TctCode='" & sTctCode & "'"
                Dim dtZonesList As DataTable = dvLocationView.ToTable(True, "Type", "LocZoneCode", "LocZoneName", "ZoneBrandList")

                For Each oRow As DataRow In dtZonesList.Rows  ' LocationTable.Rows
                    Dim sType, sLocZoneCode, sLocZoneName, sZoneBrandList As String

                    sType = oRow("Type")
                    sLocZoneCode = oRow("LocZoneCode")
                    sLocZoneName = oRow("LocZoneName")
                    sZoneBrandList = oRow("ZoneBrandList")


                    ' if its not the country required, get the next row!
                    'If sCtyCode <> CountryCode Then Continue For
                    .Append("<zone type=""" & sType & """ name=""" & sTctName & " " & sLocZoneName & """ value=""" & sTctCode & "-" & sLocZoneCode & """ brandList=""" & sZoneBrandList & """ >")

                    dvLocationView.RowFilter = "CtyCode='" & CountryCode & "'" & " AND LocZoneCode='" & sLocZoneCode & "' AND TctCode='" & sTctCode & "'"
                    Dim dtLocations As DataTable = dvLocationView.ToTable()

                    For Each oLocationRow As DataRow In dtLocations.Rows

                        Dim sLocCode, sLocName, sAddress1, sAddress2, sPhoneNumber, sBrandList, sBranchCanHire As String
                        sLocCode = oLocationRow("LocCode")
                        sLocName = oLocationRow("LocName")
                        sAddress1 = oLocationRow("Address1")
                        sAddress2 = oLocationRow("Address2")
                        sPhoneNumber = oLocationRow("PhoneNumber")
                        sBrandList = oLocationRow("BrandList")
                        sBranchCanHire = oLocationRow("BranchBookingType")

                        .Append("<branch code=""" & sLocCode & """ address1=""" & sAddress1 & """ address2=""" & sAddress2 & """ phoneNumber=""" & sPhoneNumber & """ brandList=""" & sBrandList & """  branchCanHire=""" & sBranchCanHire & """ />")

                    Next

                    .Append("</zone>")

                Next

                .Append("</zones>")

                .Append("</city>")

            End With
        Next

        ' These cities are not in the withzone dataset
        If LocationsWithoutZoneXML IsNot Nothing AndAlso LocationsWithoutZoneXML.InnerXml <> "" Then
            sbLocationsWithZone.Append(LocationsWithoutZoneXML.InnerXml)
        End If

        Return sbLocationsWithZone.ToString()
    End Function
#End Region

#Region "Get Availability and Best Buy"

#Region "Constants"

    Const AVAILABLE_YES_FLAG As Char = "Y"c
    Const AVAILABLE_NO_FLAG As Char = "N"c
    Const AVAILABLE_THRIFTY_FLAG As Char = "T"c
    Const AVAILABLE_MACCOM_FLAG As Char = "M"c
    Const AVAILABLE_REQUEST_FLAG As Char = "R"c

    Const LOCATION_TAG As String = "-LOCATION"
    Const PRODUCT_TAG As String = "-PRODUCT"

    Const BLOCKING_RULE_PREFIX As String = "BLOCKINGRULE-"
    Const TERMS_AND_CONDITIONS_PREFIX As String = "TANDC-"
    Const THRIFTY_RENTAL_PREFIX As String = "THRIFTY-"
    Const MACCOM_RENTAL_PREFIX As String = "MACCOM-"
    Const UNAVAILABLE_RATES_PREFIX As String = "RATESUNAV-"

    Dim VEHICLE_RATES_UNAVAILABLE_MESSAGE As String = ConfigurationSettings.AppSettings("UNAVMSG")
    Dim LIMITED_AVAILABILITY_MESSAGE As String = ConfigurationSettings.AppSettings("LTDAVMSG")
    Dim ON_REQUEST_MESSAGE As String = ConfigurationSettings.AppSettings("ONREQMSG")

    Dim THRIFTY_STRING As String = "THRIFTY"
    Dim BRANCH_STRING As String = "BRANCH"

    Dim SELF_CHECK_OUT_USER_CODE As String = CStr(ConfigurationSettings.AppSettings("SELFCHECKOUTUSER"))

#End Region

    <WebMethod(Description:="This method returns the inclusive package rate for an AvpId")> _
    Public Function CalculateInclusiveRate(ByVal AvpId As String, ByVal IsTestMode As Boolean, ByVal IsGross As Boolean) As XmlDocument

        LogInformation("CalculateInclusiveRate: RQ: AvpId=""" & AvpId & """, IsTestMode=""" & IsTestMode.ToString() & """")

        Dim oResultXML As XmlDocument = New XmlDocument
        Dim sResult As String
        Dim sErrorMessage As String = ""

        Try
            LogInformation("Starting:WebServiceData.GetVehiclePrice")
            sResult = WebServiceData.GetVehiclePrice(AvpId, String.Empty, String.Empty, String.Empty, _
                                                     String.Empty, Now, Now, _
                                                     String.Empty, String.Empty, 0, 0, 0, String.Empty, _
                                                     True, False, IsTestMode, IsGross, sErrorMessage)
            LogInformation("Done:WebServiceData.GetVehiclePrice. sErrorMessage=""" & sErrorMessage & """")

            If sErrorMessage <> "" Then
                Throw New Exception(sErrorMessage)
            End If

            oResultXML.LoadXml(sResult)
        Catch ex As Exception
            oResultXML = GetErrorXML(ex)
        End Try

        LogInformation("CalculateInclusiveRate: RS:" & oResultXML.OuterXml)

        Return oResultXML
    End Function

    <WebMethod(Description:="This method returns a list of available vehicles and prices for a given selection criteria")> _
    Public Function GetAvailability(ByVal Brand As String, _
                                ByVal CountryCode As String, ByVal VehicleCode As String, _
                                ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As DateTime, _
                                ByVal CheckInZoneCode As String, ByVal CheckInDateTime As DateTime, _
                                ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, _
                                ByVal AgentCode As String, ByVal PackageCode As String, _
                                ByVal IsVan As Boolean, ByVal IsBestBuy As Boolean, _
                                ByVal CountryOfResidence As String, _
                                ByVal IsTestMode As Boolean, _
                                ByVal IsGross As Boolean, _
                                ByVal AgnisInclusiveProduct As Boolean, _
                                ByVal sPromoCode As String) As XmlDocument ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct

        LogInformation("GetAvailability: RQ: Brand=""" & Brand & _
                                        """, CountryCode=""" & CountryCode & _
                                        """, VehicleCode=""" & VehicleCode & _
                                        """, CheckOutZoneCode=""" & CheckOutZoneCode & _
                                        """, CheckOutDateTime=""" & CheckOutDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                        """, CheckInZoneCode=""" & CheckInZoneCode & _
                                        """, CheckInDateTime=""" & CheckInDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                        """, NoOfAdults=""" & NoOfAdults.ToString() & _
                                        """, NoOfChildren=""" & NoOfChildren.ToString() & _
                                        """, AgentCode=""" & AgentCode & _
                                        """, PackageCode=""" & PackageCode & _
                                        """, IsVan=""" & IsVan.ToString() & _
                                        """, IsBestBuy=""" & IsBestBuy.ToString() & _
                                        """, CountryOfResidence=""" & CountryOfResidence & _
                                        """, IsTestMode=""" & IsTestMode.ToString() & _
                                        """, IsGross=""" & IsGross.ToString() & _
                                        """, AgnisInclusiveProduct=""" & AgnisInclusiveProduct.ToString() & _
                                        """, PromoCode=""" & sPromoCode & _
                                        """")

        '################################################
        ' 1. Get Available Vehicles
        ' 2. Check Fleet Status for each vehicle
        ' 2.1    If Request -> Check Blocking Rule
        ' 2.1.1     If Blocked -> Discard this vehicle from result set
        ' 2.1.2     If Not Blocked -> Check DVASS
        ' 2.1.2.1       If DVASS Says Available -> Goto 3
        ' 2.1.2.2       If DVASS Says Not Available -> Discard this vehicle from result set
        ' 2.1.3     If Bypass -> Goto 3
        ' 2.2    If FreeSale -> Goto 3
        ' 3. Retrieve Available Valid Packages
        ' 4. Calculate Rates for each package
        ' 5. Create List with Vehicle-Package combinations
        '################################################

        Dim htErrorList As Hashtable = New Hashtable

        ' Thrifty Debug Info
        Dim htThriftyCallDataList As Hashtable = New Hashtable
        ' Thrifty Debug Info

        Dim sResult As String
        Dim oRetXml As New XmlDocument
        Dim sVehicleCodeBak As String = VehicleCode
        Dim bIsGenericSearch As Boolean
        Dim bFoundMatchWithinOwnBrandForGenericSearch As Boolean = False

        bIsGenericSearch = IIf(Trim(Brand) = "", True, False)

        ' this var is only for use if u req a specific vehicle and it is available + has rates
        Dim sSpecificVehicleInfo As String = ""

        Dim AvailCode As String = ""

        Dim alternateservice As New GetAlternateVehicleService
        ''rev:mia Nov.26 2012 - one request coming from Rajesh is to exclude the
        ''                      price calculations when doing alternate availability
        ''                      inside aurora.
        Dim skipprice As Boolean = False
        If (Brand.Contains("|FromAurora") = True) Then
            skipprice = True
            Brand = Brand.Replace("|FromAurora", "")
            LogInformation("NOTE:this is alternate availability from Aurora.Price Calculation will not take effect.")
        Else
            LogInformation("NOTE:this is Normal alternate availability from B2C")
        End If


        Try

            LogInformation("Start:Data Collection")

            ' Lookup Agent+Country Combination in Global Hash Table - START
            Dim alBrandList As ArrayList = New ArrayList
            If GetProperStringValue(Brand).Equals(String.Empty) Then
                ' no brand specified, use all brands that are valid for Agent-country combination
                LogInformation("Starting:DoAgentCountryBrandLookup")
                alBrandList = DoAgentCountryBrandLookup(AgentCode, CountryCode)
                LogInformation("Done:DoAgentCountryBrandLookup")
            Else
                ' brand specified
                alBrandList.Add(Brand)
            End If
            ' Lookup Agent+Country Combination in Global Hash Table - DONE


            ' Now figure out which brands this passed in vehicle code really belongs to
            Dim alValidBrandsForThisVehicleCode As New ArrayList
            If Not String.IsNullOrEmpty(Trim(VehicleCode)) AndAlso GetProperStringValue(Brand).Equals(String.Empty) Then

                alValidBrandsForThisVehicleCode = WebServiceData.GetBrandsForVehicleCode(VehicleCode)

                Dim alTemp As New ArrayList

                For Each sBrandItem As String In alBrandList
                    alTemp.Add(sBrandItem)
                Next

                alBrandList = New ArrayList

                For Each sBrandItem As String In alValidBrandsForThisVehicleCode
                    If alTemp.Contains(sBrandItem) Then
                        alTemp.Remove(sBrandItem)
                    End If
                    alBrandList.Add(sBrandItem)
                Next

                For Each sBrandItem As String In alTemp
                    alBrandList.Add(sBrandItem)
                Next

            End If

            Dim sbResultXMLString As StringBuilder = New StringBuilder
            sbResultXMLString.Append("<data>")

            For Each sBrandCode As String In alBrandList

                If bFoundMatchWithinOwnBrandForGenericSearch Then
                    Exit For
                End If

                ' ///////////////////////////////////////////////////////////////////////
                ' Translate Zone Code to Location Code - START
                VehicleCode = VehicleCode.ToUpper().Trim()

                Dim dtCheckOutList, dtCheckInList As DataTable

                CheckOutZoneCode = UCase(GetProperStringValue(CheckOutZoneCode))
                CheckInZoneCode = UCase(GetProperStringValue(CheckInZoneCode))

                Dim rxLocationZone As New Regex(LOCATIONZONE_REGEX)
                If rxLocationZone.IsMatch(CheckOutZoneCode) Then
                    LogInformation("Starting:WebServiceData.GetLocation(CheckOut). sBrandCode=""" & sBrandCode & """")
                    dtCheckOutList = WebServiceData.GetLocation(CheckOutZoneCode, sBrandCode, IIf(IsVan, "AV", "AC"), CountryCode)
                    LogInformation("Done:WebServiceData.GetLocation(CheckOut). ")
                End If

                If rxLocationZone.IsMatch(CheckInZoneCode) Then
                    LogInformation("Starting:WebServiceData.GetLocation(CheckIn). sBrandCode=""" & sBrandCode & """")
                    dtCheckInList = WebServiceData.GetLocation(CheckInZoneCode, sBrandCode, IIf(IsVan, "AV", "AC"), CountryCode)
                    LogInformation("Done:WebServiceData.GetLocation(CheckIn). ")
                End If

                Dim bSkipToNextBrand As Boolean = False

                If dtCheckOutList.Rows.Count = 0 Then
                    ' invalid co loc code!!!!
                    bSkipToNextBrand = True
                End If

                If dtCheckInList.Rows.Count = 0 Then
                    ' invalid ci loc code!!!!
                    bSkipToNextBrand = True
                End If



                ' A hack to ensure cars error displays both branch and thrifty - START

                Dim bCOListHasBranchLocationType As Boolean = False
                Dim bCIListHasBranchLocationType As Boolean = False

                For Each rwCheckOutLocationRow As DataRow In dtCheckOutList.Rows
                    If UCase(rwCheckOutLocationRow("BranchType").ToString()) = BRANCH_STRING Then
                        bCOListHasBranchLocationType = True
                        Continue For
                    End If
                Next

                For Each rwCheckInLocationRow As DataRow In dtCheckInList.Rows
                    If UCase(rwCheckInLocationRow("BranchType").ToString()) = BRANCH_STRING Then
                        bCIListHasBranchLocationType = True
                        Continue For
                    End If
                Next

                'If Not bCOListHasBranchLocationType _
                '   AndAlso ( _
                '            VehicleCode = "" _
                '            OrElse _
                '            VehicleCode = "PFMR" _
                '            ) _
                '    AndAlso (CountryCode = "AU") _
                '    AndAlso Not IsVan _
                '    AndAlso UCase(sBrandCode) = "B" _
                'Then
                '    Dim rwDataRowTempCO1 As DataRow = dtCheckOutList.NewRow()
                '    rwDataRowTempCO1("LocCode") = ""
                '    rwDataRowTempCO1("BranchType") = BRANCH_STRING
                '    dtCheckOutList.Rows.Add(rwDataRowTempCO1)
                'End If

                'If Not bCIListHasBranchLocationType _
                '   AndAlso ( _
                '            VehicleCode = "" _
                '            OrElse _
                '            VehicleCode = "PFMR" _
                '            ) _
                '    AndAlso (CountryCode = "AU") _
                '    AndAlso Not IsVan _
                '    AndAlso UCase(sBrandCode) = "B" _
                'Then
                '    Dim rwDataRowTempCI1 As DataRow = dtCheckInList.NewRow()
                '    rwDataRowTempCI1("LocCode") = ""
                '    rwDataRowTempCI1("BranchType") = BRANCH_STRING
                '    dtCheckInList.Rows.Add(rwDataRowTempCI1)
                'End If

                ' only run for Cars

                Dim bCOListHasThriftyLocationType As Boolean = False
                Dim bCIListHasThriftyLocationType As Boolean = False

                If Not IsVan _
                   AndAlso UCase(CountryCode) = "AU" _
                   AndAlso VehicleCode <> "PFMR" _
                   AndAlso UCase(sBrandCode) = "B" _
                Then

                    For Each rwCheckOutLocationRow As DataRow In dtCheckOutList.Rows
                        If UCase(rwCheckOutLocationRow("BranchType").ToString()) = THRIFTY_STRING Then
                            bCOListHasThriftyLocationType = True
                            Continue For
                        End If
                    Next

                    For Each rwCheckInLocationRow As DataRow In dtCheckInList.Rows
                        If UCase(rwCheckInLocationRow("BranchType").ToString()) = THRIFTY_STRING Then
                            bCIListHasThriftyLocationType = True
                            Continue For
                        End If
                    Next

                    ''REV:MIA 11 APRIL 20100 THIS IS THRIFTY
                    If bCOListHasThriftyLocationType = True Or _
                         bCIListHasThriftyLocationType = True Then

                        'If Not IsProcessthrifty() Then
                        '    oRetXml.LoadXml("<data></data>")
                        '    Return oRetXml
                        'End If

                        'If Not IsThriftyAlive() Then
                        '    oRetXml.LoadXml("<data></data>")
                        '    Return oRetXml
                        'End If


                    End If


                    If Not bCOListHasThriftyLocationType Then
                        Dim rwDataRowTempCO2 As DataRow = dtCheckOutList.NewRow()
                        rwDataRowTempCO2("LocCode") = ""
                        rwDataRowTempCO2("BranchType") = THRIFTY_STRING
                        dtCheckOutList.Rows.Add(rwDataRowTempCO2)
                    End If


                    If Not bCIListHasThriftyLocationType Then
                        Dim rwDataRowTempCI2 As DataRow = dtCheckInList.NewRow()
                        rwDataRowTempCI2("LocCode") = ""
                        rwDataRowTempCI2("BranchType") = THRIFTY_STRING
                        dtCheckInList.Rows.Add(rwDataRowTempCI2)
                    End If
                End If


                If ( _
                        Not bCOListHasBranchLocationType _
                        AndAlso ( _
                                VehicleCode = "" _
                                OrElse _
                                VehicleCode = "PFMR" _
                                ) _
                        AndAlso (CountryCode = "AU") _
                        AndAlso Not IsVan _
                        AndAlso UCase(sBrandCode) = "B" _
                    ) _
                    OrElse _
                    ( _
                        Not bCOListHasBranchLocationType _
                        AndAlso _
                        Not bCOListHasThriftyLocationType _
                    ) _
                Then
                    Dim rwDataRowTempCO1 As DataRow = dtCheckOutList.NewRow()
                    rwDataRowTempCO1("LocCode") = ""
                    rwDataRowTempCO1("BranchType") = BRANCH_STRING
                    dtCheckOutList.Rows.Add(rwDataRowTempCO1)
                End If

                If ( _
                        Not bCIListHasBranchLocationType _
                        AndAlso ( _
                                VehicleCode = "" _
                                OrElse _
                                VehicleCode = "PFMR" _
                                ) _
                        AndAlso (CountryCode = "AU") _
                        AndAlso Not IsVan _
                        AndAlso UCase(sBrandCode) = "B" _
                    ) _
                    OrElse _
                    ( _
                        Not bCIListHasBranchLocationType _
                        AndAlso _
                        Not bCIListHasThriftyLocationType _
                    ) _
                Then
                    Dim rwDataRowTempCI1 As DataRow = dtCheckInList.NewRow()
                    rwDataRowTempCI1("LocCode") = ""
                    rwDataRowTempCI1("BranchType") = BRANCH_STRING
                    dtCheckInList.Rows.Add(rwDataRowTempCI1)
                End If

                ' A hack to ensure cars error displays both branch and thrifty - END


                ' Loop with location codes
                Dim rwCheckOutLocation As DataRow
                Dim rwCheckInLocation As DataRow

                Dim bHaveFoundResultForPFMR As Boolean = False
                Dim bHaveFoundResultForNonPFMR As Boolean = False

                ' weird logic to arrange locations depending on whats asked for

                If CountryCode = "AU" _
                   AndAlso Not IsVan _
                   AndAlso UCase(sBrandCode) = "B" _
                Then

                    Dim dvSort As DataView = New DataView
                    If VehicleCode = "PFMR" Then
                        ' branch before thrifty
                        dvSort.Table = dtCheckOutList
                        dvSort.Sort = "BranchType"

                        dtCheckOutList = dvSort.ToTable()

                        dvSort.Table = dtCheckInList
                        dvSort.Sort = "BranchType"

                        dtCheckInList = dvSort.ToTable()
                    Else
                        ' branch before thrifty
                        dvSort.Table = dtCheckOutList
                        dvSort.Sort = "BranchType DESC"

                        dtCheckOutList = dvSort.ToTable()

                        dvSort.Table = dtCheckInList
                        dvSort.Sort = "BranchType DESC"

                        dtCheckInList = dvSort.ToTable()
                    End If

                End If


                For i As Integer = 0 To dtCheckOutList.Rows.Count - 1
                    rwCheckOutLocation = dtCheckOutList.Rows(i)

                    VehicleCode = sVehicleCodeBak
                    For j As Integer = 0 To dtCheckInList.Rows.Count - 1
                        rwCheckInLocation = dtCheckInList.Rows(j)

                        Dim sCheckOutLocationCode, sCheckInLocationCode, sCheckOutLocationType, sCheckInLocationType As String

                        sCheckOutLocationType = UCase(rwCheckOutLocation("BranchType").ToString())
                        sCheckInLocationType = UCase(rwCheckInLocation("BranchType").ToString())

                        If sCheckInLocationType <> BRANCH_STRING AndAlso VehicleCode = "PFMR" AndAlso bHaveFoundResultForPFMR Then
                            Exit For
                        End If

                        If sCheckInLocationType <> THRIFTY_STRING AndAlso VehicleCode <> "PFMR" AndAlso VehicleCode <> "" AndAlso bHaveFoundResultForNonPFMR Then
                            Exit For
                        End If



                        If sCheckOutLocationType = sCheckInLocationType Then ' dont process branch to thrifty or vice versa

                            sCheckOutLocationCode = UCase(rwCheckOutLocation("LocCode").ToString())
                            sCheckInLocationCode = UCase(rwCheckInLocation("LocCode").ToString())

                            ' Translate Zone Code to Location Code - DONE
                            ' ///////////////////////////////////////////////////////////////////////

                            ' ///////////////////////////////////////////////////////////////////////
                            ' Get Package Id from Code
                            Dim sPackageId As String
                            If GetProperStringValue(PackageCode).Equals(String.Empty) AndAlso Not bSkipToNextBrand Then
                                sPackageId = String.Empty
                            Else
                                LogInformation("Starting:WebServiceData.GetPackageId")
                                sPackageId = WebServiceData.GetPackageId(PackageCode, sCheckOutLocationCode, CheckOutDateTime, CheckInDateTime)
                                LogInformation("Done:WebServiceData.GetPackageId. sPackageId=""" & sPackageId & """")
                            End If

                            ' ///////////////////////////////////////////////////////////////////////

                            ' ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            ' 1. Get Available Vehicles
                            Dim dtAvailableVehicles As DataTable = New DataTable("Results")

                            LogInformation("Starting:WebServiceData.GetVehicleList")
                            Dim sErrorMessageForForcedVehicleCode As String = ""
                            ''rev:mia Oct.24, 2013 - added sPkgCode as requested by rajesh
                            dtAvailableVehicles = WebServiceData.GetVehicleList(sBrandCode, sCheckOutLocationCode, CheckOutDateTime, _
                                                                                sCheckInLocationCode, CheckInDateTime, _
                                                                                CountryCode, NoOfAdults, NoOfChildren, 0, IsVan, _
                                                                                IIf(sCheckOutLocationType = THRIFTY_STRING, True, False), _
                                                                                VehicleCode, AgentCode, IsTestMode, sErrorMessageForForcedVehicleCode, skipprice, PackageCode)
                            LogInformation("Done:WebServiceData.GetVehicleList. sErrorMessageForForcedVehicleCode = " & sErrorMessageForForcedVehicleCode)

                            If sErrorMessageForForcedVehicleCode = "Not a valid vehicle for this brand" Then
                                VehicleCode = ""
                            End If

                            If dtAvailableVehicles Is Nothing Then
                                Continue For
                            End If


                            Dim dvVehiclesToConsider As DataView = New DataView(dtAvailableVehicles)

                            ' ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                            ''''''''''''''''''' EXCEPTION CASE FOR AIR NZ
                            If String.IsNullOrEmpty(sCheckInLocationCode) OrElse String.IsNullOrEmpty(sCheckOutLocationCode) Then
                                If Not String.IsNullOrEmpty(VehicleCode) Then
                                    dvVehiclesToConsider.RowFilter = "PrdShortName = '" & VehicleCode & "'"
                                End If

                                If dvVehiclesToConsider.ToTable().Rows.Count = 0 Then
                                    If String.IsNullOrEmpty(sCheckOutLocationCode) AndAlso Not String.IsNullOrEmpty(sCheckInLocationCode) Then
                                        AddErrorMessageToList(htErrorList, VehicleCode & "|" & "" & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_OUT_LOCATION)
                                    End If
                                    If String.IsNullOrEmpty(sCheckInLocationCode) AndAlso Not String.IsNullOrEmpty(sCheckOutLocationCode) Then
                                        AddErrorMessageToList(htErrorList, VehicleCode & "|" & "" & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_IN_LOCATION)
                                    End If
                                    If String.IsNullOrEmpty(sCheckOutLocationCode) AndAlso String.IsNullOrEmpty(sCheckInLocationCode) Then
                                        AddErrorMessageToList(htErrorList, VehicleCode & "|" & "" & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_OUT_AND_INVALID_CHECK_IN_LOCATION)
                                    End If
                                Else
                                    For Each rwAvailableVehicle As DataRow In dvVehiclesToConsider.ToTable().Rows
                                        If String.IsNullOrEmpty(sCheckOutLocationCode) AndAlso Not String.IsNullOrEmpty(sCheckInLocationCode) Then
                                            AddErrorMessageToList(htErrorList, rwAvailableVehicle("PrdShortName") & "|" & rwAvailableVehicle("PrdName") & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_OUT_LOCATION)
                                        End If
                                        If String.IsNullOrEmpty(sCheckInLocationCode) AndAlso Not String.IsNullOrEmpty(sCheckOutLocationCode) Then
                                            AddErrorMessageToList(htErrorList, rwAvailableVehicle("PrdShortName") & "|" & rwAvailableVehicle("PrdName") & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_IN_LOCATION)
                                        End If
                                        If String.IsNullOrEmpty(sCheckOutLocationCode) AndAlso String.IsNullOrEmpty(sCheckInLocationCode) Then
                                            AddErrorMessageToList(htErrorList, rwAvailableVehicle("PrdShortName") & "|" & rwAvailableVehicle("PrdName") & "|" & sBrandCode & PRODUCT_TAG, TERMS_AND_CONDITIONS_PREFIX & INVALID_CHECK_OUT_AND_INVALID_CHECK_IN_LOCATION)
                                        End If
                                    Next
                                End If
                                bSkipToNextBrand = True
                            End If
                            ''''''''''''''''''' EXCEPTION CASE FOR AIR NZ

                            If bSkipToNextBrand Then
                                bSkipToNextBrand = False
                                ' No sense in continuing with this brand...Next Please!
                                Continue For
                            End If


                            Dim bSkipCheck As Boolean = False
                            Dim sProductName As String = ""

                            Try
                                Dim rxAlphaNumeric As New Regex(ALPHANUMERIC_REGEX)
                                VehicleCode = UCase(GetProperStringValue(VehicleCode))

                                ' if vehicle is specified
                                If Not String.IsNullOrEmpty(GetProperStringValue(VehicleCode)) Then

                                    If Not rxAlphaNumeric.IsMatch(VehicleCode) Then
                                        ' Checking for a valid code
                                        AddErrorMessageToList(htErrorList, "INVALIDVEHICLECODE", "Invalid Vehicle Code = " & VehicleCode & " , Vechicle Code must be alphanumeric. Ignoring specified code.")
                                        VehicleCode = ""
                                        Exit Try
                                    End If


                                    ' It must exist in the list retrieved
                                    dvVehiclesToConsider.RowFilter = "PrdShortName = '" & VehicleCode & "'"
                                    If dvVehiclesToConsider.ToTable().Rows.Count = 0 Then
                                        ' it doesnt, so discard this vehicle and check the rest!
                                        If VehicleCode = "" Then
                                            AddErrorMessageToList(htErrorList, VehicleCode & "|" & sProductName & "|" & sBrandCode & PRODUCT_TAG, LIMITED_AVAILABILITY_MESSAGE)
                                        End If
                                        dvVehiclesToConsider.RowFilter = "PrdShortName <> '" & VehicleCode & "'"
                                    Else
                                        ' it does exist in the list retrieved
                                        ' Get product name
                                        sProductName = dvVehiclesToConsider.ToTable().Rows(0)("PrdName")



                                        ' check its availability

                                        If GetVehicleAvailability(sBrandCode, VehicleCode, sProductName, CountryCode, _
                                                                  sCheckOutLocationCode, CheckOutDateTime, _
                                                                  sCheckInLocationCode, CheckInDateTime, _
                                                                  AgentCode, sPackageId, htErrorList, htThriftyCallDataList _
                                                                  ) = AVAILABLE_YES_FLAG Then
                                            ' Available - So discard other vehicles and dont bother checking status again!

                                            ' Also check price!
                                            Dim sErrMsg As String = String.Empty

                                            If skipprice Then
                                                LogInformation("Starting:WebServiceData.GetVehiclePrice using GetAvailabilityForAurora. PrdShortName=""" & VehicleCode & """")
                                            Else
                                                LogInformation("Starting:WebServiceData.GetVehiclePrice. PrdShortName=""" & VehicleCode & """")
                                            End If

                                            sSpecificVehicleInfo = WebServiceData.GetVehiclePrice(String.Empty, VehicleCode, AgentCode, _
                                                                             sCheckOutLocationCode, sCheckInLocationCode, _
                                                                             CheckOutDateTime, CheckInDateTime, _
                                                                             CountryOfResidence, PackageCode, _
                                                                             NoOfAdults, NoOfChildren, 0, _
                                                                             sBrandCode, False, IsBestBuy, IsTestMode, IsGross, sErrMsg, _
                                                                             skipprice, _
                                                                             AgnisInclusiveProduct, _
                                                                             sPromoCode) ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct
                                            If skipprice Then
                                                LogInformation("Done:WebServiceData.GetVehiclePrice using GetAvailabilityForAurora. sErrorMessage=""" & sErrMsg & """")
                                            Else
                                                LogInformation("Done.Starting:WebServiceData.GetVehiclePrice. sErrorMessage=""" & sErrMsg & """")
                                            End If



                                            If String.IsNullOrEmpty(sErrMsg) AndAlso sSpecificVehicleInfo <> "" Then
                                                dvVehiclesToConsider.RowFilter = "PrdShortName = '" & VehicleCode & "'"
                                                bSkipCheck = True
                                                If VehicleCode = "PFMR" Then
                                                    bHaveFoundResultForPFMR = True
                                                Else
                                                    bHaveFoundResultForNonPFMR = True
                                                End If

                                                ' For AIRNZ
                                                If bIsGenericSearch Then
                                                    If alValidBrandsForThisVehicleCode.Contains(sBrandCode) Then
                                                        bFoundMatchWithinOwnBrandForGenericSearch = True
                                                    End If
                                                End If
                                                ' FOR AIRNZ

                                            Else
                                                ' vehicle available, but no rates!
                                                ' Change on 22.9.9 - Unavailble message now to be sent to frontend
                                                'AddErrorMessageToList(htErrorList, VehicleCode & "|" & sProductName & PRODUCT_TAG, VEHICLE_RATES_UNAVAILABLE_MESSAGE)
                                                AddErrorMessageToList(htErrorList, VehicleCode & "|" & sProductName & "|" & sBrandCode & PRODUCT_TAG, UNAVAILABLE_RATES_PREFIX & VEHICLE_RATES_UNAVAILABLE_MESSAGE)
                                                ' Change on 22.9.9 - Unavailble message now to be sent to frontend
                                                dvVehiclesToConsider.RowFilter = "PrdShortName <> '" & VehicleCode & "'"
                                            End If


                                        Else
                                            ' Not available - so discard this vehicle code and check the rest!
                                            AddErrorMessageToList(htErrorList, VehicleCode & "|" & sProductName & "|" & sBrandCode & PRODUCT_TAG, LIMITED_AVAILABILITY_MESSAGE)
                                            dvVehiclesToConsider.RowFilter = "PrdShortName <> '" & VehicleCode & "'"

                                        End If ''If GetVehicleAvailability

                                    End If ''If dvVehiclesToConsider.ToTable().Rows.Count = 0 Then


                                End If ''If Not String.IsNullOrEmpty(GetProperStringValue(VehicleCode)) Then

                            Catch ex As Exception
                                AddErrorMessageToList(htErrorList, VehicleCode & "|" & sProductName & "|" & sBrandCode & PRODUCT_TAG, ex.Message)
                                dvVehiclesToConsider.RowFilter = "PrdShortName <> '" & VehicleCode & "'"
                            End Try

                            ' ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            ' 2. Check Fleet Status and Blocking rule for each vehicle


                            For Each rwAvailableVehicle As DataRow In dvVehiclesToConsider.ToTable().Rows
                                Try
                                    If Not bSkipCheck Then
                                        If GetVehicleAvailability(sBrandCode, rwAvailableVehicle("PrdShortName"), rwAvailableVehicle("PrdName"), CountryCode, _
                                                                  sCheckOutLocationCode, CheckOutDateTime, _
                                                                  sCheckInLocationCode, CheckInDateTime, _
                                                                  AgentCode, sPackageId, htErrorList, htThriftyCallDataList _
                                                                  ) = AVAILABLE_NO_FLAG Then
                                            ' remove from list
                                            AddErrorMessageToList(htErrorList, rwAvailableVehicle("PrdShortName") & "|" & rwAvailableVehicle("PrdName") & "|" & sBrandCode & PRODUCT_TAG, LIMITED_AVAILABILITY_MESSAGE)
                                            Continue For
                                        End If
                                    End If

                                    ' Look at Best Price
                                    Dim sErrorMessage As String = ""

                                    ''rev:mia Nov.26, 2012 - skip this call if based on config and channel where it came from

                                    If Not bSkipCheck Then

                                        If skipprice Then
                                            LogInformation("Starting:WebServiceData.GetVehiclePrice using GetAvailabilityForAurora. PrdShortName=""" & rwAvailableVehicle("PrdShortName") & """")
                                        Else
                                            LogInformation("Starting:WebServiceData.GetVehiclePrice. PrdShortName=""" & rwAvailableVehicle("PrdShortName") & """")
                                        End If

                                        sResult = WebServiceData.GetVehiclePrice(String.Empty, rwAvailableVehicle("PrdShortName"), AgentCode, _
                                                                             sCheckOutLocationCode, sCheckInLocationCode, _
                                                                             CheckOutDateTime, CheckInDateTime, _
                                                                             CountryOfResidence, PackageCode, _
                                                                             NoOfAdults, NoOfChildren, 0, _
                                                                             sBrandCode, False, IsBestBuy, IsTestMode, IsGross, sErrorMessage, _
                                                                             skipprice, _
                                                                             AgnisInclusiveProduct, _
                                                                             sPromoCode) ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct

                                        If skipprice Then
                                            LogInformation("Done:WebServiceData.GetVehiclePrice using GetAvailabilityForAurora. sErrorMessage=""" & sErrorMessage & """")
                                        Else
                                            LogInformation("Done:WebServiceData.GetVehiclePrice. sErrorMessage=""" & sErrorMessage & """")
                                        End If

                                    Else
                                        sResult = sSpecificVehicleInfo
                                        sSpecificVehicleInfo = ""
                                    End If



                                    If sErrorMessage <> "" Then
                                        Throw New Exception(sErrorMessage)
                                    End If

                                Catch ex As Exception
                                    AddErrorMessageToList(htErrorList, rwAvailableVehicle("PrdShortName") & "|" & rwAvailableVehicle("PrdName") & "|" & sBrandCode & PRODUCT_TAG, ex.Message)
                                    Continue For
                                End Try

                                Dim oResultXML As XmlDocument = New XmlDocument
                                Try
                                    oResultXML.LoadXml(sResult)
                                    Dim bErrorsAdded As Boolean = False
                                    Dim sKeyToRemove As String = ""

                                    For Each oAvailRateNode As XmlNode In oResultXML.DocumentElement.SelectNodes("AvailableRate")
                                        For Each sKey As String In htErrorList.Keys
                                            Dim arrProduct As String() = sKey.Replace(PRODUCT_TAG, "").Split("|"c)
                                            If arrProduct(0) = rwAvailableVehicle("PrdShortName") Then
                                                ' product code matches
                                                If htErrorList(sKey).ToString().Contains(THRIFTY_RENTAL_PREFIX) Then
                                                    sbResultXMLString.Append(oAvailRateNode.OuterXml.Replace("</AvailableRate>", "<Errors><BlockingRuleMessage Allow='true' Message='" & htErrorList(sKey).ToString().Replace(THRIFTY_RENTAL_PREFIX, "") & "' /><ErrorMessage Allow='' Message='' /></Errors></AvailableRate>"))

                                                    bErrorsAdded = True
                                                    sKeyToRemove = sKey
                                                    Exit For
                                                End If
                                                If htErrorList(sKey).ToString().Contains(MACCOM_RENTAL_PREFIX) Then
                                                    sbResultXMLString.Append(oAvailRateNode.OuterXml.Replace("</AvailableRate>", "<Errors><BlockingRuleMessage Allow='true' Message='" & htErrorList(sKey).ToString().Replace(MACCOM_RENTAL_PREFIX, "") & "' /><ErrorMessage Allow='' Message='' /></Errors></AvailableRate>"))

                                                    bErrorsAdded = True
                                                    sKeyToRemove = sKey
                                                    Exit For
                                                End If
                                            End If
                                        Next

                                        If Not bErrorsAdded Then
                                            sbResultXMLString.Append(oAvailRateNode.OuterXml.Replace("</AvailableRate>", "<Errors><BlockingRuleMessage Allow='' Message='' /><ErrorMessage Allow='' Message='' /></Errors></AvailableRate>"))
                                        Else
                                            htErrorList.Remove(sKeyToRemove)
                                        End If
                                    Next
                                Catch ex As Exception
                                    ' do nothing
                                End Try
                            Next
                            ' ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                        End If

                    Next
                Next

            Next ' Brand Loop End

            ''XML sample for Aurora consumption
            ''returned by queryRequestVehicleAvailability
            '<Data>
            '        <AvailableVehicle>
            '        <PrdId>81852</PrdId>
            '        <CkoLocation>AKL</CkoLocation>
            '        <CkoDate>08/02/2013</CkoDate>
            '        <CkiLocation>AKL</CkiLocation>
            '        <CkiDate>20/02/2013</CkiDate>
            '        <Preference>0</Preference>
            '        <Cost>12000</Cost>
            '        <NumberOfVehicles>0</NumberOfVehicles>
            '        <ReducesRelocationCount>0</ReducesRelocationCount>
            '        </AvailableVehicle>
            '</Data>


            LogInformation("Done:Data Collection")


            '///////////Sorting now////////////////
            LogInformation("Start:Sorting")
            Dim xmlSort As New XmlDocument
            xmlSort.LoadXml(sbResultXMLString.ToString() & "</data>")

            Dim oNavigator As XPathNavigator = xmlSort.CreateNavigator()
            Dim sbSortedXML As New StringBuilder
            sbSortedXML.Append("<data>")

            Dim sXPath As String = "/data/AvailableRate/Package"
            Dim expression As XPathExpression = oNavigator.Compile(sXPath)

            expression.AddSort("@EstimatedTotal", XmlSortOrder.Ascending, XmlCaseOrder.None, String.Empty, XmlDataType.Number)

            Dim oNodeIterator As XPathNodeIterator = oNavigator.Select(expression)

            ' store the sorted list in a string
            While oNodeIterator.MoveNext()
                sbSortedXML.Append(CType(oNodeIterator.Current, IHasXmlNode).GetNode().ParentNode.OuterXml)
            End While

            ' put the sorted list into the doc
            sbResultXMLString = sbSortedXML

            LogInformation("Done:Sorting")
            '//////////Done Sorting////////////////


            LogInformation("Start:Error Collection")

            ' ////////////////////////////////////////////////////////
            ' Add Errors to XML doc - START
            Dim sbErrorsXMLString As New StringBuilder

            If htErrorList.Count > 0 Then
                sbErrorsXMLString.Append("<Errors>")
                For Each sKey As String In htErrorList.Keys
                    If sKey.Contains(PRODUCT_TAG) Then
                        Dim arrProduct As String() = sKey.Replace(PRODUCT_TAG, "").Split("|"c)

                        Dim sbVehicleErrorXMLString As New StringBuilder

                        sbVehicleErrorXMLString.Append("<AvailableRate>")
                        sbVehicleErrorXMLString.Append("  <Package BrandCode='" & arrProduct(2) & "'/>")
                        sbVehicleErrorXMLString.Append("  <Charge>")
                        sbVehicleErrorXMLString.Append("      <Det IsVeh='1' PrdCode='" & arrProduct(0) & "' PrdName='" & arrProduct(1) & "'>")
                        sbVehicleErrorXMLString.Append("          <RateBands/>")
                        sbVehicleErrorXMLString.Append("      </Det>")
                        sbVehicleErrorXMLString.Append("  </Charge>")

                        sbVehicleErrorXMLString.Append("  <Errors>")

                        Dim sBlockingRuleText As String = ""
                        sBlockingRuleText = htErrorList(sKey).ToString().Trim()
                        Try
                            If (sBlockingRuleText.IndexOf("."c) > -1) Then

                                ' this re-inserts the prefix and gets rid of the weird genxxx message part
                                Dim sPrefix As String = ""

                                If sBlockingRuleText.Contains(BLOCKING_RULE_PREFIX) Then
                                    sPrefix = BLOCKING_RULE_PREFIX
                                ElseIf sBlockingRuleText.Contains(TERMS_AND_CONDITIONS_PREFIX) Then
                                    sPrefix = TERMS_AND_CONDITIONS_PREFIX
                                ElseIf sBlockingRuleText.Contains(MACCOM_RENTAL_PREFIX) Then
                                    sPrefix = MACCOM_RENTAL_PREFIX
                                ElseIf sBlockingRuleText.Contains(THRIFTY_RENTAL_PREFIX) Then
                                    sPrefix = THRIFTY_RENTAL_PREFIX
                                ElseIf sBlockingRuleText.Contains(UNAVAILABLE_RATES_PREFIX) Then
                                    sPrefix = UNAVAILABLE_RATES_PREFIX
                                End If

                                sBlockingRuleText = sPrefix + sBlockingRuleText.Substring(sBlockingRuleText.IndexOf("."c) + 1, sBlockingRuleText.Length - sBlockingRuleText.IndexOf("."c) - 2).Trim()
                            End If
                        Catch ex As Exception
                            ' do nothing
                            sBlockingRuleText = htErrorList(sKey).ToString().Trim()
                        End Try

                        '  20.1.10 - check for thrifty prefix
                        If sBlockingRuleText.Contains(THRIFTY_RENTAL_PREFIX & ON_REQUEST_MESSAGE & Server.HtmlEncode("<br/>")) Then
                            ' more than one type of error, so get rid of the thrifty thingie
                            sBlockingRuleText = sBlockingRuleText.Replace(THRIFTY_RENTAL_PREFIX & ON_REQUEST_MESSAGE & Server.HtmlEncode("<br/>"), "")
                        End If
                        '  20.1.10 - check for thrifty prefix

                        ' 27.1.10 - check for maccom prefix
                        If sBlockingRuleText.Contains(MACCOM_RENTAL_PREFIX & ON_REQUEST_MESSAGE & Server.HtmlEncode("<br/>")) Then
                            ' more than one type of error, so get rid of the thrifty thingie
                            sBlockingRuleText = sBlockingRuleText.Replace(MACCOM_RENTAL_PREFIX & ON_REQUEST_MESSAGE & Server.HtmlEncode("<br/>"), "")
                        End If
                        ' 27.1.10 - check for maccom prefix

                        If sBlockingRuleText.Contains(BLOCKING_RULE_PREFIX) Then
                            ' blocking Rule Error
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='false' Message='" & sBlockingRuleText.Replace(BLOCKING_RULE_PREFIX, "") & "' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='' Message='' />")
                        ElseIf sBlockingRuleText.Contains(TERMS_AND_CONDITIONS_PREFIX) Then
                            ' TANDC
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='true' Message='" & sBlockingRuleText.Replace(TERMS_AND_CONDITIONS_PREFIX, "") & "' />")
                            ' Change on 22.9.9 - Unavailble message now to be sent to frontend
                        ElseIf sBlockingRuleText.Contains(UNAVAILABLE_RATES_PREFIX) Then
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='true' Message='" & sBlockingRuleText.Replace(UNAVAILABLE_RATES_PREFIX, "") & "' />")
                        ElseIf sBlockingRuleText.Contains("No valid Package found for specified brand") Then
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='true' Message='" & VEHICLE_RATES_UNAVAILABLE_MESSAGE & "' />")
                            ' Change on 22.9.9 - Unavailble message now to be sent to frontend
                            '  20.1.10 - check for thrifty prefix
                        ElseIf sBlockingRuleText.Contains(THRIFTY_RENTAL_PREFIX) Then
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='true' Message='" & sBlockingRuleText.Replace(THRIFTY_RENTAL_PREFIX, "") & "' />")
                            '  20.1.10 - check for thrifty prefix 
                            ' 27.1.10 - check for maccom prefix
                        ElseIf sBlockingRuleText.Contains(MACCOM_RENTAL_PREFIX) Then
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='true' Message='" & sBlockingRuleText.Replace(MACCOM_RENTAL_PREFIX, "") & "' />")
                            '  20.1.10 - check for thrifty prefix
                        Else
                            ' 27.1.10 - check for maccom prefix
                            ' OTHER ERROR
                            ' TANDC
                            sbVehicleErrorXMLString.Append("              <BlockingRuleMessage Allow='' Message='' />")
                            sbVehicleErrorXMLString.Append("              <ErrorMessage Allow='false' Message='" & sBlockingRuleText & "' />")
                        End If

                        sbVehicleErrorXMLString.Append("  </Errors>")

                        sbVehicleErrorXMLString.Append("</AvailableRate>")

                        ' If a specific vehicle was requested, put that error on top!
                        If UCase(Trim(arrProduct(0))) = UCase(Trim(VehicleCode)) AndAlso sbResultXMLString.ToString().IndexOf("<AvailableRate>") > -1 Then
                            sbResultXMLString.Insert(sbResultXMLString.ToString().IndexOf("<AvailableRate>"), sbVehicleErrorXMLString.ToString())
                        Else
                            sbResultXMLString.Append(sbVehicleErrorXMLString.ToString().Replace("No valid Package found for specified brand", VEHICLE_RATES_UNAVAILABLE_MESSAGE))
                        End If

                    ElseIf sKey.Contains(LOCATION_TAG) Then
                        sbErrorsXMLString.Append("<Location ZoneCode='" & XmlEncode(sKey.Replace(LOCATION_TAG, "")) & "' ErrorMessage='" & htErrorList(sKey) & "' />")
                    Else
                        ' Other weird errors!
                        sbErrorsXMLString.Append("<Error ErrCode='" & XmlEncode(sKey) & "' ErrorMessage='" & htErrorList(sKey) & "' />")
                    End If
                Next
                sbErrorsXMLString.Append("</Errors>")
            End If

            LogInformation("Done:Error Collection")

            ' Add Errors to XML doc - DONE
            ' ////////////////////////////////////////////////////////
            sbResultXMLString.Append(sbErrorsXMLString.ToString())

            sbResultXMLString.Append("</data>")

            oRetXml.LoadXml(sbResultXMLString.ToString())

            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ' Change to order errors @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            Dim sLtdAvail, sOtherErrors As New StringBuilder

            For Each oNode As XmlNode In oRetXml.DocumentElement.SelectNodes("AvailableRate")
                If oNode.SelectSingleNode("Package").Attributes("AvpId") Is Nothing Then
                    ' this is an error node
                    If Not ReferenceEquals(oNode, oRetXml.DocumentElement.FirstChild) OrElse VehicleCode.Trim() = "" Then
                        ' omit the first child, if its an error, it stays there!

                        If ( _
                                oNode.SelectSingleNode("Errors/ErrorMessage") IsNot Nothing _
                                AndAlso _
                                oNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Allow") IsNot Nothing _
                                AndAlso _
                                oNode.SelectSingleNode("Errors/ErrorMessage").Attributes("Allow").Value.ToUpper().Contains("FALSE") _
                           ) _
                           OrElse _
                           ( _
                                oNode.SelectSingleNode("Errors/BlockingRuleMessage") IsNot Nothing _
                                AndAlso _
                                oNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Allow") IsNot Nothing _
                                AndAlso _
                                oNode.SelectSingleNode("Errors/BlockingRuleMessage").Attributes("Allow").Value.ToUpper().Contains("FALSE") _
                           ) _
                        Then
                            ' this is a limited availability node
                            ' collect separately
                            sLtdAvail.Append(oNode.OuterXml)
                        Else
                            sOtherErrors.Append(oNode.OuterXml)
                        End If

                        ' take off this node
                        oNode.ParentNode.RemoveChild(oNode)
                    End If
                End If
            Next

            Dim sResultPart1, sResultPart2 As String

            If oRetXml.OuterXml.IndexOf("</AvailableRate><Errors") <> -1 Then
                sResultPart1 = oRetXml.OuterXml.Substring(0, oRetXml.OuterXml.IndexOf("</AvailableRate><Errors") + Len("</AvailableRate>"))
                sResultPart2 = oRetXml.OuterXml.Substring(oRetXml.OuterXml.IndexOf("</AvailableRate><Errors") + Len("</AvailableRate>"))
            Else
                ' ALL ERRORS IN THE XML
                sResultPart1 = oRetXml.OuterXml.Substring(0, oRetXml.OuterXml.IndexOf("<data><Errors") + Len("<data>"))
                sResultPart2 = oRetXml.OuterXml.Substring(oRetXml.OuterXml.IndexOf("<data><Errors") + Len("<data>"))
            End If '		oRetXml.OuterXml	"<data><Errors></Errors></data>"	String


            ''///////////Sorting now LTD AVAIL ERRORS////////////////
            'xmlSort.LoadXml("<data>" & sLtdAvail.ToString() & "</data>")

            'oNavigator = xmlSort.CreateNavigator()
            'sbSortedXML = New StringBuilder

            'sXPath = "/data/AvailableRate/Charge"
            'expression = oNavigator.Compile(sXPath)

            'expression.AddSort("@PrdCode", XmlSortOrder.Ascending, XmlCaseOrder.None, String.Empty, XmlDataType.Text)

            'oNodeIterator = oNavigator.Select(expression)

            '' store the sorted list in a string
            'While oNodeIterator.MoveNext()
            '    sbSortedXML.Append(CType(oNodeIterator.Current, IHasXmlNode).GetNode().ParentNode.OuterXml)
            'End While

            '' put the sorted list into the doc
            'sLtdAvail = sbSortedXML

            ''//////////Done Sorting LTD AVAIL ERRORS////////////////

            ''///////////Sorting now OTHER ERRORS////////////////
            'xmlSort.LoadXml("<data>" & sOtherErrors.ToString() & "</data>")

            'oNavigator = xmlSort.CreateNavigator()
            'sbSortedXML = New StringBuilder

            'sXPath = "/data/AvailableRate/Charge"
            'expression = oNavigator.Compile(sXPath)

            'expression.AddSort("@PrdCode", XmlSortOrder.Ascending, XmlCaseOrder.None, String.Empty, XmlDataType.Text)

            'oNodeIterator = oNavigator.Select(expression)

            '' store the sorted list in a string
            'While oNodeIterator.MoveNext()
            '    sbSortedXML.Append(CType(oNodeIterator.Current, IHasXmlNode).GetNode().ParentNode.OuterXml)
            'End While

            '' put the sorted list into the doc
            'sOtherErrors = sbSortedXML

            ''//////////Done Sorting OTHER ERRORS////////////////


            oRetXml.LoadXml(sResultPart1 & sLtdAvail.ToString() & sOtherErrors.ToString() & sResultPart2)

            'If Not String.IsNullOrEmpty(Trim(sVehicleCodeBak)) Then
            '    ' vehicle code was specified
            '    For Each oNode As XmlNode In oRetXml.DocumentElement.SelectNodes("AvailableRate")

            '        If oNode.SelectSingleNode("Charge/Det").Attributes("PrdCode").Value.ToUpper().Trim() = Trim(UCase(sVehicleCodeBak)) Then
            '            oNode.ParentNode.InsertBefore(oNode, oNode.ParentNode.FirstChild)
            '        End If

            '    Next
            'End If

            If Not skipprice Then
                For Each oNode As XmlNode In oRetXml.DocumentElement.SelectNodes("AvailableRate")
                    If Not String.IsNullOrEmpty(Trim(sVehicleCodeBak)) Then
                        If oNode.SelectSingleNode("Charge/Det").Attributes("PrdCode").Value.ToUpper().Trim() = Trim(UCase(sVehicleCodeBak)) Then
                            oNode.ParentNode.InsertBefore(oNode, oNode.ParentNode.FirstChild)
                        End If
                    End If
                    If oNode.SelectSingleNode("Charge/Det").Attributes("PrdCode").Value.ToUpper().Trim().Equals(String.Empty) Then
                        oNode.ParentNode.RemoveChild(oNode)
                    End If

                Next

                ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                ' Change to order errors @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


                ' Thrifty Debug Info
                For Each oNode As XmlNode In oRetXml.DocumentElement.SelectNodes("AvailableRate")
                    For Each sKey As String In htThriftyCallDataList.Keys
                        Dim sNodeName As String = ""
                        Try
                            sNodeName = oNode.SelectSingleNode("Charge/Det[@IsVeh='1']").Attributes("PrdCode").Value.Trim() & "|" & oNode.SelectSingleNode("Package").Attributes("BrandCode").Value.Trim()
                        Catch ex As Exception
                            Console.Write("")
                        End Try
                        If sNodeName = sKey Then
                            ' this is the node for this error
                            Dim oThriftyDebugInfo As XmlNode = oNode.OwnerDocument.CreateNode(XmlNodeType.Element, "ThriftyDebugInfo", "")
                            oThriftyDebugInfo.InnerXml = XmlDecode(htThriftyCallDataList(sKey))
                            oNode.AppendChild(oThriftyDebugInfo)
                            ' this function modifies the error message, overwriting the error section if needed
                            OverwriteErrorSectionWithThriftyError(oNode.SelectSingleNode("Errors"), oThriftyDebugInfo)
                            Exit For
                        End If
                    Next
                Next
                ' Thrifty Debug Info
            End If




        Catch ex As Exception
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            oRetXml = GetErrorXML(ex)
        End Try

        LogInformation("GetAvailability: RS:" & oRetXml.OuterXml)

        Return oRetXml

    End Function

    Sub OverwriteErrorSectionWithThriftyError(ByRef ErrorNode As XmlNode, ByVal ThriftyDebugInfoNode As XmlNode)
        ' Config things are
        ' 1 - display errors on frontend or not
        ' 2 - use predefined list of "safe" errors or not
        ' 3 - overwrite the error section or not

        If THRIFTY_OVERWRITE_ERROR_SECTION_WITH_THRIFTY_ERRORS AndAlso ThriftyDebugInfoNode.SelectSingleNode("ThriftyDebugInfo/ThriftyError") IsNot Nothing Then

            'If THRIFTY_DISPLAY_THRIFTY_ERRORS Then
            If THRIFTY_DISPLAY_SAFE_THRIFTY_ERRORS_ONLY Then

                Dim bThisErrorIsSafeToShow As Boolean = False
                For Each sErrorCode As String In THRIFTY_SAFE_THRIFTY_ERROR_CODE_LIST
                    ' if error code in safe list
                    ' othriftynode.code = sErrorCode
                    If ThriftyDebugInfoNode.SelectSingleNode("ThriftyDebugInfo/ThriftyError/Code").InnerText = sErrorCode Then
                        bThisErrorIsSafeToShow = True
                        Exit For
                    End If
                Next

                If bThisErrorIsSafeToShow Then
                    ErrorNode.InnerXml = "<BlockingRuleMessage Allow='' Message='' /><ErrorMessage Allow='" & THRIFTY_DISPLAY_THRIFTY_ERRORS.ToString().ToLower() & "' Message='" & ThriftyDebugInfoNode.SelectSingleNode("ThriftyDebugInfo/ThriftyError/Text").InnerText & "'/>"
                Else
                    ErrorNode.InnerXml = "<BlockingRuleMessage Allow='" & THRIFTY_DISPLAY_THRIFTY_ERRORS.ToString().ToLower() & "' Message='" & ThriftyDebugInfoNode.SelectSingleNode("ThriftyDebugInfo/ThriftyError/Text").InnerText & "' /><ErrorMessage Allow='' Message=''/>"
                End If

            Else ' If THRIFTY_DISPLAY_SAFE_THRIFTY_ERRORS_ONLY Then

                ErrorNode.InnerXml = "<BlockingRuleMessage Allow='' Message='' /><ErrorMessage Allow='" & THRIFTY_DISPLAY_THRIFTY_ERRORS.ToString().ToLower() & "' Message='" & ThriftyDebugInfoNode.SelectSingleNode("ThriftyDebugInfo/ThriftyError/Text").InnerText & "'/>"

            End If ' If THRIFTY_DISPLAY_SAFE_THRIFTY_ERRORS_ONLY Then

            'Else '  If THRIFTY_DISPLAY_THRIFTY_ERRORS Then

            'End If '  If THRIFTY_DISPLAY_THRIFTY_ERRORS Then


        End If ' If THRIFTY_OVERWRITE_ERROR_SECTION_WITH_THRIFTY_ERRORS Then

    End Sub

    <WebMethod(Description:="This method gets vehicle availability based on input search criteria")> _
    Public Function GetVehicleStatus(ByVal VehicleCode As String, ByVal CountryCode As String, ByVal Brand As String, _
                                    ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As DateTime, _
                                    ByVal CheckInZoneCode As String, ByVal CheckInDateTime As DateTime, _
                                    ByVal AgentCode As String, ByVal PackageCode As String, ByVal IsVan As Boolean) As String

        LogInformation("GetVehicleStatus: RQ: VehicleCode=""" & VehicleCode & _
                                         """, CountryCode=""" & CountryCode & _
                                         """, Brand=""" & Brand & _
                                         """, CheckOutZoneCode=""" & CheckOutZoneCode & _
                                         """, CheckOutDateTime=""" & CheckOutDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                         """, CheckInZoneCode=""" & CheckInZoneCode & _
                                         """, CheckInDateTime=""" & CheckInDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                         """, AgentCode=""" & AgentCode & _
                                         """, PackageCode=""" & PackageCode & _
                                         """, IsVan=""" & IsVan.ToString() & _
                                         """")

        Dim sPackageId, sCheckOutLocationCode, sCheckInLocationCode As String

        Dim dtCheckOutList, dtCheckInList As DataTable

        LogInformation("Starting:WebServiceData.GetLocation(CheckOut). sBrandCode=""" & Brand & """")
        dtCheckOutList = WebServiceData.GetLocation(CheckOutZoneCode, Brand, IIf(IsVan, "AV", "AC"), CountryCode)
        LogInformation("Done:WebServiceData.GetLocation(CheckOut). ")

        LogInformation("Starting:WebServiceData.GetLocation(CheckIn). sBrandCode=""" & Brand & """")
        dtCheckInList = WebServiceData.GetLocation(CheckInZoneCode, Brand, IIf(IsVan, "AV", "AC"), CountryCode)
        LogInformation("Done:WebServiceData.GetLocation(CheckIn). ")

        For Each rwCheckOutLocation As DataRow In dtCheckOutList.Rows
            For Each rwcheckInLocation As DataRow In dtCheckInList.Rows

                Dim sCheckOutLocationType, sCheckInLocationType As String

                sCheckOutLocationType = UCase(rwCheckOutLocation("BranchType").ToString())
                sCheckInLocationType = UCase(rwcheckInLocation("BranchType").ToString())

                If sCheckOutLocationType = sCheckInLocationType Then ' dont process branch to thrifty or vice versa

                    sCheckOutLocationCode = UCase(rwCheckOutLocation("LocCode").ToString())
                    sCheckInLocationCode = UCase(rwcheckInLocation("LocCode").ToString())

                    ' Translate Zone Code to Location Code - DONE
                    ' ///////////////////////////////////////////////////////////////////////



                End If
            Next
        Next

        LogInformation("Starting:WebServiceData.GetPackageId")
        sPackageId = WebServiceData.GetPackageId(PackageCode, sCheckOutLocationCode, CheckOutDateTime, CheckInDateTime)
        LogInformation("Done:WebServiceData.GetPackageId. sPackageId=""" & sPackageId & """")

        Dim sResultString As String = ""

        LogInformation("Starting:GetVehicleAvailability")
        sResultString = CStr(GetVehicleAvailability("", VehicleCode, "", CountryCode, _
                             sCheckOutLocationCode, CheckOutDateTime, _
                             sCheckInLocationCode, CheckInDateTime, _
                             AgentCode, sPackageId))
        LogInformation("Done:GetVehicleAvailability")

        LogInformation("GetVehicleStatus: RS:" & sResultString)

        Return sResultString

    End Function

    ''' <summary>
    ''' rev:mia Nov.19 2012 - create an optional parameter for the AvailCode 
    ''' </summary>
    ''' <param name="BrandCode"></param>
    ''' <param name="VehicleCode"></param>
    ''' <param name="VehicleName"></param>
    ''' <param name="CountryCode"></param>
    ''' <param name="CheckOutLocationCode"></param>
    ''' <param name="CheckOutDateTime"></param>
    ''' <param name="CheckInLocationCode"></param>
    ''' <param name="CheckInDateTime"></param>
    ''' <param name="AgentCode"></param>
    ''' <param name="PackageId"></param>
    ''' <param name="ErrorList"></param>
    ''' <param name="ThriftyDebugDataList"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Function GetVehicleAvailability(ByVal BrandCode As String, ByVal VehicleCode As String, ByVal VehicleName As String, _
                                    ByVal CountryCode As String, ByVal CheckOutLocationCode As String, _
                                    ByVal CheckOutDateTime As DateTime, ByVal CheckInLocationCode As String, _
                                    ByVal CheckInDateTime As DateTime, ByVal AgentCode As String, _
                                    ByVal PackageId As String, _
                                    Optional ByRef ErrorList As Hashtable = Nothing, _
                                    Optional ByRef ThriftyDebugDataList As Hashtable = Nothing _
                                    ) As Char

        LogInformation("GetVehicleAvailability: RQ: BrandCode = """ & BrandCode & _
                                               """, VehicleCode = """ & VehicleCode & _
                                               """, VehicleName=""" & VehicleName & _
                                               """, CountryCode=""" & CountryCode & _
                                               """, CheckOutLocationCode=""" & CheckOutLocationCode & _
                                               """, CheckOutDateTime=""" & CheckOutDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                               """, CheckInLocationCode=""" & CheckInLocationCode & _
                                               """, CheckInDateTime=""" & CheckInDateTime.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                               """, AgentCode=""" & AgentCode & _
                                               """, PackageId=""" & PackageId & _
                                               """, ErrorList=""" & IIf(ErrorList Is Nothing, "Nothing", "(Present)") & _
                                               """")

        Dim cAvailStatus As Char

        LogInformation("Starting:WebServiceData.GetFleetStatus")

        Dim sErrorMessage As String = ""
        Dim iDvassSequenceNumber As Int64

        cAvailStatus = WebServiceData.GetFleetStatus(VehicleCode, CheckOutLocationCode, CheckOutDateTime, _
                                                     CheckInLocationCode, CheckInDateTime, _
                                                     AgentCode, PackageId, sErrorMessage, iDvassSequenceNumber)
        ''AvailCode = cAvailStatus

        LogInformation("Done:WebServiceData.GetFleetStatus. cAvailStatus=""" & cAvailStatus & """. sErrorMessage=""" & sErrorMessage & """")

        If sErrorMessage <> "" Then
            Throw New Exception(sErrorMessage)
        End If
        Dim alternateservice As New GetAlternateVehicleService

        Select Case cAvailStatus
            Case AVAILABLE_MACCOM_FLAG
                ' thrifty request 
                ' On-Request
                If ErrorList IsNot Nothing Then
                    AddErrorMessageToList(ErrorList, VehicleCode & "|" & VehicleName & "|" & BrandCode & PRODUCT_TAG, MACCOM_RENTAL_PREFIX & ON_REQUEST_MESSAGE)

                    Try
                        alternateservice.AddAlternateVehicleResponses(cAvailStatus, _
                                                                  AVAILABLE_YES_FLAG, _
                                                                  BrandCode, _
                                                                  VehicleCode, _
                                                                  VehicleName, _
                                                                  MACCOM_RENTAL_PREFIX & ON_REQUEST_MESSAGE, _
                                                                  CheckOutLocationCode.ToString, _
                                                                  CheckOutDateTime.ToString, _
                                                                  CheckInLocationCode, _
                                                                  CheckInDateTime)
                    Catch ex As Exception
                        ''quietly escape from the exception
                    End Try



                End If
                ' Dont check!

                cAvailStatus = AVAILABLE_YES_FLAG

                Exit Select
            Case AVAILABLE_THRIFTY_FLAG
                ' thrifty request 
                Dim sThriftyStatus As String

                ' Thrifty Debug Info
                Dim sThriftyDebugInfo As String = ""
                ' Thrifty Debug Info

                ' change for TWSI
                Try
                    LogInformation("THRIFTY CALL: STARTING THRIFTY CALL")
                    Dim oLogin As New Aurora.Thrifty.WS.Login()

                    LogInformation("THRIFTY CALL: Starting oLogin.ProceedLogin()")
                    oLogin.ProceedLogin()
                    LogInformation("THRIFTY CALL: Done oLogin.ProceedLogin()")

                    Dim oThriftyAvail As New Aurora.Thrifty.WS.Avail(oLogin)

                    LogInformation("THRIFTY CALL: Starting oThriftyAvail.CheckVehicleStatus()")

                    ' log request response
                    Dim sbThriftyLogText As New StringBuilder
                    Dim htwThriftyLogTextWriter As New HtmlTextWriter(New System.IO.StringWriter(sbThriftyLogText))
                    sThriftyStatus = oThriftyAvail.CheckVehicleStatus(CheckOutLocationCode, CheckInLocationCode, CheckOutDateTime, CheckInDateTime, VehicleCode, htwThriftyLogTextWriter)
                    htwThriftyLogTextWriter.Flush()
                    LogInformation("THRIFTY WEBSERVICE DATA : " & vbNewLine & sbThriftyLogText.ToString())

                    LogInformation("THRIFTY CALL: Done oThriftyAvail.CheckVehicleStatus()")

                    ' Thrifty Debug Info
                    sThriftyDebugInfo = "<ThriftyDebugInfo><ThriftyStatus>" & sThriftyStatus & "</ThriftyStatus></ThriftyDebugInfo>"
                    ' Thrifty Debug Info
                Catch ex As ApplicationException
                    ' this should capture thrifty errors
                    LogInformation("THRIFTY CALL: EXCEPTION - " & ex.Message & vbNewLine & ex.StackTrace)
                    sThriftyStatus = AVAILABLE_NO_FLAG

                    ' Thrifty Debug Info
                    'sThriftyDebugInfo = "THRIFTY WEBSERVICE DATA : " & vbNewLine & ex.Message.Replace("Aurora.Thrifty.WS.Avail.VehAvailRate", "") & " ThriftyStatus = " & sThriftyStatus
                    sThriftyDebugInfo = "<ThriftyDebugInfo><ThriftyStatus>" & sThriftyStatus & "</ThriftyStatus>" & ex.Message & "</ThriftyDebugInfo>"
                    ' Thrifty Debug Info

                    Try
                        alternateservice.AddAlternateVehicleResponses(cAvailStatus, AVAILABLE_NO_FLAG, BrandCode, VehicleCode, VehicleName, ex.Message, CheckOutLocationCode.ToString, CheckOutDateTime.ToString, CheckInLocationCode, CheckInDateTime)
                    Catch exAltApp As Exception
                        ''quietly escape from the exception
                    End Try



                Catch ex As Exception
                    LogInformation("THRIFTY CALL: EXCEPTION - " & ex.Message & vbNewLine & ex.StackTrace)
                    sThriftyStatus = AVAILABLE_NO_FLAG

                    ' Thrifty Debug Info
                    ' sThriftyDebugInfo = "THRIFTY WEBSERVICE DATA : " & vbNewLine & " EXCEPTION " & vbNewLine & ex.Message & vbNewLine & ex.StackTrace & vbNewLine & " ThriftyStatus = " & sThriftyStatus
                    sThriftyDebugInfo = "<ThriftyDebugInfo><ThriftyStatus>" & sThriftyStatus & "</ThriftyStatus>" & ex.Message & "</ThriftyDebugInfo>"
                    ' Thrifty Debug Info

                    Try
                        alternateservice.AddAlternateVehicleResponses(cAvailStatus, AVAILABLE_NO_FLAG, BrandCode, VehicleCode, VehicleName, ex.Message, CheckOutLocationCode.ToString, CheckOutDateTime.ToString, CheckInLocationCode, CheckInDateTime)
                    Catch exAltEx As Exception
                        ''quietly escape from the exception
                    End Try



                End Try

                LogInformation("THRIFTY CALL: COMPLETE - sThriftyStatus = " & sThriftyStatus)

                ' Thrifty Debug Info
                Try
                    AddErrorMessageToList(ThriftyDebugDataList, VehicleCode & "|" & BrandCode, sThriftyDebugInfo)
                Catch ex As Exception

                End Try

                ' Thrifty Debug Info

                Select Case sThriftyStatus
                    Case AVAILABLE_YES_FLAG
                        cAvailStatus = AVAILABLE_YES_FLAG

                        Exit Select
                    Case AVAILABLE_NO_FLAG
                        cAvailStatus = AVAILABLE_NO_FLAG
                        Exit Select
                    Case AVAILABLE_REQUEST_FLAG
                        ' On-Request
                        If ErrorList IsNot Nothing Then
                            AddErrorMessageToList(ErrorList, VehicleCode & "|" & VehicleName & "|" & BrandCode & PRODUCT_TAG, THRIFTY_RENTAL_PREFIX & ON_REQUEST_MESSAGE)

                            Try
                                alternateservice.AddAlternateVehicleResponses(cAvailStatus, _
                                                                          AVAILABLE_YES_FLAG, _
                                                                          BrandCode, _
                                                                          VehicleCode, _
                                                                          VehicleName, _
                                                                          THRIFTY_RENTAL_PREFIX & ON_REQUEST_MESSAGE, _
                                                                          CheckOutLocationCode.ToString, _
                                                                          CheckOutDateTime.ToString, _
                                                                          CheckInLocationCode, _
                                                                          CheckInDateTime)

                            Catch ex As Exception
                                ''trap error and return quietly
                            End Try


                        End If

                        ' Dont check!
                        cAvailStatus = AVAILABLE_YES_FLAG
                        Exit Select
                End Select
                ' change for TWSI

            Case AVAILABLE_NO_FLAG
                ' 2.1     If Not Available -> discard result
                cAvailStatus = AVAILABLE_NO_FLAG
                Exit Select
            Case AVAILABLE_REQUEST_FLAG
                ' 2.3     If Request -> Check DVASS
                Dim bDVASSStatus As Boolean = False
                Dim xmlMessage As XmlNode
                Dim sMessage, sStatus As String
                Dim oAuroraWebService As AuroraWebService = New AuroraWebService
                oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                                 ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                                 ConfigurationSettings.AppSettings("DomainName"))

                LogInformation("Starting:oAuroraWebService.GetDVASSStatus")
                xmlMessage = oAuroraWebService.GetDVASSStatus(CountryCode, iDvassSequenceNumber.ToString(), CheckOutLocationCode, _
                                                              CheckOutDateTime, CheckInLocationCode, CheckInDateTime)
                LogInformation("Done:oAuroraWebService.GetDVASSStatus. xmlMessage=" & xmlMessage.OuterXml)

                With xmlMessage
                    sMessage = GetProperStringValue(.SelectSingleNode("Message").InnerText)
                    sStatus = GetProperStringValue(.SelectSingleNode("Status").InnerText)

                    If sStatus.ToUpper().Equals("ERROR") Then
                        ' Error, hence unavailable
                        If ErrorList IsNot Nothing AndAlso Not String.IsNullOrEmpty(sMessage) Then
                            AddErrorMessageToList(ErrorList, VehicleCode & "|" & VehicleName & "|" & BrandCode & PRODUCT_TAG, sMessage)
                        Else
                            If (Not String.IsNullOrEmpty(sMessage)) Then

                                Try
                                    alternateservice.AddAlternateVehicleResponses(cAvailStatus, _
                                                                          AVAILABLE_NO_FLAG, _
                                                                          BrandCode, _
                                                                          VehicleCode, _
                                                                          VehicleName, _
                                                                          sMessage, _
                                                                          CheckOutLocationCode.ToString, _
                                                                          CheckOutDateTime.ToString, _
                                                                          CheckInLocationCode, _
                                                                          CheckInDateTime)

                                Catch ex As Exception
                                    ''trap error and return quietly
                                End Try


                            End If
                        End If

                        ' 2.3.1       If DVASS Says Not Available -> Discard this vehicle from result set
                        cAvailStatus = AVAILABLE_NO_FLAG
                        Exit Select
                    Else
                        ' No error
                        If CBool(GetProperStringValue(.SelectSingleNode("Result").InnerText)) Then
                            ' Available
                            If ErrorList IsNot Nothing AndAlso Not String.IsNullOrEmpty(sMessage) Then
                                AddErrorMessageToList(ErrorList, VehicleCode & "|" & VehicleName & "|" & BrandCode & PRODUCT_TAG, sMessage)
                            Else
                                If (Not String.IsNullOrEmpty(sMessage)) Then

                                    Try
                                        alternateservice.AddAlternateVehicleResponses(cAvailStatus, _
                                                                              AVAILABLE_YES_FLAG, _
                                                                              BrandCode, _
                                                                              VehicleCode, _
                                                                              VehicleName, _
                                                                              sMessage, _
                                                                              CheckOutLocationCode.ToString, _
                                                                              CheckOutDateTime.ToString, _
                                                                              CheckInLocationCode, _
                                                                              CheckInDateTime)
                                    Catch ex As Exception
                                        ''trap error and return quietly
                                    End Try

                                End If
                            End If
                            ' 2.3.2       If DVASS Says Available 
                            cAvailStatus = AVAILABLE_YES_FLAG
                            Exit Select
                        Else
                            ' Unavailable
                            If ErrorList IsNot Nothing AndAlso Not String.IsNullOrEmpty(sMessage) Then
                                AddErrorMessageToList(ErrorList, VehicleCode & "|" & VehicleName & "|" & BrandCode & PRODUCT_TAG, sMessage)
                            Else
                                If (Not String.IsNullOrEmpty(sMessage)) Then
                                    Try
                                        alternateservice.AddAlternateVehicleResponses(cAvailStatus, _
                                                                              AVAILABLE_NO_FLAG, _
                                                                              BrandCode, _
                                                                              VehicleCode, _
                                                                              VehicleName, _
                                                                              sMessage, _
                                                                              CheckOutLocationCode.ToString, _
                                                                              CheckOutDateTime.ToString, _
                                                                              CheckInLocationCode, _
                                                                              CheckInDateTime)
                                    Catch ex As Exception
                                        ''trap error and return quietly
                                    End Try




                                End If
                            End If
                            ' 2.3.1       If DVASS Says Not Available -> Discard this vehicle from result set
                            cAvailStatus = AVAILABLE_NO_FLAG
                            Exit Select
                        End If
                    End If
                End With

            Case AVAILABLE_YES_FLAG
                ' 2.2     If Available 
                cAvailStatus = AVAILABLE_YES_FLAG
                Exit Select
        End Select

        LogInformation("GetVehicleAvailability: RS:" & cAvailStatus)

        Return cAvailStatus
    End Function

    Function DoAgentCountryBrandLookup(ByVal AgentCode As String, ByVal CountryCode As String) As ArrayList
        Dim htBrandList As Hashtable = CType(Application("AgentCountryBrandList"), Hashtable)
        If htBrandList(AgentCode & "-" & CountryCode) Is Nothing Then
            ' no entry, call db + add here!
            Dim alBrands As ArrayList = New ArrayList
            Dim dtBrands As DataTable
            dtBrands = WebServiceData.GetBrandsForAgentCountry(AgentCode, CountryCode)
            For Each rwBrand As DataRow In dtBrands.Rows
                alBrands.Add(rwBrand("PkgBrdCode"))
            Next
            htBrandList.Add(AgentCode & "-" & CountryCode, alBrands)
        End If
        Return CType(htBrandList(AgentCode & "-" & CountryCode), ArrayList)
    End Function

#End Region

#Region "Create Booking and add Extra Hire Items to Quote"

#Region "Constants"
    Const ADULT_STRING As String = "ADULT"
    Const CHILD_STRING As String = "CHILD"
    Const INFANT_STRING As String = "INFANT"
#End Region

    <WebMethod(Description:="This method creates a booking in Aurora, Adds Payment, Extra Hire Items and sends a confirmation via email. Errors are logged to the booking in Aurora as notes")> _
    Public Function CreateBookingWithExtraHireItemAndPayment(ByVal InputData As XmlDocument) As XmlDocument

        LogInformation("Start:CreateBookingWithExtraHireItemAndPayment. RQ : InputData='" & InputData.OuterXml & "'")

        ' change for information security purposes
        Dim oAuroraWebService As AuroraWebService = New AuroraWebService
        oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                         ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                         ConfigurationSettings.AppSettings("DomainName"))
        Dim CreditCardNumber, EncryptedCreditCardNumber As String

        ' 7.7.10 - Shoel - Change for Quote bookings
        Dim bIsCreditCardInfoPresent As Boolean = Not String.IsNullOrEmpty(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))

        If bIsCreditCardInfoPresent Then
            ' 7.7.10 - Shoel - Change for Quote bookings

            CreditCardNumber = GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
            'commented by nimesh on 23rd jan 2015 because credit card encryption is not required
            ''Changed on 14thJan 2015 - Nimesh
            ''If Not CreditCardNumber.Trim().Substring(0, CreditCardNumber.Length - 5).ToLower().Equals("xxxx-xxxx-xxxx") Then

            'Try
            '    Dim xmlEncryptedData As XmlNode
            '    LogInformation("oAuroraWebService.EncryptTextForAurora : Call start")
            '    xmlEncryptedData = oAuroraWebService.EncryptTextForAurora(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))
            '    LogInformation("oAuroraWebService.EncryptTextForAurora : Call end : Returned message : " & xmlEncryptedData.OuterXml)

            '    EncryptedCreditCardNumber = xmlEncryptedData.SelectSingleNode("EncryptedData").InnerText
            '    InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = EncryptedCreditCardNumber
            'Catch ex As Exception
            '    ' do nothing
            '    LogInformation("Call to Aurora text encryption function failed : " & ex.Message & vbNewLine & ex.StackTrace)
            'End Try
            ''Else
            ''    EncryptedCreditCardNumber = CreditCardNumber
            ''    InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = CreditCardNumber
            ''End If
            ''End Changed on 14thJan 2015 - Nimesh
            'End commented by nimesh on 23rd jan 2015 because credit card encryption is not required
            'added by nimesh on 23rd jan 2015 because credit card encryption is not required
            EncryptedCreditCardNumber = CreditCardNumber
            'end added by nimesh on 23rd jan 2015 because credit card encryption is not required
            '' change for information security purposes

            ' 7.7.10 - Shoel - Change for Quote bookings
        End If
        ' 7.7.10 - Shoel - Change for Quote bookings

        LogInformation("CreateBookingWithExtraHireItemAndPayment: RQ: InputData=""" & InputData.OuterXml)

        Dim AddBookingNumber As String, AvpId As String, _
        AgentReference As String, QuantityRequested As Int16, _
        Status As String, _
        Title As String, FirstName As String, LastName As String, _
        Email As String, PhoneNumber As String, _
        Note As String, CardType As String, _
        PaymentAmount As Decimal, PaymentSurchargeAmount As Decimal, _
        CreditCardName As String, _
        ApprovalRef As String, ExpiryDate As String, _
        DPSAuthCode As String, DPSTxnRef As String, _
        CheckOutDateTime As DateTime, CheckOutLocationCode As String, _
        CheckInDateTime As DateTime, CheckInLocationCode As String, _
        sCountryCode As String, xmlProductList As XmlNode, _
        UserCode As String, ConfirmationHeaderText As String, ConfirmationFooterText As String, _
        bGetConfXML, bIsRequestBooking As Boolean, _
        sCountryOfResidence As String, sPreferredModeOfCommunication As String, _
        sLoyaltyCardNumber As String, _
        SelectedSlot As String, _
        SlotId As String
        Dim xmlResult As New XmlDocument

        ''rev:mia April 
        Dim cvv2 As String = ""

        ''rev:mia aug 3 2012 - added billing token here for b2c consumptions
        Dim billingtoken As String = ""

        LogInformation("Starting:Reading Input XML")
        With InputData.DocumentElement
            AddBookingNumber = GetInnerText(.SelectSingleNode("RentalDetails/AddBookingNumber"))
            AvpId = GetInnerText(.SelectSingleNode("RentalDetails/AvpId"))
            AgentReference = GetInnerText(.SelectSingleNode("RentalDetails/AgentReference"))
            QuantityRequested = CInt(GetInnerText(.SelectSingleNode("RentalDetails/QuantityRequested")))
            Status = GetInnerText(.SelectSingleNode("RentalDetails/Status"))
            CheckOutDateTime = Aurora.Common.Utility.DateDBParse(GetInnerText(.SelectSingleNode("RentalDetails/CheckOutDateTime")))
            CheckOutLocationCode = GetInnerText(.SelectSingleNode("RentalDetails/CheckOutLocationCode"))
            CheckInDateTime = Aurora.Common.Utility.DateDBParse(GetInnerText(.SelectSingleNode("RentalDetails/CheckInDateTime")))
            CheckInLocationCode = GetInnerText(.SelectSingleNode("RentalDetails/CheckInLocationCode"))
            Note = GetInnerText(.SelectSingleNode("RentalDetails/Note"))
            xmlProductList = .SelectSingleNode("RentalDetails/ProductList")
            sCountryCode = GetInnerText(.SelectSingleNode("RentalDetails/CountryCode")).ToUpper()
            UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
            'Added by Nimesh on 21st Aug for slot selection
            SelectedSlot = GetInnerText(.SelectSingleNode("RentalDetails/SelectedSlot"))
            SlotId = GetInnerText(.SelectSingleNode("RentalDetails/SlotId"))
            'End added by Nimesh
            'UserCode = GetUserCodeForExtraHireItemAddition(AvpId)

            ConfirmationHeaderText = GetInnerText(.SelectSingleNode("RentalDetails/ConfirmationHeaderText"))
            ConfirmationFooterText = GetInnerText(.SelectSingleNode("RentalDetails/ConfirmationFooterText"))
            bGetConfXML = CBool(GetInnerText(.SelectSingleNode("RentalDetails/GetConfXML")))

            Title = GetInnerText(.SelectSingleNode("CustomerData/Title"))
            FirstName = GetInnerText(.SelectSingleNode("CustomerData/FirstName"))
            LastName = GetInnerText(.SelectSingleNode("CustomerData/LastName"))
            Email = GetInnerText(.SelectSingleNode("CustomerData/Email"))
            PhoneNumber = GetInnerText(.SelectSingleNode("CustomerData/PhoneNumber"))
            sCountryOfResidence = GetInnerText(.SelectSingleNode("CustomerData/CountryOfResidence"))
            sPreferredModeOfCommunication = GetInnerText(.SelectSingleNode("CustomerData/PreferredModeOfCommunication"))

            ' Shoel : 14.6.10 : Change for Loyalty Card Number
            sLoyaltyCardNumber = GetInnerText(.SelectSingleNode("CustomerData/LoyaltyCardNumber"))
            ' Shoel : 14.6.10 : Change for Loyalty Card Number

            CardType = GetInnerText(.SelectSingleNode("CreditCardDetails/CardType"))
            'Changed on 14thJan 2015 - Nimesh
            CreditCardNumber = GetInnerText(.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
            'End Changed on 14thJan 2015 - Nimesh
            CreditCardName = GetInnerText(.SelectSingleNode("CreditCardDetails/CreditCardName"))
            ApprovalRef = GetInnerText(.SelectSingleNode("CreditCardDetails/ApprovalRef"))
            ExpiryDate = GetInnerText(.SelectSingleNode("CreditCardDetails/ExpiryDate"))
            If GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentAmount")) <> "" Then
                PaymentAmount = GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentAmount"))
            End If
            If GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentSurchargeAmount")) <> "" Then
                PaymentSurchargeAmount = GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentSurchargeAmount"))
            End If

            ''rev:mia April 
            Try
                cvv2 = GetInnerText(.SelectSingleNode("CreditCardDetails/CVV2"))
            Catch ex As Exception
            End Try

            ''rev:mia aug 3 2012 - added billing token here for b2c consumptions
            Try
                billingtoken = GetInnerText(.SelectSingleNode("CreditCardDetails/BillingToken"))
            Catch ex As Exception
            End Try

            DPSAuthCode = GetInnerText(.SelectSingleNode("DPSData/DPSAuthCode"))
            DPSTxnRef = GetInnerText(.SelectSingleNode("DPSData/DPSTxnRef"))

            ' Added for Request Status bookings
            If .SelectSingleNode("RentalDetails/IsRequestBooking") IsNot Nothing Then
                bIsRequestBooking = CBool(GetInnerText(.SelectSingleNode("RentalDetails/IsRequestBooking")))
            Else
                bIsRequestBooking = False
            End If

        End With
        LogInformation("Done:Reading Input XML")

        Dim htErrorList As New Hashtable, _
            lstNotes As New List(Of String)


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                ' try Sending confirmation
                'Dim oAuroraWebService As AuroraWebService = New AuroraWebService
                'oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                '                                                                 ConfigurationSettings.AppSettings("DomainPWD"), _
                '                                                                 ConfigurationSettings.AppSettings("DomainName"))


                ' 1. Make a Booking
                Dim sReturnedBookingNumber, sReturnedError, sReturnedWarning, sRentalId As String
                Dim bIsSuccessful As Boolean = False


                If bIsRequestBooking Then
                    ' dont take payment for request!!
                    LogInformation("Starting:WebServiceData.CreateBookingWithPayment")
                    bIsSuccessful = WebServiceData.CreateBookingWithPayment(AddBookingNumber, AvpId, AgentReference, _
                                                                            QuantityRequested, Status, _
                                                                            Title, FirstName, LastName, Email, PhoneNumber, _
                                                                            sCountryOfResidence, sPreferredModeOfCommunication, _
                                                                            Note, "", 0, 0, _
                                                                            "", "", "", "", _
                                                                            "", "", APP_NAME, sLoyaltyCardNumber, _
                                                                            sReturnedBookingNumber, CheckOutLocationCode, CheckInLocationCode, _
                                                                            sReturnedWarning, sReturnedError, UserCode, SelectedSlot, SlotId)
                    LogInformation("Done:WebServiceData.CreateBookingWithPayment. bIsSuccessful=""" & bIsSuccessful.ToString() & """")

                Else
                    LogInformation("Starting:WebServiceData.CreateBookingWithPayment")
                    bIsSuccessful = WebServiceData.CreateBookingWithPayment(AddBookingNumber, AvpId, AgentReference, _
                                                                            QuantityRequested, Status, _
                                                                            Title, FirstName, LastName, Email, PhoneNumber, _
                                                                            sCountryOfResidence, sPreferredModeOfCommunication, _
                                                                            Note, CardType, PaymentAmount, PaymentSurchargeAmount, _
                                                                            EncryptedCreditCardNumber, CreditCardName, ApprovalRef, ExpiryDate, _
                                                                            DPSAuthCode, DPSTxnRef, APP_NAME, sLoyaltyCardNumber, _
                                                                            sReturnedBookingNumber, CheckOutLocationCode, CheckInLocationCode, _
                                                                            sReturnedWarning, sReturnedError, UserCode, SelectedSlot, SlotId)
                    LogInformation("Done:WebServiceData.CreateBookingWithPayment. bIsSuccessful=""" & bIsSuccessful.ToString() & """")


                End If




                If bIsSuccessful Then
                    ' success
                    sRentalId = sReturnedBookingNumber.Replace("/"c, "-"c)

                    ' 7.7.10 - Shoel - Change for Quote bookings
                    'If bIsRequestBooking AndAlso CreditCardNumber.Trim() <> "" Then
                    If bIsRequestBooking AndAlso bIsCreditCardInfoPresent AndAlso CreditCardNumber.Trim() <> "" Then
                        ' 7.7.10 - Shoel - Change for Quote bookings

                        ' Adding Encrypted Note with Credit Card Details
                        Dim sPlainTextData, sCodTyp, sCodAudTyp As String, nPriority As Integer, xmlRetMsg As XmlNode, xmlRetDoc As New XmlDocument
                        sPlainTextData = "Payment Amount = " & PaymentAmount & vbNewLine & _
                                         "Payment Surcharge Amount = " & PaymentSurchargeAmount & vbNewLine & _
                                         "Card Type = " & CardType & vbNewLine & _
                                         "Credit Card Number = " & CreditCardNumber & vbNewLine & _
                                         "Credit Card Name = " & CreditCardName & vbNewLine & _
                                         "Cvv2 = " & cvv2 & vbNewLine & _
                                         "Expiry Date = " & ExpiryDate & vbNewLine & _
                                         "Approval Ref = " & ApprovalRef & vbNewLine


                        LogInformation("Starting:oAuroraWebService.AddSecureNote")
                        xmlRetMsg = oAuroraWebService.EncryptTextForAurora(sPlainTextData)
                        LogInformation("Done:oAuroraWebService.AddSecureNote. xmlRetMsg=" & xmlRetMsg.OuterXml)

                        xmlRetDoc.LoadXml("<Data>" & xmlRetMsg.InnerXml & "</Data>")

                        If xmlRetDoc.SelectSingleNode("Data/Status") IsNot Nothing _
                           AndAlso _
                           xmlRetDoc.SelectSingleNode("Data/Status").InnerText.ToUpper() <> SUCCESS_STRING _
                        Then
                            ' error occured
                            Throw New Exception(xmlRetDoc.SelectSingleNode("Data/Message").InnerText)
                        Else
                            ' Success
                            ' Now add the note!
                            Dim sRetMsg, sEncryptedData As String
                            sEncryptedData = xmlRetDoc.SelectSingleNode("Data/EncryptedData").InnerText

                            nPriority = 3
                            sCodTyp = CARD_CODE_ID 'WebServiceData.GetCodeId(13, "Card")
                            sCodAudTyp = NOTE_AUDIENCE_INTERNAL 'WebServiceData.GetCodeId(26, "Internal")

                            sRetMsg = Aurora.Booking.Services.BookingNotes.ManageNote(String.Empty, _
                                                                                      sReturnedBookingNumber.Split("/"c)(0), _
                                                                                      sReturnedBookingNumber.Split("/"c)(1), _
                                                                                      sCodTyp, _
                                                                                      sEncryptedData, _
                                                                                      sCodAudTyp, _
                                                                                      nPriority, _
                                                                                      True, _
                                                                                      1, _
                                                                                      UserCode, _
                                                                                      PROGRAM_NAME)

                            If Not sRetMsg.ToUpper().Contains("SUCCESS") Then
                                Throw New Exception(sRetMsg)
                            End If
                            ' Adding Encrypted Note with Credit Card Details
                        End If

                    End If

                    ''rev:mia aug 3 2012 - added billing token here for b2c consumptions

                    Try
                        If (Not String.IsNullOrEmpty(billingtoken)) Then
                            LogInformation("WebServiceData.CreateBookingWithExtraHireItemAndPayment: InsertTokenObject : parameters :" & "sRentalId :" & sRentalId & " CardType: " & CardType & " CreditCardName: " & CreditCardName & " ExpiryDate :" & ExpiryDate & " billingtoken : " & billingtoken)
                            Dim result As String = InsertTokenObject(sRentalId, CardType, CreditCardName, CreditCardNumber, ExpiryDate, billingtoken, "B2C", "B2C")


                            If (String.IsNullOrEmpty(result)) Then
                                LogInformation("WebServiceData.CreateBookingWithExtraHireItemAndPayment: InsertTokenObject : Result : Successful")
                            Else
                                LogInformation("WebServiceData.CreateBookingWithExtraHireItemAndPayment: InsertTokenObject : Result : Not Successful")
                            End If
                        End If

                    Catch ex As Exception
                        LogInformation("WebServiceData.CreateBookingWithExtraHireItemAndPayment: InsertTokenObject : Result : Not Successful: " & ex.Message)
                    End Try

                Else
                    ' failed
                    Throw New Exception("Failed to create Booking" & vbNewLine & _
                                        "Warning=" & sReturnedWarning & vbNewLine & _
                                        "Error=" & sReturnedError)
                End If

                '' Check for Request Status Booking
                'If bIsRequestBooking Then
                '    oTransaction.CommitTransaction()
                '    oTransaction.Dispose()

                '    Dim strXMLResult As String = "<Data>" & _
                '                                    "<Booking>" & _
                '                                        "<Message>Success</Message>" & _
                '                                        "<ReturnedBookingNumber>" & sReturnedBookingNumber & "</ReturnedBookingNumber>" & _
                '                                    "</Booking>" & _
                '                                  "</Data>"

                '    xmlResult.LoadXml(strXMLResult)

                '    LogInformation("CreateBookingWithExtraHireItemAndPayment: RS:" & xmlResult.OuterXml)

                '    Return xmlResult
                '    Exit Function
                'End If


                ' 2. Add each of the Extra Hire Items to the Quote Booking


                Dim sProgramName As String = "SaveDetailBPD"
                Dim bSomethingIsDeleted As Boolean = False
                'Dim sCalledFrom As String = "BookedProdDetails"


                Dim sReturnedBPDId As String = ""
                Dim sReturnedRNHId As String = ""
                Dim dtCkoDate As DateTime

                '++++++++++++++++++++++++
                If xmlProductList IsNot Nothing AndAlso xmlProductList.SelectNodes("Product") IsNot Nothing Then
                    For Each oProductNode As XmlNode In xmlProductList.SelectNodes("Product")
                        Dim xmlAcutalPrrDetails As XmlDocument
                        Dim saPrrIdList(2), saPrrQtyList(2), sPrrIdList, sPrrQtyLsit As String
                        Dim dtUsageDate As Date

                        If oProductNode.Attributes.Count > 0 AndAlso oProductNode.Attributes("UsageOn") IsNot Nothing Then
                            Try
                                dtUsageDate = Aurora.Common.Utility.DateDBParse(oProductNode.Attributes("UsageOn").Value)
                            Catch ex As Exception
                                Throw New Exception("Invalid UsageOn date specified for product - " & oProductNode.Attributes("PrdShortName").Value)
                            End Try
                        End If

                        ' look for a note
                        If oProductNode.SelectNodes("Note") IsNot Nothing AndAlso oProductNode.SelectNodes("Note").Count > 0 Then
                            For Each oNoteNode As XmlNode In oProductNode.SelectSingleNode("Note")
                                lstNotes.Add(XmlEncode(oNoteNode.InnerText))
                            Next
                        End If

                        LogInformation("Starting:Processing Ferry like products")
                        If oProductNode.SelectNodes("PersonProduct") IsNot Nothing AndAlso oProductNode.SelectNodes("PersonProduct").Count > 0 Then
                            ' theres atleastt one PRR

                            ' new logic!
                            For Each oPersonRateNode As XmlNode In oProductNode.SelectNodes("PersonProduct")
                                With oPersonRateNode
                                    Select Case Trim(UCase(.Attributes("name").Value))
                                        Case ADULT_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(0) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(0) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Adult pax")
                                            End Try

                                        Case CHILD_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(1) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(1) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Child pax")
                                            End Try

                                        Case INFANT_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(2) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(2) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Infant pax")
                                            End Try
                                    End Select
                                End With
                            Next

                            ' new logic!


                            'xmlAcutalPrrDetails = WebServiceData.GetPersonRates(dtUsageDate, Trim(oProductNode.Attributes("SapId").Value))

                            'For Each oDateRangeNode As XmlNode In xmlAcutalPrrDetails.DocumentElement.FirstChild.SelectNodes("StartOn")
                            '    If (CDate(oDateRangeNode.Attributes("dateBegin").Value) <= dtUsageDate.Date And CDate(oDateRangeNode.Attributes("dateEnd").Value) >= dtUsageDate.Date) _
                            '        Or CDate(oDateRangeNode.Attributes("dateBegin").Value) = dtUsageDate _
                            '        Or CDate(oDateRangeNode.Attributes("dateEnd").Value) = dtUsageDate _
                            '    Then
                            '        For Each oPersonRateNode As XmlNode In oDateRangeNode.ChildNodes
                            '            If GetProperStringValue(oPersonRateNode.Attributes("SapId").Value) = GetProperStringValue(oProductNode.Attributes("SapId").Value) Then
                            '                Select Case Trim(UCase(oPersonRateNode.Attributes("PaxType").Value))
                            '                    ' Only add values when the prr Qty <> 0
                            '                    ' use UsageOn date instead of CkoDate, CkiDate = nothing
                            '                    Case ADULT_STRING
                            '                        Try
                            '                            If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Adult']").Attributes("Qty").Value)) > 0 Then
                            '                                saPrrIdList(0) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                            '                                saPrrQtyList(0) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Adult']").Attributes("Qty").Value)
                            '                            End If
                            '                        Catch ex As System.InvalidCastException
                            '                            Throw New Exception("Invalid Quantity specified for number of Adult pax")
                            '                        End Try

                            '                    Case CHILD_STRING
                            '                        Try
                            '                            If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Child']").Attributes("Qty").Value)) > 0 Then
                            '                                saPrrIdList(1) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                            '                                saPrrQtyList(1) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Child']").Attributes("Qty").Value)
                            '                            End If
                            '                        Catch ex As System.InvalidCastException
                            '                            Throw New Exception("Invalid Quantity specified for number of Child pax")
                            '                        End Try

                            '                    Case INFANT_STRING
                            '                        Try
                            '                            If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Infant']").Attributes("Qty").Value)) > 0 Then
                            '                                saPrrIdList(2) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                            '                                saPrrQtyList(2) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Infant']").Attributes("Qty").Value)
                            '                            End If
                            '                        Catch ex As System.InvalidCastException
                            '                            Throw New Exception("Invalid Quantity specified for number of Infant pax")
                            '                        End Try

                            '                End Select
                            '            End If
                            '        Next
                            '    End If
                            'Next

                            ' check to see if any rates were available
                            If saPrrIdList(0) Is Nothing AndAlso saPrrIdList(1) Is Nothing AndAlso saPrrIdList(2) Is Nothing Then
                                Throw New Exception("No rates defined for person dependent rate product - " & oProductNode.Attributes("PrdShortName").Value)
                            End If

                            sPrrIdList = ""
                            sPrrQtyLsit = ""
                            For j As Int16 = 0 To saPrrIdList.Length - 1
                                If Not saPrrIdList(j) Is Nothing Then
                                    sPrrIdList = sPrrIdList & saPrrIdList(j) & ","
                                    sPrrQtyLsit = sPrrQtyLsit & saPrrQtyList(j) & ","
                                End If
                            Next

                            If sPrrQtyLsit.LastIndexOf(",") = sPrrQtyLsit.Length - 1 Then
                                sPrrQtyLsit = sPrrQtyLsit.Substring(0, sPrrQtyLsit.Length - 1)
                            End If

                            If sPrrIdList.LastIndexOf(",") = sPrrIdList.Length - 1 Then
                                sPrrIdList = sPrrIdList.Substring(0, sPrrIdList.Length - 1)
                            End If

                        Else
                            ' NON PRR stuff
                            dtCkoDate = CheckOutDateTime
                            sPrrIdList = "NoPrr"
                            sPrrQtyLsit = ""
                        End If
                        LogInformation("Done:Processing Ferry like products")

                        If dtUsageDate <> DateTime.MinValue Then
                            dtCkoDate = dtUsageDate
                        End If

                        Try
                            Dim bResult As Boolean = False
                            Dim sErrorMessage As String = ""

                            ' Change for AIRNZ usercode problem
                            LogInformation("Starting:GetUserCodeForExtraHireItemAddition. AvpId=""" & AvpId & """")
                            UserCode = GetUserCodeForExtraHireItemAddition(AvpId)
                            LogInformation("Done:GetUserCodeForExtraHireItemAddition. UserCode=""" & UserCode & """")
                            ' Change for AIRNZ usercode problem

                            LogInformation("Starting:AddBookedProduct. SapId=""" & GetProperStringValue(oProductNode.Attributes("SapId").Value) & """")
                            bResult = AddBookedProduct(sRentalId, _
                                                       GetProperStringValue(oProductNode.Attributes("SapId").Value), _
                                                       dtCkoDate, _
                                                       CheckInDateTime, _
                                                       CheckOutLocationCode, _
                                                       CheckInLocationCode, _
                                                       CInt(GetProperStringValue(oProductNode.Attributes("Qty").Value)), _
                                                       "", _
                                                       sPrrIdList, _
                                                       sPrrQtyLsit, _
                                                       sReturnedRNHId, _
                                                       UserCode, _
                                                       sProgramName, _
                                                       sReturnedBPDId, _
                                                       sErrorMessage)
                            LogInformation("Done:AddBookedProduct. bResult=""" & bResult.ToString() & """. sErrorMessage=""" & sErrorMessage & """")

                            LogInformation("Starting:WebServiceData.DeleteTProcessedBPDs")
                            WebServiceData.DeleteTProcessedBPDs(sRentalId)
                            LogInformation("Done:WebServiceData.DeleteTProcessedBPDs")

                            If sErrorMessage <> "" Then
                                Throw New Exception(sErrorMessage)
                            End If

                            If Not bResult Then
                                Throw New Exception("Unable to add extra hire items")
                            End If
                        Catch ex As Exception
                            ' htErrorList.Add(oProductNode.Attributes("PrdShortName").Value, ex.Message)
                            AddErrorMessageToList(htErrorList, oProductNode.Attributes("PrdShortName").Value, ex.Message)
                        End Try

                    Next
                End If ' If xmlProductList IsNot Nothing AndAlso xmlProductList.SelectNodes("Product") IsNot Nothing Then
                '++++++++++++++++++++++++

                ' call sstrans for sReturnedBPDId
                LogInformation("Committing Transaction")
                oTransaction.CommitTransaction()

                Dim xmlReturnDoc As XmlNode
                Dim sBooId, sRntNum As String
                Dim sbNotesString As New StringBuilder
                Dim sbMessage As New StringBuilder

                sbMessage.Append("<Data>" & _
                                    "<Booking>" & _
                                        "<Message>Success</Message>" & _
                                        "<ReturnedBookingNumber>" & sReturnedBookingNumber & "</ReturnedBookingNumber>" & _
                                        "<CheckOutLocationCode>" & CheckOutLocationCode & "</CheckOutLocationCode>" & _
                                        "<CheckInLocationCode>" & CheckInLocationCode & "</CheckInLocationCode>" & _
                                        "<Errors>")

                sBooId = sReturnedBookingNumber.Split("/"c)(0)
                sRntNum = sRentalId.Split("-"c)(1)

                ' Add the notes
                If lstNotes.Count > 0 Then
                    For Each sNoteText As String In lstNotes
                        Try
                            Dim pNteCodTypId As String = WEB_CODE_ID 'WebServiceData.GetCodeId(13, "Web") ' "471AEF57-626D-434A-ACA8-9E5E9112AC3F"
                            Dim pNteCodAudTypId As String = NOTE_AUDIENCE_INTERNAL 'WebServiceData.GetCodeId(26, "Internal") '"FB4207C6-752C-4A04-BCEA-159E3B8E4BB1"
                            Dim pNtePriority As String = 3
                            Dim pNteIsActive As String = 1

                            Dim sMsg As String

                            LogInformation("Starting: Aurora.Booking.Services.BookingNotes.ManageNote - for product notes")
                            sMsg = Aurora.Booking.Services.BookingNotes.ManageNote(String.Empty, _
                                                                                      sBooId, _
                                                                                      sRntNum, _
                                                                                      pNteCodTypId, _
                                                                                      sNoteText, _
                                                                                      pNteCodAudTypId, _
                                                                                      pNtePriority, _
                                                                                      pNteIsActive, _
                                                                                      1, _
                                                                                      UserCode, _
                                                                                      PROGRAM_NAME)
                            LogInformation("Starting: Aurora.Booking.Services.BookingNotes.ManageNote - for product notes. sRetMsg=""" & sMsg & """")

                            If Not sMsg.ToUpper().Contains("SUCCESS") Then
                                Throw New Exception(sMsg)
                            Else
                                sbMessage.Append("<ProductNoteAddition Error='false' Note='" & sNoteText & "' Message='" & sMsg & "' />")
                            End If
                        Catch ex As Exception
                            sbMessage.Append("<ProductNoteAddition Error='true' Note='" & sNoteText & "' Message='" & ex.Message & "' />")
                        End Try
                    Next
                End If

                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' SS Trans 2 FINANCE
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                LogInformation("Starting:WebServiceData.SSTransToFinance.")
                WebServiceData.SSTransToFinance("", sRentalId, "", "")
                LogInformation("Done:WebServiceData.SSTransToFinance.")

                ' try Sending confirmation
                'Dim oAuroraWebService As AuroraWebService.AuroraWebService = New AuroraWebService.AuroraWebService
                'oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                '                                                                 ConfigurationSettings.AppSettings("DomainPWD"), _
                '                                                                 ConfigurationSettings.AppSettings("DomainName"))

                'xmlReturnDoc = oAuroraWebService.CreateConfirmation(sBooId, sRentalId, sRntNum, "7519EC4A-2601-4855-B703-CDD6DD22B108", "HEADER TEXT", "FOOTER TEXT", UserCode)

                If bGetConfXML = True Then


                    LogInformation("Starting:oAuroraWebService.CreateConfirmation")
                    'xmlReturnDoc = oAuroraWebService.CreateConfirmation(sBooId, sRentalId, sRntNum, WebServiceData.GetCodeId(26, "Customer"), ConfirmationHeaderText, ConfirmationFooterText, UserCode, bGetConfXML)
                    LogInformation("sBooId: " & sBooId & ", sRentalId: " & ", sRntNum: " & ", NOTE_AUDIENCE_CUSTOMER: " & NOTE_AUDIENCE_CUSTOMER & ", ConfirmationHeaderText: " & ", ConfirmationFooterText: " & ConfirmationFooterText & ", UserCode: " & UserCode & ", bGetConfXML: " & bGetConfXML)
                    xmlReturnDoc = oAuroraWebService.CreateConfirmation(sBooId, sRentalId, sRntNum, NOTE_AUDIENCE_CUSTOMER, ConfirmationHeaderText, ConfirmationFooterText, UserCode, bGetConfXML)
                    LogInformation("Done:oAuroraWebService.CreateConfirmation")

                    If xmlReturnDoc.SelectSingleNode("Status").InnerText.Trim().ToUpper().Equals("ERROR") Then
                        ' confirmation sending failed!
                        sbNotesString.Append("Failed to send confirmation for booking : " & sReturnedBookingNumber & vbNewLine)
                    End If
                Else
                    '    xmlReturnDoc = New XmlDocument
                    '   xmlReturnDoc.InnerText = "<root><Status/></root>"

                    LogInformation("No confirmation required")
                End If
                'LogInformation("Returned XML:oAuroraWebService.CreateConfirmation " & xmlReturnDoc.InnerXml)


                If htErrorList.Count > 0 Then
                    sbNotesString.Append("The following products could not be added automatically : ")
                    For Each sKey As String In htErrorList.Keys
                        sbMessage.Append("<Error PrdShortName='" & sKey & "' ErrorMessage='" & htErrorList(sKey) & "' />")
                        sbNotesString.Append(sKey & ", ")
                    Next
                    sbNotesString.Remove(sbNotesString.ToString().Length - 2, 2)
                End If

                If htErrorList.Count > 0 Then
                    Try
                        'Dim pNteCodTypId As String = WebServiceData.GetCodeId(13, "Web") ' "471AEF57-626D-434A-ACA8-9E5E9112AC3F"
                        'Dim pNteCodAudTypId As String = WebServiceData.GetCodeId(26, "Internal") '"FB4207C6-752C-4A04-BCEA-159E3B8E4BB1"

                        Dim pNteCodTypId As String = NOTE_CODE_ID
                        Dim pNteCodAudTypId As String = NOTE_AUDIENCE_INTERNAL

                        Dim pNtePriority As String = 3
                        Dim pNteIsActive As String = 1

                        Dim sRetMsg As String

                        LogInformation("Starting: Aurora.Booking.Services.BookingNotes.ManageNote")
                        sRetMsg = Aurora.Booking.Services.BookingNotes.ManageNote(String.Empty, _
                                                                                  sBooId, _
                                                                                  sRntNum, _
                                                                                  pNteCodTypId, _
                                                                                  sbNotesString.ToString(), _
                                                                                  pNteCodAudTypId, _
                                                                                  pNtePriority, _
                                                                                  pNteIsActive, _
                                                                                  1, _
                                                                                  UserCode, _
                                                                                  PROGRAM_NAME)
                        LogInformation("Starting: Aurora.Booking.Services.BookingNotes.ManageNote. sRetMsg=""" & sRetMsg & """")

                        If Not sRetMsg.ToUpper().Contains("SUCCESS") Then
                            Throw New Exception(sRetMsg)
                        Else
                            sbMessage.Append("<NoteAddition Error='false' Message='" & sRetMsg & "' />")
                        End If
                    Catch ex As Exception
                        sbMessage.Append("<NoteAddition Error='true' Message='" & ex.Message & "' />")
                    End Try
                End If

                sbMessage.Append("</Errors>" & _
                                 "</Booking>")
                sbMessage.Append("<Confirmation>")
                If (bGetConfXML = True) Then
                    sbMessage.Append(xmlReturnDoc.InnerXml)
                End If
                sbMessage.Append("</Confirmation>")

                LogInformation("Done with Aurora Part")

                If Not bIsRequestBooking Then ' no need to notify dvass for request bookings

                    ' Notify DVASS here! Webservice call 
                    Dim iCountryCode, iCkoDayPart, iCkiDayPart, iVisibility, iForceFlag As Integer
                    Dim dPriority As Double = 1.0
                    Dim sProductId, sVehicleIdList As String
                    Dim bUpdateFleet As Boolean = True

                    Dim xmlDvassDetails As XmlDocument

                    LogInformation("Starting:WebServiceData.GetDvassDetails")
                    xmlDvassDetails = WebServiceData.GetDvassDetails(sReturnedBookingNumber, sRentalId)
                    LogInformation("Done:WebServiceData.GetDvassDetails. xmlDvassDetails=" & xmlDvassDetails.OuterXml)

                    With xmlDvassDetails.DocumentElement.SelectSingleNode("Details")
                        iCountryCode = CInt(.Attributes("cty").Value)
                        sProductId = .Attributes("Prd").Value
                        iCkoDayPart = CInt(.Attributes("DayPrtFrm").Value)
                        iCkiDayPart = CInt(.Attributes("DayPrtTo").Value)
                        dPriority = CDbl(.Attributes("Priority").Value)
                        sVehicleIdList = .Attributes("Vehicles").Value
                        iForceFlag = CInt(.Attributes("Force").Value)
                        iVisibility = CInt(.Attributes("Visibility").Value)
                        bUpdateFleet = CBool(.Attributes("UpdateFleet").Value)
                    End With

                    If bUpdateFleet Then

                        LogInformation("Starting:oAuroraWebService.DVASSRentalAdd")
                        xmlReturnDoc = oAuroraWebService.DVASSRentalAdd(iCountryCode, sRentalId, sProductId, _
                                                     CheckOutLocationCode, CheckOutDateTime, iCkoDayPart, _
                                                     CheckInLocationCode, CheckInDateTime, iCkiDayPart, _
                                                     PaymentAmount, dPriority, iVisibility, _
                                                     sVehicleIdList, iForceFlag, UserCode)
                        LogInformation("Done:oAuroraWebService.DVASSRentalAdd")

                        sbMessage.Append("<DVASS>" & xmlReturnDoc.InnerXml & "</DVASS>")
                    End If

                End If

                sbMessage.Append("</Data>")

                xmlResult.LoadXml(sbMessage.ToString())

            Catch ex As Exception
                LogInformation("Exception Occured : " & ex.Message & vbNewLine & ex.StackTrace)
                oTransaction.Dispose()
                xmlResult = GetErrorXML(ex)
            End Try
        End Using

        LogInformation("CreateBookingWithExtraHireItemAndPayment: RS:" & xmlResult.OuterXml)

        Return xmlResult

    End Function

    ''rev:mia 12May2015 - addition of Rental for TCX
    Function AddBookedProduct(ByVal RentalId As String, ByVal SapId As String, ByVal CheckOutDate As DateTime, ByVal CheckInDate As DateTime, _
                              ByVal CheckOutLocationCode As String, ByVal CheckInLocationCode As String, ByVal Quantity As Integer, ByVal PtdIdList As String, _
                              ByVal PrrIdList As String, ByVal PrrQuantityList As String, ByRef RentalHistoryId As String, _
                              ByVal UserCode As String, ByVal ProgramName As String, ByRef ReturnedBPDId As String, _
                              ByRef ErrorMessage As String, Optional Rate As String = "") As Boolean
        Dim sError As String
        sError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Add( _
                                            RentalId, SapId, _
                                            False, CheckOutDate.ToString(DATE_FORMAT & " " & TIME_FORMAT), _
                                            CheckInDate.ToString(DATE_FORMAT & " " & TIME_FORMAT), CheckOutLocationCode, _
                                            CheckInLocationCode, IIf(String.IsNullOrEmpty(Rate), Nothing, Rate), _
                                            Quantity, PrrIdList, _
                                            PrrQuantityList, "", _
                                            PtdIdList, "", _
                                             "", "", _
                                            "", Nothing, _
                                            Nothing, Nothing, _
                                            Nothing, RentalHistoryId, _
                                            UserCode, ProgramName, ReturnedBPDId)

        If Not Trim(sError).Equals(String.Empty) Then
            ErrorMessage = sError
            Return False
        Else
            Return True
        End If
    End Function

    <WebMethod(Description:="This method gets the available extra hire items and insurance products for a given AvpId")> _
    Public Function GetInsuranceAndExtraHireItems(ByVal AvpId As String, ByVal IsGross As Boolean, ByVal IsTestMode As Boolean) As XmlDocument

        LogInformation("GetInsuranceAndExtraHireItems: RQ: AvpId=""" & AvpId & """")

        Dim xmlExtraHireItems As New XmlDocument
        Dim xmlInclusivePackage As New XmlDocument

        Try
            LogInformation("Starting:WebServiceData.GetExtraHireItems")
            xmlExtraHireItems = WebServiceData.GetExtraHireItems(AvpId, IsTestMode)
            LogInformation("Done:WebServiceData.GetExtraHireItems")

            LogInformation("Starting:CalculateInclusiveRate")
            xmlInclusivePackage = CalculateInclusiveRate(AvpId, IsTestMode, IsGross)
            LogInformation("Done:CalculateInclusiveRate")

            LogInformation("Starting:Sticking XMLs together")
            If xmlInclusivePackage.DocumentElement.SelectSingleNode("AvailableRate") IsNot Nothing Then
                xmlExtraHireItems.DocumentElement.InnerXml = xmlExtraHireItems.DocumentElement.InnerXml & _
                                                             "<InclusiveRate>" & _
                                                             xmlInclusivePackage.DocumentElement.SelectSingleNode("AvailableRate").OuterXml & _
                                                             "</InclusiveRate>"
            Else
                xmlExtraHireItems.DocumentElement.InnerXml = xmlExtraHireItems.DocumentElement.InnerXml & _
                                                             "<InclusiveRate>" & _
                                                             xmlInclusivePackage.DocumentElement.OuterXml & _
                                                             "</InclusiveRate>"
            End If
            LogInformation("Done:Sticking XMLs together")

        Catch ex As Exception
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            xmlExtraHireItems = GetErrorXML(ex)
        End Try

        LogInformation("GetInsuranceAndExtraHireItems: RS:" & xmlExtraHireItems.OuterXml)

        Return xmlExtraHireItems

    End Function


    ''' <summary>
    ''' This webmethod is used to create "On-Request" bookings
    ''' </summary>
    ''' <param name="InputData"></param>
    ''' <returns>xml containing Booking Number if successful</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method creates a booking in Aurora, bypassing availablity checks etc. To be used to save quotes.")> _
    Public Function ForceCreateBooking(ByVal InputData As XmlDocument) As XmlDocument

        LogInformation("Start:ForceCreateBooking. InputData='" & InputData.OuterXml & "'")

        ' change for information security purposes
        Dim oAuroraWebService As AuroraWebService = New AuroraWebService
        oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                         ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                         ConfigurationSettings.AppSettings("DomainName"))

        ' 7.7.10 - Shoel - Change for Quote bookings
        Dim bIsCreditCardInfoPresent As Boolean = Not String.IsNullOrEmpty(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))
        ' 7.7.10 - Shoel - Change for Quote bookings

        Dim CreditCardNumber, EncryptedCreditCardNumber As String

        ' 7.7.10 - Shoel - Change for Quote bookings
        If bIsCreditCardInfoPresent Then
            ' 7.7.10 - Shoel - Change for Quote bookings

            Try
                Dim xmlEncryptedData As XmlNode

                LogInformation("oAuroraWebService.EncryptTextForAurora : Call start")
                xmlEncryptedData = oAuroraWebService.EncryptTextForAurora(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))
                LogInformation("oAuroraWebService.EncryptTextForAurora : Call end : Returned message : " & xmlEncryptedData.OuterXml)

                CreditCardNumber = GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
                EncryptedCreditCardNumber = xmlEncryptedData.SelectSingleNode("EncryptedData").InnerText
                InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = EncryptedCreditCardNumber
            Catch ex As Exception
                ' do nothing
                LogInformation("Call to Aurora text encryption function failed : " & ex.Message & vbNewLine & ex.StackTrace)
            End Try
            ' change for information security purposes

            ' 7.7.10 - Shoel - Change for Quote bookings
        End If
        ' 7.7.10 - Shoel - Change for Quote bookings
        LogInformation("ForceCreateBooking: RQ: InputData=""" & InputData.OuterXml & """")

        Dim xmlReturn As New XmlDocument

        ' Variables for GetVehiclePrice call
        Dim sProductShortName, sAgentCode, sCheckOutLocationCode, sCheckInLocationCode, sCountryOfResidence, sPackageCode, sBrandCode, sCountryCode As String
        Dim bIsInclusivePackage, bIsTestMode, bIsVan As Boolean
        Dim iNoOfAdults, iNoOfChildren As Int16
        Dim dtCheckOutDate, dtCheckInDate As DateTime
        Dim IsGross As Boolean
        ' Variables for GetVehiclePrice call
        'InputData.Load(Server.MapPath("") & "\XMLFile3.xml")

        Try
            ' 1. Pickup the input params
            With InputData.DocumentElement

                ' Variables for GetVehiclePrice call
                sProductShortName = GetInnerText(.SelectSingleNode("RentalDetails/ProductShortName"))
                sAgentCode = GetInnerText(.SelectSingleNode("RentalDetails/AgentCode"))
                sCheckOutLocationCode = GetInnerText(.SelectSingleNode("RentalDetails/CheckOutLocationCode"))
                sCheckInLocationCode = GetInnerText(.SelectSingleNode("RentalDetails/CheckInLocationCode"))
                dtCheckOutDate = Aurora.Common.Utility.DateDBParse(GetInnerText(.SelectSingleNode("RentalDetails/CheckOutDateTime")))
                dtCheckInDate = Aurora.Common.Utility.DateDBParse(GetInnerText(.SelectSingleNode("RentalDetails/CheckInDateTime")))
                sPackageCode = GetInnerText(.SelectSingleNode("RentalDetails/PackageCode"))
                iNoOfAdults = CInt(GetInnerText(.SelectSingleNode("RentalDetails/NoOfAdults")))
                iNoOfChildren = CInt(GetInnerText(.SelectSingleNode("RentalDetails/NoOfChildren")))
                sBrandCode = GetInnerText(.SelectSingleNode("RentalDetails/BrandCode"))
                bIsInclusivePackage = CBool(GetInnerText(.SelectSingleNode("RentalDetails/IsInclusivePackage")))
                bIsTestMode = CBool(GetInnerText(.SelectSingleNode("RentalDetails/IsTestMode")))
                IsGross = False
                ' For Zone lookup
                sCountryCode = GetInnerText(.SelectSingleNode("RentalDetails/CountryCode")).ToUpper()
                bIsVan = CBool(GetInnerText(.SelectSingleNode("RentalDetails/IsVan")))
                ' For Zone lookup

                sCountryOfResidence = GetInnerText(.SelectSingleNode("CustomerData/CountryOfResidence"))
                ' Variables for GetVehiclePrice call

                ' Variables for CreateBookingWithPayment Call
                ' will be passed directly
                ' Variables for CreateBookingWithPayment Call

            End With

            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ' Translate the zone stuff to useful loc codes here!
            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            Dim dtCheckOutList, dtCheckInList As DataTable

            LogInformation("Starting:WebServiceData.GetLocation(CheckOut).")
            dtCheckOutList = WebServiceData.GetLocation(sCheckOutLocationCode, sBrandCode, IIf(bIsVan, "AV", "AC"), sCountryCode)
            LogInformation("Done:WebServiceData.GetLocation(CheckOut). ")

            LogInformation("Starting:WebServiceData.GetLocation(CheckIn).")
            dtCheckInList = WebServiceData.GetLocation(sCheckInLocationCode, sBrandCode, IIf(bIsVan, "AV", "AC"), sCountryCode)
            LogInformation("Done:WebServiceData.GetLocation(CheckIn). ")


            If sCountryCode = "AU" And Not bIsVan Then
                If sProductShortName = "PFMR" Then
                    ' look for branch loc
                    For Each rwCheckOutLocationRow As DataRow In dtCheckOutList.Rows
                        If UCase(rwCheckOutLocationRow("BranchType").ToString()) = BRANCH_STRING Then
                            sCheckOutLocationCode = rwCheckOutLocationRow("LocCode").ToString()
                            Exit For
                        End If
                    Next
                    For Each rwCheckInLocationRow As DataRow In dtCheckOutList.Rows
                        If UCase(rwCheckInLocationRow("BranchType").ToString()) = BRANCH_STRING Then
                            sCheckInLocationCode = rwCheckInLocationRow("LocCode").ToString()
                            Exit For
                        End If
                    Next
                Else
                    ' look for thrifty loc
                    For Each rwCheckOutLocationRow As DataRow In dtCheckOutList.Rows
                        If UCase(rwCheckOutLocationRow("BranchType").ToString()) = THRIFTY_STRING Then
                            sCheckOutLocationCode = rwCheckOutLocationRow("LocCode").ToString()
                            Exit For
                        End If
                    Next
                    For Each rwCheckInLocationRow As DataRow In dtCheckOutList.Rows
                        If UCase(rwCheckInLocationRow("BranchType").ToString()) = THRIFTY_STRING Then
                            sCheckInLocationCode = rwCheckInLocationRow("LocCode").ToString()
                            Exit For
                        End If
                    Next
                End If
            Else
                ' everything else
                sCheckOutLocationCode = dtCheckOutList.Rows(0)("LocCode").ToString()
                sCheckInLocationCode = dtCheckInList.Rows(0)("LocCode").ToString()
            End If



            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            ' Translate the zone stuff to useful loc codes here!
            ' @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


            Dim sResult As String = ""
            Dim sErrorMessage As String = ""

            LogInformation("Starting:WebServiceData.GetVehiclePrice")
            sResult = WebServiceData.GetVehiclePrice("", sProductShortName, sAgentCode, _
                                                     sCheckOutLocationCode, sCheckInLocationCode, _
                                                     dtCheckOutDate, dtCheckInDate, _
                                                     sCountryOfResidence, sPackageCode, _
                                                     iNoOfAdults, iNoOfChildren, 0, _
                                                     sBrandCode, bIsInclusivePackage, _
                                                     True, bIsTestMode, IsGross, sErrorMessage)
            LogInformation("Done:WebServiceData.GetVehiclePrice. sErrorMessage=""" & sErrorMessage & """")

            If sErrorMessage <> "" Then
                Throw New Exception(sErrorMessage)
            End If

            ' load xml
            Dim xmlVehiclePrice As New XmlDocument
            xmlVehiclePrice.LoadXml(sResult)

            ' use this AvpId in the next call
            InputData.DocumentElement.SelectSingleNode("RentalDetails/AvpId").InnerText = xmlVehiclePrice.DocumentElement.SelectSingleNode("AvailableRate/Package").Attributes("AvpId").Value

            LogInformation("Starting:CreateBookingWithExtraHireItemAndPayment")

            ' 7.7.10 - Shoel - Change for Quote bookings
            If bIsCreditCardInfoPresent Then
                ' 7.7.10 - Shoel - Change for Quote bookings

                ' replace credit card data
                Try
                    InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = CreditCardNumber
                Catch ex As Exception

                End Try

                ' 7.7.10 - Shoel - Change for Quote bookings
            End If
            ' 7.7.10 - Shoel - Change for Quote bookings

            xmlReturn = CreateBookingWithExtraHireItemAndPayment(InputData)
            LogInformation("Done:CreateBookingWithExtraHireItemAndPayment")

        Catch ex As Exception
            xmlReturn = GetErrorXML(ex)
        End Try

        LogInformation("ForceCreateBooking: RS:" & xmlReturn.OuterXml)

        Return xmlReturn

    End Function

#End Region

#Region "Save Quote Functionality Stuff"

    Const CONFIRMED_RENTAL_STATUS As String = "KK"


    ''' <summary>
    ''' This method updates the booking status to KK, Adds/Removes products, Updates Customer Info, Adds Payments and finally Creates a confirmation.
    ''' </summary>
    ''' <param name="InputData">See the xml format in the method comment</param>
    ''' <returns>Xml document</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method updates the booking status to KK, Adds/Removes products, Updates Customer Info, Adds Payments and finally Creates a confirmation.")> _
    Public Function ModifyBooking(ByVal InputData As XmlDocument) As XmlDocument

        'LogInformation("Start:ModifyBooking. InputData='" & InputData.OuterXml & "'") -- Do not log this as it can contain CC info!!!!

        Dim oAuroraWebService As AuroraWebService = New AuroraWebService
        oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationManager.AppSettings("DomainUID"), _
                                                                         ConfigurationManager.AppSettings("DomainPWD"), _
                                                                         ConfigurationManager.AppSettings("DomainName"))

        Dim bIsCreditCardInfoPresent As Boolean = Not String.IsNullOrEmpty(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))

        Dim CreditCardNumber, EncryptedCreditCardNumber As String


        If bIsCreditCardInfoPresent Then
            Try
                Dim xmlEncryptedData As XmlNode

                LogInformation("oAuroraWebService.EncryptTextForAurora : Call start")
                xmlEncryptedData = oAuroraWebService.EncryptTextForAurora(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))
                LogInformation("oAuroraWebService.EncryptTextForAurora : Call end : Returned message : " & xmlEncryptedData.OuterXml)

                CreditCardNumber = GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
                EncryptedCreditCardNumber = xmlEncryptedData.SelectSingleNode("EncryptedData").InnerText
                InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = EncryptedCreditCardNumber
            Catch ex As Exception
                ' do nothing
                LogInformation("Call to Aurora text encryption function failed : " & ex.Message & vbNewLine & ex.StackTrace)
            End Try
        End If

        LogInformation("ModifyBooking: RQ: InputData=""" & InputData.OuterXml & """")

        Dim xmlReturn As New XmlDocument, _
            RentalId As String, _
            UserCode As String, _
            CheckOutDateTime As DateTime, _
            CheckInDateTime As DateTime, _
            CheckOutLocationCode As String, _
            CheckInLocationCode As String, _
            RentalNum As String, _
            BookingId As String, _
            NoteText As String, _
            AgentReference As String, _
            bFleetUpdate As Boolean = False, _
            sbReturnXMLText As New StringBuilder, _
            ConfirmationHeaderText As String, _
            ConfirmationFooterText As String, _
            GetConfXML As Boolean = False, _
            PaymentAmount As Decimal = 0, _
            SelectedSlot As String, _
            SlotId As String, _
            UserId As String = ""

        Try
            ' 1. Modify booking status

            ' Get the rental info
            LogInformation("Starting:Reading Input XML")
            With InputData.DocumentElement
                RentalId = GetInnerText(.SelectSingleNode("RentalDetails/RentalId"))
                'NoteText = GetInnerText(.SelectSingleNode("RentalDetails/Note"))  -- this kills the modify method!
                AgentReference = GetInnerText(.SelectSingleNode("RentalDetails/AgentReference"))
                ConfirmationHeaderText = GetInnerText(.SelectSingleNode("RentalDetails/ConfirmationHeaderText"))
                ConfirmationFooterText = GetInnerText(.SelectSingleNode("RentalDetails/ConfirmationFooterText"))
                ''''''''''''''''''''UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
                'Added by Nimesh on 27th Jan 2015 for slot selection
                SelectedSlot = GetInnerText(.SelectSingleNode("RentalDetails/SelectedSlot"))
                SlotId = GetInnerText(.SelectSingleNode("RentalDetails/SlotId"))
                'End added by Nimesh
                Try
                    GetConfXML = CBool(GetInnerText(.SelectSingleNode("RentalDetails/GetConfXML")))
                Catch ex As Exception
                End Try

                Try
                    PaymentAmount = CDec(GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentAmount")))
                Catch ex As Exception
                End Try
            End With
            LogInformation("Done:Reading Input XML")
        Catch ex As Exception
            Return GetErrorXML(ex)
        End Try

        Dim xmlIntermediateResult As New XmlDocument

        xmlIntermediateResult = New XmlDocument ' reset the result set
        xmlIntermediateResult = AddUpdateCustomerData(InputData)
        If GetInnerText(xmlIntermediateResult.SelectSingleNode("Data/Status")) <> "SUCCESS" Then
            ' there is an error
            sbReturnXMLText.Append("<AddUpdateCustomerData>Failed</AddUpdateCustomerData>")
        Else
            ' no error
            sbReturnXMLText.Append("<AddUpdateCustomerData>Success</AddUpdateCustomerData>")
        End If

        Dim sNote As String = "", sRetMsg As String = ""
        If InputData.DocumentElement.SelectSingleNode("RentalDetails/Note") IsNot Nothing Then
            sNote = GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/Note"))
        Else
            sNote = ""
        End If
        LogInformation("Starting:WebServiceData.GetDetailsFromRentalId")
        WebServiceData.GetDetailsFromRentalId(RentalId:=RentalId, _
                                              CheckOutDateTime:=CheckOutDateTime, _
                                              CheckInDateTime:=CheckInDateTime, _
                                              CheckOutLocationCode:=CheckOutLocationCode, _
                                              CheckInLocationCode:=CheckInLocationCode, _
                                              RentalNo:=RentalNum, _
                                              BookingId:=BookingId)
        LogInformation("Done:WebServiceData.GetDetailsFromRentalId.")

        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ' Fetch Rental Info
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



        ' Fetch the correct user code for this rental
        LogInformation("Starting:WebServiceData.GetUserCodeForRentalModify")
        UserCode = WebServiceData.GetUserCodeForRentalModify(RentalId, UserId)
        LogInformation("Done:WebServiceData.GetUserCodeForRentalModify. UserCode = '" & UserCode & "', UserId = '" & UserId & "'")

        ' put this user code into the input xml
        Try
            LogInformation("Starting:updating the input xml with proper user code")
            InputData.DocumentElement.SelectSingleNode("RentalDetails/UserCode").InnerText = UserCode
            LogInformation("Done:updating the input xml with proper user code")
        Catch ex As Exception
            LogInformation("Failed:updating the input xml with proper user code. " & ex.Message & vbNewLine & ex.StackTrace)
        End Try

        If sNote.Trim().Length > 0 Then
            sRetMsg = Aurora.Booking.Services.BookingNotes.ManageNote(String.Empty, _
                                                                          BookingId, _
                                                                          RentalNum, _
                                                                          RENTAL_CODE_ID, _
                                                                          sNote, _
                                                                          NOTE_AUDIENCE_INTERNAL, _
                                                                          3, _
                                                                          True, _
                                                                          1, _
                                                                          UserCode, _
                                                                          PROGRAM_NAME)
            If Not sRetMsg.ToUpper().Contains("SUCCESS") Then
                LogInformation("ModifyBooking - Add Note failed")
            End If

        End If


        Try
            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                ' read the xml input data for RentalId, NoteText, AgentReference

                LogInformation("Starting:WebServiceData.ModifyBooking with params : " & RentalId & ", " & CheckOutLocationCode & ", " & CheckInLocationCode & ", " & CheckOutDateTime & ", " & CheckInDateTime & ", " & CONFIRMED_RENTAL_STATUS & ", " & NoteText & ", " & AgentReference & ", " & UserId & ", " & PROGRAM_NAME)
                xmlIntermediateResult = WebServiceData.ModifyBooking(BookingId & "/" & RentalNum, CheckOutLocationCode, CheckInLocationCode, CheckOutDateTime, CheckInDateTime, CONFIRMED_RENTAL_STATUS, NoteText, AgentReference, UserId, PROGRAM_NAME, SelectedSlot, SlotId)
                LogInformation("Done:WebServiceData.ModifyBooking : xmlIntermediateResult = '" & xmlIntermediateResult.OuterXml & "'")

                If xmlIntermediateResult.DocumentElement.SelectSingleNode("Error") IsNot Nothing Then
                    ' there is an error
                    sbReturnXMLText.Append("<ModifyBooking>Failed</ModifyBooking>")
                    With xmlIntermediateResult.DocumentElement.SelectSingleNode("Error")
                        If .SelectSingleNode("MessageSection/Message") IsNot Nothing Then
                            Throw New Exception(GetInnerText(.SelectSingleNode("MessageSection/Message")) & " - " & GetInnerText(.SelectSingleNode("MessageSection/Warning")))
                        Else
                            Throw New Exception(GetInnerText(.SelectSingleNode("MessageSection")))
                        End If
                    End With
                Else
                    ' no error
                    bFleetUpdate = CBool(GetInnerText(xmlIntermediateResult.DocumentElement.SelectSingleNode("FleetUpdate")))
                    sbReturnXMLText.Append("<ModifyBooking>Success</ModifyBooking>")
                End If
                oTransaction.CommitTransaction()
            End Using

            ' 2. Call AddUpdateCustomerData
            If bIsCreditCardInfoPresent Then
                ' replace credit card data
                Try
                    InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = CreditCardNumber
                Catch ex As Exception
                End Try
            End If


            ' 3. Call AddPayment
            xmlIntermediateResult = New XmlDocument ' reset the result set
            xmlIntermediateResult = AddPayment(InputData)
            If GetInnerText(xmlIntermediateResult.DocumentElement.SelectSingleNode("PaymentMessage")) <> "Payment successful" Then
                ' there is an error
                sbReturnXMLText.Append("<AddPayment>Failed</AddPayment>")
            Else
                ' no error
                sbReturnXMLText.Append("<AddPayment>Success</AddPayment>")
            End If

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' SS Trans 2 FINANCE
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            LogInformation("Starting:WebServiceData.SSTransToFinance.")
            WebServiceData.SSTransToFinance("", RentalId, "", "")
            LogInformation("Done:WebServiceData.SSTransToFinance.")

            ' 4. Send the confirmation email
            Dim xmlConf As XmlNode
            LogInformation("Starting:oAuroraWebService.CreateConfirmation")
            xmlConf = oAuroraWebService.CreateConfirmation(BookingId, RentalId, RentalNum, NOTE_AUDIENCE_CUSTOMER, ConfirmationHeaderText, ConfirmationFooterText, UserCode, GetConfXML)
            LogInformation("Done:oAuroraWebService.CreateConfirmation")

            If xmlConf.SelectSingleNode("Status").InnerText.Trim().ToUpper().Equals("ERROR") Then
                ' confirmation sending failed!
                sbReturnXMLText.Append("<CreateConfirmation>Failed</CreateConfirmation>")
            Else
                ' confirmation sent!
                sbReturnXMLText.Append("<CreateConfirmation>Success</CreateConfirmation>")
            End If

            ' Notify DVASS here! Webservice call 
            Dim iCountryCode, iCkoDayPart, iCkiDayPart, iVisibility, iForceFlag As Integer
            Dim dPriority As Double = 1.0
            Dim sProductId, sVehicleIdList As String
            Dim bUpdateFleet As Boolean = True

            xmlIntermediateResult = New XmlDocument ' reset the result set

            LogInformation("Starting:WebServiceData.GetDvassDetails")
            xmlIntermediateResult = WebServiceData.GetDvassDetails(BookingId & "/" & RentalNum, RentalId)
            LogInformation("Done:WebServiceData.GetDvassDetails. xmlDvassDetails=" & xmlIntermediateResult.OuterXml)

            With xmlIntermediateResult.DocumentElement.SelectSingleNode("Details")
                iCountryCode = CInt(.Attributes("cty").Value)
                sProductId = .Attributes("Prd").Value
                iCkoDayPart = CInt(.Attributes("DayPrtFrm").Value)
                iCkiDayPart = CInt(.Attributes("DayPrtTo").Value)
                dPriority = CDbl(.Attributes("Priority").Value)
                sVehicleIdList = .Attributes("Vehicles").Value
                iForceFlag = CInt(.Attributes("Force").Value)
                iVisibility = CInt(.Attributes("Visibility").Value)
                bUpdateFleet = CBool(.Attributes("UpdateFleet").Value)
            End With

            If bUpdateFleet Then

                LogInformation("Starting:oAuroraWebService.DVASSRentalAdd")
                xmlConf = oAuroraWebService.DVASSRentalAdd(iCountryCode, RentalId, sProductId, _
                                             CheckOutLocationCode, CheckOutDateTime, iCkoDayPart, _
                                             CheckInLocationCode, CheckInDateTime, iCkiDayPart, _
                                             PaymentAmount, dPriority, iVisibility, _
                                             sVehicleIdList, iForceFlag, UserCode)
                LogInformation("Done:oAuroraWebService.DVASSRentalAdd")

                sbReturnXMLText.Append("<DVASS>Success</DVASS>")
            End If

        Catch ex As Exception
            LogInformation(ex.Message & " - " & ex.StackTrace)
            xmlReturn = GetErrorXML(ex)
        End Try



        xmlReturn.LoadXml("<data>" & sbReturnXMLText.ToString() & xmlReturn.OuterXml & "</data>")

        LogInformation("ModifyBooking: RS: xmlReturn=""" & xmlReturn.OuterXml & """")

        Return xmlReturn
    End Function

    ''' <summary>
    ''' This method retrieves a quote booking, after validating quote validity and availability.
    ''' </summary>
    ''' <param name="RentalNumber">Rental Number in 1234567/1 format</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method retrieves a quote booking, after validating quote validity and availability.")> _
    Public Function RetrieveBooking(ByVal RentalNumber As String) As XmlDocument
        LogInformation("RetrieveBooking: RQ: RentalNumber=""" & RentalNumber & """")

        Dim xmlResult As New XmlDocument
        Dim sErrorMessage As String = ""


        Try
            ' 1. RetrieveBooking
            LogInformation("Starting:WebServiceData.RetrieveBooking. RentalNumber = '" & RentalNumber & "'")
            xmlResult = WebServiceData.RetrieveBooking(RentalNumber, "", sErrorMessage)
            LogInformation("Done:WebServiceData.RetrieveBooking. sErrorMessage = '" & sErrorMessage & "', xmlResult = '" & xmlResult.OuterXml & "'")

            'If Not String.IsNullOrEmpty(sErrorMessage.Trim()) Then
            '    ' some error
            '    Throw New Exception(sErrorMessage)
            'End If

            If xmlResult.DocumentElement.SelectSingleNode("CustomerInfo") Is Nothing Then
                Return xmlResult
            End If

            ' 2. Check availability based on quote params

            ' read the booking info xml
            Dim sBrandCode As String = "", _
                sVehicleCode As String = "", _
                sProductName As String = "", _
                sCountryCode As String = "", _
                sCheckOutLocationCode As String = "", _
                datCheckOutDateTime As DateTime, _
                sCheckInLocationCode As String = "", _
                datCheckInDateTime As DateTime, _
                sAgentCode As String = "", _
                sPackageId As String = ""

            LogInformation("Starting:Reading the booking info xml")
            With xmlResult.DocumentElement.SelectSingleNode("RentalInfo/Rental")
                sBrandCode = .Attributes("BrandCode").Value.Trim()
                sVehicleCode = .Attributes("ProductCode").Value.Trim()
                sProductName = .Attributes("VehicleName").Value.Trim()
                sCountryCode = .Attributes("CountryCode").Value.Trim()
                sCheckOutLocationCode = .Attributes("PickupLocCode").Value.Trim()
                datCheckOutDateTime = Aurora.Common.DateDBParse(.Attributes("PickupDateTime").Value.Trim())
                sCheckInLocationCode = .Attributes("DropOffLocCode").Value.Trim()
                datCheckInDateTime = Aurora.Common.DateDBParse(.Attributes("DropOffDateTime").Value.Trim())
                sAgentCode = .Attributes("AgentCode").Value.Trim()
                sPackageId = .Attributes("PackageId").Value.Trim()

            End With
            LogInformation("Done:Reading the booking info xml")

            LogInformation("Starting:GetVehicleAvailability")
            If GetVehicleAvailability(sBrandCode, sVehicleCode, sProductName, sCountryCode, _
                                      sCheckOutLocationCode, datCheckOutDateTime, _
                                      sCheckInLocationCode, datCheckInDateTime, _
                                      sAgentCode, sPackageId, Nothing, Nothing) = AVAILABLE_YES_FLAG Then
                ' the quote passed the availability checks
                LogInformation("Done:GetVehicleAvailability - Passed")
                ' do nothing 
            Else
                ' the quote has failed the availability checks
                LogInformation("Done:GetVehicleAvailability - Failed")
                Throw New Exception("This quote cannot be processed due to vehicle unavailability")
            End If

        Catch ex As Exception
            LogInformation("Errors in RetrieveBooking - " & ex.Message & vbNewLine & ex.StackTrace)
            'xmlResult.LoadXml("<Error>" & ex.Message & "</Error>")


            Dim xmlNodeTemp As XmlNode
            xmlNodeTemp = xmlResult.CreateNode(XmlNodeType.Element, "Error", "")
            If ex.Message = "This quote cannot be processed due to vehicle unavailability" Then
                xmlNodeTemp.InnerXml = "<Details>" & ex.Message & "</Details>"
            Else
                xmlNodeTemp.InnerXml = "<Details>" & XmlEncode(ex.Message & vbNewLine & ex.StackTrace) & "</Details>"
            End If

            xmlResult.DocumentElement.AppendChild(xmlNodeTemp)


        End Try

        LogInformation("RetrieveBooking: RS: xmlResult=""" & xmlResult.OuterXml & """")
        Return xmlResult
    End Function

    ''' <summary>
    ''' This method retrieves the list of Extra Hire Items and Insurance Options for a given Booking Reference (eg. B123456/1) or reference id
    ''' </summary> 
    ''' <param name="BookingReference">Booking Reference</param>
    ''' <param name="ReferenceId">Reference Id</param>
    ''' <returns>XML Doc with info</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method fetches the available extra hire items and insurance products for a booking reference or reference id.")> _
    Public Function GetInsuranceAndExtraHireItemsForRentalQuote(ByVal BookingReference As String, ByVal ReferenceId As String) As XmlDocument

        LogInformation("GetInsuranceAndExtraHireItemsForRentalQuote: RQ: BookingReference=""" & BookingReference & """, ReferenceId = """ & ReferenceId & """")

        Dim xmlExtraHireItems As New XmlDocument ' , xmlBookingInfo As New XmlDocument
        Dim sErrorMessage As String = "" ' , sRentalId As String = ""

        Try
            ' '' '' pull up the booking info
            '' ''LogInformation("Starting:WebServiceData.RetrieveBooking. RentalNumber = '" & BookingReference & "'")
            '' ''xmlBookingInfo = WebServiceData.RetrieveBooking(BookingReference, ReferenceId, sErrorMessage)
            '' ''LogInformation("Done:WebServiceData.RetrieveBooking. sErrorMessage = '" & sErrorMessage & "', xmlResult = '" & xmlBookingInfo.OuterXml & "'")

            '' ''If xmlBookingInfo.DocumentElement.SelectSingleNode("RentalInfo") Is Nothing Then
            '' ''    Throw New Exception("Unable to retrieve booking info from the booking reference")
            '' ''End If

            ' '' '' read the booking info xml
            '' ''LogInformation("Starting:Reading the booking info xml")
            '' ''With xmlBookingInfo.DocumentElement.SelectSingleNode("RentalInfo/Rental")
            '' ''    sRentalId = .Attributes("RentalId").Value.Trim()
            '' ''End With
            '' ''LogInformation("Done:Reading the booking info xml")

            ' '' '' get extra hire items for the booking
            '' ''LogInformation("Starting:WebServiceData.GetExtraHireItemsForRental")
            '' ''xmlExtraHireItems = WebServiceData.GetExtraHireItemsForRental(sRentalId)
            '' ''LogInformation("Done:WebServiceData.GetExtraHireItemsForRental")

            ' redone!
            ' 1 . just call WEBP_GetInsuranceAndExtraHireItems and ta-da!

            LogInformation("Starting:WebServiceData.GetInsuranceAndExtraHireItemsForRentalQuote. BookingReference = '" & BookingReference & "', ReferenceId = '" & ReferenceId & "'")
            xmlExtraHireItems = WebServiceData.GetInsuranceAndExtraHireItemsForRentalQuote(BookingReference, ReferenceId, sErrorMessage)
            LogInformation("Done:WebServiceData.GetInsuranceAndExtraHireItemsForRentalQuote. sErrorMessage = '" & sErrorMessage & "', xmlResult = '" & xmlExtraHireItems.OuterXml & "'")


        Catch ex As Exception
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            xmlExtraHireItems = GetErrorXML(ex)
        End Try

        LogInformation("GetInsuranceAndExtraHireItemsForRentalQuote: RS:" & xmlExtraHireItems.OuterXml)

        Return xmlExtraHireItems

    End Function

#End Region

#Region "Helper Functions"

    Private Function GetProperStringValue(ByVal InputString As String) As String
        If Not String.IsNullOrEmpty(InputString) AndAlso Not Trim(InputString).Equals(String.Empty) Then
            Return Trim(InputString)
        Else
            Return String.Empty
        End If
    End Function

    Function GetInnerText(ByVal Node As XmlNode) As String
        If Node Is Nothing Then
            Return String.Empty
        Else
            Return GetProperStringValue(Node.InnerText)
        End If
    End Function

    Function GetErrorXML(ByVal ex As Exception) As XmlDocument
        Dim xmlError As New XmlDocument

        If DEBUG_MODE _
           OrElse _
           ( _
                ex.InnerException IsNot Nothing _
                AndAlso _
                ex.InnerException.Message = MESSAGE_FLAG _
            ) _
        Then
            xmlError.LoadXml("<Error><Message>" & Server.HtmlEncode(ex.Message) & "</Message></Error>")
        Else
            xmlError.LoadXml("<Error><Message>" & PUBLIC_ERROR_MESSAGE & "</Message></Error>")
        End If

        Try
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
        Catch
        End Try
        Return xmlError
    End Function

    Sub AddErrorMessageToList(ByRef ErrorList As Hashtable, ByVal Key As String, ByVal Value As String)
        If ErrorList(Key) Is Nothing Then
            ' key doesnt exist, so create
            ErrorList(Key) = XmlEncode(Value)
        Else
            ' key exists
            ' is it the same message?
            If ErrorList(Key) = XmlEncode(Value) Then
                ' do nothing!
            Else
                ' new message , so append message
                ' special case
                If ErrorList(Key).ToString().Contains(VEHICLE_RATES_UNAVAILABLE_MESSAGE) Then
                    ' do nothing
                ElseIf Value.Contains(VEHICLE_RATES_UNAVAILABLE_MESSAGE) Then
                    ' overwrite with this message
                    ErrorList(Key) = XmlEncode(Value)
                Else
                    ErrorList(Key) = ErrorList(Key) & Server.HtmlEncode("<br/>") & XmlEncode(Value)
                End If
            End If
        End If
    End Sub

    Public Shared Function XmlEncode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39, 37, 58, 63}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, Chr(aryChars(i)), "&#" & aryChars(i) & ";")
        Next
        XmlEncode = strText
    End Function

    Public Shared Function XmlDecode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39, 37, 58, 63}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, "&#" & aryChars(i) & ";", Chr(aryChars(i)))
        Next
        XmlDecode = strText
    End Function

    Public Sub LogInformation(ByVal TextToLog As String)
        Try
            If ENABLE_LOGGING Then
                If String.IsNullOrEmpty(RequestIdentifier) Then
                    ' initialise the RequestIdentifier
                    Dim iRandom As Integer = CInt(Rnd() * 10)
                    If CInt(iRandom / 2) = iRandom \ 2 Then
                        RequestIdentifier = "RI" & CStr(CInt(Math.Max(Rnd() * 1000000, Rnd() * 1000000))).PadLeft(6, "0"c)
                    Else
                        RequestIdentifier = "RI" & CStr(CInt(Math.Min(Rnd() * 1000000, Rnd() * 1000000))).PadLeft(6, "0"c)
                    End If

                End If

                Aurora.Common.Logging.LogInformation(PROGRAM_NAME & " " & RequestIdentifier, TextToLog)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Function IsSQLCodePresent(ByVal InputString As String) As Boolean
        For Each sSQLSymbol As String In SQL_CODE_SYMBOL_LIST
            If InputString.Contains(sSQLSymbol) Then
                Return True
            End If
        Next

        Return False

    End Function

#End Region

#Region "Self Checkout"

    ''' <summary>
    ''' This method retrieves details of a rental based on information entered by the customer
    ''' </summary>
    ''' <param name="BookingNumberOrAgentReference">Booking Number / Agent Reference</param>
    ''' <param name="FirstName">Primary Hirer's First Name</param>
    ''' <param name="LastName">Primary Hirer's Last Name</param>
    ''' <param name="PickupLocationCode">Check Out Location Code</param>
    ''' <param name="PickupDate">Check Out date</param>
    ''' <returns>XML Doc with Rental Info</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method returns the details of a booking. For use with Self Check-out")> _
    Public Function GetBookingInfo(ByVal BookingNumberOrAgentReference As String, _
                                   ByVal FirstName As String, _
                                   ByVal LastName As String, _
                                   ByVal PickupLocationCode As String, _
                                   ByVal PickupDate As DateTime, _
                                   ByVal BpdID As String, _
                                   ByVal RntId As String _
                                   ) As XmlDocument

        LogInformation("GetBookingInfo: RQ: BookingNumberOrAgentReference='" & BookingNumberOrAgentReference & _
                                        "', FirstName='" & FirstName & _
                                        "', LastName='" & LastName & _
                                        "', PickupLocationCode='" & PickupLocationCode & _
                                        "', PickupDate='" & PickupDate.ToString(DATE_FORMAT & " " & TIME_FORMAT) & _
                                        "', BpdID='" & BpdID & "'")

        Dim xmlResult As New XmlDocument

        Try
            'If IsSQLCodePresent(FirstName) Then
            '    Throw New Exception("Please enter a valid First Name", New Exception(MESSAGE_FLAG))
            'End If

            'If IsSQLCodePresent(LastName) Then
            '    Throw New Exception("Please enter a valid Last Name", New Exception(MESSAGE_FLAG))
            'End If

            'If IsSQLCodePresent(PickupLocationCode) Then
            '    Throw New Exception("Please enter a valid Pickup Location Code", New Exception(MESSAGE_FLAG))
            'End If

            'If IsSQLCodePresent(BookingNumberOrAgentReference) Then
            '    Throw New Exception("Please enter a valid Booking Number or Agent Reference", New Exception(MESSAGE_FLAG))
            'End If

            'If IsSQLCodePresent(BpdID) Then
            '    Throw New Exception("Please enter a valid BpdID", New Exception(MESSAGE_FLAG))
            'End If

            ' WEBA_getBookingDetails
            Dim sReturnedErrors, sBookingInfoXMLString As String

            LogInformation("Starting:WebServiceData.GetBookingInfo")
            sBookingInfoXMLString = WebServiceData.GetBookingInfo(BookingNumberOrAgentReference, FirstName, LastName, PickupLocationCode, PickupDate, BpdID, RntId, sReturnedErrors)
            LogInformation("Done:WebServiceData.GetBookingInfo. sReturnedErrors=""" & sReturnedErrors & """ , xml=""" & sBookingInfoXMLString & """")

            If String.IsNullOrEmpty(sReturnedErrors) AndAlso Not String.IsNullOrEmpty(sBookingInfoXMLString) Then
                xmlResult.LoadXml(sBookingInfoXMLString)
            ElseIf Not String.IsNullOrEmpty(sReturnedErrors) Then
                ' error message present
                Throw New Exception(sReturnedErrors, New Exception(MESSAGE_FLAG))
            ElseIf String.IsNullOrEmpty(sBookingInfoXMLString) Then
                ' no data
                Throw New Exception("No relevant booking information found", New Exception(MESSAGE_FLAG))
            End If

            ''  xmlResult.DocumentElement.SelectSingleNode("CustomerInfo").InnerXml = GetRelevantXML(xmlResult.DocumentElement.SelectSingleNode("CustomerInfo")).InnerXml


        Catch ex As Exception
            xmlResult = GetErrorXML(ex)
        End Try

        LogInformation("GetBookingInfo: RS:" & xmlResult.OuterXml)

        Return xmlResult
    End Function

    ''' <summary>
    ''' This method retrieves the list of Extra Hire Items and Insurance Options for a given Rental
    ''' </summary>
    ''' <param name="RentalId">Rental Id</param>
    ''' <returns>XML Doc with info</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method fetches the available extra hire items and insurance products for a rental id. For use with Self Check out")> _
    Public Function GetInsuranceAndExtraHireItemsForRental(ByVal RentalId As String) As XmlDocument

        LogInformation("GetInsuranceAndExtraHireItemsForRental: RQ: RntId=""" & RentalId & """")

        Dim xmlExtraHireItems As New XmlDocument

        Try
            LogInformation("Starting:WebServiceData.GetExtraHireItemsForRental")
            xmlExtraHireItems = WebServiceData.GetExtraHireItemsForRental(RentalId)
            LogInformation("Done:WebServiceData.GetExtraHireItemsForRental")

        Catch ex As Exception
            Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            xmlExtraHireItems = GetErrorXML(ex)
        End Try

        LogInformation("GetInsuranceAndExtraHireItemsForRental: RS:" & xmlExtraHireItems.OuterXml)

        Return xmlExtraHireItems

    End Function

    ''' <summary>
    ''' This method Updates the list of Products in a Rental
    ''' </summary>
    ''' <param name="InputData">XML Doc</param>
    ''' <returns>XML Doc</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method adds/removes extra hire items, insurance products to a rental. For use with Self Check out")> _
    Public Function AddRemoveProducts(ByVal InputData As XmlDocument) As XmlDocument

        LogInformation("AddRemoveProducts: RQ: InputData=""" & InputData.OuterXml)

        '<Data>
        '   <RentalDetails>
        '       <RentalId>W3414099-1</RentalId>
        '       <AddProductList>
        '           <Product SapId='03D96C9B-12AB-447D-97A8-69EE2AE846BB' Qty='1' PrdShortName='MPPACK'/>
        '           <Product SapId='94697627-AE78-459A-B756-B157D2D31DA7' Qty='1' PrdShortName='TABLE'/>
        '           <Product SapId='BF3698E3-5B7A-4690-A413-4C65CF134190' Qty='2' PrdShortName='CHAIR'/>
        '           <Product SapId='6FAE9635-C1B6-4D34-9F1F-065905D7EECC' Qty='1' PrdShortName='CHAINS'/>
        '           <Product SapId='BE5F7CB4-9CC4-4852-B3D9-AC1940EE9AC8' Qty='1' PrdShortName='HEATER'/>
        '           <Product Qty='1' PrdShortName='FERRPAX' UsageOn='01/04/2009' SapId='7A3B7C2F-4F77-458F-BE77-5A9BFC272695' >
        '               <PersonProduct name='Adult' Qty='1' />
        '               <PersonProduct name='Child' Qty='0' />
        '               <PersonProduct name='Infant' Qty='0' />
        '           </Product>
        '       </AddProductList>
        '       <RemoveProductList>
        '           <Product BpdId='ZZZZ' />
        '           <Product BpdId='ZZZZ' />
        '       </RemoveProductList>
        '       <UserCode>SP7</UserCode>
        '   </RentalDetails>
        '</Data>


        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ' Read info from input XML
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        Dim RentalId As String, _
            xmlAddProductList As XmlNode, _
            xmlRemoveProductList As XmlNode, _
            CheckOutDateTime As DateTime, _
            CheckInDateTime As DateTime, _
            CheckOutLocationCode As String, _
            CheckInLocationCode As String, _
            UserCode As String, _
            htErrorList As New Hashtable, _
            xmlResult As New XmlDocument

      
        Try

            LogInformation("Starting:Reading Input XML")
            With InputData.DocumentElement
                RentalId = GetInnerText(.SelectSingleNode("RentalDetails/RentalId"))
                xmlAddProductList = .SelectSingleNode("RentalDetails/AddProductList")
                xmlRemoveProductList = .SelectSingleNode("RentalDetails/RemoveProductList")
                'UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
                UserCode = SELF_CHECK_OUT_USER_CODE
            End With

            If String.IsNullOrEmpty(RentalId) Then
                Throw New Exception("Rental Id must be specified", New Exception(MESSAGE_FLAG))
            End If

            If String.IsNullOrEmpty(UserCode) Then
                Throw New Exception("User code must be specified", New Exception(MESSAGE_FLAG))
            End If

            If (xmlAddProductList Is Nothing OrElse xmlAddProductList.ChildNodes.Count = 0) _
            AndAlso (xmlRemoveProductList Is Nothing OrElse xmlRemoveProductList.ChildNodes.Count = 0) Then
                Throw New Exception("Either a list of products to be added or a list of products to be removed must be specified", New Exception(MESSAGE_FLAG))
            End If

            LogInformation("Done:Reading Input XML")

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' Fetch Rental Info
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            LogInformation("Starting:WebServiceData.GetDetailsFromRentalId")
            WebServiceData.GetDetailsFromRentalId(RentalId:=RentalId, _
                                                  CheckOutDateTime:=CheckOutDateTime, _
                                                  CheckInDateTime:=CheckInDateTime, _
                                                  CheckOutLocationCode:=CheckOutLocationCode, _
                                                  CheckInLocationCode:=CheckInLocationCode)
            LogInformation("Done:WebServiceData.GetDetailsFromRentalId.")


            Dim sProgramName As String = "SaveDetailBPD"
            Dim bSomethingIsDeleted As Boolean = False

            Dim sReturnedBPDId As String = ""
            Dim sReturnedRNHId As String = ""
            Dim dtCkoDate As DateTime


            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
                Try
                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    ' Add the products that have to be added
                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    

                    If xmlAddProductList IsNot Nothing AndAlso xmlAddProductList.ChildNodes.Count > 0 Then
                        For Each oProductNode As XmlNode In xmlAddProductList.SelectNodes("Product")


                            ''rev:mia 12May2015 - addition of Rental for TCX
                            Dim rate As String = "0.00"
                            Dim noteText As String = ""
                            Dim slNumber As String = ""
                            Dim prodshortname As String = ""

                            Try
                                rate = oProductNode.Attributes("Rate").Value
                                noteText = oProductNode.Attributes("NoteText").Value
                                slNumber = oProductNode.Attributes("SLNum").Value
                                prodshortname = oProductNode.Attributes("PrdShortName").Value

                            Catch ex As Exception
                                rate = "0.00"
                                noteText = ""
                                slNumber = ""
                                prodshortname = ""
                                LogInformation("Error getting these attributes Rate , NoteText , SLNum")
                            End Try

                            ''Dim xmlAcutalPrrDetails As XmlDocument
                            Dim saPrrIdList(2), saPrrQtyList(2), sPrrIdList, sPrrQtyLsit As String
                            Dim dtUsageDate As Date

                            If oProductNode.Attributes.Count > 0 AndAlso oProductNode.Attributes("UsageOn") IsNot Nothing Then
                                dtUsageDate = Aurora.Common.Utility.DateDBParse(oProductNode.Attributes("UsageOn").Value)
                            End If

                            LogInformation("Starting:Processing Ferry like products")
                            If oProductNode.SelectNodes("PersonProduct") IsNot Nothing AndAlso oProductNode.SelectNodes("PersonProduct").Count > 0 Then
                                ' theres atleast one PRR

                                ' new logic!
                                For Each oPersonRateNode As XmlNode In oProductNode.SelectNodes("PersonProduct")
                                    With oPersonRateNode
                                        Select Case Trim(UCase(.Attributes("name").Value))
                                            Case ADULT_STRING
                                                Try
                                                    If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                        saPrrIdList(0) = GetProperStringValue(.Attributes("PrrId").Value)
                                                        saPrrQtyList(0) = GetProperStringValue(.Attributes("Qty").Value)
                                                    End If
                                                Catch ex As System.InvalidCastException
                                                    Throw New Exception("Invalid Quantity specified for number of Adult pax")
                                                End Try

                                            Case CHILD_STRING
                                                Try
                                                    If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                        saPrrIdList(1) = GetProperStringValue(.Attributes("PrrId").Value)
                                                        saPrrQtyList(1) = GetProperStringValue(.Attributes("Qty").Value)
                                                    End If
                                                Catch ex As System.InvalidCastException
                                                    Throw New Exception("Invalid Quantity specified for number of Child pax")
                                                End Try

                                            Case INFANT_STRING
                                                Try
                                                    If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                        saPrrIdList(2) = GetProperStringValue(.Attributes("PrrId").Value)
                                                        saPrrQtyList(2) = GetProperStringValue(.Attributes("Qty").Value)
                                                    End If
                                                Catch ex As System.InvalidCastException
                                                    Throw New Exception("Invalid Quantity specified for number of Infant pax")
                                                End Try
                                        End Select
                                    End With
                                Next

                                ' new logic!

                                ' ''xmlAcutalPrrDetails = WebServiceData.GetPersonRates(dtUsageDate, Trim(oProductNode.Attributes("SapId").Value))

                                ' ''For Each oDateRangeNode As XmlNode In xmlAcutalPrrDetails.DocumentElement.FirstChild.SelectNodes("StartOn")
                                ' ''    If (CDate(oDateRangeNode.Attributes("dateBegin").Value) < dtUsageDate And CDate(oDateRangeNode.Attributes("dateEnd").Value) > dtUsageDate) _
                                ' ''        Or CDate(oDateRangeNode.Attributes("dateBegin").Value) = dtUsageDate _
                                ' ''        Or CDate(oDateRangeNode.Attributes("dateEnd").Value) = dtUsageDate _
                                ' ''    Then
                                ' ''        For Each oPersonRateNode As XmlNode In oDateRangeNode.ChildNodes
                                ' ''            If GetProperStringValue(oPersonRateNode.Attributes("SapId").Value) = GetProperStringValue(oProductNode.Attributes("SapId").Value) Then
                                ' ''                Select Case Trim(UCase(oPersonRateNode.Attributes("PaxType").Value))
                                ' ''                    ' Only add values when the prr Qty <> 0
                                ' ''                    ' use UsageOn date instead of CkoDate, CkiDate = nothing
                                ' ''                    Case ADULT_STRING
                                ' ''                        If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Adult']").Attributes("Qty").Value)) > 0 Then
                                ' ''                            saPrrIdList(0) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                                ' ''                            saPrrQtyList(0) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Adult']").Attributes("Qty").Value)
                                ' ''                        End If
                                ' ''                    Case CHILD_STRING
                                ' ''                        If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Child']").Attributes("Qty").Value)) > 0 Then
                                ' ''                            saPrrIdList(1) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                                ' ''                            saPrrQtyList(1) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Child']").Attributes("Qty").Value)
                                ' ''                        End If
                                ' ''                    Case INFANT_STRING
                                ' ''                        If CInt(GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Infant']").Attributes("Qty").Value)) > 0 Then
                                ' ''                            saPrrIdList(2) = GetProperStringValue(oPersonRateNode.Attributes("PrrId").Value)
                                ' ''                            saPrrQtyList(2) = GetProperStringValue(oProductNode.SelectSingleNode("PersonProduct [@name='Infant']").Attributes("Qty").Value)
                                ' ''                        End If
                                ' ''                End Select
                                ' ''            End If
                                ' ''        Next
                                ' ''    End If
                                ' ''Next

                                sPrrIdList = ""
                                sPrrQtyLsit = ""
                                For j As Int16 = 0 To saPrrIdList.Length - 1
                                    If Not saPrrIdList(j) Is Nothing Then
                                        sPrrIdList = sPrrIdList & saPrrIdList(j) & ","
                                        sPrrQtyLsit = sPrrQtyLsit & saPrrQtyList(j) & ","
                                    End If
                                Next

                                If sPrrQtyLsit.LastIndexOf(",") = sPrrQtyLsit.Length - 1 Then
                                    sPrrQtyLsit = sPrrQtyLsit.Substring(0, sPrrQtyLsit.Length - 1)
                                End If

                                If sPrrIdList.LastIndexOf(",") = sPrrIdList.Length - 1 Then
                                    sPrrIdList = sPrrIdList.Substring(0, sPrrIdList.Length - 1)
                                End If

                            Else
                                ' NON PRR stuff
                                dtCkoDate = CheckOutDateTime
                                sPrrIdList = "NoPrr"
                                sPrrQtyLsit = ""
                            End If
                            LogInformation("Done:Processing Ferry like products")

                            If dtUsageDate <> DateTime.MinValue Then
                                dtCkoDate = dtUsageDate
                            End If

                            'Try
                            Dim bResult As Boolean = False
                            Dim sErrorMessage As String = ""

                            LogInformation("Starting:AddBookedProduct. SapId=""" & GetProperStringValue(oProductNode.Attributes("SapId").Value) & """")
                            bResult = AddBookedProduct(RentalId, _
                                                       GetProperStringValue(oProductNode.Attributes("SapId").Value), _
                                                       dtCkoDate, _
                                                       CheckInDateTime, _
                                                       CheckOutLocationCode, _
                                                       CheckInLocationCode, _
                                                       CInt(GetProperStringValue(oProductNode.Attributes("Qty").Value)), _
                                                       "", _
                                                       sPrrIdList, _
                                                       sPrrQtyLsit, _
                                                       sReturnedRNHId, _
                                                       UserCode, _
                                                       sProgramName, _
                                                       sReturnedBPDId, _
                                                       sErrorMessage, rate) ''rev:mia 12May2015 - addition of Rental for TCX
                            LogInformation("Done:AddBookedProduct. bResult=""" & bResult.ToString() & """. sErrorMessage=""" & sErrorMessage & """")

                            If sErrorMessage <> "" Then
                                Throw New Exception(sErrorMessage)
                            End If

                            If Not bResult Then
                                Throw New Exception("Unable to add extra hire items")
                            End If

                            ''rev:mia 12May2015 - addition of Rental for TCX
                            Try
                                If (Not String.IsNullOrEmpty(noteText)) Then
                                    LogInformation("Starting:WebServiceData.AddRentalNote with RentalId : " & RentalId & ", NoteText: " & noteText)
                                    AddRentalNote(RentalId, noteText, UserCode, sProgramName, Nothing, Nothing)
                                    LogInformation("Done:WebServiceData.AddRentalNote")
                                End If

                            Catch ex As Exception
                                LogInformation("Done With Error:AddRentalNote " & ex.Message & ", " & ex.StackTrace)
                            End Try

                            Try
                                If (Not String.IsNullOrEmpty(slNumber)) Then
                                    LogInformation("Starting:SaveProductSerialNumber with BpdId: " & sReturnedBPDId & ", Serial #: " & slNumber)
                                    Dim result As String = SaveProductSerialNumber(sReturnedBPDId, slNumber, UserCode, sProgramName)
                                    LogInformation("Done:SaveProductSerialNumber")
                                    If result = "ERROR" Then
                                        LogInformation("Done with error:SaveProductSerialNumber with BpdId: " & sReturnedBPDId & ", Serial #: " & slNumber)
                                    End If
                                End If
                            Catch ex As Exception
                                LogInformation("Done With Error:SaveProductSerialNumber " & ex.Message & ", " & ex.StackTrace)
                            End Try

                            


                            LogInformation("Starting:WebServiceData.DeleteTProcessedBPDs")
                            WebServiceData.DeleteTProcessedBPDs(RentalId)
                            LogInformation("Done:WebServiceData.DeleteTProcessedBPDs")
                            '        Catch ex As Exception
                            '    AddErrorMessageToList(htErrorList, oProductNode.Attributes("PrdShortName").Value, ex.Message)
                            'End Try
                        Next

                    End If
                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    ' Remove the products that have to be removed
                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    Dim lstBpdIdList As List(Of String) = New List(Of String)
                    Dim sErrMsg As String, bSuccess As Boolean = False

                    'Try
                    If xmlRemoveProductList IsNot Nothing AndAlso xmlRemoveProductList.ChildNodes.Count > 0 Then

                        For Each oProductNode As XmlNode In xmlRemoveProductList.SelectNodes("Product")
                            lstBpdIdList.Add(GetProperStringValue(oProductNode.Attributes("BpdId").Value))
                        Next

                        LogInformation("Starting:RemoveBookedProduct.")
                        bSuccess = RemoveBookedProduct(RentalId, lstBpdIdList, UserCode, sErrMsg)
                        LogInformation("Done:RemoveBookedProduct.")

                        If Not bSuccess OrElse GetProperStringValue(sErrMsg) <> "" Then
                            Throw New Exception(sErrMsg)
                        End If

                    End If

                    'Catch ex As Exception
                    '    AddErrorMessageToList(htErrorList, "DELETE_BPD", ex.Message)
                    'End Try

                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    ' SS Trans 2 FINANCE
                    '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    LogInformation("Starting:WebServiceData.SSTransToFinance.")
                    WebServiceData.SSTransToFinance("", RentalId, "", "")
                    LogInformation("Done:WebServiceData.SSTransToFinance.")

                    ' Got here...all done! :)
                    oTransaction.CommitTransaction()
                    LogInformation("CommitTransaction Complete")

                    xmlResult.LoadXml("<Data><Message>Success</Message></Data>")
                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    LogInformation("RollbackTransaction Complete")
                    xmlResult = GetErrorXML(ex)
                End Try
            End Using
        Catch ex As Exception
            xmlResult = GetErrorXML(ex)
        End Try
        LogInformation("AddRemoveProducts: RS: OutputData=""" & xmlResult.OuterXml)

        Return xmlResult

    End Function

    ''' <summary>
    ''' This method removes booked products from the specified rental
    ''' </summary>
    ''' <param name="RentalId">The Rental Id to use</param>
    ''' <param name="BookedProductIdList">A string array with the BPD Ids</param>
    ''' <param name="UserCode">User Code</param>
    ''' <returns>True if successful/False if some error occurs</returns>
    ''' <remarks></remarks>
    Function RemoveBookedProduct(ByVal RentalId As String, _
                                 ByVal BookedProductIdList As List(Of String), _
                                 ByVal UserCode As String, _
                                 ByRef ErrorMessage As String) As Boolean
        'sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_manageBookedProductList", sRntId, oXMLDom.OuterXml, bResult, UserCode).ToString()

        ' <Data>
        '   <Rental id="1">
        '		<BpdId>FE03399B4A7D4D419668B6BC321FF7F5</BpdId>
        '		<Cancel>1</Cancel>
        '	</Rental>
        '   . . .
        ' </Data>

        Dim sbXMLString As New StringBuilder
        Dim sReturnXMLString As String
        Dim iRollingCounter As Int16 = 1

        sbXMLString.Append("<Data>")

        For Each sBPDId As String In BookedProductIdList
            sbXMLString.Append("<Rental id=""" & iRollingCounter.ToString() & """>")
            sbXMLString.Append("    <BpdId>" & sBPDId & "</BpdId>")
            sbXMLString.Append("    <Cancel>1</Cancel>")
            sbXMLString.Append("</Rental>")

            iRollingCounter = iRollingCounter + 1
        Next

        sbXMLString.Append("</Data>")

        sReturnXMLString = Aurora.Common.Data.ExecuteScalarSP("RES_manageBookedProductList", RentalId, sbXMLString.ToString(), True, UserCode).ToString()

        Dim xmlReturnMessage As New XmlDocument

        xmlReturnMessage.LoadXml("<Data>" & sReturnXMLString & "</Data>")

        If xmlReturnMessage.DocumentElement.SelectSingleNode("Errors").InnerText <> "" Then
            ErrorMessage = xmlReturnMessage.DocumentElement.SelectSingleNode("Errors").InnerText
            Return False
        Else
            Return True
        End If

    End Function

    Const RENTALAGREEMENT_XSL_PATH As String = "xsl/RentalAgreement.xsl"

    ''' <summary>
    ''' This webmethod allows addition/updation of customer info + addition and removal of Extra Driver Fee (or any other product)
    ''' </summary>
    ''' <param name="InputData">XML with Customer Data + Extra Driver Fee (optional) Data</param>
    ''' <returns>XML</returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This webmethod allows addition/updation of customer info + addition and removal of Extra Driver Fee (or any other product)")> _
    Public Function AddUpdateCustomerData(ByVal InputData As XmlDocument) As XmlDocument
        LogInformation("AddUpdateCustomerData: RQ: InputData=""" & InputData.OuterXml)

        '<Data>
        '   <CustomerInfo>
        '       <Customer>
        '           <CustomerDetails>
        '               <!-- START common for all customers -->
        '               <CusId></CusId>

        '               <FirstName></FirstName>
        '               <LastName></LastName>
        '               <Title></Title>

        '               <LicenceNum></LicenceNum>
        '               <ExpiryDate></ExpiryDate>
        '               <IssingAuthority></IssingAuthority>

        '               <DOB></DOB>
        '               <!-- END common for all customers -->

        '               <!-- START only for primary hirer-->
        '               <PhDayCtyCode></PhDayCtyCode>
        '               <PhDayAreaCode></PhDayAreaCode>
        '               <PhDayNumber></PhDayNumber>

        '               <MobileCtyCode></MobileCtyCode>
        '               <MobileAreaCode></MobileAreaCode>
        '               <MobileNumber></MobileNumber>

        '               <Email></Email>
        '               <LoyaltyCardNumber>Fly buys 123</LoyaltyCardNumber>

        '           </CustomerDetails>
        '           <PhysicalAddress>
        '               <Street></Street>
        '               <CityTown></CityTown>
        '               <Country></Country>
        '               <!-- END only for primary hirer-->
        '           </PhysicalAddress>
        '       </Customer>
        '       <Customer>
        '           ...
        '       </Customer>
        '       <Customer>
        '            <CustomerDetails>
        '               <CusId>1234</CusId>
        '               <Remove>1</Remove>                        
        '            </CustomerDetails>
        '       </Customer>
        '   </CustomerInfo>
        '   <RentalDetails>
        '       <AddProductList>
        '           <Product SapId='F6E8123C-9666-411F-B102-464231E03ECE' Qty='1' PrdShortName='EXTRADB'/>
        '           <Product SapId='94697627-AE78-459A-B756-B157D2D31DA7' Qty='1' PrdShortName='XTRADRV'/>
        '       </AddProductList>
        '       <RemoveProductList>
        '           <Product BpdId='ZZZZ' />
        '           <Product BpdId='ZZZZ' />
        '       </RemoveProductList>
        '       <RentalId>W3414099-1</RentalId>
        '       <CountryCode>NZ</CountryCode>
        '       <UserCode>B2CNZ</UserCode>
        '       <ArrivalRef>Flight 714</ArrivalRef>
        '       <ArrivalDateTime>12-Mar-2010 11:00 PM</ArrivalDateTime>
        '       <DepartureRef>Flight 724</DepartureRef>
        '       <DepartureDateTime>12-May-2010 11:00 PM</DepartureDateTime>
        '       <ArrivalAtBranchDateTime>13/07/2010 13:00</ArrivalAtBranchDateTime>
        '   </RentalDetails>
        '</Data>

        Dim xmlResult As New XmlDocument, _
            RentalId As String, _
            RentalCountryCode As String, _
            UserCode As String, _
            xmlCustomerInfo As XmlNode, _
            xmlAddProductList As XmlNode, _
            xmlRemoveProductList As XmlNode, _
            CheckOutDateTime As DateTime, _
            CheckInDateTime As DateTime, _
            CheckOutLocationCode As String, _
            CheckInLocationCode As String, _
            RentalNum As String, _
            BookingId As String, _
            ArrivalRef As String, _
            ArrivalDateTime As DateTime, _
            DepartureRef As String, _
            DepartureDateTime As DateTime, _
            ArrivalAtBranchDateTime As DateTime, _
            DropOffAtBranchDateTime As DateTime

        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ' Read info from input XML
        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        LogInformation("Starting:Reading Input XML")
        With InputData.DocumentElement
            RentalId = GetInnerText(.SelectSingleNode("RentalDetails/RentalId"))
            If String.IsNullOrEmpty(RentalId) Then
                Throw New Exception("Rental Id must be specified", New Exception(MESSAGE_FLAG))
            End If

            RentalCountryCode = GetInnerText(.SelectSingleNode("RentalDetails/CountryCode"))
            If String.IsNullOrEmpty(RentalId) Then
                Throw New Exception("Rental country code must be specified")
            End If

            'UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
            ' change for Retrieve/modify quote
            ' use the passed in UserCode if any
            If String.IsNullOrEmpty(GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))) Then
                UserCode = SELF_CHECK_OUT_USER_CODE
            Else
                UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
            End If
            ' change for Retrieve/modify quote

            If String.IsNullOrEmpty(RentalId) Then
                Throw New Exception("User code must be specified", New Exception(MESSAGE_FLAG))
            End If

            xmlCustomerInfo = .SelectSingleNode("CustomerInfo")

            xmlAddProductList = .SelectSingleNode("RentalDetails/AddProductList")
            xmlRemoveProductList = .SelectSingleNode("RentalDetails/RemoveProductList")

            Try
                ArrivalDateTime = CDate(GetInnerText(.SelectSingleNode("RentalDetails/ArrivalDateTime")))
            Catch ex As Exception
                ArrivalDateTime = DateTime.MinValue
            End Try

            ArrivalRef = GetInnerText(.SelectSingleNode("RentalDetails/ArrivalRef"))
            DepartureRef = GetInnerText(.SelectSingleNode("RentalDetails/DepartureRef"))

            Try
                DepartureDateTime = CDate(GetInnerText(.SelectSingleNode("RentalDetails/DepartureDateTime")))
            Catch ex As Exception
                DepartureDateTime = DateTime.MinValue
            End Try

            Try

                Dim strCompleteDate As String = GetInnerText(.SelectSingleNode("RentalDetails/ArrivalAtBranchDateTime"))
                Dim strDateArray() As String = strCompleteDate.Split(" ")
                ''rev:mia 17march2015- Investigate SCI Non Time Slot Arrival Time not retrieving or updating Aurora
                If (strDateArray.Length = 2) Then
                    Dim strtime As String = strDateArray(1)
                    ArrivalAtBranchDateTime = CDate(strCompleteDate)
                    ArrivalAtBranchDateTime = New DateTime(ArrivalAtBranchDateTime.Year, _
                                                           ArrivalAtBranchDateTime.Month, _
                                                           ArrivalAtBranchDateTime.Day, _
                                                           strtime.Split(":")(0), strtime.Split(":")(1), 0)
                Else
                    ArrivalAtBranchDateTime = CDate(strCompleteDate)
                End If

            Catch ex As Exception
                ArrivalAtBranchDateTime = DateTime.MinValue
            End Try

            '-- RKS : Mod 25-May-2012
            '-- For mighty brands
            Try
                DropOffAtBranchDateTime = CDate(GetInnerText(.SelectSingleNode("RentalDetails/DropOffAtBranchDateTime")))
            Catch ex As Exception
                DropOffAtBranchDateTime = DateTime.MinValue
            End Try

        End With
        LogInformation("Done:Reading Input XML")

        LogInformation("CustomerData - Transaction Starting")
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' Fetch Rental Info
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            LogInformation("Starting:WebServiceData.GetDetailsFromRentalId")
            WebServiceData.GetDetailsFromRentalId(RentalId:=RentalId, _
                                                  CheckOutDateTime:=CheckOutDateTime, _
                                                  CheckInDateTime:=CheckInDateTime, _
                                                  CheckOutLocationCode:=CheckOutLocationCode, _
                                                  CheckInLocationCode:=CheckInLocationCode, _
                                                  RentalNo:=RentalNum, _
                                                  BookingId:=BookingId)
            LogInformation("Done:WebServiceData.GetDetailsFromRentalId.")

            Dim sProgramName As String = "SaveDetailBPD"
            Dim bIsConfirmQuote As Boolean = False
            If InputData.DocumentElement.SelectSingleNode("RentalDetails/ConfirmQuote") IsNot Nothing Then
                bIsConfirmQuote = CBool(GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/ConfirmQuote")))
            Else
                bIsConfirmQuote = False
            End If

            If bIsConfirmQuote = True Then
                sProgramName = sProgramName + "-QuoteConf"
            End If
            Dim bSomethingIsDeleted As Boolean = False

            Dim sReturnedBPDId As String = ""
            Dim sReturnedRNHId As String = ""
            Dim dtCkoDate As DateTime

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' Save Customer Info
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            Try
                If xmlCustomerInfo IsNot Nothing AndAlso xmlCustomerInfo.ChildNodes.Count > 0 Then
                    For Each xmlCustomerNode As XmlNode In xmlCustomerInfo.ChildNodes
                        Dim bResult As Boolean, sErrorMessage As String

                        LogInformation("Starting:AddUpdateCustomerInfo")
                        bResult = AddUpdateCustomerInfo(xmlCustomerNode, RentalId, UserCode, CheckInDateTime, BookingId, RentalNum, sErrorMessage)
                        LogInformation("Done:AddUpdateCustomerInfo. bResult=" & bResult.ToString() & ", sErrorMessage=" & CStr(sErrorMessage))

                        If Not bResult OrElse Not String.IsNullOrEmpty(sErrorMessage) Then
                            Throw New Exception(sErrorMessage)
                        End If
                    Next
                End If

                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' Save Customer Info - DONE!
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' Save Extra Driver Fees if any!
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                If xmlAddProductList IsNot Nothing AndAlso xmlAddProductList.ChildNodes.Count > 0 Then
                    For Each oProductNode As XmlNode In xmlAddProductList.SelectNodes("Product")
                        Dim xmlAcutalPrrDetails As XmlDocument
                        Dim saPrrIdList(2), saPrrQtyList(2), sPrrIdList, sPrrQtyLsit As String
                        Dim dtUsageDate As Date

                        If oProductNode.Attributes.Count > 0 AndAlso oProductNode.Attributes("UsageOn") IsNot Nothing Then
                            dtUsageDate = Aurora.Common.Utility.DateDBParse(oProductNode.Attributes("UsageOn").Value)
                        End If

                        If dtUsageDate <> DateTime.MinValue Then
                            dtCkoDate = dtUsageDate
                        End If

                        Dim bResult As Boolean = False
                        Dim sErrorMessage As String = ""

                        LogInformation("Starting:Processing Ferry like products")
                        If oProductNode.SelectNodes("PersonProduct") IsNot Nothing AndAlso oProductNode.SelectNodes("PersonProduct").Count > 0 Then
                            ' theres atleastt one PRR

                            ' new logic!
                            For Each oPersonRateNode As XmlNode In oProductNode.SelectNodes("PersonProduct")
                                With oPersonRateNode
                                    Select Case Trim(UCase(.Attributes("name").Value))
                                        Case ADULT_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(0) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(0) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Adult pax")
                                            End Try

                                        Case CHILD_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(1) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(1) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Child pax")
                                            End Try

                                        Case INFANT_STRING
                                            Try
                                                If CInt(GetProperStringValue(.Attributes("Qty").Value)) > 0 Then
                                                    saPrrIdList(2) = GetProperStringValue(.Attributes("PrrId").Value)
                                                    saPrrQtyList(2) = GetProperStringValue(.Attributes("Qty").Value)
                                                End If
                                            Catch ex As System.InvalidCastException
                                                Throw New Exception("Invalid Quantity specified for number of Infant pax")
                                            End Try
                                    End Select
                                End With
                            Next

                            ' new logic!

                            ' check to see if any rates were available
                            If saPrrIdList(0) Is Nothing AndAlso saPrrIdList(1) Is Nothing AndAlso saPrrIdList(2) Is Nothing Then
                                Throw New Exception("No rates defined for person dependent rate product - " & oProductNode.Attributes("PrdShortName").Value)
                            End If

                            sPrrIdList = ""
                            sPrrQtyLsit = ""
                            For j As Int16 = 0 To saPrrIdList.Length - 1
                                If Not saPrrIdList(j) Is Nothing Then
                                    sPrrIdList = sPrrIdList & saPrrIdList(j) & ","
                                    sPrrQtyLsit = sPrrQtyLsit & saPrrQtyList(j) & ","
                                End If
                            Next

                            If sPrrQtyLsit.LastIndexOf(",") = sPrrQtyLsit.Length - 1 Then
                                sPrrQtyLsit = sPrrQtyLsit.Substring(0, sPrrQtyLsit.Length - 1)
                            End If

                            If sPrrIdList.LastIndexOf(",") = sPrrIdList.Length - 1 Then
                                sPrrIdList = sPrrIdList.Substring(0, sPrrIdList.Length - 1)
                            End If

                        Else
                            ' NON PRR stuff
                            dtCkoDate = CheckOutDateTime
                            sPrrIdList = "NoPrr"
                            sPrrQtyLsit = ""
                        End If
                        LogInformation("Done:Processing Ferry like products")

                        If dtUsageDate <> DateTime.MinValue Then
                            dtCkoDate = dtUsageDate ' this ensures that things like FerrVeh use the specified UsageOn date
                        End If

                        LogInformation("Starting:AddBookedProduct. SapId=""" & GetProperStringValue(oProductNode.Attributes("SapId").Value) & """")
                        bResult = AddBookedProduct(RentalId, _
                                                   GetProperStringValue(oProductNode.Attributes("SapId").Value), _
                                                   dtCkoDate, _
                                                   CheckInDateTime, _
                                                   CheckOutLocationCode, _
                                                   CheckInLocationCode, _
                                                   CInt(GetProperStringValue(oProductNode.Attributes("Qty").Value)), _
                                                   "", _
                                                   sPrrIdList, _
                                                   sPrrQtyLsit, _
                                                   sReturnedRNHId, _
                                                   UserCode, _
                                                   sProgramName, _
                                                   sReturnedBPDId, _
                                                   sErrorMessage)
                        LogInformation("Done:AddBookedProduct. bResult=""" & bResult.ToString() & """. sErrorMessage=""" & sErrorMessage & """")

                        If sErrorMessage <> "" Then
                            Throw New Exception(sErrorMessage)
                        End If

                        If Not bResult Then
                            Throw New Exception("Unable to add extra hire items")
                        End If

                        LogInformation("Starting:WebServiceData.DeleteTProcessedBPDs")
                        WebServiceData.DeleteTProcessedBPDs(RentalId)
                        LogInformation("Done:WebServiceData.DeleteTProcessedBPDs")

                        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        ' Save Extra Driver Fees if any! - DONE
                        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                    Next
                End If

                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' Remove the products that have to be removed
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                Dim lstBpdIdList As List(Of String) = New List(Of String)
                Dim sErrMsg As String, bSuccess As Boolean = False

                'Try
                If xmlRemoveProductList IsNot Nothing AndAlso xmlRemoveProductList.ChildNodes.Count > 0 Then

                    For Each oProductNode As XmlNode In xmlRemoveProductList.SelectNodes("Product")
                        lstBpdIdList.Add(GetProperStringValue(oProductNode.Attributes("BpdId").Value))
                    Next

                    LogInformation("Starting:RemoveBookedProduct.")
                    bSuccess = RemoveBookedProduct(RentalId, lstBpdIdList, UserCode, sErrMsg)
                    LogInformation("Done:RemoveBookedProduct.")

                    If Not bSuccess OrElse GetProperStringValue(sErrMsg) <> "" Then
                        Throw New Exception(sErrMsg)
                    End If

                End If
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' Remove the products that have to be removed - DONE
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                ' SS Trans 2 FINANCE
                '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                LogInformation("Starting:WebServiceData.SSTransToFinance.")
                WebServiceData.SSTransToFinance("", RentalId, "", "")
                LogInformation("Done:WebServiceData.SSTransToFinance.")

                ' save arrival and departure flight details - start
                LogInformation("Starting:WebServiceData.SaveArrivalAndDepartureDetails")
                sErrMsg = "" ' reset the error

                ' skip if all 4 are missing

                If ArrivalDateTime = DateTime.MinValue _
                   AndAlso _
                   DepartureDateTime = DateTime.MinValue _
                   AndAlso _
                   String.IsNullOrEmpty(ArrivalRef) _
                   AndAlso _
                   String.IsNullOrEmpty(DepartureRef) _
                   AndAlso _
                   ArrivalAtBranchDateTime = DateTime.MinValue _
                   AndAlso _
                   DropOffAtBranchDateTime = DateTime.MinValue _
                Then
                    LogInformation("SKIPPED:WebServiceData.SaveArrivalAndDepartureDetails")
                Else
                    ''rev:mia 03nov2014 - SLOT MANAGEMENT
                    Dim RntSelectedSlotId As String = GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/ArrivalSlotTimeId"))
                    Dim RntSelectedSlot As String = GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/ArrivalSlotTime"))
                     If WebServiceData.SaveArrivalAndDepartureDetails(RentalId, ArrivalRef, ArrivalDateTime, DepartureRef, DepartureDateTime, ArrivalAtBranchDateTime, DropOffAtBranchDateTime, UserCode, sErrMsg, RntSelectedSlotId, RntSelectedSlot) Then
                        LogInformation("SUCCESS:WebServiceData.SaveArrivalAndDepartureDetails")
                    Else
                        LogInformation("ERROR:WebServiceData.SaveArrivalAndDepartureDetails" & vbNewLine & sErrMsg)
                    End If
                End If

                ''rev:mia 02Dec2014 - AUDIT TRAIL FOR SCI VERIFIED DRIVERS
                Try
                    Dim AllDriverConfirmed As String = GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/AllDriverConfirmed"))
                    Dim ConfirmedBy As String = GetInnerText(InputData.DocumentElement.SelectSingleNode("RentalDetails/ConfirmedBy"))


                    If (Not String.IsNullOrEmpty(AllDriverConfirmed) And Not String.IsNullOrEmpty(ConfirmedBy)) Then
                        LogInformation("Calling WebServiceData.SCIVerifiedDrivers: AllDriverConfirmed: " & AllDriverConfirmed & ", ConfirmedBy: " & ConfirmedBy)
                        Dim result As String = WebServiceData.SCIVerifiedDrivers(RentalId, ConfirmedBy)
                        If (result = "ERROR") Then
                            LogInformation("ERROR:WebServiceData.SCIVerifiedDrivers")
                        Else
                            LogInformation("SUCCESS:WebServiceData.SCIVerifiedDrivers")
                        End If
                    Else
                        LogInformation("SKIPPINg WebServiceData.SCIVerifiedDrivers: AllDriverConfirmed: " & AllDriverConfirmed & ", ConfirmedBy: " & ConfirmedBy)
                    End If


                    ''VerifyDriver
                Catch ex As Exception
                    ''do nothing
                End Try



                LogInformation("Done:WebServiceData.SaveArrivalAndDepartureDetails")
                ' save arrival and departure flight details - done

                oTransaction.CommitTransaction()
                LogInformation("CustomerData - Transaction Committed")

                xmlResult.LoadXml("<Data><Status>SUCCESS</Status><Message>Customer Data Add-Update Successful</Message></Data>")
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                LogInformation("CustomerData - Transaction Rolledback" & ex.StackTrace)
                xmlResult = GetErrorXML(ex)
            End Try
        End Using

        LogInformation("AddUpdateCustomerData: RS: xmlResult=""" & xmlResult.OuterXml)

        Return xmlResult
    End Function

    '<WebMethod()> _
    'Public Function GetCountriesForSelfCheckOut(ByVal CompanyCode As String, ByVal BrandCode As String) As XmlDocument

    '    LogInformation("GetCountriesForSelfCheckOut: RQ: CompanyCode:=" & CompanyCode & _
    '                                                  ", BrandCode:=" & BrandCode)

    '    Dim dtLocations As DataTable

    '    ' cache the list of locations if its not already cached
    '    If Application("SCO_LocationList") Is Nothing Then
    '        ' get the list
    '        LogInformation("Starting:WebServiceData.GetLocationsForSelfCheckOut")
    '        dtLocations = WebServiceData.GetLocationsForSelfCheckOut("", "", "")
    '        LogInformation("Done:WebServiceData.GetLocationsForSelfCheckOut")
    '        Application("SCO_LocationList") = dtLocations
    '    Else
    '        LogInformation("Using Cached Location List")
    '        dtLocations = CType(Application("SCO_LocationList"), DataTable)
    '    End If

    '    Dim dvFilteredView As New DataView(dtLocations)
    '    Dim sbFilter As New StringBuilder

    '    If GetProperStringValue(CompanyCode) <> "" Then
    '        sbFilter.Append("ComCode = '" & CompanyCode & "' ")
    '    End If

    '    If GetProperStringValue(BrandCode) <> "" Then
    '        If sbFilter.ToString().Length > 0 Then
    '            ' already something in here...
    '            sbFilter.Append("AND ")
    '        End If
    '        sbFilter.Append("BrdCode = '" & BrandCode & "' ")
    '    End If

    '    dvFilteredView.RowFilter = sbFilter.ToString()

    '    Dim sbXMLResult As New StringBuilder

    '    sbXMLResult.Append("<Countries>")

    '    For Each rwLocation As DataRow In dvFilteredView.ToTable(True, New String() {"CtyCode", "CtyName"}).Rows
    '        sbXMLResult.Append("<Country CtyCode='" & rwLocation("CtyCode") & "' CtyName='" & rwLocation("CtyName") & "' />")
    '    Next

    '    sbXMLResult.Append("</Countries>")

    '    Dim xmlResult As New XmlDocument
    '    xmlResult.LoadXml(sbXMLResult.ToString())

    '    LogInformation("GetCountriesForSelfCheckOut: RS: xmlResult:=" & xmlResult.OuterXml)

    '    Return xmlResult


    'End Function

    '<WebMethod()> _
    'Public Function GetLocationsForSelfCheckOut(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As XmlDocument
    '    LogInformation("GetLocationsForSelfCheckOut: RQ: CountryCode:=" & CountryCode & _
    '                                                  ", CompanyCode:=" & CompanyCode & _
    '                                                  ", BrandCode:=" & BrandCode)

    '    Dim dtLocations As DataTable

    '    ' cache the list of locations if its not already cached
    '    If Application("SCO_LocationList") Is Nothing Then
    '        ' get the list
    '        LogInformation("Starting:WebServiceData.GetLocationsForSelfCheckOut")
    '        dtLocations = WebServiceData.GetLocationsForSelfCheckOut("", "", "")
    '        LogInformation("Done:WebServiceData.GetLocationsForSelfCheckOut")
    '        Application("SCO_LocationList") = dtLocations
    '    Else
    '        LogInformation("Using Cached Location List")
    '        dtLocations = CType(Application("SCO_LocationList"), DataTable)
    '    End If

    '    Dim dvFilteredView As New DataView(dtLocations)
    '    Dim sbFilter As New StringBuilder

    '    If GetProperStringValue(CountryCode) <> "" Then
    '        sbFilter.Append("CtyCode = '" & GetProperStringValue(CountryCode) & "' ")
    '    End If

    '    If GetProperStringValue(CompanyCode) <> "" Then
    '        If sbFilter.ToString().Length > 0 Then
    '            ' already something in here...
    '            sbFilter.Append("AND ")
    '        End If
    '        sbFilter.Append("ComCode = '" & CompanyCode & "' ")
    '    End If

    '    If GetProperStringValue(BrandCode) <> "" Then
    '        If sbFilter.ToString().Length > 0 Then
    '            ' already something in here...
    '            sbFilter.Append("AND ")
    '        End If
    '        sbFilter.Append("BrdCode = '" & BrandCode & "' ")
    '    End If

    '    dvFilteredView.RowFilter = sbFilter.ToString()

    '    Dim sbXMLResult As New StringBuilder

    '    sbXMLResult.Append("<Locations>")

    '    For Each rwLocation As DataRow In dvFilteredView.ToTable(True, New String() {"LocCode", "LocName"}).Rows
    '        sbXMLResult.Append("<Location LocCode='" & rwLocation("LocCode") & "' LocName='" & rwLocation("LocName") & "' />")
    '    Next

    '    sbXMLResult.Append("</Locations>")

    '    Dim xmlResult As New XmlDocument
    '    xmlResult.LoadXml(sbXMLResult.ToString())

    '    LogInformation("GetLocationsForSelfCheckOut: RS: xmlResult:=" & xmlResult.OuterXml)

    '    Return xmlResult

    'End Function

    <WebMethod(Description:="This method returns a country-location mapping. For use with Self Check out")> _
    Public Function GetCountryLocationData() As DataTable

        LogInformation("GetCountryLocationData: Start")

        Dim dtLocations As DataTable

        ' get the list
        LogInformation("Starting:WebServiceData.GetLocationsForSelfCheckOut")
        dtLocations = WebServiceData.GetLocationsForSelfCheckOut("", "", "")
        LogInformation("Done:WebServiceData.GetLocationsForSelfCheckOut")

        Return dtLocations

    End Function

    ''' <summary>
    ''' This method does the following tasks :-
    ''' 1. Adds Payment - if specified
    ''' 2. Checks out the rental - if specified
    ''' 3. Generates a Rental Confirmation - if specified
    ''' </summary>
    ''' <param name="InputData"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="This method adds payment to a rental. For use with Seld Check out")> _
    Public Function AddPayment(ByVal InputData As XmlDocument) As XmlDocument
        ' change for information security purposes
        Dim oAuroraWebService As AuroraWebService = New AuroraWebService
        oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                         ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                         ConfigurationSettings.AppSettings("DomainName"))
        Dim CreditCardNumber, EncryptedCreditCardNumber As String

        ' 7.7.10 - Shoel - Change for Quote bookings
        Dim bIsCreditCardInfoPresent As Boolean = Not String.IsNullOrEmpty(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))

        If bIsCreditCardInfoPresent Then
            ' 7.7.10 - Shoel - Change for Quote bookings

            CreditCardNumber = GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
            'Removed By Nimesh as we are not encrypting Credit Card Details on 23rd Jan 2015
            'Try
            '    Dim xmlEncryptedData As XmlNode
            '    LogInformation("oAuroraWebService.EncryptTextForAurora : Call start")
            '    xmlEncryptedData = oAuroraWebService.EncryptTextForAurora(GetInnerText(InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber")))
            '    LogInformation("oAuroraWebService.EncryptTextForAurora : Call end : Returned message : " & xmlEncryptedData.OuterXml)

            '    EncryptedCreditCardNumber = xmlEncryptedData.SelectSingleNode("EncryptedData").InnerText
            '    InputData.DocumentElement.SelectSingleNode("CreditCardDetails/CreditCardNumber").InnerText = EncryptedCreditCardNumber
            'Catch ex As Exception
            '    ' do nothing
            '    LogInformation("Call to Aurora text encryption function failed : " & ex.Message & vbNewLine & ex.StackTrace)
            'End Try
            'End Removed by Nimesh on 23rd jan 2015
            'Added by Nimesh on 23rd jan 2015
            EncryptedCreditCardNumber = CreditCardNumber
            'End added by nimesh on 23rd jan 2015
            ' change for information security purposes

            ' 7.7.10 - Shoel - Change for Quote bookings
        End If
        ' 7.7.10 - Shoel - Change for Quote bookings
        ' change for information security purposes

        LogInformation("AddPayment: RQ: InputData=""" & InputData.OuterXml & """")

        '<Data>
        '   <RentalDetails>
        '       <RentalId>W3414099-1</RentalId>
        '       <CountryCode>NZ</CountryCode>
        '       <UserCode>B2CNZ</UserCode>
        '   </RentalDetails>
        '	<CreditCardDetails>
        '		<CardType>VISA</CardType>
        '		<CreditCardNumber>1111-1111-1111-1111</CreditCardNumber>
        '		<CreditCardName>V ISA</CreditCardName>
        '		<ApprovalRef>123</ApprovalRef>
        '		<ExpiryDate>12/12</ExpiryDate>
        '       <PaymentType>R</PaymentType>
        '		<PaymentAmount>1000.00</PaymentAmount>
        '		<PaymentSurchargeAmount>2.00</PaymentSurchargeAmount>
        '       <CVV2></CVV2>
        '       <BillingToken></BillingToken>
        '	</CreditCardDetails>
        '	<DPSData>
        '		<DPSAuthCode>DPSAUTH</DPSAuthCode>
        '		<DPSTxnRef>DPSTXNREF</DPSTxnRef>
        '	</DPSData>
        '	<AddPayment>True</AddPayment>
        '	<DoCheckOut>True</DoCheckOut>
        '	<PrintRentalAgreement>True</PrintRentalAgreement>
        '</Data>

        Dim xmlResult As New XmlDocument, _
            RentalId As String, _
            RentalCountryCode As String, _
            UserCode As String, _
            CardType As String, _
            PaymentAmount As Decimal, _
            PaymentSurchargeAmount As Decimal, _
            PaymentType As Char, _
            CreditCardName As String, _
            ApprovalRef As String, _
            ExpiryDate As String, _
            DPSAuthCode As String, _
            DPSTxnRef As String, _
            ShouldDoCheckOut As Boolean = False, _
            PrintRentalAgreement As Boolean = False, _
            ShouldAddPayment As Boolean = False, _
            sbResultXMLString As New StringBuilder

        ''rev:mia April 11,2012 - Addition of CVC2 to capture in the service
        Dim cvc2 As String = Nothing

        ''rev:mia May 24,2012 - Addition of billingToken to capture in the service
        Dim billingtoken As String = Nothing
        Dim DoBillingTokenOnly As Boolean = False
        Try

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' Read info from input XML
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            LogInformation("Starting:Reading Input XML")
            With InputData.DocumentElement
                RentalId = GetInnerText(.SelectSingleNode("RentalDetails/RentalId"))
                If String.IsNullOrEmpty(RentalId) Then
                    Throw New Exception("Rental Id must be specified", New Exception(MESSAGE_FLAG))
                End If

                RentalCountryCode = GetInnerText(.SelectSingleNode("RentalDetails/CountryCode"))
                If String.IsNullOrEmpty(RentalId) Then
                    Throw New Exception("Rental country code must be specified")
                End If

                'UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
                ' change for Retrieve/modify quote
                ' use the passed in UserCode if any
                If String.IsNullOrEmpty(GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))) Then
                    UserCode = SELF_CHECK_OUT_USER_CODE
                Else
                    UserCode = GetInnerText(.SelectSingleNode("RentalDetails/UserCode"))
                End If
                ' change for Retrieve/modify quote
                ''Added by Nimesh on 27th Jan 2015 for slot selection
                'SelectedSlot = GetInnerText(.SelectSingleNode("RentalDetails/SelectedSlot"))
                'SlotId = GetInnerText(.SelectSingleNode("RentalDetails/SlotId"))
                ''End added by Nimesh
                If String.IsNullOrEmpty(RentalId) Then
                    Throw New Exception("RentalId must be specified", New Exception(MESSAGE_FLAG))
                End If

                CardType = GetInnerText(.SelectSingleNode("CreditCardDetails/CardType"))
                'CreditCardNumber = GetInnerText(.SelectSingleNode("CreditCardDetails/CreditCardNumber"))
                CreditCardName = GetInnerText(.SelectSingleNode("CreditCardDetails/CreditCardName"))
                ApprovalRef = GetInnerText(.SelectSingleNode("CreditCardDetails/ApprovalRef"))
                ExpiryDate = GetInnerText(.SelectSingleNode("CreditCardDetails/ExpiryDate"))


                ''rev:mia April 11,2012 - Addition of CVC2 to capture in the service
                Try
                    cvc2 = GetInnerText(.SelectSingleNode("CreditCardDetails/CVV2"))

                    ''rev:mia May 24,2012 - Addition of BillingToken to capture in the service
                    billingtoken = GetInnerText(.SelectSingleNode("CreditCardDetails/BillingToken"))
                Catch ex As Exception
                    ''ignore
                End Try


                Try
                    PaymentAmount = CDec(GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentAmount")))
                Catch ex As InvalidCastException
                End Try
                Try
                    PaymentSurchargeAmount = CDec(GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentSurchargeAmount")))
                Catch ex As InvalidCastException
                End Try

                PaymentType = GetInnerText(.SelectSingleNode("CreditCardDetails/PaymentType"))

                DPSAuthCode = GetInnerText(.SelectSingleNode("DPSData/DPSAuthCode"))
                DPSTxnRef = GetInnerText(.SelectSingleNode("DPSData/DPSTxnRef"))

                Try
                    ShouldDoCheckOut = CBool(GetInnerText(.SelectSingleNode("DoCheckOut")))
                Catch ex As InvalidCastException
                End Try
                Try
                    PrintRentalAgreement = CBool(GetInnerText(.SelectSingleNode("PrintRentalAgreement")))
                Catch ex As InvalidCastException
                End Try
                Try
                    ShouldAddPayment = CBool(GetInnerText(.SelectSingleNode("AddPayment")))
                Catch ex As InvalidCastException
                End Try

                If Not ShouldDoCheckOut AndAlso Not PrintRentalAgreement AndAlso Not ShouldAddPayment Then
                    ' nothing specified
                    Throw New Exception("No activity to perform", New Exception(MESSAGE_FLAG))
                End If

                Try
                    ''rev:mia may 29 2012 - MIGHTY CHANGE
                    DoBillingTokenOnly = IIf(GetInnerText(.SelectSingleNode("DoBillingTokenOnly")) = "True", True, False)

                Catch ex As Exception

                End Try

            End With
            LogInformation("Done:Reading Input XML")

            ''rev:mia may 29 2012 - MIGHTY CHANGE
            Try
                If (DoBillingTokenOnly And Not String.IsNullOrEmpty(billingtoken)) Then
                    Dim result As String = InsertTokenObject(RentalId, CardType, CreditCardName, CreditCardNumber, ExpiryDate, billingtoken, "SCI", "SCI")
                    LogInformation("WebServiceData.AddPayment. DoBillingTokenOnly And Not String.IsNullOrEmpty(billingtoken)=" & result)
                    xmlResult = New XmlDocument
                    If (String.IsNullOrEmpty(result)) Then
                        xmlResult.LoadXml("<Data><PaymentMessage>Payment Successful</PaymentMessage></Data>")
                    Else
                        xmlResult.LoadXml("<Data><PaymentMessage>Payment Not Successful</PaymentMessage></Data>")
                    End If

                    Return xmlResult
                End If

            Catch ex As Exception

            End Try

            If ShouldAddPayment Then
                LogInformation("Payment - Transaction Starting")
                Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                    Try
                        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        ' Add the payment
                        '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

                        Dim sReturnedWarning As String, _
                            sReturnedError As String, _
                            bIsSuccessful As Boolean = False
                        Dim xmlPaymentData As New XmlDocument

                        LogInformation("Starting:WebServiceData.AddPayment")
                        bIsSuccessful = WebServiceData.AddPayment(RentalId, _
                                                                  CardType, _
                                                                  PaymentAmount, _
                                                                  PaymentSurchargeAmount, _
                                                                  PaymentType, _
                                                                  CreditCardNumber, _
                                                                  CreditCardName, _
                                                                  ExpiryDate, _
                                                                  DPSTxnRef, _
                                                                  APP_NAME, _
                                                                  sReturnedWarning, _
                                                                  sReturnedError, _
                                                                  cvc2, _
                                                                  billingtoken) ''rev:mia May 24,2012 - Addition of BillingToken to capture in the service

                        LogInformation("Done:WebServiceData.AddPayment. bIsSuccessful=""" & bIsSuccessful.ToString() & """")

                        If Not bIsSuccessful Then
                            ' failed
                            Throw New Exception("Failed to add payment" & vbNewLine & _
                                                "Warning=" & sReturnedWarning & vbNewLine & _
                                                "Error=" & sReturnedError)
                        Else
                            ' check details

                            'Dim bShouldContinue As Boolean = False
                            'xmlPaymentData = GetPaymentDetails(RentalId, bShouldContinue)
                            xmlPaymentData = GetPaymentDetails(RentalId, ShouldDoCheckOut)

                            ' hack for changing status in continue cko bit
                            Try
                                If CInt(xmlPaymentData.DocumentElement.SelectSingleNode("Rental/ContinueCKO").Attributes("status").Value) = -1 Then
                                    sbResultXMLString.Append("<CheckOutMessage>CheckOut successful</CheckOutMessage>")
                                    LogInformation("Fake Cko Done")
                                    xmlPaymentData.DocumentElement.SelectSingleNode("Rental/ContinueCKO").Attributes("status").Value = Math.Abs(CInt(xmlPaymentData.DocumentElement.SelectSingleNode("Rental/ContinueCKO").Attributes("status").Value))
                                End If
                            Catch
                            End Try
                            ' hack for changing status in continue cko bit

                            PrintRentalAgreement = ShouldDoCheckOut

                            sbResultXMLString.Append("<PaymentData>" & xmlPaymentData.InnerXml & "</PaymentData>")

                            'If Not bShouldContinue Then
                            '    Throw New Exception("Failed to add payment")
                            'End If

                        End If

                        oTransaction.CommitTransaction()
                        LogInformation("Payment - Transaction Committed")

                        sbResultXMLString.Append("<PaymentMessage>Payment successful</PaymentMessage>")

                    Catch ex As Exception
                        sbResultXMLString.Append("<PaymentMessage>Payment Not Successful</PaymentMessage>")
                        oTransaction.RollbackTransaction()
                        xmlResult = GetErrorXML(ex)
                        xmlResult.LoadXml("<Error>" & xmlResult.DocumentElement.InnerXml & sbResultXMLString.ToString() & "</Error>")
                        Return xmlResult
                    End Try

                End Using
            End If

            ' Get more data with the RentalId
            Dim dtCheckOut As DateTime, dtCheckIn As DateTime, sCheckOutLoc As String, sCheckInLoc As String, sBookingId As String

            If ShouldDoCheckOut OrElse PrintRentalAgreement Then
                LogInformation("Starting:WebServiceData.GetDetailsFromRentalId")
                WebServiceData.GetDetailsFromRentalId(RentalId:=RentalId, _
                                                      CheckOutDateTime:=dtCheckOut, _
                                                      CheckInDateTime:=dtCheckIn, _
                                                      CheckOutLocationCode:=sCheckOutLoc, _
                                                      CheckInLocationCode:=sCheckInLoc, _
                                                      BookingId:=sBookingId)

                LogInformation("Done:WebServiceData.GetDetailsFromRentalId" & vbNewLine & _
                               "CheckOutDateTime:=" & dtCheckOut & vbNewLine & _
                               "CheckInDateTime:=" & dtCheckIn & vbNewLine & _
                               "CheckOutLocationCode:=" & sCheckOutLoc & vbNewLine & _
                               "CheckInLocationCode:= " & sCheckInLoc & vbNewLine & _
                               "BookingId:= " & sBookingId)
            End If

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' CheckOut
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
            If ShouldDoCheckOut Then
                LogInformation("CheckOut - Transaction Starting")
                Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
                    Try

                        'Dim oAuroraWebService As AuroraWebService = New AuroraWebService
                        'oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                        '                                                                 ConfigurationSettings.AppSettings("DomainPWD"), _
                        '                                                                 ConfigurationSettings.AppSettings("DomainName"))

                        ' Get Checkout data
                        Dim xmlCheckOutData As New XmlDocument, xmlReturnDoc As New XmlDocument

                        LogInformation("Starting:oAuroraWebService.GetCheckOutData")
                        xmlCheckOutData.InnerXml = "<Data>" & oAuroraWebService.GetCheckOutData(sBookingId, RentalId, UserCode, RentalCountryCode).InnerXml & "</Data>"

                        LogInformation("Done:oAuroraWebService.GetCheckOutData" & vbNewLine & _
                                       "xmlCheckOutData := " & xmlCheckOutData.OuterXml)

                        If xmlCheckOutData.SelectSingleNode("//Error/Message") IsNot Nothing Then
                            ' BAD XML!!!!
                            Throw New Exception(xmlCheckOutData.SelectSingleNode("//Error/Message").InnerText)
                        End If

                        If xmlCheckOutData.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlCheckOutData.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("DVASS ERROR") Then
                            Throw New Exception(xmlCheckOutData.DocumentElement.SelectSingleNode("Message").InnerText)
                        End If

                        Dim sUnitNumber As String, sRego As String, iOdometerOut As Integer, sErrorMessage As String

                        LogInformation("Starting:Read XML Data for CheckOut")
                        With xmlCheckOutData.DocumentElement
                            sUnitNumber = GetInnerText(.SelectSingleNode("UnitNo"))

                            LogInformation("Starting:WebServiceData.GetUnitDetails")
                            Try
                                WebServiceData.GetUnitDetails(sUnitNumber, RentalId, sRego, iOdometerOut)
                            Catch ex As Exception
                                Throw New Exception("Unable to retrieve details for Unit Number = " & sUnitNumber)
                            End Try

                            LogInformation("Done:WebServiceData.GetUnitDetailst" & vbNewLine & _
                                           "sUnitNumber := " & sUnitNumber & vbNewLine & _
                                           "sRego := " & sRego & vbNewLine & _
                                           "iOdometerOut := " & iOdometerOut.ToString())

                            .SelectSingleNode("Rego").InnerText = sRego
                            .SelectSingleNode("OddometerOut").InnerText = iOdometerOut.ToString()
                            .SelectSingleNode("RntCkoTime").InnerText = Now.ToString(TIME_FORMAT)
                            .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                            .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                            .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText
                            .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText

                            .InnerXml = .InnerXml & _
                                        "<Forced>0</Forced>" & _
                                        "<UserCode>" & UserCode & "</UserCode>" & _
                                        "<ProgramName>" & PROGRAM_NAME & "</ProgramName>"
                        End With
                        LogInformation("Done:Read XML Data for CheckOut" & vbNewLine & _
                                       "xmlCheckOutData := " & xmlCheckOutData.OuterXml)

                        ' Do checkout
                        LogInformation("Starting:DoCheckOut")
                        xmlReturnDoc = DoCheckOut(xmlCheckOutData, oAuroraWebService)
                        LogInformation("Done:DoCheckOut. xmlReturnDoc=" & xmlReturnDoc.OuterXml)

                        If Not xmlReturnDoc.SelectSingleNode("//Error/Message") Is Nothing Then
                            ' some error is present
                            Throw New Exception(GetInnerText(xmlReturnDoc.SelectSingleNode("//Error/Message")))
                        End If

                        oTransaction.CommitTransaction()
                        LogInformation("CheckOut - Transaction Committed")

                        If Not xmlReturnDoc.SelectSingleNode("//Data/Note") Is Nothing Then
                            ' some note
                            sbResultXMLString.Append(xmlReturnDoc.SelectSingleNode("//Data/Note").OuterXml)
                        End If

                        sbResultXMLString.Append("<CheckOutMessage>CheckOut successful</CheckOutMessage>")

                    Catch ex As Exception
                        oTransaction.RollbackTransaction()
                        sbResultXMLString.Append("<CheckOutMessage>CheckOut Not successful</CheckOutMessage>")
                        xmlResult = GetErrorXML(ex)
                        xmlResult.LoadXml("<Error>" & xmlResult.DocumentElement.InnerXml & sbResultXMLString.ToString() & "</Error>")
                        Return xmlResult
                    End Try
                End Using
            End If

            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            ' PrintRentalAgreement
            '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            If PrintRentalAgreement Then
                Dim xmlRentalAgreementData As New XmlDocument

                LogInformation("RentalAgreement - Transaction Starting")
                Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                    Try
                        LogInformation("Starting:Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement")
                        xmlRentalAgreementData = Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement(RentalId)
                        LogInformation("Starting:Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement. xmlRentalAgreement=" & xmlRentalAgreementData.OuterXml)

                        xmlRentalAgreementData.LoadXml("<Data>" & xmlRentalAgreementData.DocumentElement.InnerXml & "</Data>")

                        Dim oTransform As Xsl.XslTransform = New Xsl.XslTransform()
                        oTransform.Load(Server.MapPath(RENTALAGREEMENT_XSL_PATH))

                        Using oWriter As System.IO.StringWriter = New System.IO.StringWriter
                            oTransform.Transform(xmlRentalAgreementData, Nothing, oWriter, Nothing)

                            sbResultXMLString.Append("<RentalAgreementMessage>Rental Agreement Generated Successfully</RentalAgreementMessage>")
                            sbResultXMLString.Append("<RentalAgreementHTMLData>" & XmlEncode(oWriter.ToString()) & "</RentalAgreementHTMLData>")

                            LogInformation("Rental Agreement HTML = " & oWriter.ToString())
                        End Using

                        Aurora.Booking.Services.BookingCheckInCheckOut.MaintainContractHistory(sBookingId, RentalId, UserCode, PROGRAM_NAME)

                        oTransaction.CommitTransaction()
                        LogInformation("RentalAgreement - Transaction Committed")

                    Catch ex As Exception
                        oTransaction.RollbackTransaction()
                        sbResultXMLString.Append("<RentalAgreementMessage>Rental Agreement Not Generated Successfully</RentalAgreementMessage>")
                        xmlResult = GetErrorXML(ex)
                        xmlResult.LoadXml("<Error>" & xmlResult.DocumentElement.InnerXml & sbResultXMLString.ToString() & "</Error>")
                        Return xmlResult
                    End Try
                End Using
            End If

            ' All done!!!!
            sbResultXMLString = sbResultXMLString.Insert(0, "<Data>")
            sbResultXMLString.Append("</Data>")
            xmlResult.LoadXml(sbResultXMLString.ToString())
        Catch ex As Exception
            xmlResult = GetErrorXML(ex)
        End Try
        ''LogInformation("AddRemoveProducts: RS: OutputData=""" & xmlResult.OuterXml)

        Return xmlResult

    End Function

    <WebMethod(Description:="This method retrieves payment info. For use with Self Check out")> _
    Public Function GetPaymentData(ByVal RentalId As String) As XmlDocument
        LogInformation("Start:GetPaymentData RQ : RentalId = """ & RentalId & """")

        Dim xmlPaymentData As New XmlDocument
        xmlPaymentData = GetPaymentDetails(RentalId, True)

        LogInformation("Done:GetPaymentData RS : xmlPaymentData = """ & xmlPaymentData.OuterXml & """")

        Return xmlPaymentData
    End Function




    ''' <summary>
    ''' This method does the actual Check Out and AIMS activities
    ''' </summary>
    ''' <param name="InputData">XML data containing data for the Check Out process</param>
    ''' <param name="oAuroraWebService">Reference to an instance of the Aurora Web Service to process aims and dvass work</param>
    ''' <returns>XML with details on success or failure</returns>
    ''' <remarks></remarks>
    Public Function DoCheckOut(ByVal InputData As XmlDocument, ByRef oAuroraWebService As AuroraWebService) As XmlDocument

        LogInformation("DoCheckOut: RQ: InputData=""" & InputData.OuterXml)

        Dim iRntIntegrityNo As Integer, _
            iBooIntegrityNo As Integer, _
            sRentalId As String, _
            sBookingId As String, _
            sUserCode As String, _
            sProgramName As String, _
            bCrossHire As Boolean, _
            sActivityType As String = "Rental", _
            nExpectedEndOdometer As Integer = -1, _
            sOtherAssets As String = "", _
            sExternalRef As String, _
            sUnit As String, _
            sStartLocation As String, _
            dtStart As DateTime, _
            nStartOdometer As Integer, _
            sExpectedEndLocation As String, _
            dtExpectedEnd As DateTime, _
            sCountryCode As String, _
            bDVASSCall As Boolean, _
            bForced As Boolean, _
            xmlReturn As New XmlDocument

        Try
            LogInformation("Starting:Read Data from InputXML")
            With InputData.DocumentElement

                sBookingId = .SelectSingleNode("BooId").InnerText
                sRentalId = .SelectSingleNode("RntId").InnerText

                iRntIntegrityNo = CInt(.SelectSingleNode("RntIntNo").InnerText)
                iBooIntegrityNo = CInt(.SelectSingleNode("BooIntNo").InnerText)

                bCrossHire = CBool(.SelectSingleNode("CrossHr").InnerText)

                'sUserCode = .SelectSingleNode("UserCode").InnerText
                sUserCode = SELF_CHECK_OUT_USER_CODE
                sProgramName = .SelectSingleNode("ProgramName").InnerText

                sExternalRef = .SelectSingleNode("BooRntNum").InnerText
                sUnit = .SelectSingleNode("UnitNo").InnerText
                sStartLocation = UCase(Trim(.SelectSingleNode("UserLocCode").InnerText))
                dtStart = CDate(.SelectSingleNode("RntCkoWhen").InnerText & " " & .SelectSingleNode("RntCkoTime").InnerText)
                nStartOdometer = CInt(.SelectSingleNode("OddometerOut").InnerText)
                sExpectedEndLocation = UCase(.SelectSingleNode("RntCkiLocCode").InnerText)
                dtExpectedEnd = CDate(.SelectSingleNode("RntCkiWhen").InnerText & " " & .SelectSingleNode("RntCkiTime").InnerText)
                sCountryCode = UCase(.SelectSingleNode("CtyCode").InnerText)

                bDVASSCall = CBool(.SelectSingleNode("DVASSCall").InnerText)
                bForced = CBool(.SelectSingleNode("Forced").InnerText)
            End With
            LogInformation("Done:Read Data from InputXML")

            ' integrity Check!! :)
            Dim bPassedIntegrityCheck As Boolean = False

            LogInformation("Starting:Aurora.Booking.Services.Booking.CheckIntegrity")
            bPassedIntegrityCheck = Aurora.Booking.Services.Booking.CheckIntegrity(sBookingId, iRntIntegrityNo, iBooIntegrityNo, sRentalId)
            LogInformation("Done:Aurora.Booking.Services.Booking.CheckIntegrity" & vbNewLine & _
                           "Result = " & bPassedIntegrityCheck.ToString())

            If Not bPassedIntegrityCheck Then
                Throw New Exception("ERROR/This rental has been modified by another user. Please refresh the screen.")
            End If
            ' integrity Check passed - continue

            ' VALIDATIONS CHECK
            Dim oXml As XmlDocument

            LogInformation("Starting:Aurora.Common.Data.GetPopUpData")
            oXml = Aurora.Common.Data.GetPopUpData(Aurora.Popups.Data.PopupType.VALIDATEBOOKING, sBookingId, sRentalId, sUserCode, "", "", sUserCode)
            LogInformation("Done:Aurora.Common.Data.GetPopUpData" & vbNewLine & _
                           "oXml = " & oXml.OuterXml)

            If Not oXml.DocumentElement.SelectSingleNode("Error") Is Nothing Then
                If Not oXml.DocumentElement.SelectSingleNode("Error").InnerText.Trim.Equals(String.Empty) Then
                    ' some errors
                    Throw New Exception("VALIDATIONERRORS/" & oXml.DocumentElement.SelectSingleNode("Error").InnerText)
                End If
            ElseIf Not oXml.DocumentElement.SelectSingleNode("Wrn") Is Nothing Then
                If Not oXml.DocumentElement.SelectSingleNode("Wrn").InnerText.Trim.Equals(String.Empty) Then
                    ' some warnings
                    Throw New Exception("VALIDATIONWARNINGS/" & oXml.DocumentElement.SelectSingleNode("Wrn").InnerText)
                End If
            End If


            ' VALIDATIONS CHCK done :)

            ' db call
            LogInformation("Starting:Aurora.Booking.Data.DataRepository.ManageCheckOut")
            oXml = Aurora.Booking.Data.DataRepository.ManageCheckOut(InputData, sUserCode, sProgramName, 0)
            LogInformation("Done:Aurora.Booking.Data.DataRepository.ManageCheckOut" & vbNewLine & _
                           "oXml = " & oXml.OuterXml)


            If oXml.DocumentElement.InnerText <> "SUCCESS" And Not oXml.DocumentElement.InnerText.Trim().Equals(String.Empty) Then
                Throw New Exception(oXml.DocumentElement.InnerText)
            End If

            If Not bCrossHire Then

                Dim xmlAIMS As XmlNode, xmlAIMSDoc As New XmlDocument

                LogInformation("Starting:oAuroraWebService.DoAIMSF6ActivityCreate")
                xmlAIMS = oAuroraWebService.DoAIMSF6ActivityCreate(sActivityType, sExternalRef, sUnit, _
                                                         sStartLocation, dtStart, nStartOdometer, _
                                                         sExpectedEndLocation, dtExpectedEnd, _
                                                         nExpectedEndOdometer, sOtherAssets, sUserCode, _
                                                         bDVASSCall, bForced)
                LogInformation("Done:oAuroraWebService.DoAIMSF6ActivityCreate" & vbNewLine & _
                               "xmlAIMS = " & xmlAIMS.OuterXml)

                xmlAIMSDoc.LoadXml("<Data>" & xmlAIMS.InnerXml & "</Data>")

                If xmlAIMSDoc.DocumentElement.SelectSingleNode("AIMSMessage") Is Nothing _
                   OrElse _
                   xmlAIMSDoc.DocumentElement.SelectSingleNode("AIMSMessage").InnerText <> "SUCCESS" _
                   OrElse _
                   xmlAIMSDoc.DocumentElement.SelectSingleNode("Error") IsNot Nothing _
                Then
                    Throw New Exception("AIMSERROR/" & xmlAIMSDoc.DocumentElement.SelectSingleNode("Error").InnerText)
                End If

                'cross hire = false
            Else
                'cross hire = true
                If bDVASSCall Then
                    'dvasscall = 1

                    'RENTAL CANCEL
                    Dim xmlDvass As XmlNode, xmlDvassDoc As New XmlDocument

                    LogInformation("Starting:oAuroraWebService.DVASSRentalCancel")
                    xmlDvass = oAuroraWebService.DVASSRentalCancel(sRentalId, 1, 0, sCountryCode, sUserCode)
                    LogInformation("Done:oAuroraWebService.DVASSRentalCancel" & vbNewLine & _
                                   "xmlDvass = " & xmlDvass.OuterXml)

                    xmlDvassDoc.LoadXml("<Data>" & xmlDvass.InnerXml & "</Data>")


                    If xmlDvassDoc.DocumentElement.SelectSingleNode("DVASSStatus") Is Nothing _
                       OrElse _
                       xmlDvassDoc.DocumentElement.SelectSingleNode("DVASSStatus").InnerText <> "SUCCESS" _
                       OrElse _
                       xmlDvassDoc.DocumentElement.SelectSingleNode("Error") IsNot Nothing _
                    Then
                        Throw New Exception("DVASSERROR/" & xmlDvassDoc.DocumentElement.SelectSingleNode("Error").InnerText)
                    End If

                    'dvasscall = 1
                End If

                'cross hire = true
            End If
            'cross hire

            'update rental integrity
            LogInformation("Starting:Aurora.Booking.Data.DataRepository.UpdateRentalIntegrityNumber")
            Aurora.Booking.Data.DataRepository.UpdateRentalIntegrityNumber(sRentalId)
            LogInformation("Done:Aurora.Booking.Data.DataRepository.UpdateRentalIntegrityNumber")


            ' Check for GPS Serial thingie
            Dim xmlSerialProducts As XmlDocument

            LogInformation("Starting:Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber")
            xmlSerialProducts = Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber(sRentalId, False)
            LogInformation("Done:Aurora.Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber")

            If xmlSerialProducts.DocumentElement.InnerXml.Trim() <> "<Products></Products>" Then
                ' serial number(s) needed
                xmlReturn.LoadXml("<Data><Message>SUCCESS</Message><Note>Serial Number needs to be entered</Note></Data>")
            Else
                ' nothing needed
                xmlReturn.LoadXml("<Data><Message>SUCCESS</Message></Data>")
            End If


        Catch ex As Exception
            xmlReturn = GetErrorXML(ex)
        End Try

        LogInformation("DoCheckOut: RS: xmlReturn=""" & xmlReturn.OuterXml)

        Return xmlReturn

    End Function

    ''' <summary>
    ''' This method does the work of adding/updating customer information
    ''' </summary>
    ''' <param name="InputData">An xml node with all the customer info</param>
    ''' <param name="RentalId">Rental Id</param>
    ''' <param name="UserCode">User Code</param>
    ''' <param name="ErrorMessage">Error Message</param>
    ''' <returns>True if successful, False if not</returns>
    ''' <remarks></remarks>
    Public Function AddUpdateCustomerInfo(ByVal InputData As XmlNode, _
                                          ByVal RentalId As String, _
                                          ByVal UserCode As String, _
                                          ByVal CheckInDate As DateTime, _
                                          ByVal BookingId As String, _
                                          ByVal RentalNo As String, _
                                          ByRef ErrorMessage As String) As Boolean

        LogInformation("AddUpdateCustomerInfo: RQ: InputData=""" & InputData.OuterXml)

        ' override it
        UserCode = SELF_CHECK_OUT_USER_CODE

        Dim sCustomerId As String = "" _
        , bIsHirer As Boolean = False _
        , sAction As String = "" _
        , sErrorMessage As String = "" _
        , sLicenseNumber As String = "" _
        , sLicenseExpiryDate As String = "" _
        , sLicenseIssuingAuthority As String = "" _
        , sDateOfBirth As String = "" _
        , datDateOfBirth As Date = DateTime.MinValue _
        , datLicenseExpiryDate As Date = DateTime.MinValue _
 _
        , sDayPhoneIntegrityNo As String = "" _
        , sNightPhoneIntegrityNo As String = "" _
        , sMobilePhoneIntegrityNo As String = "" _
        , sFaxIntegrityNo As String = "" _
        , sEmailIntegrityNo As String = "" _
 _
        , sDayPhoneNumber As String = "" _
        , sNightPhoneNumber As String = "" _
        , sMobilePhoneNumber As String = "" _
        , sFaxNumber As String = "" _
        , sEmailAddress As String = "" _
 _
        , sDayPhoneAreaCode As String = "" _
        , sNightPhoneAreaCode As String = "" _
        , sMobilePhoneAreaCode As String = "" _
        , sFaxAreaCode As String = "" _
 _
        , sDayPhoneCountryCode As String = "" _
        , sNightPhoneCountryCode As String = "" _
        , sMobilePhoneCountryCode As String = "" _
        , sFaxCountryCode As String = "" _
 _
        , sDayPhoneId As String = "" _
        , sNightPhoneId As String = "" _
        , sMobilePhoneId As String = "" _
        , sFaxId As String = "" _
        , sEmailId As String = "" _
 _
        , sDayPhoneCodId As String = "" _
        , sNightPhoneCodId As String = "" _
        , sMobilePhoneCodId As String = "" _
        , sFaxCodId As String = "" _
        , sEmailCodId As String = "" _
 _
        , sPhysicalAddressStreet As String = "" _
        , sPostalAddressStreet As String = "" _
 _
        , sPhysicalAddressSuburb As String = "" _
        , sPostalAddressSuburb As String = "" _
 _
        , sPhysicalAddressCityTown As String = "" _
        , sPostalAddressCityTown As String = "" _
 _
        , sPhysicalAddressState As String = "" _
        , sPostalAddressState As String = "" _
 _
        , sPhysicalAddressPostCode As String = "" _
        , sPostalAddressPostCode As String = "" _
 _
        , sPhysicalAddressCountry As String = "" _
        , sPostalAddressCountry As String = "" _
 _
        , sPhysicalAddressId As String = "" _
        , sPostalAddressId As String = "" _
 _
        , sPhysicalAddressCodeId As String = "" _
        , sPostalAddressCodeId As String = "" _
 _
        , sPhysicalAddressIntegrityNo As String = "" _
        , sPostalAddressIntegrityNo As String = "" _
 _
        , sIntegrityNo As String = "" _
 _
        , sCustomerFirstName As String = "" _
        , sCustomerLastName As String = "" _
        , sCustomerTitle As String = "" _
 _
        , bSurveyOptIn As Boolean = False _
        , bMailingListOptIn As Boolean = False _
        , bVIP As Boolean = False _
        , bFleetDriver As Boolean = False _
 _
        , sPersonTypeId As String = "" _
        , sOccupation As String = "" _
        , sGender As String = "" _
        , sPreferredModeOfCommunication As String = "" _
 _
        , sPassportNumber As String = "" _
        , sPassportIssuingCountry As String = "" _
 _
        , bRemoveCustomer As Boolean = False _
 _
        , sLoyaltyCardNumber As String = ""

       

        LogInformation("Starting:Read Data from Input XML")
        ' Read data from Input XML
        With InputData
            ' common for all customers
            sCustomerId = GetInnerText(.SelectSingleNode("CustomerDetails/CusId"))

            sCustomerFirstName = GetInnerText(.SelectSingleNode("CustomerDetails/FirstName"))
            sCustomerLastName = GetInnerText(.SelectSingleNode("CustomerDetails/LastName"))
            sCustomerTitle = GetInnerText(.SelectSingleNode("CustomerDetails/Title"))

            sLicenseNumber = GetInnerText(.SelectSingleNode("CustomerDetails/LicenceNum"))
            sLicenseExpiryDate = GetInnerText(.SelectSingleNode("CustomerDetails/ExpiryDate"))
            sLicenseIssuingAuthority = GetInnerText(.SelectSingleNode("CustomerDetails/IssingAuthority"))

            sDateOfBirth = GetInnerText(.SelectSingleNode("CustomerDetails/DOB"))
            ' common for all customers

            ' only for primary hirer
            sDayPhoneCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayCtyCode"))
            sDayPhoneAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayAreaCode"))
            sDayPhoneNumber = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayNumber"))

            sMobilePhoneCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/MobileCtyCode"))
            sMobilePhoneAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/MobileAreaCode"))
            sMobilePhoneNumber = GetInnerText(.SelectSingleNode("CustomerDetails/MobileNumber"))

            sEmailAddress = GetInnerText(.SelectSingleNode("CustomerDetails/Email"))

            sPhysicalAddressStreet = GetInnerText(.SelectSingleNode("PhysicalAddress/Street"))
            sPhysicalAddressCityTown = GetInnerText(.SelectSingleNode("PhysicalAddress/CityTown"))
            sPhysicalAddressCountry = GetInnerText(.SelectSingleNode("PhysicalAddress/Country"))
            '** Add post code for self-checkIn
            '** RKS: 23-Mar-2011
            '** START
            If Not .SelectSingleNode("PhysicalAddress/Postcode") Is Nothing Then
                sPhysicalAddressPostCode = GetInnerText(.SelectSingleNode("PhysicalAddress/Postcode"))
            Else
                sPhysicalAddressPostCode = "NA"
            End If
            '** END
            sLoyaltyCardNumber = GetInnerText(.SelectSingleNode("CustomerDetails/LoyaltyCardNumber"))
            ' only for primary hirer

            If GetInnerText(.SelectSingleNode("CustomerDetails/Remove")) <> "" Then
                bRemoveCustomer = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/Remove")))
            End If

            If GetInnerText(.SelectSingleNode("CustomerDetails/MailLists")) <> "" Then
                bMailingListOptIn = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/MailLists")))
            End If

        End With

        ''rev:mia 26MAY2015 - UPDATE VERIFIED DRIVERS
        Try
            Dim IsDriverVerified As String = GetInnerText(InputData.SelectSingleNode("CustomerDetails/IsDriverVerified"))
            Dim IsDriverVerifiedBy As String = GetInnerText(InputData.SelectSingleNode("CustomerDetails/IsDriverVerifiedBy"))
            Dim IsDriverVerifiedDate As String = GetInnerText(InputData.SelectSingleNode("CustomerDetails/IsDriverVerifiedDate"))

            If (Not String.IsNullOrEmpty(IsDriverVerified) And Not String.IsNullOrEmpty(IsDriverVerifiedBy) And Not String.IsNullOrEmpty(IsDriverVerifiedDate)) Then
                VerifyDriver(RentalId, sCustomerId, Convert.ToBoolean(IsDriverVerified), "AddUpdateCustomerInfo", UserCode)
            End If

        Catch ex As Exception
            LogInformation("ERROR: VerifyDriver inside AddUpdateCustomerInfo " & vbCrLf & ex.Message & " " & ex.StackTrace)
        End Try

        LogInformation("Done:Read Data from Input XML")

        If (bRemoveCustomer) Then
            LogInformation("Starting:Delete Customer data")

            sErrorMessage = ""

            sErrorMessage = Aurora.Booking.Services.BookingCustomer.ValidateDeletion(sCustomerId, RentalId)
            If Not String.IsNullOrEmpty(sErrorMessage) Then
                ErrorMessage = sErrorMessage
                Return False
            End If

            ' delete the customer and associated tables
            ' validate the if any rental associates with the customer
            sErrorMessage = ""
            sErrorMessage = Aurora.Booking.Services.BookingCustomer.DeleteCustomer(sCustomerId)
            If Not String.IsNullOrEmpty(sErrorMessage) Then
                ErrorMessage = sErrorMessage
                Return False
            End If

            LogInformation("Done:Delete Customer data")

            LogInformation("AddUpdateCustomerInfo: RS: True")

            Return True

        End If

        Dim xmlDatabaseFormat As New XmlDocument
        LogInformation("Starting:Aurora.Common.Data.ExecuteSqlXmlSPDoc(""WEBA_GetCustomerDetails"", '" & sCustomerId & "')")
        xmlDatabaseFormat = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBA_GetCustomerDetails", sCustomerId, RentalId)
        LogInformation("Done:Aurora.Common.Data.ExecuteSqlXmlSPDoc(""WEBA_GetCustomerDetails"", '" & sCustomerId & "'). xmlDatabaseFormat = " & xmlDatabaseFormat.OuterXml)

        ' Read info from database XML
        LogInformation("Starting:Read Data From database XML")
        With xmlDatabaseFormat.DocumentElement.SelectSingleNode("Customer")
            'sCustomerId = GetInnerText(.SelectSingleNode("CustomerDetails/CusId"))

            bIsHirer = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/Hirer")))

            'sLicenseNumber = GetInnerText(.SelectSingleNode("CustomerDetails/LicenceNum"))
            'sLicenseExpiryDate = GetInnerText(.SelectSingleNode("CustomerDetails/ExpiryDate"))
            'sLicenseIssuingAuthority = GetInnerText(.SelectSingleNode("CustomerDetails/IssingAuthority"))

            'sDateOfBirth = GetInnerText(.SelectSingleNode("CustomerDetails/DOB"))

            'sCustomerFirstName = GetInnerText(.SelectSingleNode("CustomerDetails/FirstName"))
            'sCustomerLastName = GetInnerText(.SelectSingleNode("CustomerDetails/LastName"))
            'sCustomerTitle = GetInnerText(.SelectSingleNode("CustomerDetails/Title"))

            'sDayPhoneCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayCtyCode"))
            sNightPhoneCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightCtyCode"))
            'sMobilePhoneCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/MobileCtyCode"))
            sFaxCountryCode = GetInnerText(.SelectSingleNode("CustomerDetails/FaxCtyCode"))

            'sDayPhoneAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayAreaCode"))
            sNightPhoneAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightAreaCode"))
            'sMobilePhoneAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/MobileAreaCode"))
            sFaxAreaCode = GetInnerText(.SelectSingleNode("CustomerDetails/FaxAreaCode"))

            'sDayPhoneNumber = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayNumber"))
            sNightPhoneNumber = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightNumber"))
            'sMobilePhoneNumber = GetInnerText(.SelectSingleNode("CustomerDetails/MobileNumber"))
            sFaxNumber = GetInnerText(.SelectSingleNode("CustomerDetails/FaxNumber"))
            'sEmailAddress = GetInnerText(.SelectSingleNode("CustomerDetails/Email"))

            sDayPhoneId = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayId"))
            sNightPhoneId = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightId"))
            sMobilePhoneId = GetInnerText(.SelectSingleNode("CustomerDetails/MobileId"))
            sFaxId = GetInnerText(.SelectSingleNode("CustomerDetails/FaxId"))
            sEmailId = GetInnerText(.SelectSingleNode("CustomerDetails/EmailId"))

            sDayPhoneCodId = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayCodId"))
            sNightPhoneCodId = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightCodId"))
            sMobilePhoneCodId = GetInnerText(.SelectSingleNode("CustomerDetails/MobileCodId"))
            sFaxCodId = GetInnerText(.SelectSingleNode("CustomerDetails/FaxCodId"))
            sEmailCodId = GetInnerText(.SelectSingleNode("CustomerDetails/EmailCodId"))

            sDayPhoneIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/PhDayIntNum"))
            sNightPhoneIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/PhNightIntNum"))
            sMobilePhoneIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/MobileIntNum"))
            sEmailIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/EmailIntNum"))
            sFaxIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/FaxIntNum"))

            sIntegrityNo = GetInnerText(.SelectSingleNode("CustomerDetails/IntegrityNo"))

            'sPhysicalAddressStreet = GetInnerText(.SelectSingleNode("PhysicalAddress/Street"))
            sPostalAddressStreet = GetInnerText(.SelectSingleNode("PostalAddress/Street"))

            sPhysicalAddressSuburb = GetInnerText(.SelectSingleNode("PhysicalAddress/Suburb"))
            sPostalAddressSuburb = GetInnerText(.SelectSingleNode("PostalAddress/Suburb"))

            'sPhysicalAddressCityTown = GetInnerText(.SelectSingleNode("PhysicalAddress/CityTown"))
            sPostalAddressCityTown = GetInnerText(.SelectSingleNode("PostalAddress/CityTown"))

            sPhysicalAddressState = GetInnerText(.SelectSingleNode("PhysicalAddress/State"))
            sPostalAddressState = GetInnerText(.SelectSingleNode("PostalAddress/State"))

            '** Add post code for self-checkIn
            '** RKS: 23-Mar-2011
            '** START
            '** if post code is not passed-in use save post code
            If sPhysicalAddressPostCode = "NA" Then
                sPhysicalAddressPostCode = GetInnerText(.SelectSingleNode("PhysicalAddress/Postcode"))
            End If
            '** END

            sPostalAddressPostCode = GetInnerText(.SelectSingleNode("PostalAddress/Postcode"))

            'sPhysicalAddressCountry = GetInnerText(.SelectSingleNode("PhysicalAddress/Country"))
            sPostalAddressCountry = GetInnerText(.SelectSingleNode("PostalAddress/Country"))

            sPhysicalAddressId = GetInnerText(.SelectSingleNode("PhysicalAddress/AddId"))
            sPostalAddressId = GetInnerText(.SelectSingleNode("PostalAddress/AddId"))

            sPhysicalAddressCodeId = GetInnerText(.SelectSingleNode("PhysicalAddress/CodeId"))
            sPostalAddressCodeId = GetInnerText(.SelectSingleNode("PostalAddress/CodeId"))

            sPhysicalAddressIntegrityNo = GetInnerText(.SelectSingleNode("PhysicalAddress/IntegrityNo"))
            sPostalAddressIntegrityNo = GetInnerText(.SelectSingleNode("PostalAddress/IntegrityNo"))

            bSurveyOptIn = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/SurVeys")))
            '            bMailingListOptIn = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/MailLists")))
            bVIP = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/Vip")))
            bFleetDriver = CBool(GetInnerText(.SelectSingleNode("CustomerDetails/FleetDriver")))

            sPersonTypeId = GetInnerText(.SelectSingleNode("CustomerDetails/PersonType"))
            sOccupation = GetInnerText(.SelectSingleNode("CustomerDetails/Occupation"))
            sGender = GetInnerText(.SelectSingleNode("CustomerDetails/Gender"))
            sPreferredModeOfCommunication = GetInnerText(.SelectSingleNode("CustomerDetails/PrefComm"))

            sPassportNumber = GetInnerText(.SelectSingleNode("CustomerDetails/PassportNo"))
            sPassportIssuingCountry = GetInnerText(.SelectSingleNode("CustomerDetails/PassportCountry"))

        End With
        LogInformation("Done:Read Data From database XML")

        LogInformation("Starting:Running validations on data")
        If Not String.IsNullOrEmpty(sLicenseNumber) Then
            If String.IsNullOrEmpty(sLicenseExpiryDate) Or String.IsNullOrEmpty(sLicenseIssuingAuthority) Or String.IsNullOrEmpty(sDateOfBirth) Then
                ErrorMessage = "You must enter Expiry Date, Issuing Authority and Date of Birth for licence details"
                Return False
            End If
        End If

        If String.IsNullOrEmpty(sDayPhoneIntegrityNo) Then
            sDayPhoneIntegrityNo = "0"
        End If
        If String.IsNullOrEmpty(sNightPhoneIntegrityNo) Then
            sNightPhoneIntegrityNo = "0"
        End If
        If String.IsNullOrEmpty(sMobilePhoneIntegrityNo) Then
            sMobilePhoneIntegrityNo = "0"
        End If
        If String.IsNullOrEmpty(sFaxIntegrityNo) Then
            sFaxIntegrityNo = "0"
        End If
        If String.IsNullOrEmpty(sEmailIntegrityNo) Then
            sEmailIntegrityNo = "0"
        End If
        If String.IsNullOrEmpty(sIntegrityNo) Then
            sIntegrityNo = "0"
        End If

        If String.IsNullOrEmpty(sDateOfBirth) Then
            datDateOfBirth = DateTime.MinValue
        Else
            datDateOfBirth = Aurora.Common.Utility.DateDBParse(sDateOfBirth)
        End If

        If String.IsNullOrEmpty(sLicenseExpiryDate) Then
            datLicenseExpiryDate = DateTime.MinValue
        Else
            If (sLicenseExpiryDate = "dd/mm/yyyy") Then
                ' this license doesnt have an expiry date!
                datLicenseExpiryDate = CheckInDate.AddMonths(2)
                ' add a note in aurora to this effect
                Dim sRetMsg As String = ""
                Dim sNoteMessage As String = LICENSE_WITH_NO_EXPIRY_DATE_NOTE_TEXT
                If sNoteMessage.Contains(TOKEN_LICENSE_NUMBER) Then
                    sNoteMessage = sNoteMessage.Replace(TOKEN_LICENSE_NUMBER, sLicenseNumber)
                End If
                If sNoteMessage.Contains(TOKEN_EXPIRY_DATE) Then
                    sNoteMessage = sNoteMessage.Replace(TOKEN_EXPIRY_DATE, datLicenseExpiryDate.ToString(DATE_FORMAT))
                End If

                sRetMsg = Aurora.Booking.Services.BookingNotes.ManageNote(String.Empty, _
                                                                          BookingId, _
                                                                          RentalNo, _
                                                                          RENTAL_CODE_ID, _
                                                                          sNoteMessage, _
                                                                          NOTE_AUDIENCE_INTERNAL, _
                                                                          3, _
                                                                          True, _
                                                                          1, _
                                                                          UserCode, _
                                                                          PROGRAM_NAME)
                ' note added
            Else
                datLicenseExpiryDate = Aurora.Common.Utility.DateDBParse(sLicenseExpiryDate)
            End If
        End If

        'Check for Mandatory Fields
        ''rev:mia 12may2015 - Allow Phoenix Ws call to allow first name as non mandatory
        'If String.IsNullOrEmpty(sCustomerLastName) Or String.IsNullOrEmpty(sCustomerFirstName) Then
        '    ErrorMessage = "Customer First Name and Last Name must be entered"
        '    Return False
        'End If

        If bIsHirer Then
            If String.IsNullOrEmpty(sPhysicalAddressCityTown) Or String.IsNullOrEmpty(sPhysicalAddressCountry) Then
                ErrorMessage = "Country and City/Town must be entered for Primary Hirer"
                Return False
            End If
        End If

        'validate the driver if the driver licence field is not empty
        If Not String.IsNullOrEmpty(sLicenseNumber) Then
            If String.IsNullOrEmpty(sDateOfBirth) Or String.IsNullOrEmpty(sLicenseIssuingAuthority) Or String.IsNullOrEmpty(sLicenseExpiryDate) Then
                ErrorMessage = "Expiry Date, Issuing Authority and Date of Birth must be entered for licence details"
                Return False
            End If
            ''the customer is driver

            sErrorMessage = ""

            LogInformation("Starting:Aurora.Booking.Services.BookingCustomer.ValidateDriver")
            sErrorMessage = Aurora.Booking.Services.BookingCustomer.ValidateDriver(RentalId, datDateOfBirth, datLicenseExpiryDate)
            LogInformation("Done:Aurora.Booking.Services.BookingCustomer.ValidateDriver. sErrorMessage=" & CStr(sErrorMessage))

            If Not String.IsNullOrEmpty(sErrorMessage) Then
                ErrorMessage = sErrorMessage
                Return False
            End If
        End If

        'Generate Customer ID
        If String.IsNullOrEmpty(sCustomerId) Then
            sCustomerId = Aurora.Common.Data.GetNewId()
            sAction = "I"
        Else
            sAction = "M"
        End If

        LogInformation("Starting:Validating Day Phone")
        'Validating Phone Details by Executing stored procedure "sp_validatePhoneNumber"
        If Not ValidatePhoneNumber(sDayPhoneNumber, "", sDayPhoneId, sDayPhoneCodId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Day Phone")

        LogInformation("Starting:Validating Night Phone")
        'Validating night Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not ValidatePhoneNumber(sNightPhoneNumber, "", sNightPhoneId, sNightPhoneCodId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Night Phone")

        LogInformation("Starting:Validating Mobile Phone")
        'Validating mobile Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not ValidatePhoneNumber(sMobilePhoneNumber, "", sMobilePhoneId, sMobilePhoneCodId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Mobile Phone")

        LogInformation("Starting:Validating Fax")
        'Validating FAX Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not ValidatePhoneNumber(sFaxNumber, "", sFaxId, sFaxCodId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Fax")

        LogInformation("Starting:Validating Email")
        'Validating Email Details by Executing the stored procedure "sp_validatePhoneNumber"
        If Not ValidatePhoneNumber("", sEmailAddress, sEmailId, sEmailCodId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Email")

        LogInformation("Starting:Validating Physical Address")
        'Validating Physical Address details by Executing stored procedure "sp_validateAddress"
        If Not ValidateAddress(sPhysicalAddressCountry, sPhysicalAddressState, sPhysicalAddressId, sPhysicalAddressCodeId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Physical Address")

        LogInformation("Starting:Validating Postal Address")
        'Validating Postal Address details by Executing stored procedure "sp_validateAddress"
        If Not ValidateAddress(sPostalAddressCountry, sPostalAddressState, sPostalAddressId, sPostalAddressCodeId, sCustomerId, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Validating Postal Address")

        LogInformation("Done:Running validations on data")

        LogInformation("Starting:Saving Customer data")
        'Update customer details       
        sErrorMessage = ""

        LogInformation("Starting:Aurora.Booking.Services.BookingCustomer.UpdateCustomer")
        sErrorMessage = Aurora.Booking.Services.BookingCustomer.UpdateCustomer(sCustomerId, _
                                                                               sCustomerFirstName, _
                                                                               sCustomerLastName, _
                                                                               bVIP, _
                                                                               bFleetDriver, _
                                                                               sPersonTypeId, _
                                                                               sCustomerTitle, _
                                                                               datDateOfBirth, _
                                                                               sLicenseNumber, _
                                                                               datLicenseExpiryDate, _
                                                                               sLicenseIssuingAuthority, _
                                                                               sGender, _
                                                                               sOccupation, _
                                                                               sPreferredModeOfCommunication, _
                                                                               bMailingListOptIn, _
                                                                               bSurveyOptIn, _
                                                                               sIntegrityNo, _
                                                                               sPassportNumber, _
                                                                               sPassportIssuingCountry, _
                                                                               RentalId, _
                                                                               UserCode, _
                                                                               UserCode, _
                                                                               PROGRAM_NAME, _
                                                                               sLoyaltyCardNumber)
        LogInformation("Done:Aurora.Booking.Services.BookingCustomer.UpdateCustomer. sErrorMessage=" & CStr(sErrorMessage))

        If Not String.IsNullOrEmpty(sErrorMessage) Then
            ErrorMessage = sErrorMessage
            Return False
        End If

        LogInformation("Done:Saving Customer data")

        LogInformation("Starting:Saving Day Phone data")
        'Updating Day Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not UpdatePhoneNumber(sDayPhoneId, sDayPhoneCodId, sDayPhoneAreaCode, sDayPhoneCountryCode, sDayPhoneNumber, sDayPhoneIntegrityNo, "", sCustomerId, UserCode, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Saving Day Phone data")

        LogInformation("Starting:Saving Night Phone data")
        'Updating Night Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not UpdatePhoneNumber(sNightPhoneId, sNightPhoneCodId, sNightPhoneAreaCode, sNightPhoneCountryCode, sNightPhoneNumber, sNightPhoneIntegrityNo, "", sCustomerId, UserCode, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Saving Night Phone data")

        LogInformation("Starting:Saving Mobile Phone data")
        'Updating Mobile Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not UpdatePhoneNumber(sMobilePhoneId, sMobilePhoneCodId, sMobilePhoneAreaCode, sMobilePhoneCountryCode, sMobilePhoneNumber, sMobilePhoneIntegrityNo, "", sCustomerId, UserCode, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Saving Mobile Phone data")

        LogInformation("Starting:Saving Fax data")
        'Updating Fax Phone Number details by Executing stored procedure "sp_updatePhoneNumber"
        If Not UpdatePhoneNumber(sFaxId, sFaxCodId, sFaxAreaCode, sFaxCountryCode, sFaxNumber, sFaxIntegrityNo, "", sCustomerId, UserCode, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Saving Fax data")

        LogInformation("Starting:Saving Email data")
        'Updating Email details by Executing stored procedure "sp_updatePhoneNumber"
        If Not UpdatePhoneNumber(sEmailId, sEmailCodId, "", "", "", sEmailIntegrityNo, sEmailAddress, sCustomerId, UserCode, ErrorMessage) Then
            Return False
        End If
        LogInformation("Done:Saving Email data")


        LogInformation("Starting:Saving Physical Address data")

        ''Updating Physical Address details by Executing stored procedure "sp_updateAddress"
        If Not UpdateAddress(sPhysicalAddressId, sPhysicalAddressStreet, sPhysicalAddressCodeId, _
                             sCustomerId, sPhysicalAddressSuburb, sPhysicalAddressPostCode, _
                             sPhysicalAddressCountry, sPhysicalAddressCityTown, sPhysicalAddressState, _
                             sPhysicalAddressIntegrityNo, UserCode, ErrorMessage) Then
            Return False
        End If

        LogInformation("Done:Saving Physical Address data")

        LogInformation("Starting:Saving Postal Address data")

        ''Updating Postal Address details by Executing stored procedure "sp_updateAddress"
        If Not UpdateAddress(sPostalAddressId, sPostalAddressStreet, sPostalAddressCodeId, _
                             sCustomerId, sPostalAddressSuburb, sPostalAddressPostCode, _
                             sPostalAddressCountry, sPostalAddressCityTown, sPostalAddressState, _
                             sPostalAddressIntegrityNo, UserCode, ErrorMessage) Then
            Return False
        End If

        LogInformation("Done:Saving Postal Address data")

        LogInformation("Starting:Saving Traveller data")

        'Update traveller details
        'JL 2008/07/10
        'use DriverCheckBox rather than licenceNumTextBox
        Dim isDriver As Boolean = True 'driverCheckBox.Checked
        'If Not String.IsNullOrEmpty(sLicenseNumber) Then
        '    isDriver = True
        'End If
        Dim isPrimaryHirer As Boolean = bIsHirer  'primaryHirerCheckBox.Checked

        sErrorMessage = ""

        LogInformation("Starting:Aurora.Booking.Services.BookingCustomer.UpdateTraveller")
        sErrorMessage = Aurora.Booking.Services.BookingCustomer.UpdateTraveller(sCustomerId, RentalId, isDriver, bIsHirer, isPrimaryHirer, UserCode, UserCode, PROGRAM_NAME)
        LogInformation("Done:Aurora.Booking.Services.BookingCustomer.UpdateTraveller. sErrorMessage=" & CStr(sErrorMessage))

        If Not String.IsNullOrEmpty(sErrorMessage) Then
            ErrorMessage = sErrorMessage
            Return False
        End If

        LogInformation("Done:Saving Traveller data")

        'Set success message
        'If sAction = "I" Then
        'SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
        'ElseIf sAction = "M" Then
        'SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        'End If
        LogInformation("AddUpdateCustomerInfo: RS: True")

        Return True


    End Function

#Region "Helper functions for AddUpdateCustomerInfo"

    Private Function ValidatePhoneNumber(ByVal Number As String, ByVal Email As String, ByVal PhoneId As String, _
                                         ByVal CodId As String, ByVal CustomerId As String, ByRef ErrorMessage As String) As Boolean

        Dim action As String
        If Not String.IsNullOrEmpty(Number) Then
            If String.IsNullOrEmpty(PhoneId) Then
                action = "I"
            Else
                action = "M"
            End If

            ErrorMessage = Aurora.Booking.Services.BookingCustomer.ValidatePhoneNumber(action, "Customer", CustomerId, CodId, Number, Email)

            If Not String.IsNullOrEmpty(ErrorMessage) Then
                Return False
            End If
        End If
        Return True
    End Function

    Private Function ValidateAddress(ByVal Country As String, ByVal State As String, ByVal AddressId As String, _
                                     ByVal CodId As String, ByVal CustomerId As String, ByRef ErrorMessage As String) As Boolean
        Dim action As String = ""

        If String.IsNullOrEmpty(AddressId) Then
            action = "I"
        Else
            action = "M"
        End If

        ErrorMessage = Aurora.Booking.Services.BookingCustomer.ValidateAddress(action, "Customer", CustomerId, CodId, Country, State)

        If Not String.IsNullOrEmpty(ErrorMessage) Then
            Return False
        End If
        Return True
    End Function

    Private Function UpdatePhoneNumber(ByVal PhoneId As String, ByVal CodId As String, ByVal AreaCode As String, _
                                       ByVal CityCode As String, ByVal Number As String, ByVal IntegrityNo As String, _
                                       ByVal Email As String, ByVal CustomerId As String, ByVal UserCode As String, _
                                       ByRef ErrorMessage As String) As Boolean

        Dim action As String = ""
        If String.IsNullOrEmpty(PhoneId) Then
            action = "I"
        Else
            action = "M"
        End If

        ErrorMessage = Aurora.Booking.Services.BookingCustomer.UpdatePhoneNumber(action, PhoneId, "Customer", CustomerId, CodId, _
            AreaCode, CityCode, Number, IntegrityNo, Email, UserCode, PROGRAM_NAME)

        If Not String.IsNullOrEmpty(ErrorMessage) Then
            Return False
        End If
        Return True
    End Function

    Private Function UpdateAddress(ByVal AddressId As String, ByVal Street As String, _
                                   ByVal CodeId As String, ByVal CustomerId As String, _
                                   ByVal Suburb As String, ByVal PostCode As String, _
                                   ByVal CountryCode As String, ByVal City As String, _
                                   ByVal StateCode As String, ByVal IntegrityNo As String, _
                                   ByVal UserCode As String, ByRef ErrorMessage As String) As Boolean

        Dim active As String = ""
        If String.IsNullOrEmpty(AddressId) Then
            active = "I"
        Else
            active = "M"
        End If

        ErrorMessage = Aurora.Booking.Services.BookingCustomer.UpdateAddress(active, AddressId, "Customer", Street, CodeId, CustomerId, Suburb, _
            PostCode, CountryCode, City, StateCode, IntegrityNo, UserCode, "CustomerMgt.aspx")

        If Not String.IsNullOrEmpty(ErrorMessage) Then
            Return False
        End If
        Return True

    End Function

#End Region

    Function GetRelevantXML(ByVal InputData As XmlNode) As XmlNode
        Dim sbXMLDataToReturn As New StringBuilder

        ' we get the CustomerInfo Node
        For Each xmlCustomerNode As XmlNode In InputData.SelectNodes("Customer")
            With xmlCustomerNode
                sbXMLDataToReturn.Append("<Customer>" & vbNewLine & _
                                         "  <CustomerDetails>" & vbNewLine & _
                                         "      <CusId>" & GetInnerText(.SelectSingleNode("CustomerDetails/CusId")) & "</CusId>" & vbNewLine & _
                                         "      <LastName>" & GetInnerText(.SelectSingleNode("CustomerDetails/LastName")) & "</LastName>" & vbNewLine & _
                                         "      <FirstName>" & GetInnerText(.SelectSingleNode("CustomerDetails/FirstName")) & "</FirstName>" & vbNewLine & _
                                         "      <Title>" & GetInnerText(.SelectSingleNode("CustomerDetails/Title")) & "</Title>" & vbNewLine & _
                                         "      <LicenceNum>" & GetInnerText(.SelectSingleNode("CustomerDetails/LicenceNum")) & "</LicenceNum>" & vbNewLine & _
                                         "      <ExpiryDate>" & GetInnerText(.SelectSingleNode("CustomerDetails/ExpiryDate")) & "</ExpiryDate>" & vbNewLine & _
                                         "      <IssingAuthority>" & GetInnerText(.SelectSingleNode("CustomerDetails/IssingAuthority")) & "</IssingAuthority>" & vbNewLine & _
                                         "      <DOB>" & GetInnerText(.SelectSingleNode("CustomerDetails/DOB")) & "</DOB>" & vbNewLine & _
                                         "      <PhDayCtyCode>" & GetInnerText(.SelectSingleNode("CustomerDetails/PhDayCtyCode")) & "</PhDayCtyCode>" & vbNewLine & _
                                         "      <PhDayAreaCode>" & GetInnerText(.SelectSingleNode("CustomerDetails/PhDayAreaCode")) & "</PhDayAreaCode>" & vbNewLine & _
                                         "      <PhDayNumber>" & GetInnerText(.SelectSingleNode("CustomerDetails/PhDayNumber")) & "</PhDayNumber>" & vbNewLine & _
                                         "      <MobileCtyCode>" & GetInnerText(.SelectSingleNode("CustomerDetails/MobileCtyCode")) & "</MobileCtyCode>" & vbNewLine & _
                                         "      <MobileAreaCode>" & GetInnerText(.SelectSingleNode("CustomerDetails/MobileAreaCode")) & "</MobileAreaCode>" & vbNewLine & _
                                         "      <MobileNumber>" & GetInnerText(.SelectSingleNode("CustomerDetails/MobileNumber")) & "</MobileNumber>" & vbNewLine & _
                                         "      <Email>" & GetInnerText(.SelectSingleNode("CustomerDetails/Email")) & "</Email>" & vbNewLine & _
                                         "      <Hirer>" & GetInnerText(.SelectSingleNode("CustomerDetails/Hirer")) & "</Hirer>" & vbNewLine & _
                                         "      <PaxType>" & GetInnerText(.SelectSingleNode("CustomerDetails/PaxType")) & "</PaxType>" & vbNewLine & _
                                         "      <MailLists>" & GetInnerText(.SelectSingleNode("CustomerDetails/MailLists")) & "</MailLists>" & vbNewLine & _
                                         "      <SurVeys>" & GetInnerText(.SelectSingleNode("CustomerDetails/PaxType")) & "</SurVeys>" & vbNewLine & _
                                         "      <LoyaltyCardNumber>" & GetInnerText(.SelectSingleNode("CustomerDetails/LoyaltyCardNumber")) & "</LoyaltyCardNumber>" & vbNewLine & _
                                         "  </CustomerDetails>" & vbNewLine & _
                                         "	<PhysicalAddress>" & vbNewLine & _
                                         "      <Street>" & GetInnerText(.SelectSingleNode("PhysicalAddress/Street")) & "</Street>" & _
                                         "      <CityTown>" & GetInnerText(.SelectSingleNode("PhysicalAddress/CityTown")) & "</CityTown>" & _
                                         "      <Country>" & GetInnerText(.SelectSingleNode("PhysicalAddress/Country")) & "</Country>" & _
                                         "      <Postcode>" & GetInnerText(.SelectSingleNode("PhysicalAddress/Postcode")) & "</Postcode>" & _
                                         "	</PhysicalAddress>" & vbNewLine & _
                                         "</Customer>")
            End With
        Next

        sbXMLDataToReturn.Insert(0, "<CustomerInfo>")
        sbXMLDataToReturn.Append("</CustomerInfo>")

        Dim xmlReturn As New XmlDocument
        xmlReturn.LoadXml(sbXMLDataToReturn.ToString())

        Return xmlReturn.DocumentElement

    End Function

#End Region

#Region "Health check method"

    <WebMethod(Description:="This Method Tests If the Webservice is alive and also ensures connectivity to the SQL Server and Aurora WebService is present")> _
    Function PerformHealthCheck() As XmlDocument
        LogInformation("Start:PerformHealthCheck")

        Dim xmlReturn As New XmlDocument
        Dim sbReturnXML As New StringBuilder

        sbReturnXML.Append("<data>")

        ' 1. Got this far so Phoenix Webservice is alive
        sbReturnXML.Append("<PhoenixWebServiceAlive>True</PhoenixWebServiceAlive>")

        ' 2. Check DB connectivity
        Dim oResult As New Object
        Try
            oResult = Aurora.Common.Data.ExecuteScalarSQL("SELECT '<AuroraDatabaseConnectivity>True</AuroraDatabaseConnectivity><AuroraDatabaseTimeStamp>' + CONVERT(VARCHAR,GETDATE(),103) + ' ' + CONVERT(VARCHAR,GETDATE(),108) +  '</AuroraDatabaseTimeStamp>'")
            sbReturnXML.Append(oResult.ToString())
        Catch ex As Exception
            ' something bad happened
            sbReturnXML.Append("<AuroraDatabaseConnectivity>False</AuroraDatabaseConnectivity><AuroraDatabaseError>" & ex.Message & "</AuroraDatabaseError>")
        End Try

        ' 3. Try the Aurora WS
        Try
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            Dim xmlOutput As XmlNode
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))
            xmlOutput = oAuroraWebService.EncryptTextForAurora("TESTDATA")
            If GetInnerText(xmlOutput.SelectSingleNode("Status")).ToUpper().Equals("SUCCESS") Then
                sbReturnXML.Append("<AuroraWebSericeAlive>True</AuroraWebSericeAlive>")
            Else
                Throw New Exception("Aurora WebService unavailable")
            End If

        Catch ex As Exception
            sbReturnXML.Append("<AuroraWebSericeAlive>False</AuroraWebSericeAlive>")
            sbReturnXML.Append("<AuroraWebSericeError>" & ex.Message & "</AuroraWebSericeError>")
        End Try

        sbReturnXML.Append("</data>")

        xmlReturn.LoadXml(sbReturnXML.ToString())

        LogInformation("Done:PerformHealthCheck. RS : " & xmlReturn.OuterXml)

        Return xmlReturn
    End Function


#End Region


#Region "Alternate Availability"
    '' REVISIONS
    '' NOV172010 - MIA - ADDED FILLWITHTESTDATA AND NEW CONFIG KEY FOR MY TESTING
    Public VehicleInfo As New WS.AlternateAvailability.Model.VehicleInformation
    Public Passenger As New WS.AlternateAvailability.Model.PassengerInformation

    ''<WebMethod(Description:="Return Alternate Availability in five different search criteria (MoveForward,MoveBackWard,SwitchLocation,PickUpBased,DropOffBased,NormalBased)", MessageName:="Expected parameters are ObjectBased")> _
    Public Overloads Function GetAlternateAvailability(ByVal vehicleInfo As WS.AlternateAvailability.Model.VehicleInformation, _
                                                    ByVal passenger As WS.AlternateAvailability.Model.PassengerInformation, _
                                                    ByVal DisplayMode As String) As XmlDocument
        Dim warningmessage As String = ""
        Dim service As New GetAlternateVehicleService
        Return service.GetAlternateVehicleAvailability(vehicleInfo, _
                                                       passenger, _
                                                       DisplayMode, _
                                                       warningmessage)
        LogInformation("service.GetAlternateVehicleAvailability WARNING: " & warningmessage)

    End Function


    ''rev:mia 25.03.11 changed DisplayMode from Integer array to String
    <WebMethod(EnableSession:=True, Description:="This method return Alternate Availability in six different search criteria (MoveForward,MoveBackWard,SwitchLocation,PickUpBased,DropOffBased,NormalBased,Oneway(SwitchLocation and Location based on Pickup))")> _
    Public Overloads Function GetAlternateAvailability(ByVal VehicleCode As String, _
                                                    ByVal CountryCode As String, _
                                                    ByVal Brand As String, _
                                                    ByVal CheckOutZoneCode As String, _
                                                     ByVal CheckOutDateTime As DateTime, _
                                                    ByVal CheckInZoneCode As String, _
                                                    ByVal CheckInDateTime As DateTime, _
                                                    ByVal AgentCode As String, _
                                                    ByVal PackageCode As String, _
                                                    ByVal IsVan As Boolean, _
                                                    ByVal NumberOfAdults As Integer, _
                                                    ByVal NoOfChildren As Integer, _
                                                    ByVal isBestBuy As Boolean, _
                                                    ByVal countryOfResidence As String, _
                                                    ByVal DisplayMode As String, _
                                                    ByVal Istestmode As Boolean, _
                                                    ByVal IsGross As Boolean _
                                                    ) As XmlDocument

        Dim warningmessage As String = ""
        Dim service As New GetAlternateVehicleService

        Dim VehInfo As New WS.AlternateAvailability.Model.VehicleInformation
        Dim PasInfo As New WS.AlternateAvailability.Model.PassengerInformation

        With VehInfo
            .Brand = Brand
            .CountryCode = CountryCode
            .VehicleCode = VehicleCode
            .CheckOutZoneCode = CheckOutZoneCode
            .CheckOutDateTime = CheckOutDateTime
            .CheckInZoneCode = CheckInZoneCode
            .CheckInDateTime = CheckInDateTime
            .AgentCode = AgentCode
            .PackageCode = PackageCode
            .IsVan = IsVan
            .IsTestMode = Istestmode
            .IsGross = IsGross
            .IsAuroraRequest = IIf(DisplayMode.Contains("7") = True, True, False)
        End With

        With PasInfo
            .NoOfAdults = NumberOfAdults
            .NoOfChildren = NoOfChildren
            .IsBestBuy = isBestBuy
            .CountryOfResidence = countryOfResidence
        End With

        
        If VehInfo.IsAuroraRequest = True Then
            Dim xmlBase As New XmlDocument
            Try
                xmlBase = service.GetAlternateVehicleAvailability(VehInfo, _
                                                       PasInfo, _
                                                       DisplayMode, _
                                                       warningmessage)

                Dim xmlelems As XmlNodeList = xmlBase.SelectNodes("data/AvailableRate/Errors")
                For Each item As XmlElement In xmlelems
                    item.ParentNode.RemoveChild(item)
                Next

                xmlelems = xmlBase.SelectNodes("data/Traces")
                For Each item As XmlElement In xmlelems
                    item.ParentNode.RemoveChild(item)
                Next

                xmlelems = xmlBase.SelectNodes("data/AvailableRate/Charge")
                For Each item As XmlElement In xmlelems
                    item.ParentNode.RemoveChild(item)
                Next

                xmlelems = xmlBase.SelectNodes("data/AvailableRate/Package")
                Dim AvpId As String = String.Empty
                Dim PickupDate As String = String.Empty
                Dim DropOffDate As String = String.Empty
                Dim BrandCode As String = String.Empty

                For Each item As XmlElement In xmlelems

                    If (Not item.Attributes("AvpId") Is Nothing) Then
                        AvpId = item.Attributes("AvpId").Value

                        If (Not item.Attributes("PickupDate") Is Nothing) Then
                            PickupDate = item.Attributes("PickupDate").Value
                        End If

                        If (Not item.Attributes("DropOffDate") Is Nothing) Then
                            DropOffDate = item.Attributes("DropOffDate").Value
                        End If

                        If (Not item.Attributes("BrandCode") Is Nothing) Then
                            BrandCode = item.Attributes("BrandCode").Value
                        End If


                        item.RemoveAllAttributes()

                        item.SetAttribute("AvpId", AvpId)
                        item.SetAttribute("PickupDate", PickupDate)
                        item.SetAttribute("DropOffDate", DropOffDate)
                        item.SetAttribute("BrandCode", BrandCode)

                    End If

                Next

                Return xmlBase

            Catch ex As Exception
                Return xmlBase
            End Try

        Else
            Return service.GetAlternateVehicleAvailability(VehInfo, _
                                                       PasInfo, _
                                                       DisplayMode, _
                                                       warningmessage)
        End If


    End Function


#End Region

#Region "Check THRIFTY WEBSERVICE if its up and can accept request"
    Private ReadOnly Property GetThriftyConnectivityURL() As String
        Get
            Dim thriftyquerystring As String = "?soap_method=VehLocDetailsNotif&pickup=&account=&token=&vehicle=&promotion=&subaccount="
            Return String.Concat(System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityURL"), thriftyquerystring)
        End Get
    End Property

    Private ReadOnly Property GetProcessThrifty() As Boolean
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ProcessThrifty")
        End Get
    End Property

    Private ReadOnly Property GetThriftyConnectivityTimeOutCheck() As String
        Get
            Return System.Configuration.ConfigurationManager.AppSettings("ThriftyConnectivityTimeOutCheck")
        End Get
    End Property


    ''' <summary>
    ''' Thrifty WEBSERVICE Checking
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <WebMethod(Description:="Check if the Thrifty Webservice is up and running")> _
    Public Function IsThriftyAlive() As Boolean
        Return IsThriftyConnectionAvailable()
    End Function

#Region "Unused"
    '<WebMethod(Description:="Check if the Thrifty Webservice is up and running. If there is no parameter pass then config will be use")> _
    Private Function IsURLThriftyAlive(ByVal wsUrl As String) As Boolean
        If String.IsNullOrEmpty(wsUrl) Then
            'get key in the appconfig
            Return IsThriftyConnectionAvailable()
        Else
            'get key in the parameter
            Return IsThriftyConnectionAvailable(wsUrl)
        End If

    End Function

    '<WebMethod(Description:="Check if the Thrifty Webservice will be call for processing")> _
    Private Function IsProcessthrifty() As Boolean
        Return GetProcessThrifty
    End Function

#End Region

    ''' <summary>
    ''' addition of thrifty WEBSERVICE Checking
    ''' </summary>
    ''' <param name="wsURL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsThriftyConnectionAvailable(Optional ByVal wsURL As String = "") As Boolean
        IsThriftyConnectionAvailable = False

        Try
            System.Diagnostics.Debug.WriteLine("start: " & Now.ToLongTimeString)
            Dim tempURL As String = ""
            If String.IsNullOrEmpty(wsURL) Then
                ''get key in the appconfig 
                tempURL = GetThriftyConnectivityURL()
            Else
                tempURL = wsURL
            End If

            Dim thriftyWSRQ As WebRequest = WebRequest.Create(tempURL)
            thriftyWSRQ.Timeout = Convert.ToInt32(GetThriftyConnectivityTimeOutCheck())
            thriftyWSRQ.Method = "GET"
            thriftyWSRQ.ContentType = "application/x-www-form-urlencoded"
            LogInformation("IsThriftyConnectionAvailable() url: " & GetThriftyConnectivityURL())

            Dim thriftyWSRS As HttpWebResponse = thriftyWSRQ.GetResponse

            If Not thriftyWSRS Is Nothing Then
                Dim sr As New StreamReader(thriftyWSRS.GetResponseStream)
                Dim xd As New XmlDocument
                xd.LoadXml(sr.ReadToEnd.TrimEnd)
                LogInformation("IsThriftyConnectionAvailable() response: " & xd.OuterXml)


                ''200 response is ok
                IsThriftyConnectionAvailable = IIf(thriftyWSRS.StatusCode = 200, True, False)
            End If

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("end: " & Now.ToLongTimeString)
            LogInformation("IsThriftyConnectionAvailable() xml exception: " & ex.Message & " " & ex.StackTrace)
            Return IsThriftyConnectionAvailable
        End Try
        System.Diagnostics.Debug.WriteLine("end: " & Now.ToLongTimeString)
        Return IsThriftyConnectionAvailable

    End Function

#End Region


#Region "rev:mia Mighty"

    Private Shared Function InsertTokenObject(ByVal PmtBillToRntId As String, _
                                       ByVal PmtBillToCardType As String, _
                                       ByVal PmtBillToCardName As String, _
                                       ByVal PmtBillCardNumber As String, _
                                       ByVal PmtBillToCardExpDate As String, _
                                       ByVal PmtBillToToken As String, _
                                       ByVal AddUsrId As String, _
                                       ByVal ModUsrId As String) As String

        Dim result As String = "OK"
        Try

            ''this return nothing if successful
            result = WebServiceData.TokenObject(PmtBillToRntId, _
                                                                        PmtBillToCardType, _
                                                                        PmtBillToCardName, _
                                                                        PmtBillCardNumber, _
                                                                        PmtBillToCardExpDate, _
                                                                        PmtBillToToken, _
                                                                        AddUsrId, _
                                                                        AddUsrId)


        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try

        Return result

    End Function

    <WebMethod(Description:="Method for SelfCheckin that will  update video show through field ")> _
    Public Function VideoShow(ByVal RentalId As String, ByVal status As Boolean) As String
        Dim result As String = "OK"
        Try
            ''this return nothing if successful
            result = WebServiceData.MarkVideoShowAsViewed(RentalId, IIf(status = True, 1, 0))
        Catch ex As Exception
            LogInformation("VideoShow ERROR: " & ex.Message)
            Return "ERROR: " + ex.Message
        End Try
        Return result
    End Function

    <WebMethod(Description:="Method for SelfCheckin that will  update Terms And Condtion field ")> _
    Public Function AcceptTermsAndCondition(ByVal RentalId As String, ByVal status As Boolean) As String
        Dim result As String = "OK"
        Try
            ''this return nothing if successful
            result = WebServiceData.AcceptTemrsAndCondtion(RentalId, IIf(status = True, 1, 0))

        Catch ex As Exception
            LogInformation("AcceptTermsAndCondition ERROR: " & ex.Message)
            Return "ERROR: " + ex.Message
        End Try
        Return result
    End Function

    <WebMethod(Description:="Method for SelfCheckin that will display and transform rental agreements ")> _
    Public Function DisplayRentalAgreements(ByVal RentalId As String) As String
        Dim result As String = ""
        Try
            result = WebServiceData.RentalAgreement(RentalId, Server.MapPath(RENTALAGREEMENT_XSL_PATH))
            LogInformation("DisplayRentalAgreements : " & result)
            If (result.Contains("ERROR") = True) Then
                Return result
            End If

        Catch ex As Exception
            LogInformation("DisplayRentalAgreements ERROR: " & ex.Message)
            Return "ERROR: " + ex.Message
        End Try
        Return result
    End Function

#End Region

#Region "rev:mia Dec.3 2012 - Availability in Grids"

    <WebMethod(Description:="Return the status for particular vehicle")> _
    Public Function GetFleetStatus(ByVal ProductShortName As String, _
                                   ByVal CheckOutLocationCode As String, _
                                   ByVal CheckOutDate As DateTime, _
                                   ByVal CheckInLocationCode As String, _
                                   ByVal CheckInDate As DateTime, _
                                   ByVal AgentCode As String, _
                                   ByVal PackageCode As String, _
                                   ByRef ErrorMessage As String, _
                                   ByRef DvassSequenceNumber As Int64) As Char


        LogInformation("GetFleetStatus: RQ: ProductShortName = """ & ProductShortName & _
                                              """, CheckOutLocationCode = """ & CheckOutLocationCode & _
                                              """, CheckOutDate=""" & CheckOutDate & _
                                              """, CheckInLocationCode=""" & CheckInLocationCode & _
                                              """, CheckInDate=""" & CheckInDate & _
                                              """, AgentCode=""" & AgentCode & _
                                              """, PackageCode=""" & PackageCode & _
                                              """")
        Dim cAvailStatus As Char
        Try
            cAvailStatus = WebServiceData.GetFleetStatus(ProductShortName, _
                                             CheckOutLocationCode, _
                                             CheckOutDate, _
                                             CheckInLocationCode, _
                                             CheckInDate, _
                                             AgentCode, _
                                             PackageCode, _
                                             ErrorMessage, _
                                             DvassSequenceNumber)

            LogInformation("GetFleetStatus: RS: AvailStatus = """ & cAvailStatus & _
                                              """, ErrorMessage = """ & ErrorMessage & _
                                              """, DvassSequenceNumber=""" & DvassSequenceNumber & _
                                             """")
        Catch ex As Exception
            cAvailStatus = "N"
            ErrorMessage = String.Concat(ErrorMessage, " , ", ex.Message, " , ", ex.StackTrace)
            LogInformation("GetFleetStatus: RS: AvailStatus = """ & cAvailStatus & _
                                              """, ErrorMessage = """ & ErrorMessage & _
                                            """")
        End Try

        Return cAvailStatus

    End Function


    <WebMethod(Description:="This method returns a list of available vehicles within number of days")> _
    Public Function GetAvailabilitiesBasedOnDays(ByVal Brand As String, _
                                ByVal CountryCode As String, ByVal VehicleCode As String, _
                                ByVal CheckOutZoneCode As String, ByVal CheckOutDateTime As DateTime, _
                                ByVal CheckInZoneCode As String, ByVal CheckInDateTime As DateTime, _
                                ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, _
                                ByVal AgentCode As String, ByVal PackageCode As String, _
                                ByVal IsVan As Boolean, ByVal IsBestBuy As Boolean, _
                                ByVal CountryOfResidence As String, ByVal IsTestMode As Boolean, ByVal IsGross As Boolean) As XmlDocument

        Dim service As New GetVehicleList(CInt(ConfigurationManager.AppSettings("MoveableDaysForSingleVehicle").ToString))
        Return service.GetAvailabilitiesBasedOnDays(Brand, _
                                             CountryCode, _
                                             VehicleCode, _
                                             CheckOutZoneCode, _
                                             CheckOutDateTime, _
                                             CheckInZoneCode, _
                                             CheckInDateTime, _
                                             NoOfAdults, _
                                             NoOfChildren, _
                                             AgentCode, _
                                             PackageCode, _
                                             IsVan, _
                                             IsBestBuy, _
                                             CountryOfResidence, _
                                             IsTestMode, _
                                             IsGross)

    End Function
#End Region

#Region "rev:mia March 19 2013 - ConfirmationSend exposed to SCI"
    <WebMethod(Description:="Method for SelfCheckin that will create and send confirmation")> _
    Public Function CreateAndSendconfirmation(ByVal RentalId As String) As String
        Dim result As String = "OK"
        Try
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                            ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                            ConfigurationSettings.AppSettings("DomainName"))
            oAuroraWebService.CreateAndSendconfirmation(RentalId)
            ''this return nothing if successful
        Catch ex As Exception
            LogInformation("CreateAndSendconfirmation ERROR: " & ex.Message & " - " & ex.StackTrace)
            Return "ERROR: " + ex.Message
        End Try
        Return result
    End Function
#End Region

#Region "rev:mia 04nov2014 - SLOT MANAGEMENT"
    <WebMethod(Description:="This method get the time slot available for selection")> _
    Public Function GetAvailableSlot(sRntId As String, sAvpId As String, sBpdId As String) As XmlDocument
        Dim result As DataSet
        Dim xmlDoc As New XmlDocument
        Try
            result = WebServiceData.GetAvailableSlot(sRntId, sAvpId, sBpdId)
            If (result.Tables(0).Rows.Count - 1 <> -1) Then
                xmlDoc.LoadXml(result.GetXml())
                Dim xmlText As String = xmlDoc.OuterXml
                xmlText = xmlText.Replace("NewDataSet", "AvailableSlots")
                xmlDoc.LoadXml(xmlText)
            End If

        Catch ex As Exception
            LogInformation("GetAvailableSlot ERROR: " & ex.Message & " - " & ex.StackTrace)
        End Try

        Return xmlDoc
    End Function
#End Region

#Region "rev:mia 02Dec2014 - AUDIT TRAIL FOR SCI VERIFIED DRIVERS plus AUTOCHECKOUT"
    <WebMethod(Description:="This method is going to be use by SCI after Agents verified Drivers")> _
    Public Function AutoCheckOut(ByVal BookingId As String, _
                                 ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String) As String

        Try
            LogInformation("AutoCheckOut")
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))
            Return oAuroraWebService.AutoCheckOut(BookingId, RentalId, UserCode, RentalCountryCode)
        Catch ex As Exception
            LogInformation("AutoCheckOut Error in Phoenix " & ex.Source & vbCrLf & ex.Message)
            Return "ERROR:" & ex.Message
        End Try

    End Function

    <WebMethod(Description:="This method is going to be use by SCI to verify Agents/CSR credentials")> _
    Public Function VerifyUser(UsrCode As String, UsrPsswd As String) As XmlDocument
        LogInformation("VerifyUser: Parameters : UsrCode : " & UsrCode)

        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Verify User failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try

            Dim result As String = WebServiceData.SCIVerifiedUser(UsrCode, UsrPsswd)
            newXml.LoadXml(result)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("VerifyUser: Parameters : UsrCode : " & UsrCode & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return newXml



    End Function


    <WebMethod(Description:="This method is going to be send SCI Rental Confirmation")> _
    Public Function EmailRentalAgreement(rentalid As String, CustomerEmail As String) As String
        LogInformation("EmailRentalAgreement: Parameters : rentalid : " & rentalid & ", CustomerEmail : " & CustomerEmail)
        Return WebServiceData.EmailRentalAgreement(rentalid, CustomerEmail)
    End Function

#End Region


#Region "rev:mia 11March2015- Part of B2B Auto generated Aurora Booking Confirmation"

    
    <WebMethod(Description:="This Web Method will be use by the B2B where a confirmation is auto generated for a QN, NN or KK booking made online, and sent by email")> _
    Public Function B2BCreateAndSendconfirmation(ByVal RentalInfo As String, EmailAddress As String, isCustomer As Boolean) As String
        LogInformation("B2BCreateAndSendconfirmation: Parameters : rentalid : " & RentalInfo & ", EmailAddress : " & EmailAddress & ", isCustomer : " & isCustomer)
        Try
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))
            Return oAuroraWebService.B2BCreateAndSendconfirmation(RentalInfo, EmailAddress, isCustomer)
        Catch ex As Exception
            LogInformation("B2BCreateAndSendconfirmation Error in Phoenix " & ex.Source & vbCrLf & ex.Message)
            Return "ERROR:" & ex.Message
        End Try
    End Function


    <WebMethod(Description:="This Web Method will be use by the B2B (but will not store History in Aurora)  where a confirmation is auto generated for a QN, NN or KK booking made online, and sent by email")> _
    Public Function B2BSendConfirmation(emailAddress As String, subject As String, content As String, b2bagent As String) As String
        LogInformation("B2BSendConfirmation: Parameters :  EmailAddress : " & emailAddress & ", subject : " & subject & ", content : " & content & ", agent : " & b2bagent)
        Dim result As String = ""
        Try
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))
            result = oAuroraWebService.B2BSendConfirmation(emailAddress, subject, content, b2bagent)

        Catch ex As Exception
            LogInformation("B2BSendConfirmation Error in Phoenix " & ex.Source & vbCrLf & ex.Message)
            result = "ERROR:" & ex.Message
        End Try
        Return result
    End Function

    <WebMethod(Description:="This Web Method will be use by the B2B to get the Agent Email Address")> _
    Public Function B2BAgentEmailAddress(agentcode As String) As String
        LogInformation("B2BAgentEmailAddress: Parameters :  AgentCode : " & agentcode)
        Dim result As String = ""
        Try
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))
            result = oAuroraWebService.B2BAgentEmailAddress(agentcode)

        Catch ex As Exception
            LogInformation("B2BSendConfirmation Error in Phoenix " & ex.Source & vbCrLf & ex.Message)
            result = "ERROR:" & ex.Message
        End Try
        Return result
    End Function

#End Region

#Region "rev:mia 20March2015-exposing new endpoints for RESTAPI - working"

    <WebMethod(Description:="This is for RESTAPI. Its a webmethod allows addition/updation of customer info + addition and removal of Extra Driver Fee (or any other product)")> _
    Public Function AddUpdateCustomerDataAPI(ByVal InputData As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Customer Data was not updated</Message></Data>"
        Dim newXml As New XmlDocument
        Try
            LogInformation("AddUpdateCustomerDataAPI: Parameters : InputData : " & InputData)

            newXml.LoadXml(InputData)
            newXml = AddUpdateCustomerData(newXml)

        Catch ex As Exception
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: AddUpdateCustomerDataAPI: Parameters : InputData : " & InputData & vbCrLf & ex.StackTrace & vbCrLf & ex.Message)
        End Try

        Return newXml
    End Function

    <WebMethod(Description:="This is for RESTAPI. This method adds/removes extra hire items, insurance products to a rental. For use with Self Check out")> _
    Public Function AddRemoveProductsAPI(ByVal InputData As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Add or Remove product failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try
            LogInformation("AddRemoveProductsAPI: Parameters : InputData : " & InputData)

            newXml.LoadXml(InputData)
            newXml = AddRemoveProducts(newXml)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: AddRemoveProductsAPI: Parameters : InputData : " & InputData & vbCrLf & ex.StackTrace & vbCrLf & ex.Message)
        End Try

        Return newXml

    End Function

    <WebMethod(Description:="This is for RESTAPI. This method return all Rental Activity . For use with Self Check out")> _
    Public Function GetTodayActivityPerLocationAPI(PickUpDate As String, PickUpLocation As String, IsPickUp As Boolean) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Rental Activity failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try
            LogInformation("GetTodayActivityPerLocationAPI: Parameters : PickUpData : " & PickUpDate & ", PickUpLocation : " & PickUpLocation & ", IsPickUp : " & IsPickUp)
            newXml = GetTodayActivityPerLocation(PickUpDate, PickUpLocation, IsPickUp)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: GetTodayActivityPerLocationAPI: Parameters : PickUpData : " & PickUpDate & ", PickUpLocation : " & PickUpLocation & ", IsPickUp : " & IsPickUp & vbCrLf & ex.StackTrace & vbCrLf & ex.Message)
        End Try

        Return newXml
    End Function

    <WebMethod(Description:="This is for RESTAPI. This method will add Rental note.")> _
    Public Function AddNoteToRental(RntId As String, NoteText As String, UsrCode As String, PrgmName As String, RntAudTypeId As String, RntNoteTypeId As String) As String
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Adding of Rental Note failed</Message></Data>"
        Dim result As String = ""
        Try
            result = AddRentalNote(RntId, NoteText, UsrCode, PrgmName, RntAudTypeId, RntNoteTypeId)
            If (result = "ERROR") Then
                result = ERROR_MESSAGE
            End If

        Catch ex As Exception
            LogInformation("ERROR: AddNoteToRenta: Parameters : RntId : " & RntId & ", NoteText : " & NoteText & ", UsrCode : " & UsrCode & ", PrgmName : " & PrgmName & ", RntAudTypeId : " & RntAudTypeId & ", RntNoteTypeId : " & RntNoteTypeId & vbCrLf & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return result
    End Function

    <WebMethod(Description:="This is for RESTAPI. Exposing AddPayment for Self Check out")> _
    Public Function AddPaymentAPI(ByVal InputData As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Add Payment failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try
            newXml.LoadXml(InputData)
            newXml = AddPayment(newXml)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: AddPaymentAPI: Parameters : InputData : " & InputData & vbCrLf & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function

    <WebMethod(Description:="This is for RESTAPI. Get All Non-Vehicle products for Self Check out")> _
    Public Function GetNonVehicleProductForRentalId(RntId As String, UsrCode As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Getting non-vehicle products failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try

            newXml = GetAllNonVehicleProductForRentalId(RntId, UsrCode)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: GetNonVehicleProductForRentalId: Parameters : RntId : " & RntId & vbCrLf & ", UsrCode : " & UsrCode & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function

    <WebMethod(Description:="This is for RESTAPI. Get All Non-Vehicle products Price for Self Check out")> _
    Public Function GetPriceForNonVehicleProduct(RntId As String, PrdId As String, UsrCode As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Getting Price for specific non-vehicle product failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try

            newXml = GetPriceForSpecificProduct(RntId, PrdId, UsrCode)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: GetPriceForNonVehicleProduct: Parameters : RntId : " & RntId & ", PrdId : " & PrdId & vbCrLf & ", UsrCode : " & UsrCode & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function

#End Region

#Region "rev:mia 07May2014 - exposing endpoints for RESTAPI"
    <WebMethod(Description:="This is for RESTAPI. Get All Notes")> _
    Public Function GetNotesAPI() As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Getting Notes failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try

            newXml = GetNotes()

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: GetNotes: " & vbCrLf & ", UsrCode : " & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function
#End Region
  
#Region "rev:mia 20MAY2015 - MANNY'S TEST"
    ''<WebMethod(Description:="this is for restapi. print ra for sci. printername should be '\\servername\printername' ")> _
    Public Function printradocument(urlorcontent As String, printername As String, isurl As Boolean) As String
        Dim error_message As String = "<data><status>error</status><message>rental agreement printing failed</message></data>"
        Dim result As String = "ok"
        Try
            result = PrinterSelection.PrintDocument(urlorcontent, printername, isurl)
        Catch ex As Exception
            result = error_message
            LogInformation("error: printradocument: parameters : urlorcontent : " & urlorcontent & ", printername : " & printername & vbCrLf & ", isurl : " & isurl & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return result
    End Function
#End Region

#Region "rev:mia 21May2015 - adding Rental Flag"
    <WebMethod(Description:="This is for RESTAPI. Save Rental Flags and Filename ")> _
    Public Function SaveRentalFlags(rntId As String, acceptTC As Boolean, RAFilename As String, regoNum As String, addPrgName As String, addUserCode As String) As String
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Saving Rental Flags failed</Message></Data>"
        Try
            SavingRentalFlags(rntId, acceptTC, RAFilename, regoNum, addPrgName, addUserCode)
        Catch ex As Exception
            LogInformation("ERROR: SaveRentalFlags: Parameters : RntId : " & rntId & ", acceptTC : " & acceptTC & ", regoNum : " & regoNum & ", addPrgName : " & addPrgName & ", addUserCode : " & addUserCode & ex.StackTrace & vbCrLf & ex.Message)
            Return ERROR_MESSAGE
        End Try
        Return "OK"
    End Function

    <WebMethod(Description:="This is for RESTAPI. Verify Drivers in SCI ")> _
    Public Function VerifyDriver(rntId As String, cusId As String, isVerified As Boolean, addPrgName As String, addUserCode As String) As String
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Verifying of Drivers failed</Message></Data>"
        Try
            VerifyingOfDrivers(rntId, cusId, isVerified, addPrgName, addUserCode)
        Catch ex As Exception
            LogInformation("ERROR: VerifyDriver: Parameters : RntId : " & rntId & ", cusId : " & cusId & ", isVerified : " & isVerified & ", addPrgName : " & addPrgName & ", addUserCode : " & addUserCode & ex.StackTrace & vbCrLf & ex.Message)
            Return ERROR_MESSAGE
        End Try
        Return "OK"
    End Function

    <WebMethod(Description:="This is for RESTAPI. Get Vehicle Data for SCI ")> _
    Public Function GetVehicleData(rntId As String, unitNumber As String, regoNumber As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>Getting Vehicle Data failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try

            newXml = GetVehicles(rntId, unitNumber, regoNumber)

            If (newXml Is Nothing) Then
                Throw New Exception()
            End If

        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("ERROR: GetVehicleData: Parameters : RntId : " & rntId & ", unitNumber : " & unitNumber & vbCrLf & ", regoNumber : " & regoNumber & ex.StackTrace & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function
#End Region

#Region "rev:25May2015 - Adding of complete pickup process"
    <WebMethod(Description:="This method is going to be use by SCI for completing the Pickup/CheckOut process.(eg:RentalId = '3000140-1')")> _
    Public Function CompletePickupProcess(ByVal RentalId As String, _
                                 ByVal RentalCountryCode As String, _
                                 ByVal RegoNumber As String, _
                                 ByVal OdoOut As String, _
                                 ByVal UnitNumber As String, _
                                 ByVal UserCode As String) As XmlDocument
        Dim ERROR_MESSAGE As String = "<Data><Status>ERROR</Status><Message>CompletePickUpProcess failed</Message></Data>"
        Dim newXml As New XmlDocument
        Try


            LogInformation("CompletePickupProcess")
            Dim oAuroraWebService As AuroraWebService = New AuroraWebService
            oAuroraWebService.Credentials = New System.Net.NetworkCredential(ConfigurationSettings.AppSettings("DomainUID"), _
                                                                             ConfigurationSettings.AppSettings("DomainPWD"), _
                                                                             ConfigurationSettings.AppSettings("DomainName"))

            Dim result As String = oAuroraWebService.CompletePickupProcess(RentalId, UserCode, RentalCountryCode, RegoNumber, OdoOut, UnitNumber)
            newXml.LoadXml(result)
            ''Return oAuroraWebService.CompletePickupProcess(RentalId, UserCode, RentalCountryCode, RegoNumber, OdoOut, UnitNumber)
        Catch ex As Exception
            newXml = New XmlDocument()
            newXml.LoadXml(ERROR_MESSAGE)
            LogInformation("CompletePickupProcess Error in Phoenix " & ex.Source & vbCrLf & ex.Message)
        End Try
        Return newXml
    End Function
#End Region

#Region "rev:mia 26MAY2015 - FLEXIBLE GETAVAILABILITY"

    <WebMethod(Description:="This method returns a list of available vehicles and prices for a given selection criteria and flexible data")> _
    Public Function GetFlexibleAvailability(ByVal Brand As String, _
                               ByVal CountryCode As String, _
                               ByVal VehicleCode As String, _
                               ByVal CheckOutZoneCode As String, _
                               ByVal CheckOutDateTime As DateTime, _
                               ByVal CheckInZoneCode As String, _
                               ByVal CheckInDateTime As DateTime, _
                               ByVal NoOfAdults As Int16, _
                               ByVal NoOfChildren As Int16, _
                               ByVal AgentCode As String, _
                               ByVal PackageCode As String, _
                               ByVal IsVan As Boolean, _
                               ByVal IsBestBuy As Boolean, _
                               ByVal CountryOfResidence As String, _
                               ByVal IsTestMode As Boolean, _
                               ByVal IsGross As Boolean, _
                               ByVal AgnisInclusiveProduct As Boolean, _
                               ByVal sPromoCode As String, _
                               ByVal bflexiblePickUpdate As Boolean) As XmlDocument

        LogInformation("GetFlexibleAvailability: RQ")

        Dim oRetXml As XmlDocument = GetAvailability(Brand, _
                                                     CountryCode, _
                                                     VehicleCode, _
                                                     CheckOutZoneCode, _
                                                     CheckOutDateTime, _
                                                     CheckInZoneCode, _
                                                     CheckInDateTime, _
                                                     NoOfAdults, _
                                                     NoOfChildren, _
                                                     AgentCode, _
                                                     PackageCode, _
                                                     IsVan, _
                                                     IsBestBuy, _
                                                     CountryOfResidence, _
                                                     IsTestMode, _
                                                     IsGross, _
                                                     AgnisInclusiveProduct, _
                                                     sPromoCode)




        If (bflexiblePickUpdate) Then

            Dim htErrorList As Hashtable = New Hashtable
            Dim htThriftyCallDataList As Hashtable = New Hashtable

            Dim countOfAvail As Integer = oRetXml.SelectNodes("data/AvailableRate/Package[@AvpId != '']").Count
            Dim vehicleNodes As XmlNodeList = oRetXml.SelectNodes("data/AvailableRate")


            Dim flexibleDaysForVehicles As String = ConfigurationManager.AppSettings("FlexibleDays_ForVehicles")
            flexibleDaysForVehicles = IIf(String.IsNullOrEmpty(flexibleDaysForVehicles), "10", flexibleDaysForVehicles)
            If (countOfAvail < CInt(flexibleDaysForVehicles)) Then
                ''start with zero
                Dim ctr As Integer = 1
                Dim flexiAvail As Char = Nothing
                Dim flexiDateOut As Date = CheckOutDateTime
                Dim flexiDateIn As Date = CheckInDateTime

                Dim elemAvailability As XmlElement = Nothing
                Dim elemVehicle As XmlElement = Nothing
                FlexiHelper.CreateFlexiAvailabilityHeader(oRetXml, elemAvailability, countOfAvail, flexibleDaysForVehicles, Brand)


                flexiAvail = AVAILABLE_NO_FLAG
                countOfAvail = countOfAvail + 1
                Dim countOfnonAvail As Integer = 1

                Do While ((countOfAvail <= CInt(flexibleDaysForVehicles)) And ((countOfnonAvail) < (CInt(flexibleDaysForVehicles) - countOfAvail)))
                    flexiDateOut = flexiDateOut.AddDays(1)
                    flexiDateIn = flexiDateIn.AddDays(1)

                    ''addtional trap
                    If ((countOfnonAvail) > (CInt(flexibleDaysForVehicles) - countOfAvail)) Then
                        Exit Do
                    End If


                    For Each vehNode As XmlNode In vehicleNodes
                        If (vehNode("Package").Attributes("AvpId") Is Nothing) Then
                            Continue For
                        End If

                        If (countOfAvail > CInt(flexibleDaysForVehicles)) Then
                            countOfAvail = countOfAvail + 1
                            Exit For
                        End If

                        Dim avpId As String = vehNode("Package").Attributes("AvpId").Value
                        Dim pkgCopde As String = vehNode("Package").Attributes("PkgCode").Value
                        Dim prodCode As String = vehNode("Charge")("Det").Attributes("PrdCode").Value
                        Dim prodName As String = vehNode("Charge")("Det").Attributes("PrdName").Value
                        Dim brandCode As String = vehNode("Package").Attributes("BrandCode").Value
                        Dim brandName As String = vehNode("Package").Attributes("BrandName").Value
                        Dim pickupLocCode As String = vehNode("Package").Attributes("PickupLocCode").Value
                        Dim dropOffLocCode As String = vehNode("Package").Attributes("DropOffLocCode").Value
                        Dim packageId As String = ""

                        ''If (oRetXml.SelectSingleNode("data/FlexiAvailability/Availability/Vehicle[@code = '" & prodCode & "' and @avpId = '" & avpId & "']") Is Nothing) Then
                        If (oRetXml.SelectSingleNode("data/FlexiAvailability/Availability/Vehicle[@code = '" & prodCode & "']") Is Nothing) Then
                            FlexiHelper.CreateFlexiAvailabilityVehicle(oRetXml, elemVehicle, elemAvailability, prodCode, avpId, pkgCopde)
                        Else
                            elemVehicle = oRetXml.SelectSingleNode("data/FlexiAvailability/Availability/Vehicle[@code = '" & prodCode & "']")
                        End If

                        If (Not String.IsNullOrEmpty(PackageCode)) Then
                            packageId = WebServiceData.GetPackageId(PackageCode, pickupLocCode, flexiDateOut, flexiDateIn)
                        End If


                        flexiAvail = GetVehicleAvailability(brandCode, prodCode, prodName, CountryCode, _
                                  pickupLocCode, flexiDateOut, _
                                  dropOffLocCode, flexiDateIn, _
                                  AgentCode, packageId, htErrorList, htThriftyCallDataList _
                                  )


                        FlexiHelper.CreateFlexiAvailabilityDate(oRetXml, elemVehicle, flexiDateOut, flexiDateIn, flexiAvail)

                        If (flexiAvail = AVAILABLE_YES_FLAG) Then
                            countOfAvail = countOfAvail + 1
                        Else
                            countOfnonAvail = countOfnonAvail + 1
                        End If

                    Next
                Loop

            Else
                If (oRetXml.DocumentElement.SelectSingleNode("FlexAvailability") Is Nothing) Then
                    Dim elem As XmlElement = oRetXml.CreateElement("FlexAvailability")
                    Dim att As XmlAttribute = oRetXml.CreateAttribute("message")
                    att.Value = "Total number available is " & countOfAvail & ". Required vehicle was " & flexibleDaysForVehicles & ". Skipped Flexi-Availability"
                    elem.Attributes.Append(att)
                    oRetXml.DocumentElement.AppendChild(elem)
                End If
            End If
        End If


        LogInformation("GetFlexibleAvailability: RS:" & oRetXml.OuterXml)

        Return oRetXml


    End Function

#End Region


#Region "rev:mia 29MAY2015 - get RA using API Token"
    <WebMethod(Description:="This method returns the url of SignedRentalAgreement")> _
    Public Function GetsignedRentalAgreement(username As String, password As String, rentalId As String) As String

        Dim result As String = ""
        Try
            LogInformation("CompletePickupProcess -- checking printer ")
            Dim printername As String = GetPrintersAssignedToBranchAndUser(username)
            If (String.IsNullOrEmpty(printername)) Then
                Return "ERROR:  The '" & username & "' used has no default printer assigned to Aurora Profile"
            End If

            Dim urlAmazon As String = RAgreement.PrintRentalAgreement(username, password, rentalId)


            If (urlAmazon.IndexOf("http") <> -1) Then
                result = PrinterSelection.PrintDocument(urlAmazon, printername, True, rentalId)
            Else
                result = "ERROR: This URL is not valid '" & urlAmazon & "'"
            End If
            
        Catch ex As Exception
            result = "ERROR:" & ex.Message
            LogInformation("GetsignedRentalAgreement Error in Phoenix " & ex.Source & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return result
    End Function
#End Region

End Class

