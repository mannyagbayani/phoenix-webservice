﻿' Change Log 
'   9.6.9   -   Shoel   -   Stable Version 
'   24.6.9  -   Shoel   -   GetExtraHireItems now returns product list sorted by DisplayOrder attribute
'   17.7.9  -   Shoel   -   Phenomenal Logging capabilities now included! Hence the changes here
'   29.7.9  -   Shoel   -   The read xml logic to convert a dataset into an xml string is now moved into a helper function that is referenced everywhere
'                           Self Checkout stuff makes its debut!
'   4.8.9   -   Shoel   -   Fixed a problem noticed when testing for US - Apollo. Program assumed presense of atleast one location that was not mapped to a zone, this is now fixed!
'                           GetCountryCompanyList is the method that gets the Country-Company list now.
'   20.8.9  -   Shoel   -   Now includes all the Self Checkout stuff!
'   29.9.9  -   Shoel   -   Fixed a dumb bug with params array for AddPayment :(
'   9.11.9  -   Shoel   -   Fix for AIRNZ UserCode problem  
'   3.12.9  -   Shoel   -   GetVehicles proc now uses check out date and check in date to validate against non-availability table
'   21.1.10 -   Shoel   -   Fixed incorrect reading of output param in webp_getvehicles that caused weird display of results
'   5.3.10  -   Shoel   -   Drivers licences that dont have expiry dates now get stored as  Ckidate + 2months and a note is added
'   14.6.10 -   Shoel   -   Change for Loyalty Card Number
'   24.8.10 -   Guess?  -   RetrieveBooking tweaked to get all data even if the quote had expired or the vehicle is unavailable
'   31.8.10 -   Guess?  -   RetrieveBooking now also accepts BpdId to search for rental data
'   10.9.10 -   Me again-   GetPersonRates is no longer called, as the frontend is now provided the prrids and is expected to pass them in again
'                           A new method - GetInsuranceAndExtraHireItemsForRentalQuote is added to reload quotes properly and with ferry stuff (something that SCO doesnt use)
'   25.05.12    RKS         Changes for Mighty Drop-off details                     

Imports Microsoft.VisualBasic
Imports Aurora.Common.Data
Imports System.Xml
Imports System.Xml.XPath
Imports System.Data
Imports System.Xml.Xsl
Imports System.IO
Imports WS.Caching
Imports System.Web
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Net

Public Class WebServiceData

#Region "GLOBAL CONSTANTS"
    Const APP_NAME As String = "WEB"
    Const PROGRAM_NAME As String = "PHOENIX_WEBSERVICE"
    Const DATE_FORMAT As String = "dd/MM/yyyy"
    Public Shared AURORA_USER_CODE As String = System.Configuration.ConfigurationSettings.AppSettings("AURORAUSERCODE")
    Public Shared DEBUG_MODE As Boolean = CBool(ConfigurationSettings.AppSettings("DebugMode"))
    Public Shared PUBLIC_ERROR_MESSAGE As String = ConfigurationSettings.AppSettings("PUBLICERRORMESSAGE")
#End Region

#Region "Master XML stuff"

    Public Shared Function GetCountryCompanyList(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As XmlDocument
        '[WEBP_GetCountryCompanyList]
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetCountryCompanyList", CountryCode, CompanyCode, BrandCode)
        Return xmlResult
    End Function

    Public Shared Function GetCountryVehicles(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String, ByVal VehicleCode As String) As XmlDocument
        'ALTER PROC WEBP_GetCountryVehicles --'NZ','KXS','X','SCARX'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        ',@sVehCode AS VARCHAR(64) = ''
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetCountryVehicles", CountryCode, CompanyCode, BrandCode, VehicleCode)
        Return xmlResult
    End Function

    Public Shared Function GetLocationsWithoutZone(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As XmlDocument
        'ALTER PROC WEBP_GetLocationsWithoutZone --  'NZ','KXS','X'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetLocationsWithoutZone", CountryCode, CompanyCode, BrandCode)
        Return xmlResult
    End Function

    Public Shared Function GetAgentList(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String, ByVal VehicleCode As String) As XmlDocument
        'ALTER PROC WEBP_GetAgentList -- 'NZ','KXS','X','SCARX'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        ',@sVehCode AS VARCHAR(64) = ''
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetAgentList", CountryCode, CompanyCode, BrandCode, VehicleCode)
        Return xmlResult
    End Function

    Public Shared Function GetBrandLookup(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As XmlDocument
        'ALTER PROC WEBP_GetBrandLookups  --  'NZ','THL','B'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetBrandLookups", CountryCode, CompanyCode, BrandCode)
        Return xmlResult
    End Function

    Public Shared Function GetMaxAdultChildrenCount(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String, ByVal VehicleCode As String) As XmlDocument
        'ALTER PROC WEBP_GetMaxAdultChildrenForVehicleTypes --   'NZ','THL','B','2BBXS'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        ',@sVehCode AS VARCHAR(64) = ''
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_GetMaxAdultChildrenForVehicleTypes", CountryCode, CompanyCode, BrandCode, VehicleCode)
        Return xmlResult
    End Function

    Public Shared Function GetLocationsWithZone(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As DataTable
        'ALTER PROC WEBP_GetLocationsWithZone    -- 'NZ','THL','P'--,'2BBXS'
        '@sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        Dim dtResult As DataTable = New DataTable
        dtResult = ExecuteTableSP("WEBP_GetLocationsWithZone", dtResult, CountryCode, CompanyCode, BrandCode)
        Return dtResult
    End Function

    Public Shared Function GetOperationHours(ByVal CountryCode As String) As XmlDocument
        'WEBP_getMinMaxOperatingHrs @sCtyCode = 'NZ'
        Dim xmlResult As XmlDocument
        xmlResult = ExecuteSqlXmlSPDoc("WEBP_getMinMaxOperatingHrs", CountryCode)
        Return xmlResult
    End Function

#End Region

#Region "Best Buy stuff"
    ''rev:mia Oct.24, 2013 - added sPkgCode as requested by rajesh
    Public Shared Function GetVehicleList(ByVal Brand As String, _
                                          ByVal CheckOutLocationCode As String, ByVal CheckOutDateTime As DateTime, _
                                          ByVal CheckInLocationCode As String, ByVal CheckInDateTime As DateTime, _
                                          ByVal CountryCode As String, _
                                          ByVal NumberOfAdults As Integer, ByVal NumberOfChildren As Integer, ByVal NumberOfInfants As Integer, _
                                          ByVal IsVan As Boolean, ByVal IsThriftyLocation As Boolean, ByVal VehicleCode As String, _
                                          ByVal AgentCode As String, ByVal IsTestMode As Boolean, _
                                          ByRef ErrorMessage As String, _
                                          Optional IsAuroraRequest As Boolean = False, _
                                          Optional sPkgCode As String = "") As DataTable

        ''rev:mia Oct.24, 2013 - Layout of Stored Proc
        ' [WEBP_GetVehicles]()
        '     @sBrdCode VARCHAR(1)            
        '   , @sCkoLoc VARCHAR(3)            
        '   , @sCkiLoc VARCHAR(3)      
        '   , @sCtyCode VARCHAR(3)          
        '   , @nAdults INT            
        '   , @nChildren INT            
        '   , @nInfants INT            
        '   , @bIsVan BIT          
        '   , @bIsThriftyLocation BIT   = 0  
        ', @sVehicleCode VARCHAR(64) = NULL  
        ', @dCkoWhen DATETIME = NULL  
        ', @dCkiWhen DATETIME = NULL  
        '-- RKS : 02-Nov-2011  
        '-- START : to handle special condition for B2B  
        ', @sAgnCode VARCHAR(64) = NULL  
        ', @bIsB2B  BIT   = NULL  
        '-- END : to handle special condition for B2B  
        ', @sPkgCode     VARCHAR(64) = NULL
        ', @sErrorMessage VARCHAR(MAX) OUTPUT  

        Dim sbkey As New StringBuilder

        ''rev:mia Oct.24, 2013 - Added for caching
        If (Not String.IsNullOrEmpty(sPkgCode)) Then
            sbkey.AppendFormat("PKG_{0}_", sPkgCode)
        End If

        If (Not String.IsNullOrEmpty(Brand)) Then
            sbkey.AppendFormat("BRD_{0}_", Brand)
        End If

        If (Not String.IsNullOrEmpty(CheckOutLocationCode)) Then
            sbkey.AppendFormat("CKO_{0}_", CheckOutLocationCode)
        End If

        If (Not String.IsNullOrEmpty(CheckOutDateTime)) Then
            sbkey.AppendFormat("CKODATE_{0}_", CheckOutDateTime.ToShortDateString.Replace("/", ""))
        End If

        If (Not String.IsNullOrEmpty(CheckInLocationCode)) Then
            sbkey.AppendFormat("CKI_{0}_", CheckInLocationCode)
        End If

        If (Not String.IsNullOrEmpty(CheckInDateTime)) Then
            sbkey.AppendFormat("CKIDATE_{0}_", CheckInDateTime.ToShortDateString.Replace("/", ""))
        End If

        If (Not String.IsNullOrEmpty(VehicleCode)) Then
            sbkey.AppendFormat("VEHICLE_{0}_", VehicleCode)
        End If

        If (Not String.IsNullOrEmpty(AgentCode)) Then
            sbkey.AppendFormat("AGENT_{0}_", AgentCode)
        End If

        If (Not String.IsNullOrEmpty(CountryCode)) Then
            sbkey.AppendFormat("CTY_{0}_", CountryCode)
        End If

        If (NumberOfAdults > 0) Then
            sbkey.AppendFormat("ADLT_{0}_", NumberOfAdults)
        End If

        If (NumberOfChildren > 0) Then
            sbkey.AppendFormat("CHLD_{0}_", NumberOfChildren)
        End If

        If (NumberOfInfants > 0) Then
            sbkey.AppendFormat("INFNT_{0}_", NumberOfInfants)
        End If

        If (IsVan = True) Then
            sbkey.AppendFormat("VAN_{0}_", True)
        End If
        If (IsThriftyLocation = True) Then
            sbkey.AppendFormat("THRIFT_{0}_", IsThriftyLocation)
        End If

        If (IsTestMode = True) Then
            sbkey.AppendFormat("TEST_{0}_", IsTestMode)
        End If

        Try
            If (IsAuroraRequest = True) Then
                If (Not ConfigurationManager.AppSettings("cachealways_auroraalternate") Is Nothing And IsAuroraRequest = True) Then
                    If (ConfigurationManager.AppSettings("cachealways_auroraalternate").ToString = "true") Then
                        sbkey.Append("AURORAREQUEST")
                    End If
                End If
            Else
                If (Not ConfigurationManager.AppSettings("cachealways_b2cchannelalternate") Is Nothing And IsAuroraRequest = False) Then
                    If (ConfigurationManager.AppSettings("cachealways_b2cchannelalternate").ToString = "true") Then
                        sbkey.Append("B2CREQUEST")
                    End If
                End If

            End If
        Catch ex As Exception
            Helper.LogInformation("Getvehiclelist", "ConfigurationManager.AppSettings(cachealways_auroraalternate) or ConfigurationManager.AppSettings(cachealways_b2cchannelalternate)dont exist in web.config")
        End Try



        'If (IsAuroraRequest = True) Then
        '    sbkey.Append("AURORAREQUEST")
        'Else
        '    sbkey.Append("B2CREQUEST")
        'End If



        Dim key As String = sbkey.ToString
        Dim _cacheservice As New CacheService(Nothing, CachingObjectType.datatables)
        If _cacheservice.isExist(key) Then
            Return _cacheservice.Retrieve(key)
        End If



        Dim dstResult As New DataSet
        ''rev:mia Oct.24, 2013 - change number of parameter
        Dim params(15) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sBrdCode", DbType.String, Brand, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, CountryCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("nAdults", DbType.Int16, NumberOfAdults, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(5) = New Aurora.Common.Data.Parameter("nChildren", DbType.Int16, NumberOfChildren, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("nInfants", DbType.Int16, NumberOfInfants, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("bIsVan", DbType.Boolean, IsVan, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("bIsThriftyLocation", DbType.Boolean, IsThriftyLocation, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sVehicleCode", DbType.String, VehicleCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(10) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.DateTime, CheckOutDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.DateTime, CheckInDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(12) = New Aurora.Common.Data.Parameter("sAgnCode", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("bIsB2B", DbType.Boolean, IsTestMode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ''rev:mia Oct.24, 2013 - added spkgcode
        params(14) = New Aurora.Common.Data.Parameter("sPkgCode", DbType.String, sPkgCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(15) = New Aurora.Common.Data.Parameter("sErrorMessage", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetVehicles", params)

        ''rev:mia Oct.24, 2013 - get error message from param(15)
        ErrorMessage = params(15).Value.ToString()


        If dstResult.Tables.Count > 0 Then
            _cacheservice.Add(key, dstResult.Tables(0))
            Return dstResult.Tables(0)

        End If

    End Function

    Public Shared Function GetBrandsForVehicleCode(ByVal VehicleCode As String) As ArrayList
        Dim alResult As New ArrayList
        Dim dtResult As New DataTable

        dtResult = ExecuteTableSP("WEBP_GetBrandForVehicleCode", dtResult, VehicleCode)

        If dtResult IsNot Nothing Then
            For Each rwResult As DataRow In dtResult.Rows
                alResult.Add(rwResult("BrandCode").ToString())
            Next
        End If

        Return alResult
    End Function

    Public Shared Function GetFleetStatus(ByVal ProductShortName As String, _
                                          ByVal CheckOutLocationCode As String, _
                                          ByVal CheckOutDate As DateTime, _
                                          ByVal CheckInLocationCode As String, _
                                          ByVal CheckInDate As DateTime, _
                                          ByVal AgentCode As String, _
                                          ByVal PackageCode As String, _
                                          ByRef ErrorMessage As String, _
                                          ByRef DvassSequenceNumber As Int64) As Char

        Dim params(12) As Aurora.Common.Data.Parameter, sAvail As String, sErrors As String, sWarnings As String

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sPrdSrtNam", DbType.String, ProductShortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, AURORA_USER_CODE, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, APP_NAME, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(9) = New Aurora.Common.Data.Parameter("sAvail", DbType.String, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(10) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(11) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(12) = New Aurora.Common.Data.Parameter("sDVASSSeqNum", DbType.Int64, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("WEBP_CheckAvailability", params)

        sAvail = params(9).Value

        sErrors = params(10).Value
        sWarnings = params(11).Value
        DvassSequenceNumber = params(12).Value

        If String.IsNullOrEmpty(sErrors) AndAlso String.IsNullOrEmpty(sWarnings) Then
            Return sAvail
        Else
            ErrorMessage = sErrors & " " & sWarnings
            Return "N"
        End If

    End Function

    Public Shared Function GetPackageId(ByVal PackageCode As String, ByVal CheckOutLocationCode As String, _
                          ByVal CheckOutDate As DateTime, ByVal CheckInDate As DateTime) As String

        ' CREATE PROC WEBP_GetPackageId
        ' @sPkgCode VARCHAR(64)
        ',@sCkoLocCode VARCHAR(12)
        ',@dCkoDate DATETIME
        ',@dCkiDate DATETIME
        ',@sPkgId VARCHAR(64) OUTPUT

        Dim params(4) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sPkgCode", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(4) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("WEBP_GetPackageId", params)

        Return params(4).Value

    End Function

    ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct
    Public Shared Function GetVehiclePrice(ByVal AvpId As String, ByVal ProductShortName As String, ByVal AgentCode As String, ByVal CheckOutLocationCode As String, ByVal CheckInLocationCode As String, _
                             ByVal CheckOutDate As DateTime, ByVal CheckInDate As DateTime, ByVal CountryOfResidence As String, ByVal PackageCode As String, _
                             ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, ByVal NoOfInfants As Int16, _
                             ByVal BrandCode As String, ByVal IsInclusivePackage As Boolean, ByVal IsBestBuy As Boolean, ByVal IsTestMode As Boolean, _
                             ByRef IsGross As Boolean, ByRef ErrorMessage As String, Optional isAuroraCall As Boolean = False, Optional AgnisInclusiveProduct As Boolean = True, _
                             Optional sPromoCode As String = "") As String
        'ALTER PROCEDURE [dbo].[WEBP_GetVehiclePrice]  
        '0:' @sAvpId				VARCHAR(64)		= NULL,   -- Only require when request for Inclusive Package version, also set @bInclusive = 1  

        '1:' @sBrdcode				VARCHAR(3)		= '',  
        '2:' @sAgnCode				VARCHAR(64),  
        '3:' @sPrdShortName			VARCHAR(24),  
        '4:' @sCountryOfResidence	VARCHAR(12)		= '',  

        '5:' @sCkoLoc				VARCHAR(12),  
        '6:' @dCkoDate				DATETIME,  
        '7:' @sCkiLoc				VARCHAR(12),   
        '8:' @dCkiDate				DATETIME,  

        '9:' @NumberOfAdults		INT				= 0,  
        '10:' @NumberOfChildren		INT				= 0,  
        '11:' @NumberOfInfants		INT				= 0,  

        '12:' @sPkgCode				VARCHAR(64)		= '',  

        '13:' @sPrgName				VARCHAR(64)		= 'WEB',  
        '14:' @DvassValue			VARCHAR(5000)	= '',  

        '15:' @bInclusive			BIT				= 0,    -- Only require when request for Inclusive Package version, also set @bInclusive = 1  
        '16:' @bBestBuy				BIT				= 1,  
        '17:' @bTestMode				BIT				= 0,  
        '18:' @sErrors				VARCHAR(1000)	OUTPUT,  
        '19:' @sWarnings				VARCHAR(1000)	OUTPUT  

        Dim params(22) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        'params(0) = New Aurora.Common.Data.Parameter("sAvpId", DbType.String, IIf(String.IsNullOrEmpty(AvpId), AvpId, DBNull.Value), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(0) = New Aurora.Common.Data.Parameter("sAvpId", DbType.String, AvpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sBrdcode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sAgnCode", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sPrdShortName", DbType.String, ProductShortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sCountryOfResidence", DbType.String, CountryOfResidence, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(5) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(9) = New Aurora.Common.Data.Parameter("NumberOfAdults", DbType.Int16, NoOfAdults, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("NumberOfChildren", DbType.Int16, NoOfChildren, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("NumberOfInfants", DbType.Int16, NoOfInfants, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'params(12) = New Aurora.Common.Data.Parameter("sPkgCode", DbType.String, IIf(String.IsNullOrEmpty(PackageCode), PackageCode, DBNull.Value), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sPkgCode", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("bAgnisInclusiveProduct", DbType.Boolean, AgnisInclusiveProduct, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(14) = New Aurora.Common.Data.Parameter("sPromoCode", DbType.String, IIf(String.IsNullOrEmpty(sPromoCode), sPromoCode, DBNull.Value), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("sPromoCode", DbType.String, sPromoCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(15) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, APP_NAME, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(16) = New Aurora.Common.Data.Parameter("DvassValue", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params() = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, "SP7", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(17) = New Aurora.Common.Data.Parameter("bInclusive", DbType.Boolean, IsInclusivePackage, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(18) = New Aurora.Common.Data.Parameter("bBestBuy", DbType.Boolean, IsBestBuy, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(19) = New Aurora.Common.Data.Parameter("bTestMode", DbType.Boolean, IsTestMode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(17) = New Aurora.Common.Data.Parameter("bTestMode", DbType.Boolean, IsTestMode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(20) = New Aurora.Common.Data.Parameter("bGross", DbType.Boolean, IsGross, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(21) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(22) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        ''rev:mia 07July2014 - adding of @bAgnisInclusiveProduct



        Dim dstResult As DataSet = New DataSet
        Dim spname As String = ""
        Try

            spname = IIf(isAuroraCall = False, "WEBP_GetVehiclePrice", "WEBP_GetAvailabilityForAurora")
            ''dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetVehiclePrice", params)
            dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP(spname, params)


        Catch ex As System.Data.SqlClient.SqlException
            Aurora.Common.Logging.LogError(PROGRAM_NAME, ex.Message)
            If DEBUG_MODE Then
                ErrorMessage = SanitizedExceptions(ex.Message)
            Else
                ErrorMessage = PUBLIC_ERROR_MESSAGE
            End If
            Return ""
        End Try

        If String.IsNullOrEmpty(params(21).Value) AndAlso String.IsNullOrEmpty(params(22).Value) Then
            Return ReadXMLStringFromDataSet(dstResult)
        Else
            ErrorMessage = params(19).Value & params(20).Value
            Return ""
        End If

    End Function

    Public Shared Function GetLocation(ByVal LocationZoneCode As String, _
                                       ByVal BrandCode As String, _
                                       ByVal ClassCode As String, _
                                       ByVal CountryCode As String) As DataTable
        'ALTER PROC WEBP_GetLocationFromZoneAndTownCity -- 'NZ','THL','P','CIF','AKL',1
        ' @sCtyCode AS VARCHAR(2) = ''
        ',@sComCode AS VARCHAR(3) = ''
        ',@sBrdCode AS VARCHAR(1) = ''
        ',@sZoneCode AS VARCHAR(3)
        ',@sTctCode AS VARCHAR(3)
        ',@bTestMode AS BIT = 0
        'Dim oResult As Object
        'oResult = ExecuteScalarSP("WEBP_GetLocationFromZoneAndTownCity", CountryCode, CompanyCode, BrandCode, ZoneCode, TownCityCode, IsTestMode)
        'If oResult IsNot Nothing Then
        '    Return CStr(oResult)
        'Else
        '    Return String.Empty
        'End If

        'exec(WEBP_getLocCodeForZone)
        '    @sLocZoneCode     = 'SYD-CTY' ,
        '    @sBrdCode         = 'P',
        '    @sClaCode         = 'AV'

        'ALTER PROCEDURE [dbo].[WEBP_getLocCodeForZone]
        '@sLocZoneCode	VARCHAR(25),
        '@sBrdCode		VARCHAR(25),
        '@sClaCode		VARCHAR(5)		-- AV, AC, BK

        'Dim oResult As Object
        'oResult = ExecuteScalarSP("WEBP_getLocCodeForZone", LocationZoneCode, BrandCode, ClassCode, CountryCode)
        'If oResult IsNot Nothing Then
        '    Return CStr(oResult)
        'Else
        '    Return String.Empty
        'End If

        Dim dstResult As New DataSet

        dstResult = ExecuteDataSetSP("WEBP_getLocCodeForZone", dstResult, LocationZoneCode, BrandCode, ClassCode, CountryCode)

        If dstResult.Tables.Count > 0 Then
            Return dstResult.Tables(0)
        Else
            Return New DataTable
        End If

    End Function

    Public Shared Function RemoveSpecialCharacters(ByVal InputString As String) As String
        Return InputString.Replace("&", System.Web.HttpUtility.HtmlAttributeEncode("&"))
    End Function

    Public Shared Function GetBrandsForAgentCountry(ByVal AgentCode As String, ByVal CountryCode As String) As DataTable
        'WEBP_getBrandsForAgent
        Dim dtBrands As System.Data.DataTable = New System.Data.DataTable()
        dtBrands = Aurora.Common.Data.ExecuteTableSP("WEBP_getBrandsForAgent", dtBrands, CountryCode, AgentCode)
        Return dtBrands
    End Function

#End Region

#Region "Confirmation Mailing"

    Public Shared Function GetCodeId(ByVal CodeTypeNumber As Integer, ByVal Code As String) As String
        Dim sRetMsg As String
        '        sRetMsg = ExecuteScalarSQL("SELECT dbo.getCodeId(13,'Web')")
        '        sRetMsg = ExecuteScalarSQL("SELECT dbo.getCodeId(26,'Internal')")
        sRetMsg = ExecuteScalarSQL("SELECT dbo.getCodeId(" & CodeTypeNumber.ToString() & ",'" & Code & "')")
        Return sRetMsg
    End Function

#End Region

#Region "Create Booking + Extra Hire Items"

    ''' <summary>
    ''' This function is no longer in use, as the frontend is now provided the prrids and is expected to pass them in again
    ''' </summary>
    ''' <param name="UsageDate"></param>
    ''' <param name="SapId"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetPersonRates(ByVal UsageDate As DateTime, ByVal SapId As String) As XmlDocument
        Return ExecuteSqlXmlDoc("SELECT dbo.GetPersonRates(null,null,'" & UsageDate.ToString(DATE_FORMAT) & "','" & SapId & "',0,'')")
    End Function

    Public Shared Function GetExtraHireItems(ByVal AvpId As String, ByVal IsTestMode As Boolean) As XmlDocument
        '[WEBP_getInsurenceAndExtraVehicleProduct]
        Dim xmlReturnDoc As XmlDocument = New XmlDocument

        Dim sTarget As String

        If IsTestMode = True Then
            sTarget = "B2B-Web"
        Else
            sTarget = "B2C"
        End If

        xmlReturnDoc.LoadXml(ExecuteSqlXmlSP("WEBP_getInsurenceAndExtraVehicleProduct", AvpId, sTarget))

        Dim alProductCategories As New ArrayList

        If xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts") IsNot Nothing Then
            For Each oProductCategoryNode As XmlNode In xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts").ChildNodes
                alProductCategories.Add(oProductCategoryNode.Name)
            Next
        End If


        Dim oNavigator As XPathNavigator = xmlReturnDoc.CreateNavigator()
        Dim sbReturnXMLString As New StringBuilder

        For Each sProductCategory As String In alProductCategories
            Dim sXPath As String = "/data/ConfigurationProducts/" & sProductCategory & "/Product"
            Dim expression As XPathExpression = oNavigator.Compile(sXPath)

            expression.AddSort("@DisplayOrder", XmlSortOrder.Ascending, XmlCaseOrder.None, String.Empty, XmlDataType.Number)

            Dim oNodeIterator As XPathNodeIterator = oNavigator.Select(expression)

            ' store the sorted list in a string
            While oNodeIterator.MoveNext()
                sbReturnXMLString.Append(oNodeIterator.Current.OuterXml)
            End While

            ' put the sorted list into the doc
            xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts/" & sProductCategory).InnerXml = sbReturnXMLString.ToString()
            sbReturnXMLString = New StringBuilder
        Next

        Return xmlReturnDoc
    End Function

    Public Shared Function CreateBookingWithPayment(ByVal AddBookingNumber As String, ByVal AvpId As String, _
                                                    ByVal AgentReference As String, ByVal QuantityRequested As Int16, _
                                                    ByVal Status As String, _
                                                    ByVal Title As String, ByVal FirstName As String, ByVal LastName As String, _
                                                    ByVal Email As String, ByVal PhoneNumber As String, _
                                                    ByVal CountryOfResidence As String, ByVal PreferredModeOfCommunication As String, _
                                                    ByVal Note As String, ByVal CardType As String, _
                                                    ByVal PaymentAmount As Decimal, ByVal PaymentSurchargeAmount As Decimal, _
                                                    ByVal CreditCardNumber As String, ByVal CreditCardName As String, _
                                                    ByVal ApprovalRef As String, ByVal ExpiryDate As String, _
                                                    ByVal DPSAuthCode As String, ByVal DPSTxnRef As String, _
                                                    ByVal ProgramName As String, _
                                                    ByVal LoyaltyCardNumber As String, _
                                                    ByRef ReturnedBookingNumber As String, _
                                                    ByRef ReturnedCKOCode As String, _
                                                    ByRef ReturnedCKICode As String, _
                                                    ByRef ReturnedWarningMessage As String, _
                                                    ByRef ReturnedErrorMessage As String, _
                                                    ByRef sUserCode As String, _
                                                    ByVal SelectedSlot As String, _
                                                    ByVal SlotId As String _
                                                    ) As Boolean
        ' CREATE Procedure [dbo].[WEBP_CreateBookingWithPayment]  
        '	0	        '@sAddBooNum  VARCHAR(64)   = NULL,  -- Optional Booking number. Please provide this number if you want to force specific booking number  
        '		
        '	1	        '@sAvpId   VARCHAR(64),      -- Mandatory Available Package Id  
        '	2	        '@spAgnRef  VARCHAR(24)   = '',   
        '		
        '	3	        '@iQtyRequested INTEGER    = 1,  
        '	4	        '@sStatus  VARCHAR(20),      -- QN - Quote, KB - Knock Back, WL - Waitlist, KK - Confirm  
        '		
        '		        '-- Customer details  
        '	5	        '@spTitle  VARCHAR(10),  
        '	6	        '@spFirstName VARCHAR(128),  
        '	7	        '@spLastName  VARCHAR(128),  
        '	8	        '@sEmail   VARCHAR(8000)  = '',  
        '	9	        '@sTelPhNum  VARCHAR(8000)  = '',  
        '	10	        '@sCtyCode  VARCHAR(64)   = '',  
        '	11	        '@sPrefModComm VARCHAR(25)   = '',  -- Email,Phone  
        '		
        '	12	        '@sRntNote  VARCHAR(8000)  = '',  
        '		
        '	13	        '@sCardType   VARCHAR(256) = NULL,  
        '	14	        '@mPmtAmt   MONEY   = NULL,  -- Amount including surcharge  
        '	15	        '@mPaymentSurAmt  MONEY   = NULL,  -- Surcharge Amount   
        '	16	        '@sCrCardNum   VARCHAR(256) = NULL,  -- Card number is mandatory to add payment if NULL or Blank payment will not be added in Aurora  
        '	17	        '@sName    VARCHAR(256) = NULL,  
        '	18	        '@sApprovRef   VARCHAR(256) = NULL,  
        '	19	        '@sExpiryDt   VARCHAR(20)  = NULL,  
        '	20	        '@sDpsAuthCode  VARCHAR(64)  = NULL,   
        '	21	        '@sDpsTxnRef   VARCHAR(64)  = NULL,  -- B2C DPS TxnRef  
        '		        '-- END Payment details  
        '	22	        '@sPrgName   VARCHAR(64)  = 'WEB',  

        '   23          '@sLoyaltyCardNumber VARCHAR(64)		=  NULL, -- Shoel : 14.6.10 : Change for Loyalty Card Number

        '		
        '	24	        '@sBooNum   VARCHAR(24)  OUTPUT,  
        '	25	        '@sCkoLocCode  VARCHAR(24)  OUTPUT,  
        '	26	        '@sCkiLocCode  VARCHAR(24)  OUTPUT,  
        '	27	        '@sErrors   VARCHAR(1000) OUTPUT,  
        '	28	        '@sWarnings   VARCHAR(1000) OUTPUT 

        'Changed on 14thJan 2015 - Nimesh
        Dim params(31) As Aurora.Common.Data.Parameter
        'End Changed on 14thJan 2015 - Nimesh
        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sAddBooNum", DbType.String, AddBookingNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sAvpId", DbType.String, AvpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("spAgnRef", DbType.String, AgentReference, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("iQtyRequested", DbType.Int16, QuantityRequested, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sStatus", DbType.String, Status, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(5) = New Aurora.Common.Data.Parameter("spTitle", DbType.String, Title, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("spFirstName", DbType.String, FirstName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("spLastName", DbType.String, LastName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sEmail", DbType.String, Email, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sTelPhNum", DbType.String, PhoneNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(10) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, CountryOfResidence, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("sPrefModComm", DbType.String, PreferredModeOfCommunication, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sRntNote", DbType.String, Note, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sCardType", DbType.String, CardType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("mPmtAmt", DbType.Decimal, PaymentAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(15) = New Aurora.Common.Data.Parameter("mPaymentSurAmt", DbType.Decimal, PaymentSurchargeAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(16) = New Aurora.Common.Data.Parameter("sCrCardNum", DbType.String, CreditCardNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(17) = New Aurora.Common.Data.Parameter("sName", DbType.String, CreditCardName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(18) = New Aurora.Common.Data.Parameter("sApprovRef", DbType.String, ApprovalRef, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(19) = New Aurora.Common.Data.Parameter("sExpiryDt", DbType.String, ExpiryDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(20) = New Aurora.Common.Data.Parameter("sDpsAuthCode", DbType.String, DPSAuthCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(21) = New Aurora.Common.Data.Parameter("sDpsTxnRef", DbType.String, DPSTxnRef, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(22) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' Shoel : 14.6.10 : Change for Loyalty Card Number
        params(23) = New Aurora.Common.Data.Parameter("sLoyaltyCardNumber", DbType.String, LoyaltyCardNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' Shoel : 14.6.10 : Change for Loyalty Card Number

        ' OUTPUT parameters
        params(24) = New Aurora.Common.Data.Parameter("sBooNum", DbType.String, 24, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(25) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, 24, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(26) = New Aurora.Common.Data.Parameter("sCkiLocCode", DbType.String, 24, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(27) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(28) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(29) = New Aurora.Common.Data.Parameter("sSelectedSlot", DbType.String, SelectedSlot, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' Nim : Change for adding SlotId
        'Changed on 14thJan 2015 - Nimesh
        params(30) = New Aurora.Common.Data.Parameter("sSelectedSlotId", DbType.Int32, CType(SlotId, Int32), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'End Changed on 14thJan 2015 - Nimesh
        params(31) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        Dim dstResult As DataSet = New DataSet
        dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_CreateBookingWithPayment", params)

        If String.IsNullOrEmpty(params(27).Value) AndAlso String.IsNullOrEmpty(params(28).Value) Then
            ReturnedBookingNumber = params(24).Value
            ReturnedCKOCode = params(25).Value
            ReturnedCKICode = params(26).Value

            Return True
        Else
            ReturnedWarningMessage = params(28).Value
            ReturnedErrorMessage = params(27).Value

            Throw New Exception("Warning=" & params(28).Value & vbNewLine & "Error=" & params(27).Value)
            Aurora.Common.Logging.LogError(PROGRAM_NAME, params(28).Value & vbNewLine & params(27).Value)
            Return False
        End If

    End Function

    Public Shared Function GetDvassDetails(ByVal BookingNumber As String, ByVal RentalId As String) As XmlDocument

        'ALTER PROCEDURE [dbo].[WEB_GetDvassDetails]
        '@psBooNum	VARCHAR(12)	= NULL,
        '@psRntId	VARCHAR(64) = NULL,
        '@sErrors	VARCHAR(1000)	OUTPUT,
        '@sWarnings	VARCHAR(1000)	OUTPUT
        Dim xmlResult As XmlDocument = New XmlDocument
        Dim params(3) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("psBooNum", DbType.String, BookingNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("psRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(2) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(3) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Dim dstResult As DataSet = New DataSet
        dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEB_GetDvassDetails", params)

        If String.IsNullOrEmpty(params(2).Value) AndAlso String.IsNullOrEmpty(params(3).Value) Then
            xmlResult.LoadXml(ReadXMLStringFromDataSet(dstResult))
        Else
            Throw New Exception(params(2).Value & params(3).Value)
            Aurora.Common.Logging.LogError(PROGRAM_NAME, "Error=" & params(2).Value & vbNewLine & "Warning=" & params(3).Value)
        End If

        Return xmlResult

    End Function

    Public Shared Sub DeleteTProcessedBPDs(ByVal RentalId As String)
        Aurora.Common.Data.ExecuteScalarSP("RES_DeleteTProcessedBpds", RentalId)
        'exec RES_DeleteTProcessedBpds @sRntId='B3437251-1'
    End Sub

    Public Shared Function GetUserCodeForExtraHireItemAddition(ByVal AvpId As String) As String

        Dim params(1) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sAvpId", DbType.String, AvpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(1) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetUserCodeForExtraHireItemAddition", params)
        Return params(1).Value.ToString()

    End Function

#End Region

#Region "Self Checkout"
    Public Shared Function GetBookingInfo(ByVal BookingNumber As String, _
                                          ByVal CustomerFirstName As String, _
                                          ByVal CustomerLastName As String, _
                                          ByVal PickupLocationCode As String, _
                                          ByVal PickupDate As DateTime, _
                                          ByVal BpdID As String, _
                                          ByVal RntId As String, _
                                          ByRef ReturnedErrorMessage As String _
                                          ) As String

        '   ALTER PROCEDURE [dbo].[WEBA_getBookingDetails] 
        '   @sBooNum        VARCHAR(64)	= NULL,                      
        '   @sCusFirstName  VARCHAR(128)	= NULL,                  
        '   @sCusLastName   VARCHAR(128)	= NULL,                  
        '   @sPickupLocCode VARCHAR(12)	= NULL,                      
        '   @dPickupDate    DATETIME 	    = NULL, 
        '   @sBpdId		 VARCHAR(64)	= NULL,
        '   @sRetError      VARCHAR(128) OUTPUT                      

        Dim oParams(7) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        oParams(0) = New Aurora.Common.Data.Parameter("sBooNum", DbType.String, BookingNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(1) = New Aurora.Common.Data.Parameter("sCusFirstName", DbType.String, CustomerFirstName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(2) = New Aurora.Common.Data.Parameter("sCusLastName", DbType.String, CustomerLastName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(3) = New Aurora.Common.Data.Parameter("sPickupLocCode", DbType.String, PickupLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(4) = New Aurora.Common.Data.Parameter("dPickupDate", DbType.DateTime, PickupDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(5) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, BpdID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(6) = New Aurora.Common.Data.Parameter("psRntId", DbType.String, RntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        oParams(7) = New Aurora.Common.Data.Parameter("sRetError", DbType.String, 128, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Dim dstResult As DataSet = New DataSet
        dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_getBookingDetails", oParams)

        If String.IsNullOrEmpty(oParams(7).Value) Then
            ReturnedErrorMessage = ""
            Return ReadXMLStringFromDataSet(dstResult)
        Else
            ReturnedErrorMessage = oParams(7).Value
            Return ""
        End If

    End Function

    Public Shared Function GetExtraHireItemsForRental(ByVal RntId As String) As XmlDocument

        Dim xmlReturnDoc As XmlDocument = New XmlDocument
        xmlReturnDoc.LoadXml(ExecuteSqlXmlSP("WEBA_GetInsuranceAndExtraHireItems", RntId))

        'Dim alProductCategories As New ArrayList

        'If xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts") IsNot Nothing Then
        '    For Each oProductCategoryNode As XmlNode In xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts").ChildNodes
        '        alProductCategories.Add(oProductCategoryNode.Name)
        '    Next
        'End If

        'Dim oNavigator As XPathNavigator = xmlReturnDoc.CreateNavigator()
        'Dim sbReturnXMLString As New StringBuilder

        'For Each sProductCategory As String In alProductCategories
        '    Dim sXPath As String = "/data/ConfigurationProducts/" & sProductCategory & "/Product"
        '    Dim expression As XPathExpression = oNavigator.Compile(sXPath)

        '    expression.AddSort("@DisplayOrder", XmlSortOrder.Ascending, XmlCaseOrder.None, String.Empty, XmlDataType.Number)

        '    Dim oNodeIterator As XPathNodeIterator = oNavigator.Select(expression)

        '    ' store the sorted list in a string
        '    While oNodeIterator.MoveNext()
        '        sbReturnXMLString.Append(oNodeIterator.Current.OuterXml)
        '    End While

        '    ' put the sorted list into the doc
        '    xmlReturnDoc.DocumentElement.SelectSingleNode("ConfigurationProducts/" & sProductCategory).InnerXml = sbReturnXMLString.ToString()
        '    sbReturnXMLString = New StringBuilder
        'Next

        Return xmlReturnDoc
    End Function

    Public Shared Sub GetDetailsFromRentalId(ByVal RentalId As String, _
                                             Optional ByRef ProductId As String = Nothing, _
                                             Optional ByRef CheckOutDateTime As DateTime = Nothing, _
                                             Optional ByRef CheckInDateTime As DateTime = Nothing, _
                                             Optional ByRef CheckOutLocationCode As String = Nothing, _
                                             Optional ByRef CheckInLocationCode As String = Nothing, _
                                             Optional ByRef AgentId As String = Nothing, _
                                             Optional ByRef PackageId As String = Nothing, _
                                             Optional ByRef BookingId As String = Nothing, _
                                             Optional ByRef BookedDateTime As DateTime = Nothing, _
                                             Optional ByRef RentalNo As String = Nothing _
                                            )
        'ALTER PROCEDURE [dbo].[WEBA_GetDetailsFromRntId]  
        '@sRntId           VARCHAR(64) ,
        '@sPrdId           VARCHAR(64) OUTPUT,  
        '@dCkoWhen         DATETIME OUTPUT,  
        '@dCkiWhen         DATETIME OUTPUT,  
        '@sCkoLocationCode VARCHAR(12) OUTPUT,  
        '@sCkiLocationCode VARCHAR(12) OUTPUT,  
        '@sAgnid           VARCHAR(64) OUTPUT,  
        '@sPkgid           VARCHAR(64) OUTPUT,  
        '@sBooId           VARCHAR(64) OUTPUT,
        '@dBooWhen         DATETIME OUTPUT   

        Dim params(10) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(1) = New Aurora.Common.Data.Parameter("sPrdId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(2) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.DateTime, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(3) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.DateTime, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(4) = New Aurora.Common.Data.Parameter("sCkoLocationCode", DbType.String, 12, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(5) = New Aurora.Common.Data.Parameter("sCkiLocationCode", DbType.String, 12, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(6) = New Aurora.Common.Data.Parameter("sAgnid", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(7) = New Aurora.Common.Data.Parameter("sPkgid", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(8) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(9) = New Aurora.Common.Data.Parameter("dBooWhen", DbType.DateTime, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(10) = New Aurora.Common.Data.Parameter("nRentalNo", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_GetDetailsFromRntId", params)

        ' Set the values
        ProductId = CStr(params(1).Value)
        CheckOutDateTime = CDate(params(2).Value)
        CheckInDateTime = CDate(params(3).Value)
        CheckOutLocationCode = CStr(params(4).Value)
        CheckInLocationCode = CStr(params(5).Value)
        AgentId = CStr(params(6).Value)
        PackageId = CStr(params(7).Value)
        BookingId = CStr(params(8).Value)
        BookedDateTime = CDate(params(9).Value)
        RentalNo = CStr(params(10).Value)

    End Sub

    Public Shared Function SSTransToFinance(ByVal BookingId As String, ByVal RentalId As String, ByVal SaleableProductId As String, ByVal BookedProductId As String) As String
        'CREATE PROCEDURE [dbo].[RES_ssTransToFinance]  
        '@sBooId   VARCHAR  (64) = NULL ,  
        '@sRntId   VARCHAR  (64) = NULL ,  
        '@sSapId   VARCHAR  (64) = NULL ,  
        '@sBpdId   VARCHAR  (64) = NULL  
        Return Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", BookingId, RentalId, SaleableProductId, BookedProductId)
    End Function

    ''rev:mia April 11,2012 - Addition of CVC2 to capture in the service
    ''rev:mia May 24,2012 - Addition of BillingToken to capture in the service
    Public Shared Function AddPayment(ByVal RentalId As String, _
                                      ByVal CardType As String, _
                                      ByVal PaymentAmount As Decimal, _
                                      ByVal PaymentSurchargeAmount As Decimal, _
                                      ByVal PaymentType As Char, _
                                      ByVal CreditCardNumber As String, _
                                      ByVal CreditCardName As String, _
                                      ByVal ExpiryDate As String, _
                                      ByVal DPSTxnRef As String, _
                                      ByVal ProgramName As String, _
                                      Optional ByRef ReturnedWarningMessage As String = "", _
                                      Optional ByRef ReturnedErrorMessage As String = "", _
                                      Optional ByVal cvc2 As String = "", _
                                      Optional ByVal BillingToken As String = "") As Boolean

        'CREATE Procedure [dbo].[WEBA_AddPayment]    
        '@sRntId  VARCHAR(64)   = NULL,  

        '@sCardType   VARCHAR(256) = NULL,    
        '@mPmtAmt   MONEY   = NULL,  -- Amount including surcharge    
        '@mPaymentSurAmt  MONEY   = NULL,  -- Surcharge Amount     
        '@sPmtType VARCHAR(1) = 'R', -- R = Rental, B = Bond
        '@sCrCardNum   VARCHAR(256) = NULL,  -- Card number is mandatory to add payment if NULL or Blank payment will not be added in Aurora    
        '@sName    VARCHAR(256) = NULL,    
        '@sExpiryDt   VARCHAR(20)  = NULL,    
        '@sDpsTxnRef   VARCHAR(64)  = NULL,  -- B2C DPS TxnRef    
        '-- END Payment details    
        '@sPrgName   VARCHAR(64)  = 'WEB',    

        '@sErrors   VARCHAR(1000) OUTPUT,    
        '@sWarnings   VARCHAR(1000) OUTPUT      

        ''rev:mia April 11,2012 - Addition of CVC2 to capture in the service
        ''rev:mia May 24,2012 - Addition of BillingToken to capture in the service
        Dim params(13) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCardType", DbType.String, CardType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("mPmtAmt", DbType.Decimal, PaymentAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("mPaymentSurAmt", DbType.Decimal, PaymentSurchargeAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sPmtType", DbType.String, PaymentType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(5) = New Aurora.Common.Data.Parameter("sCrCardNum", DbType.String, CreditCardNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sName", DbType.String, CreditCardName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sExpiryDt", DbType.String, ExpiryDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sDpsTxnRef", DbType.String, DPSTxnRef, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(10) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(11) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        ''rev:mia April 11,2012 - Addition of CVC2 to capture in the service
        params(12) = New Aurora.Common.Data.Parameter("CVC2", DbType.String, cvc2, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ''rev:mia May 24,2012 - Addition of BillingToken to capture in the service
        params(13) = New Aurora.Common.Data.Parameter("PmtBillToToken", DbType.String, BillingToken, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_AddPayment", params)

        If String.IsNullOrEmpty(params(10).Value) AndAlso String.IsNullOrEmpty(params(11).Value) Then
            Return True
        Else
            ReturnedWarningMessage = params(11).Value
            ReturnedErrorMessage = params(10).Value

            Aurora.Common.Logging.LogError(PROGRAM_NAME, params(10).Value & vbNewLine & params(11).Value)
            Return False
        End If

    End Function

    Public Shared Function GetPaymentDetails(ByVal RentalId As String, ByRef ShouldContinue As Boolean) As XmlDocument
        'ALTER PROCEDURE WEBA_getPaymentDetails  
        '   @sRntId   VARCHAR(64),  
        '   @bContinueCKO BIT OUTPUT  

        Dim oParams(1) As Aurora.Common.Data.Parameter
        Dim xmlResult As New XmlDocument
        Dim dstResult As DataSet = New DataSet

        ' INPUT parameters
        oParams(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        oParams(1) = New Aurora.Common.Data.Parameter("bContinueCKO", DbType.Int16, 16, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_getPaymentDetails", oParams)

        If oParams(1) Is Nothing Then
            ShouldContinue = False
        Else
            'ShouldContinue = CBool(oParams(1).Value)
            If CInt(oParams(1).Value) = 1 Then
                ShouldContinue = True
            Else
                ShouldContinue = False
            End If
        End If

        xmlResult.LoadXml(ReadXMLStringFromDataSet(dstResult))

        Return xmlResult

    End Function

    Public Shared Sub GetUnitDetails(ByVal UnitNumber As String, ByVal RentalId As String, ByRef RegistrationNumber As String, ByRef OdometerOut As Integer)

        'ALTER PROC WEBA_GetUnitDetails
        '@sUnitNumber AS VARCHAR(12),
        '@sRego AS VARCHAR(50) OUTPUT,
        '@iOdometerIn AS INT OUTPUT, 
        '@iOdometerOut AS INT OUTPUT 

        Dim params(3) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sUnitNumber", DbType.String, UnitNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sRentalId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(2) = New Aurora.Common.Data.Parameter("sRego", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(3) = New Aurora.Common.Data.Parameter("iOdometerOut", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_GetUnitDetails", params)

        RegistrationNumber = CStr(params(2).Value)
        OdometerOut = CInt(params(3).Value)

    End Sub

    Public Shared Function GetLocationsForSelfCheckOut(ByVal CountryCode As String, ByVal CompanyCode As String, ByVal BrandCode As String) As DataTable
        Dim dtResult As New DataTable
        dtResult = Aurora.Common.Data.ExecuteTableSP("WEBA_GetLocationsForSelfCheckOut", dtResult, CountryCode, CompanyCode, BrandCode)
        If String.IsNullOrEmpty(dtResult.TableName) Then
            dtResult.TableName = "MasterCountryLocationData"
        End If
        Return dtResult
    End Function

    ''rev:mia 04nov2014 - SLOT MANAGEMENT - Addition of RntSelectedSlotId and  RntSelectedSlot
    Public Shared Function SaveArrivalAndDepartureDetails(ByVal RentalId As String, ByVal ArrivalReference As String, _
                                                          ByVal ArrivalDateTime As DateTime, ByVal DepartureReference As String, _
                                                          ByVal DepartureDateTime As DateTime, ByVal ArrivalAtBranchDateTime As String, ByVal DropOffAtBranchDateTime As DateTime, _
                                                          ByVal UserCode As String, _
                                                          ByRef ErrorMessage As String, _
                                                          Optional RntSelectedSlotId As String = "", _
                                                          Optional RntSelectedSlot As String = "") As Boolean
        Dim bResult As Boolean = False

        Try

            'CREATE PROC WEBA_UpdateRentalArrivalAndDepartureDetails
            '@sRntId VARCHAR(64),
            '@sArrRef VARCHAR(64),
            '@dArrDate DATETIME,
            '@DeptRef VARCHAR(64),
            '@dDeptDate DATETIME,
            '@sUsrCode VARCHAR(64)

            'Aurora.Common.Data.ExecuteScalarSP("WEBA_UpdateRentalArrivalAndDepartureDetails", RentalId, ArrivalReference, ArrivalDateTime, DepartureReference, DepartureDataTime, UserCode)
            '-- RKS : Mod 25-May-2012
            '-- Changes for Mighty
            'Dim params(6) As Aurora.Common.Data.Parameter

            ''rev:mia 04nov2014 -SLOT MANAGEMENT
            Dim params(9) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sArrRef", DbType.String, ArrivalReference, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("dArrDate", DbType.DateTime, IIf(ArrivalDateTime = DateTime.MinValue, System.DBNull.Value, ArrivalDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("DeptRef", DbType.String, DepartureReference, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("dDeptDate", DbType.DateTime, IIf(DepartureDateTime = DateTime.MinValue, System.DBNull.Value, DepartureDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("dArrAtBranchDate", DbType.DateTime, IIf(ArrivalAtBranchDateTime = DateTime.MinValue, System.DBNull.Value, ArrivalAtBranchDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("dRntDropOffAtBranchDateTime", DbType.DateTime, IIf(DropOffAtBranchDateTime = DateTime.MinValue, System.DBNull.Value, DropOffAtBranchDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, UserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("RntSelectedSlotId", DbType.Int32, IIf(String.IsNullOrEmpty(RntSelectedSlotId), 0, RntSelectedSlotId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("RntSelectedSlot", DbType.String, IIf(String.IsNullOrEmpty(RntSelectedSlot), DBNull.Value, RntSelectedSlot), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)


            Aurora.Common.Data.ExecuteOutputSP("WEBA_UpdateRentalArrivalAndDepartureDetails", params)

            bResult = True

        Catch ex As Exception
            ErrorMessage = ex.Message + vbNewLine + ex.StackTrace
        End Try

        Return bResult
    End Function

#End Region

#Region "Save Quote Functionality Stuff"

    Public Shared Function ModifyBooking(ByVal RentalId As String, ByVal CheckOutLocationCode As String, _
                                         ByVal CheckInLocationCode As String, ByVal CheckOutDateTime As DateTime, _
                                         ByVal CheckInDateTime As DateTime, ByVal NewRentalStatus As String, _
                                         ByVal NoteText As String, ByVal AgentReference As String, _
                                         ByVal UserId As String, ByVal ProgramName As String, ByVal SelectedSlot As String, ByVal SlotId As String) As XmlDocument
        'ALTER  PROCEDURE	[dbo].[WEB_ModifyBooking]
        '0	@sExtRef			VARCHAR	 (200) ,			--	BookingNum/RentalNum
        '1	@sCkoLocCode		VARCHAR	 (64) ,				--	Check-Out Location Code (Screen value)
        '2	@sCkiLocCode		VARCHAR	 (64) ,				--	Check-In Location Code 	(Screen value)
        '3	@dCkoWhen			DATETIME ,					--	Check-Out Date and Time (Screen value)
        '4	@dCkiWhen			DATETIME ,					--	Check-In Date and Time  (Screen value)
        '5	@sStatus			VARCHAR	 (64) ,				--	Status of rental		(Screen value)

        '	--	Customer Details
        '6	@spFirstName		VARCHAR(128)	=	'',	--	Given Name
        '7	@spLastName			VARCHAR(128)	=	'',	--	SurName
        '8	@spTitle			VARCHAR(10)		=	'',	--	Name Prefix
        '9	@sLicNum			VARCHAR(256)	=	'',
        '10	@sIssuAuth			VARCHAR(256)	=	'',
        '11	@dExpDate			DATETIME		=	NULL,
        '12	@dDBO				DATETIME		=	NULL,
        '13	@sAdd1				VARCHAR(64)		=	'',
        '14	@sAdd2				VARCHAR(64)		=	'',
        '15	@sCity				VARCHAR(12)		=	'',
        '16	@sCtyCode			VARCHAR(24)		=	'',
        '17	@sRntNote			VARCHAR(8000)	=	'',
        '18	@bIsPrimary			VARCHAR(1)		=	'1',
        '19	@sUsrId				VARCHAR	 (64) ,
        '20	@sPrgmName			VARCHAR	 (64) ,
        '21	@sAgnRef			VARCHAR(64)		=	'' ,
        '22	@bFleetUpdate		BIT 			OUTPUT ,	--	Status whether to modify DVASS or Not
        '23	@sErrors			VARCHAR(1000)	OUTPUT ,	
        '24	@sWarnings			VARCHAR(1000)	OUTPUT

        Dim params(26) As Aurora.Common.Data.Parameter
        Dim sbResultXMLText As New StringBuilder
        Dim xmlResult As New XmlDocument

        sbResultXMLText.Append("<data>")

        Try
            params(0) = New Aurora.Common.Data.Parameter("sExtRef", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("sCkiLocCode", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.DateTime, CheckOutDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.DateTime, CheckInDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("sStatus", DbType.String, NewRentalStatus, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            ' useless params
            params(6) = New Aurora.Common.Data.Parameter("spFirstName", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("spLastName", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("spTitle", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("sLicNum", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("sIssuAuth", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("dExpDate", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(12) = New Aurora.Common.Data.Parameter("dDBO", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(13) = New Aurora.Common.Data.Parameter("sAdd1", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(14) = New Aurora.Common.Data.Parameter("sAdd2", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(15) = New Aurora.Common.Data.Parameter("sCity", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(16) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(18) = New Aurora.Common.Data.Parameter("bIsPrimary", DbType.String, "0", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ' useless params

            params(17) = New Aurora.Common.Data.Parameter("sRntNote", DbType.String, NoteText, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(19) = New Aurora.Common.Data.Parameter("sUsrId", DbType.String, UserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(20) = New Aurora.Common.Data.Parameter("sPrgmName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(21) = New Aurora.Common.Data.Parameter("sAgnRef", DbType.String, AgentReference, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'Added by Nimesh on 27th Jan 15 for slot data
            params(22) = New Aurora.Common.Data.Parameter("sSelectedSlot", DbType.String, SelectedSlot, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(23) = New Aurora.Common.Data.Parameter("sSelectedSlotId", DbType.Int32, CType(SlotId, Int32), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'params(23) = New Aurora.Common.Data.Parameter("sSelectedSlotId", DbType.Int64, Int64.Parse(SlotId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'End added by nimesh on  27th jan

            params(24) = New Aurora.Common.Data.Parameter("bFleetUpdate", DbType.Boolean, 1, Parameter.ParameterType.AddOutParameter)
            params(25) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Parameter.ParameterType.AddOutParameter)
            params(26) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("WEB_ModifyBooking", params)

            sbResultXMLText.Append("<FleetUpdate>" & CBool(params(24).Value).ToString() & "</FleetUpdate>")

            If CStr(params(25).Value).Trim() <> "" OrElse CStr(params(26).Value).Trim() <> "" Then
                Throw New Exception("<Message>" & CStr(params(25).Value).Trim() & "</Message><Warning>" & CStr(params(26).Value).Trim() & "</Warning>")
            End If

        Catch ex As Exception
            'ErrorMessage = ex.Message + vbNewLine + ex.StackTrace
            sbResultXMLText.Append("<Error><MessageSection>" & ex.Message & "</MessageSection><StackTrace>" & ex.StackTrace & "</StackTrace></Error>")
        End Try

        sbResultXMLText.Append("</data>")

        xmlResult.LoadXml(sbResultXMLText.ToString())

        Return xmlResult

    End Function

    Public Shared Function RetrieveBooking(ByVal RentalNumber As String, ByVal BpdId As String, ByRef ErrorMessage As String) As XmlDocument
        Dim oParams(2) As Aurora.Common.Data.Parameter
        Dim xmlResult As New XmlDocument
        Dim dstResult As DataSet = New DataSet

        ' INPUT parameters
        oParams(0) = New Aurora.Common.Data.Parameter("sRentalNumber", DbType.String, RentalNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(1) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, BpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        oParams(2) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Try
            dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_RetrieveBooking", oParams)

            If oParams(2) IsNot Nothing AndAlso CStr(oParams(2).Value).Trim() <> "" Then
                ' error present
                ErrorMessage = CStr(oParams(2).Value).Trim()
            End If
            ' try loading the result xml dataset
            Try
                xmlResult.LoadXml(ReadXMLStringFromDataSet(dstResult))
            Catch
                ' do nothing
                xmlResult = New XmlDocument
                xmlResult.LoadXml("<data/>")
            End Try

        Catch ex As Exception
            ErrorMessage = ErrorMessage & " - " & ex.Message & " - " & ex.StackTrace
        End Try

        If Not String.IsNullOrEmpty(ErrorMessage) Then
            Dim xmlNodeTemp As XmlNode
            If xmlResult.OuterXml = "" Then
                xmlResult = New XmlDocument
                xmlResult.LoadXml("<data/>")
            End If
            xmlNodeTemp = xmlResult.CreateNode(XmlNodeType.Element, "Error", "")
            xmlNodeTemp.InnerXml = "<Details>" & PhoenixWebService.XmlEncode(ErrorMessage) & "</Details>"
            xmlResult.DocumentElement.AppendChild(xmlNodeTemp)
        End If

        Return xmlResult
    End Function

    Public Shared Function GetUserCodeForRentalModify(ByVal RentalId As String, ByRef UserId As String) As String

        Dim params(2) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(1) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, 24, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(2) = New Aurora.Common.Data.Parameter("sUsrId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetUserCodeForRentalModify", params)

        UserId = params(2).Value.ToString()
        Return params(1).Value.ToString()

    End Function

    ''' <summary>
    ''' This method retrieves the list of Extra Hire Items and Insurance Options for a given Booking Reference (eg. B123456/1) or reference id
    ''' </summary> 
    ''' <param name="RentalNumber">Booking Reference</param>
    ''' <param name="BpdId">Reference Id</param>
    ''' <returns>XML Doc with info</returns>
    ''' <remarks></remarks>
    Public Shared Function GetInsuranceAndExtraHireItemsForRentalQuote(ByVal RentalNumber As String, ByVal BpdId As String, ByRef ErrorMessage As String) As XmlDocument

        ' CREATE Procedure [dbo].[WEBP_GetInsuranceAndExtraHireItems]    
        ' @sRentalNumber VARCHAR(64),
        ' @sBpdId VARCHAR(64),
        ' @sErrors VARCHAR(MAX) OUTPUT

        Dim oParams(2) As Aurora.Common.Data.Parameter
        Dim xmlResult As New XmlDocument
        Dim dstResult As DataSet = New DataSet

        ' INPUT parameters
        oParams(0) = New Aurora.Common.Data.Parameter("sRentalNumber", DbType.String, RentalNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        oParams(1) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, BpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        oParams(2) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Try
            dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetInsuranceAndExtraHireItems", oParams)

            If oParams(2) IsNot Nothing AndAlso CStr(oParams(2).Value).Trim() <> "" Then
                ' error present
                ErrorMessage = CStr(oParams(2).Value).Trim()
            End If
            ' try loading the result xml dataset
            Try
                xmlResult.LoadXml(ReadXMLStringFromDataSet(dstResult))
            Catch
                ' do nothing
                xmlResult = New XmlDocument
                xmlResult.LoadXml("<data/>")
            End Try

        Catch ex As Exception
            ErrorMessage = ErrorMessage & " - " & ex.Message & " - " & ex.StackTrace
        End Try

        If Not String.IsNullOrEmpty(ErrorMessage) Then
            Dim xmlNodeTemp As XmlNode
            If xmlResult.OuterXml = "" Then
                xmlResult = New XmlDocument
                xmlResult.LoadXml("<data/>")
            End If
            xmlNodeTemp = xmlResult.CreateNode(XmlNodeType.Element, "Error", "")
            xmlNodeTemp.InnerXml = "<Details>" & PhoenixWebService.XmlEncode(ErrorMessage) & "</Details>"
            xmlResult.DocumentElement.AppendChild(xmlNodeTemp)
        End If

        Return xmlResult

    End Function

#End Region

#Region "Helper Methods"

    Public Shared Function ReadXMLStringFromDataSet(ByVal DBOutputDataSet As DataSet) As String
        Using oResult As New System.IO.StringWriter()
            oResult.Write("<data>")
            For Each dtTemp As DataTable In DBOutputDataSet.Tables
                For Each rwTemp As DataRow In dtTemp.Rows
                    oResult.Write(RemoveSpecialCharacters(rwTemp(0).ToString()))
                Next
            Next
            oResult.Write("</data>")
            Return oResult.ToString()
        End Using
    End Function

#End Region

#Region "ALTERNATEVEHICLEAVAILABILITY"
    ''this is for alternate availability when error happened
    Public Shared Function GetSingleVehicleInfo(ByVal vehicleCode As String) As DataView
        Dim result As New DataTable
        result = ExecuteTableSP("WEBP_GetSingleVehicle", result, vehicleCode)
        Return result.DefaultView
    End Function

#End Region

#Region "Converted System Exception"
    Private Shared Function SanitizedExceptions(ByVal message As String) As String
        Try
            Dim newstring As String = message.Replace("""", String.Empty)
            Return newstring.Replace("'", String.Empty)
        Catch ex As Exception
            Return message
        End Try

    End Function
#End Region

#Region "rev:mia Mighty"
    Public Shared Function TokenObject(ByVal PmtBillToRntId As String, _
                                       ByVal PmtBillToCardType As String, _
                                       ByVal PmtBillToCardName As String, _
                                       ByVal PmtBillCardNumber As String, _
                                       ByVal PmtBillToCardExpDate As String, _
                                       ByVal PmtBillToToken As String, _
                                       ByVal AddUsrId As String, _
                                       ByVal ModUsrId As String) As String

        Try
            If (PmtBillToCardType.Contains("Imprint") = True) Then
                PmtBillToCardType = PmtBillToCardType.Replace("Imprint", "").Trim
            End If
            Return Aurora.Common.Data.ExecuteScalarSP("BillingTokenInsert", PmtBillToRntId, _
                                                                        PmtBillToCardType, _
                                                                        PmtBillToCardName, _
                                                                        PmtBillCardNumber, _
                                                                        PmtBillToCardExpDate, _
                                                                        PmtBillToToken, _
                                                                        AddUsrId)

        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try


    End Function

    Public Shared Function MarkVideoShowAsViewed(ByVal RentalId As String, ByVal status As Integer) As String
        Try
            Return Aurora.Common.Data.ExecuteScalarSP("WEBP_UpdateRentalVideoShow", RentalId, status)

        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try

    End Function

    Public Shared Function AcceptTemrsAndCondtion(ByVal RentalId As String, ByVal status As Integer) As String
        Try
            Return Aurora.Common.Data.ExecuteScalarSP("WEBP_UpdateRentalAcceptTermsAndCondition", RentalId, status)

        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try

    End Function


    Public Shared Function RentalAgreement(ByVal RentalId As String, ByVal xslRentalAgreement As String) As String

        Dim HTMLoutput As String = "ERROR"
        Try

            Dim xmlRentalAgreementData As New XmlDocument
            xmlRentalAgreementData = Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement(RentalId)

            xmlRentalAgreementData.LoadXml("<Data>" & xmlRentalAgreementData.DocumentElement.InnerXml & "</Data>")

            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.OmitXmlDeclaration = True

            Dim oTransform As New XslCompiledTransform
            oTransform.Load(xslRentalAgreement)


            HTMLoutput = xmlRentalAgreementData.OuterXml

            Dim writer As New StringWriter()
            oTransform.Transform(xmlRentalAgreementData, Nothing, writer)
            HTMLoutput = writer.ToString()
            writer.Close()


        Catch ex As Exception
            HTMLoutput = "ERROR " & ex.Message
        End Try

        Return HTMLoutput

    End Function
#End Region

#Region "rev:mia Nov.26 2012 - alternate availability inside aurora"
    Private Shared Function GetAvailabilityForAurora(ByVal AvpId As String, ByVal ProductShortName As String, ByVal AgentCode As String, ByVal CheckOutLocationCode As String, ByVal CheckInLocationCode As String, _
                            ByVal CheckOutDate As DateTime, ByVal CheckInDate As DateTime, ByVal CountryOfResidence As String, ByVal PackageCode As String, _
                            ByVal NoOfAdults As Int16, ByVal NoOfChildren As Int16, ByVal NoOfInfants As Int16, _
                            ByVal BrandCode As String, ByVal IsInclusivePackage As Boolean, ByVal IsBestBuy As Boolean, ByVal IsTestMode As Boolean, _
                            ByRef IsGross As Boolean, ByRef ErrorMessage As String) As String


        Dim params(20) As Aurora.Common.Data.Parameter

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sAvpId", DbType.String, AvpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(1) = New Aurora.Common.Data.Parameter("sBrdcode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sAgnCode", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sPrdShortName", DbType.String, ProductShortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sCountryOfResidence", DbType.String, CountryOfResidence, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(5) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(9) = New Aurora.Common.Data.Parameter("NumberOfAdults", DbType.Int16, NoOfAdults, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("NumberOfChildren", DbType.Int16, NoOfChildren, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("NumberOfInfants", DbType.Int16, NoOfInfants, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(12) = New Aurora.Common.Data.Parameter("sPkgCode", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, APP_NAME, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("DvassValue", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params() = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, "SP7", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(15) = New Aurora.Common.Data.Parameter("bInclusive", DbType.Boolean, IsInclusivePackage, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(16) = New Aurora.Common.Data.Parameter("bBestBuy", DbType.Boolean, IsBestBuy, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(17) = New Aurora.Common.Data.Parameter("bTestMode", DbType.Boolean, IsTestMode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(17) = New Aurora.Common.Data.Parameter("bTestMode", DbType.Boolean, IsTestMode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(18) = New Aurora.Common.Data.Parameter("bGross", DbType.Boolean, IsGross, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(19) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(20) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Dim dstResult As DataSet = New DataSet
        Try
            dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBP_GetAvailabilityForAurora", params)
        Catch ex As System.Data.SqlClient.SqlException
            Aurora.Common.Logging.LogError(PROGRAM_NAME, ex.Message)
            If DEBUG_MODE Then
                ErrorMessage = SanitizedExceptions(ex.Message)
            Else
                ErrorMessage = PUBLIC_ERROR_MESSAGE
            End If
            Return ""
        End Try

        If String.IsNullOrEmpty(params(19).Value) AndAlso String.IsNullOrEmpty(params(20).Value) Then
            Return ReadXMLStringFromDataSet(dstResult)
        Else
            ErrorMessage = params(19).Value & params(20).Value
            Return ""
        End If

    End Function


    Public Shared Function GetAndSkipBlockingRuleForFleetStatus(ByVal ProductShortName As String, _
                                        ByVal CheckOutLocationCode As String, _
                                        ByVal CheckOutDate As DateTime, _
                                        ByVal CheckInLocationCode As String, _
                                        ByVal CheckInDate As DateTime, _
                                        ByVal AgentCode As String, _
                                        ByVal PackageCode As String, _
                                        ByRef ErrorMessage As String, _
                                        ByRef DvassSequenceNumber As Int64) As Char

        Dim params(12) As Aurora.Common.Data.Parameter, sAvail As String, sErrors As String, sWarnings As String

        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sPrdSrtNam", DbType.String, ProductShortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, AURORA_USER_CODE, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, "WEB_CheckOneDayAvailability", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(9) = New Aurora.Common.Data.Parameter("sAvail", DbType.String, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(10) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(11) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(12) = New Aurora.Common.Data.Parameter("sDVASSSeqNum", DbType.Int64, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("WEBP_CheckOneDayAvailability", params)

        sAvail = params(9).Value

        sErrors = params(10).Value
        sWarnings = params(11).Value
        DvassSequenceNumber = params(12).Value
        ErrorMessage = sErrors

        'ErrorMessage = String.Concat(sErrors, " ", sWarnings)
        'Try
        '    If (ErrorMessage.Contains("-") = True) Then

        '        Dim sAryWord As String() = ErrorMessage.Split("-")
        '        If (sAryWord.Length = 2) Then
        '            ErrorMessage = sAryWord(1).Trim
        '        Else
        '            ErrorMessage = sAryWord(2).Trim
        '        End If

        '    End If
        'Catch ex As Exception
        'End Try


        'If (ErrorMessage.Contains("Request Declined.") = True) Then
        '    ErrorMessage = ErrorMessage.Replace("Request Declined.", "").Trim
        'End If

        'If (ErrorMessage.Contains(",") = True) Then
        '    ErrorMessage = ErrorMessage.Split(",")(0).Trim
        'End If

        If (String.IsNullOrEmpty(sAvail)) Then sAvail = "R"
        Return sAvail


    End Function
#End Region

#Region "rev:mia 04nov2014 - SLOT MANAGEMENT"

    Public Shared Function GetAvailableSlot(sRntId As String, sAvpId As String, sBpdId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("getAvailableSlotForRntIdOrAvp", result, sRntId, sAvpId, sBpdId, "1", False)
            'Changed By Nimesh on 24th Dec 14 to handle empty rental id string

            'Aurora.Common.Data.ExecuteDataSetSP("getAvailableSlotForRntIdOrAvp", result, DBNull.Value, sAvpId, sBpdId, "1", False)
            'Aurora.Common.Data.ExecuteDataSetSP("getAvailableSlotForRntIdOrAvp", result, IIf(sRntId.Trim().Equals(String.Empty), DBNull.Value, sRntId), sAvpId, sBpdId, "1", False)
            'End Changed By Nimesh on 24th Dec 14 to handle empty rental id string
            result.Tables(0).TableName = "Slot"
        Catch ex As Exception
            Helper.LogInformation("GetAvailableSlot", ex.Message & "," & ex.StackTrace)
        End Try
        Return result
    End Function
#End Region

#Region "rev:mia 02Dec2014 - AUDIT TRAIL FOR SCI VERIFIED DRIVERS plus AUTOCHECKOUT"

    Public Shared Function SCIVerifiedDrivers(vdRentalId As String, UserId As String) As String
        Dim result As String = "OK"
        Try
            Dim out As Object = Aurora.Common.Data.ExecuteScalarSP("SCI_InsVerifiedDrivers", vdRentalId, UserId)
        Catch ex As Exception
            Helper.LogInformation("SCIVerifiedDrivers", ex.Message & "," & ex.StackTrace)
            result = "ERROR"
        End Try
        Return result
    End Function

    Public Shared Function SCIVerifiedUser(pUsrCode As String, pUsrPsswd As String) As String
        Dim result As String = "OK"
        Try

            Dim errorResult As String = ""
            Dim strXml As String = Aurora.Common.Data.ExecuteSqlXmlSP("SCI_ValidateAndGetUserData", pUsrCode, pUsrPsswd, errorResult)
            Dim sb As New StringBuilder

            Dim doc As New XmlDocument
            If (Not String.IsNullOrEmpty(strXml) And Not strXml.Equals("<data></data>")) Then
                doc.LoadXml(strXml)
                sb.Append("<Data>")
                sb.Append("<UserDetails>")
                ''<User UserCode="ma2" UserName="Manuel Agbayani" UserEmail="Manuel.Agbayani@thlonline.com" UserLocation="AKL" />
                sb.AppendFormat("<UserCode>{0}</UserCode>", doc.SelectSingleNode("data/User").Attributes("UserCode").Value)
                sb.AppendFormat("<UserName>{0}</UserName>", doc.SelectSingleNode("data/User").Attributes("UserName").Value)
                sb.AppendFormat("<UserEmail>{0}</UserEmail>", doc.SelectSingleNode("data/User").Attributes("UserEmail").Value)
                sb.AppendFormat("<UserLocation>{0}</UserLocation>", doc.SelectSingleNode("data/User").Attributes("UserLocation").Value)
                sb.Append("</UserDetails>")
                ''sb.Append("<Error Message='' />")
                sb.Append("</Data>")
            Else
                sb.Append("<Data>")
                sb.Append("<UserDetails/>")
                sb.Append("<Error Message='Invalid Username or password' />")
                sb.Append("</Data>")

            End If
            result = sb.ToString

            ''rev:mia 11may2015 - change the content of the result
            'Dim params(2) As Aurora.Common.Data.Parameter
            'params(0) = New Aurora.Common.Data.Parameter("pUsrCode", DbType.String, pUsrCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'params(1) = New Aurora.Common.Data.Parameter("pUsrPsswd", DbType.String, pUsrPsswd, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'params(2) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            'Aurora.Common.Data.ExecuteOutputSP("SCI_ValidateUser", params)
            'result = params(2).Value
            'If (String.IsNullOrEmpty(result)) Then
            '    result = "OK"
            'Else
            '    result = "ERROR:" & result
            'End If
        Catch ex As Exception
            Helper.LogInformation("SCIVerifiedUser", ex.Message & "," & ex.StackTrace)
            result = "ERROR"
        End Try
        Return result
    End Function

    ''transferred
    Public Shared Function IsAllowedToCheckout(RentalId As String) As Boolean
        Dim result As String = ""
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("SCI_GetForceVehicle", RentalId)
            Helper.LogInformation("IsAllowedToCheckout  : ", "Force Vehicle: " & result)
        Catch ex As Exception
            Helper.LogInformation("ERROR: IsAllowedToCheckout  : ", result)
            Return "ERROR"
        End Try
        Return IIf(result = "EMPTY", False, True)
    End Function

    ''transferred
    Public Shared Function GetErrorMessage(xmlDoc As XmlDocument) As String
        GetErrorMessage = String.Empty
        If (Not xmlDoc Is Nothing) Then
            If Not xmlDoc.SelectSingleNode("Error/Message") Is Nothing Then
                GetErrorMessage = xmlDoc.SelectSingleNode("Error/Message").InnerText
            End If
        End If
        Return GetErrorMessage
    End Function

    ''transferred
    Public Shared Function PrepareCheckOutData(ByVal BookingId As String, _
                                 ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String) As XmlDocument
        Dim oXmlScreenData As XmlDocument = New XmlDocument
        Try
            oXmlScreenData = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, UserCode, 0, "", RentalCountryCode)
            If (Not String.IsNullOrEmpty(GetErrorMessage(oXmlScreenData))) Then
                Throw New Exception(String.Concat("", oXmlScreenData.SelectSingleNode("Error/Message").InnerText))
            End If

            Dim elem As XmlElement
            If (oXmlScreenData.DocumentElement.SelectSingleNode("Forced") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("Forced")
                elem.InnerText = 1
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("Forced").InnerText = 1
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("UnitNoOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("UnitNoOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("VehicleOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("VehicleOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            With oXmlScreenData.DocumentElement
                .SelectSingleNode("RntCkoTime").InnerText = String.Concat(Now.Hour, ":", Now.Minute)
                .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("DispLoc").InnerText = .SelectSingleNode("DispLoc").InnerText.Split("-"c)(0).Trim()
            End With

        Catch ex As Exception
            Helper.LogInformation("Precheckout: CheckOut Error: ", ex.Message & "," & ex.StackTrace)
            Dim errors As String = "<Error><Message>" & ex.Message & "</Message></Error>"
            oXmlScreenData.LoadXml(errors)
        End Try

        Return oXmlScreenData
    End Function

    ''transferred
    Public Shared Function CheckOut(ByVal oXmlScreenData As XmlDocument, _
                             ByVal UserCode As String) As XmlDocument
        Dim oRetXml As System.Xml.XmlDocument
        Dim sErr As String = ""

        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManageCheckOut(oXmlScreenData, _
                                UserCode, _
                                 "SCI/PhoenixWS/CheckOut", _
                                 "0", _
                                 sErr, _
                                False, _
                                String.Empty, _
                            )

        Return oRetXml
    End Function

    Const RENTALAGREEMENT_XSL_PATH As String = "xsl/RentalAgreement.xsl"
    Public Shared Function EmailRentalAgreement(rentalid As String, CustomerEmail As String) As String
        Dim emailsender As String = ConfigurationSettings.AppSettings("emailFrom")
        Dim emailSubject As String = ConfigurationSettings.AppSettings("emailSubject") & rentalid



        Dim result As String = ""
        Try

            result = RentalAgreement(rentalid, HttpContext.Current.Server.MapPath(RENTALAGREEMENT_XSL_PATH))


            Dim omailmsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
            omailmsg.From = emailsender
            omailmsg.To = CustomerEmail
            omailmsg.Headers.Add("reply-to", emailsender)
            omailmsg.BodyEncoding = System.Text.Encoding.ASCII
            omailmsg.Subject = emailSubject
            omailmsg.Body = result
            omailmsg.BodyFormat = Web.Mail.MailFormat.Html

            Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("mailserverurl"))
            Web.Mail.SmtpMail.Send(omailmsg)

            result = "OK"
            Helper.LogInformation("EmailRentalAgreement", emailSubject & " was sent")

        Catch ex As Exception
            result = "ERROR: Confirmation Email failed"
            Helper.LogInformation("EmailRentalAgreement", emailSubject & " failed " & ex.Message & vbCrLf & ex.StackTrace)
        End Try

        Return result

    End Function


    Private Shared Function SendMail(emailsender As String, recipient As String, body As String, emailsubject As String) As String

        Try
            Dim mailMsg As New MailMessage()

            ' To
            mailMsg.[To].Add(New MailAddress(recipient, ""))

            ' From
            mailMsg.From = New MailAddress(emailsender, "")

            ' Subject and multipart/alternative Body
            mailMsg.Subject = emailsubject
            ''Dim text As String = "text body"
            Dim html As String = body
            ''mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, Nothing, MediaTypeNames.Text.Plain))
            mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, Nothing, MediaTypeNames.Text.Html))

            ' Init SmtpClient and send
            Dim smtpClient As New SmtpClient("smtp.sendgrid.net", Convert.ToInt32(587))
            Dim credentials As New System.Net.NetworkCredential("sci.thlonline.com", "@dmin4Sci")
            smtpClient.Credentials = credentials

            smtpClient.Send(mailMsg)
        Catch ex As Exception
            Throw
        End Try

        Return "OK"

    End Function

#End Region

#Region "rev:mia 01April2014 - exposing endpoints for RESTAPI"
    Public Shared Function GetTodayActivityPerLocation(PickUpDate As String, PickUpLocation As String, IsPickUp As Boolean) As XmlDocument
        Dim result As New DataSet
        Dim doc As New XmlDocument

        Try
            Aurora.Common.Data.ExecuteDataSetSP("TCX_getTodaysActivityForLocation", result, PickUpDate, PickUpLocation, IsPickUp)

            result.Tables(0).TableName = "Rental"
            doc.LoadXml(result.GetXml().Replace("NewDataSet", "Rentals"))

        Catch ex As Exception
            Helper.LogInformation("GetTodayActivityPerLocation", ex.Message & "," & ex.StackTrace)

        End Try
        Return doc
    End Function


    Public Shared Function AddRentalNote(RntId As String, NoteText As String, UsrCode As String, PrgmName As String, RntAudTypeId As String, RntNoteTypeId As String) As String
        Try
            Aurora.Common.Data.ExecuteScalarSP("TCX_AddRentalNote", RntId, NoteText, RntAudTypeId, RntNoteTypeId, UsrCode, PrgmName)
        Catch ex As Exception
            Helper.LogInformation("AddRentalNote", ex.Message & "," & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK"
    End Function


#End Region


#Region "rev:mia 22April2014 - exposing endpoints for RESTAPI"
    Public Shared Function GetAllNonVehicleProductForRentalId(RntId As String, UsrCode As String) As XmlDocument

        Dim doc As New XmlDocument

        Try
            Dim strXml As String = Aurora.Common.Data.ExecuteSqlXmlSP("TCX_getALLNonVehicleProductForRentalId", RntId, UsrCode)
            doc.LoadXml(strXml)
        Catch ex As Exception
            Helper.LogInformation("GetAllNonVehicleProductForRentalId", ex.Message & "," & ex.StackTrace)
        End Try
        Return doc
    End Function


    Public Shared Function GetPriceForSpecificProduct(RntId As String, PrdId As String, UsrCode As String) As XmlDocument
        Dim errorResult As String = ""
        Dim doc As New XmlDocument

        Try
            Dim strXml As String = Aurora.Common.Data.ExecuteSqlXmlSP("TCX_GetPriceForNonVehicleProduct", RntId, PrdId, UsrCode, errorResult)
            If (String.IsNullOrEmpty(errorResult)) Then
                doc.LoadXml(strXml)
            End If

        Catch ex As Exception
            Helper.LogInformation("GetPriceForSpecificProduct", ex.Message & "," & ex.StackTrace)
        End Try
        Return doc
    End Function
#End Region


#Region "rev:mia 07May2014 - exposing endpoints for RESTAPI"
    Public Shared Function GetNotes() As XmlDocument

        Dim doc As New XmlDocument

        Try
            Dim strXml As String = Aurora.Common.Data.ExecuteSqlXmlSP("TCX_getDropDownDataForNote")
            doc.LoadXml(strXml)
        Catch ex As Exception
            Helper.LogInformation("GetNotes", ex.Message & "," & ex.StackTrace)
        End Try
        Return doc
    End Function

#End Region

#Region "14May2015 - adding SaveProductSerialNumber"
    Public Shared Function SaveProductSerialNumber(ByVal BookedProductId As String, ByVal SerialNumber As String, ByVal UserCode As String, ByVal ProgramName As String) As String
        Try
            Aurora.Common.Data.ExecuteScalarSP("WEBP_UpdateBPDSerialNumbers", BookedProductId, SerialNumber, UserCode, ProgramName)
        Catch ex As Exception
            Helper.LogInformation("SaveProductSerialNumber", ex.Message & "," & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK"
    End Function
#End Region


#Region "rev:mia 20MAY2015 - Adding of printer"
    'Public Shared Function PrintDocument(urlOrContent As String, printerName As String, IsUrl As Boolean) As String
    '    Try
    '        If (PrinterSelection.IsValidPrinter(printerName)) Then
    '            Dim webcontent As String = urlOrContent
    '            If (IsUrl) Then
    '                webcontent = PrinterSelection.GetWebContent(urlOrContent)
    '                Helper.LogInformation("PrintDocument HTML", webcontent)
    '                If (webcontent.IndexOf("ERROR") <> -1) Then
    '                    Helper.LogInformation("PrintDocument ERROR", webcontent)
    '                    Return "ERROR: Error retrieving URL content from " & urlOrContent
    '                End If
    '            End If
    '            ''PrinterSelection.PrintRaw(CType(printerName, String), webcontent)
    '            Return "OK"
    '        Else
    '            Return "ERROR: Invalid Printer " & printerName
    '        End If
    '    Catch ex As Exception
    '        Helper.LogInformation("PrintDocument EXCEPTION", ex.Message & "," & ex.StackTrace)
    '        Return "ERROR: " & ex.Message
    '    End Try
    '    Return "OK"
    'End Function


#End Region


#Region "rev:mia 21May2015 - adding Rental Flag"
    Public Shared Function SavingRentalFlags(rntId As String, acceptTC As Boolean, RAFilename As String, regoNum As String, addPrgName As String, addUserCode As String) As String
        Try
            Aurora.Common.Data.ExecuteScalarSP("TCX_UpdateRentalFlags", rntId, acceptTC, RAFilename, regoNum, addPrgName, addUserCode)
        Catch ex As Exception
            Helper.LogInformation("SavingRentalFlags", ex.Message & "," & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK"
    End Function


    Public Shared Function VerifyingOfDrivers(rntId As String, cusId As String, isVerified As Boolean, addPrgName As String, addUserCode As String) As String
        Try
            Aurora.Common.Data.ExecuteScalarSP("TCX_VerifyDriver", rntId, cusId, isVerified, addPrgName, addUserCode)
        Catch ex As Exception
            Helper.LogInformation("VerifyingOfDrivers", ex.Message & "," & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK"
    End Function


    Public Shared Function GetVehicles(rntId As String, unitNumber As String, regoNumber As String) As XmlDocument
        Dim doc As New XmlDocument
        Try

            Dim params(3) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, rntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("iUnitNumber", DbType.String, unitNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("sRegoNumber", DbType.String, regoNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sErrorMessage", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Dim dsResult As DataSet = Aurora.Common.Data.ExecuteOutputDatasetSP("TCX_getVehicleDetails", params)

            Dim sErrorMessage As String = params(3).Value.ToString()
            '<vehicle>
            '    <ScheduleCode>2YXQ</ScheduleCode>
            '        <OddometerOut>290569</OddometerOut>
            '</vehicle>
            '<Error Message = ""/>

            Dim sb As New StringBuilder
            sb.Append("<data>")
            sb.Append("<vehicle>")
            If (dsResult.Tables.Count <> 0) Then
                sb.AppendFormat("<ScheduleCode>{0}</ScheduleCode>", dsResult.Tables(0).Rows(0).Item("FleetModelCode"))
                sb.AppendFormat("<OdometerOut>{0}</OdometerOut>", dsResult.Tables(0).Rows(0).Item("OddometerOut"))
            Else
                sb.Append("<ScheduleCode/>")
                sb.Append("<OdometerOut/>")
            End If
            sb.Append("</vehicle>")
            If (Not String.IsNullOrEmpty(sErrorMessage)) Then
                sb.AppendFormat("<Error Message ='{0}' />", sErrorMessage)
            End If
            sb.Append("</data>")
            doc.LoadXml(sb.ToString())
        Catch ex As Exception
            Helper.LogInformation("GetVehicles", ex.Message & "," & ex.StackTrace)
        End Try
        Return doc
    End Function


#End Region

#Region "rev:mia 29MAY2015 - get RA using API Token"
    Public Shared Function GetRAFilename(rntId As String) As String
        Dim result As String = ""
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("TCX_GetRentalFilename", rntId)
        Catch ex As Exception
            Helper.LogInformation("SavingRentalFlags", ex.Message & "," & ex.StackTrace)
            Return "ERROR:" & ex.Message
        End Try
        Return result
    End Function


    Public Shared Function GetPrintersAssignedToBranchAndUser(ByVal username As String) As String
        Dim DSprinters As New DataSet
        Dim result As String = ""
        Try
            Aurora.Common.Data.ExecuteDataSetSP("tcx_GetPrintersAssignedToBranchAndUser", _
                                                        DSprinters, _
                                                        username)

            If (DSprinters.Tables.Count <> 0) Then
                result = DSprinters.Tables(0).Rows(0).Item("LocPrnPrinterName").ToString
            End If
        Catch ex As Exception
            Helper.LogInformation("GetPrintersAssignedToBranchAndUser", ex.Message & vbCrLf & ex.StackTrace)
            result = "ERROR: " & ex.Message
        End Try
        Return result
    End Function

#End Region

End Class
