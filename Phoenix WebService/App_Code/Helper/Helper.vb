﻿Imports Microsoft.VisualBasic
Imports Aurora.Common
Imports System.Runtime.InteropServices
Imports System.Web
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Net.Security
Imports System.Web.Script.Serialization
Imports WebServiceData
Imports WS.Caching

Namespace WS.Caching
    Public Class Helper

        Public Shared Sub LogInformation(ByVal functionName As String, ByVal TextToLog As String)
            Aurora.Common.Logging.LogInformation(vbCrLf & functionName, String.Concat(vbCrLf, vbCrLf, MessageInformationFormat(functionName, TextToLog)))
        End Sub

        Public Shared Function MessageInformationFormat(ByVal methodName As String, ByVal ParamArray strmessage As String()) As String
            Dim sb As New StringBuilder
            Dim skipfirstHeaderInlog As Boolean = False

            Try
                If (strmessage.Length = 1) Then
                    skipfirstHeaderInlog = True
                End If
            Catch ex As Exception
                skipfirstHeaderInlog = False
            End Try


            If (Not skipfirstHeaderInlog) Then
                sb.AppendFormat("{0}{1}{2}{3}{4}{5}", vbCrLf, vbCrLf, New String("-", 30), "STARTING", New String("-", 30), vbCrLf)
                sb.Append(methodName.ToUpper)
                sb.AppendFormat("{0}{1}{2}", vbCrLf, New String("-", 68), vbCrLf & vbCrLf)

            End If

            Dim ctr As Integer = 0
            For Each strvalue As String In strmessage
                If Not String.IsNullOrEmpty(strvalue) Then
                    strvalue = strvalue.Trim
                    strvalue = strvalue.PadRight(30)
                    If ctr = 0 Then
                        sb.AppendFormat("{0}:", strvalue)
                        ctr = ctr + 1
                    Else
                        sb.AppendFormat("{0}{1}", strvalue, vbCrLf)
                        ctr = 0
                    End If
                End If
            Next
            If (Not skipfirstHeaderInlog) Then
                ''sb.AppendFormat("{0}{1}{2}", vbCrLf, "----------------END-------------", vbCrLf & vbCrLf)
                sb.AppendFormat("{0}{1}{2}{3}{4}{5}{6}{7}", vbCrLf, vbCrLf, New String("-", 30), "END HERE", New String("-", 30), vbCrLf, vbCrLf, vbCrLf)
            End If

            Return sb.ToString
        End Function

    End Class
End Namespace

Namespace WS.Printer

    Public Class PrinterSelection
        ' ----- Define the data type that supplies basic
        '       print job information to the spooler.
        <StructLayout(LayoutKind.Sequential, _
            CharSet:=CharSet.Unicode)> _
        Public Structure DOCINFO
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pDocName As String
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pOutputFile As String
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pDataType As String
        End Structure

        ' ----- Define interfaces to the functions supplied
        '       in the DLL.
        <DllImport("winspool.drv", EntryPoint:="OpenPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function OpenPrinter( _
       ByVal printerName As String, ByRef hPrinter As IntPtr, _
       ByVal printerDefaults As Integer) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="ClosePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ClosePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="StartDocPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartDocPrinter( _
       ByVal hPrinter As IntPtr, ByVal level As Integer, _
       ByRef documentInfo As DOCINFO) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="EndDocPrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndDocPrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="StartPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="EndPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="WritePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function WritePrinter( _
       ByVal hPrinter As IntPtr, ByVal buffer As IntPtr, _
       ByVal bufferLength As Integer, _
       ByRef bytesWritten As Integer) As Boolean
        End Function

        Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
        ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, _
        ByVal nShowCmd As Long) As Long


        Public Shared Function PrintRaw( _
              ByVal printerName As String, _
              ByVal origString As String) As Boolean
            Dim result As Boolean = True
            Dim hPrinter As IntPtr
            Dim spoolData As New DOCINFO
            Dim dataToSend As IntPtr
            Dim dataSize As Integer
            Dim bytesWritten As Integer

            ' ----- The internal format of a .NET String is just
            '       different enough from what the printer expects
            '       that there will be a problem if we send it
            '       directly. Convert it to ANSI format before
            '       sending.
            dataSize = origString.Length()
            dataToSend = Marshal.StringToCoTaskMemAnsi(origString)

            ' ----- Prepare information for the spooler.
            spoolData.pDocName = "Rental Agreement"
            spoolData.pDataType = "RAW"

            Try
                Call OpenPrinter(printerName, hPrinter, 0)

                ' ----- Start a new document and Section 1.1.
                Call StartDocPrinter(hPrinter, 1, spoolData)
                Call StartPagePrinter(hPrinter)

                ' ----- Send the data to the printer.
                Call WritePrinter(hPrinter, dataToSend, _
                   dataSize, bytesWritten)

                ' ----- Close everything that we opened.
                EndPagePrinter(hPrinter)
                EndDocPrinter(hPrinter)
                ClosePrinter(hPrinter)
            Catch ex As Exception
                Logging.LogError("WS.Printer PrintRaw", ex.Message & vbCrLf & ex.StackTrace)
                result = False
            Finally
                ' ----- Get rid of the special ANSI version.
                Marshal.FreeCoTaskMem(dataToSend)
            End Try
            Return result
        End Function


        Public Shared Function IsValidPrinter(printerpath As String) As Boolean
            Dim online As Boolean = False
            Try

                Dim printDocument As New Drawing.Printing.PrintDocument()
                printDocument.PrinterSettings.PrinterName = printerpath
                online = printDocument.PrinterSettings.IsValid
            Catch
                online = False
            End Try
            Return online
        End Function

        Private Shared Function GetWebContent(url As String) As String
            Dim result As String = ""
            Try
                Dim myRequest As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
                myRequest.Method = "GET"
                Dim myResponse As WebResponse = myRequest.GetResponse()
                Dim sr As New StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                result = sr.ReadToEnd()
                sr.Close()
                myResponse.Close()

            Catch ex As Exception
                Return "ERROR: " & ex.Message & "," & ex.StackTrace
            End Try

            Return result
        End Function

        Private Shared Function AppendHtmlStructure(content As String) As String
            Dim sb As New StringBuilder()
            sb.AppendFormat("{0}", "<html>")
            sb.AppendFormat("{0}", "<head>")
            sb.AppendFormat("{0}", "<title>Self-Checkin Rental Agreement</title>")
            sb.AppendFormat("{0}", "</head>")
            sb.AppendFormat("{0}", "<body>")
            sb.AppendFormat("{0}", content.Replace("<br>", ""))
            sb.AppendFormat("{0}", "</body>")
            sb.AppendFormat("{0}", "</html>")

            Dim replacethis As String = "xmlns:fo=""http://www.w3.org/1999/XSL/Format"""
            sb.Replace(replacethis, "") ''.Replace("<!DOCTYPE>", "")

            Return sb.ToString
        End Function

        Public Shared Function PrintDocument(urlOrContent As String, printerName As String, IsUrl As Boolean, Optional rentalId As String = "") As String
            Try

                If (PrinterSelection.IsValidPrinter(printerName)) Then
                    Dim webcontent As String = urlOrContent
                    If (IsUrl) Then
                        webcontent = AppendHtmlStructure(GetWebContent(urlOrContent))
                        Helper.LogInformation("PrintDocument HTML", urlOrContent & vbCrLf & vbCrLf & webcontent)
                        If (webcontent.IndexOf("ERROR") <> -1) Then
                            Helper.LogInformation("PrintDocument ERROR", webcontent)
                            Return "ERROR: Error retrieving URL content from " & urlOrContent
                        End If
                    End If

                    Dim logOutputOfRentalToPrint As Boolean = CBool(ConfigurationManager.AppSettings("LogOutputOfRentalToPrint"))
                    If (logOutputOfRentalToPrint) Then
                        Dim tempRentalPath As String = ConfigurationManager.AppSettings("tempRentalPath")
                        Dim fullpath As String = tempRentalPath & rentalId & ".htm"
                        SaveTempFile(webcontent, rentalId, fullpath)
                        '' SendFileToPrinter(printerName, fullpath)
                        ''DeleteFile(fullpath)
                    End If

                    If (PrinterSelection.PrintRaw(CType(printerName, String), webcontent)) Then
                        Return "OK"
                    Else
                        Return "ERROR: Sending Rental Agreement to " & printerName
                    End If

                Else
                    Return "ERROR: Invalid Printer " & printerName
                End If
            Catch ex As Exception
                Helper.LogInformation("PrintDocument EXCEPTION", ex.Message & "," & ex.StackTrace)
                Return "ERROR: " & ex.Message
            End Try
            Return "OK"
        End Function

        

#Region "Manny's Testing"
        Private Shared Function SendBytesToPrinter(ByVal szPrinterName As String, ByVal pBytes As IntPtr, ByVal dwCount As Int32) As Boolean
            Dim hPrinter As IntPtr      ' The printer handle.
            Dim dwError As Int32        ' Last error - in case there was trouble.
            Dim di As DOCINFO          ' Describes your document (name, port, data type).
            Dim dwWritten As Int32      ' The number of bytes written by WritePrinter().
            Dim bSuccess As Boolean     ' Your success code.

            ' Set up the DOCINFO structure.
            With di
                .pDocName = "RA Test printing"
                .pDataType = "RAW"
            End With
            ' Assume failure unless you specifically succeed.
            bSuccess = False
            If OpenPrinter(szPrinterName, hPrinter, 0) Then
                If StartDocPrinter(hPrinter, 1, di) Then
                    If StartPagePrinter(hPrinter) Then
                        ' Write your printer-specific bytes to the printer.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, dwWritten)
                        EndPagePrinter(hPrinter)
                    End If
                    EndDocPrinter(hPrinter)
                End If
                ClosePrinter(hPrinter)
            End If
            ' If you did not succeed, GetLastError may give more information
            ' about why not.
            If bSuccess = False Then
                dwError = Marshal.GetLastWin32Error()
            End If
            Return bSuccess
        End Function ' SendBytesToPrinter()

        Public Shared Function SendFileToPrinter(ByVal szPrinterName As String, ByVal szFileName As String) As Boolean
            ' Open the file.
            Dim fs As New FileStream(szFileName, FileMode.Open)
            ' Create a BinaryReader on the file.
            Dim br As New BinaryReader(fs)
            ' Dim an array of bytes large enough to hold the file's contents.
            Dim bytes(fs.Length) As Byte
            Dim bSuccess As Boolean
            ' Your unmanaged pointer.
            Dim pUnmanagedBytes As IntPtr

            ' Read the contents of the file into the array.
            bytes = br.ReadBytes(fs.Length)
            ' Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(fs.Length)
            ' Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, fs.Length)
            ' Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, fs.Length)
            ' Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes)
            Return bSuccess
        End Function ' SendFileToPrinter()

        Private Shared Sub SaveTempFile(content As String, rentalId As String, path As String)

            If (File.Exists(path)) Then
                File.Delete(path)
            End If
            Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(path)
            Try
                sw.WriteLine(content)
            Catch ex As Exception
            Finally
                sw.Close()
            End Try
        End Sub

        Private Shared Sub DeleteFile(path As String)
            If (File.Exists(path)) Then
                File.Delete(path)
            End If
        End Sub
#End Region

    End Class
End Namespace

Namespace WS.FlexiAvailability
    Public Class FlexiHelper
        Public Shared Sub CreateFlexiAvailabilityHeader(ByRef oRetXml As System.Xml.XmlDocument, _
                                                        ByRef elemAvailability As XmlElement, _
                                                           countOfAvail As Integer, _
                                                           flexibleDaysForVehicles As String, _
                                                           Brand As String)

            Dim nodeFlexiAvailability As XmlNode = oRetXml.CreateNode(XmlNodeType.Element, "FlexiAvailability", String.Empty)

            elemAvailability = oRetXml.CreateElement("Availability")

            Dim attbrand As XmlAttribute = oRetXml.CreateAttribute("brand")
            Dim attAvail As XmlAttribute = oRetXml.CreateAttribute("availabilityFound")
            attAvail.Value = countOfAvail
            Dim attNeed As XmlAttribute = oRetXml.CreateAttribute("totalAvailabilityNeed")
            attNeed.Value = flexibleDaysForVehicles

            Dim attExtraNeed As XmlAttribute = oRetXml.CreateAttribute("additionalAvailabilityNeed")
            attExtraNeed.Value = CInt(flexibleDaysForVehicles) - countOfAvail


            attbrand.Value = Brand
            elemAvailability.Attributes.Append(attbrand)
            elemAvailability.Attributes.Append(attAvail)
            elemAvailability.Attributes.Append(attNeed)
            elemAvailability.Attributes.Append(attExtraNeed)


            nodeFlexiAvailability.AppendChild(elemAvailability)
            oRetXml.DocumentElement.AppendChild(nodeFlexiAvailability)

        End Sub

        Public Shared Sub CreateFlexiAvailabilityVehicle(ByRef oRetXml As System.Xml.XmlDocument, _
                                                         ByRef elemVehicle As XmlElement, _
                                                         ByRef elemAvailability As XmlElement, _
                                                         prodCode As String, _
                                                         avpId As String, _
                                                         pkgCode As String)

            elemVehicle = oRetXml.CreateElement("Vehicle")
            Dim attProdCode As XmlAttribute = oRetXml.CreateAttribute("code")
            Dim attAvpId As XmlAttribute = oRetXml.CreateAttribute("avpId")
            Dim attPkgCode As XmlAttribute = oRetXml.CreateAttribute("pkgCode")

            attPkgCode.Value = pkgCode
            attProdCode.Value = prodCode
            attAvpId.Value = avpId
            elemVehicle.Attributes.Append(attProdCode)
            ''elemVehicle.Attributes.Append(attAvpId)
            ''elemVehicle.Attributes.Append(attPkgCode)
            elemAvailability.AppendChild(elemVehicle)
        End Sub

        Public Shared Sub CreateFlexiAvailabilityDate(ByRef oRetXml As System.Xml.XmlDocument, _
                                                         ByRef elemVehicle As XmlElement, _
                                                         flexiDateOut As DateTime, _
                                                         flexiDateIn As DateTime, _
                                                         flexiAvail As Char)

            Dim elemDate As XmlElement = oRetXml.CreateElement("Date")
            Dim attFrom As XmlAttribute = oRetXml.CreateAttribute("from")
            attFrom.Value = flexiDateOut.ToShortDateString
            Dim attTo As XmlAttribute = oRetXml.CreateAttribute("to")
            attTo.Value = flexiDateIn.ToShortDateString


            elemDate.Attributes.Append(attFrom)
            elemDate.Attributes.Append(attTo)
            elemDate.InnerText = flexiAvail.ToString

            elemVehicle.AppendChild(elemDate)
        End Sub
    End Class
End Namespace

Namespace WS.RentalAgreement

    Public Class TokenObject
        Private _access_token As String
        Public Property access_token As String
            Get
                Return _access_token
            End Get
            Set(value As String)
                _access_token = value
            End Set
        End Property

        Private _token_type As String
        Public Property token_type As String
            Get
                Return _token_type
            End Get
            Set(value As String)
                _token_type = value
            End Set
        End Property
    End Class

    Public Class AmazonUrl
        Private _url As String
        Public Property url As String
            Get
                Return _url
            End Get
            Set(value As String)
                _url = value
            End Set
        End Property
    End Class

    Public Class RAgreement

        Public Shared Function PrintRentalAgreement(username As String, password As String, rentalId As String) As String

            ''get RA location
            Dim raFilename As String = GetRAFilename(rentalId)
            Dim urlAmazon As String = String.Empty

            If (Not String.IsNullOrEmpty(raFilename) And raFilename.IndexOf("ERROR") = -1) Then
                ''get token
                Dim tokenresult As String = GetToken(username, password)
                Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer
                Dim tokenObject As TokenObject = jsSerializer.Deserialize(Of TokenObject)(tokenresult)
                urlAmazon = GetrentalAgreementUrl(tokenObject.access_token, tokenObject.token_type, raFilename)
                Dim amazonUrl As AmazonUrl = jsSerializer.Deserialize(Of AmazonUrl)(urlAmazon)
                urlAmazon = amazonUrl.url
            Else
                If (raFilename.IndexOf("ERROR") <> -1) Then
                    Return "ERROR:" & raFilename
                End If
            End If

            Return urlAmazon

        End Function

        Private Shared Function GetrentalAgreementUrl(access_token As String, token_type As String, urlRA As String) As String
            Dim amazonUrl As String = ConfigurationManager.AppSettings("amazonUrl")
            Dim wr As HttpWebRequest = CallRegisterAPIjson(amazonUrl, "url=" & urlRA, "POST", access_token, token_type)
            Return GetResponse(wr)
        End Function

        Private Shared Function GetToken(username As String, password As String) As String
            Dim client_id As String = ConfigurationManager.AppSettings("client_id")
            Dim client_secret As String = ConfigurationManager.AppSettings("client_secret")
            Dim tokenUrl As String = ConfigurationManager.AppSettings("tokenUrl")

            Dim bodytext As String = String.Format("grant_type=password&username={0}&password={1}&client_id={2}&client_secret={3}", username, password, client_id, client_secret)
            Dim wr As HttpWebRequest = CallRegisterAPIjson(tokenUrl, bodytext, "POST")
            Return GetResponse(wr)
        End Function

        Private Shared Function SetProxyAndCredentials() As IWebProxy

            ''ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(Function() True)
            Dim apiProxy As String = ConfigurationManager.AppSettings("apiProxy")
            Dim apiProxyNo As Integer = IIf(String.IsNullOrEmpty(apiProxy), 3128, CInt(apiProxy))
            Dim proxy As IWebProxy = New WebProxy("proxy", apiProxyNo)
            ' port number is of type integer 
            Dim credentials As ICredentials = CredentialCache.DefaultNetworkCredentials ''CredentialCache.DefaultCredentials
            proxy.Credentials = credentials
            Return proxy
        End Function


        Private Shared Function BuildBodyRequest(url As String, data As String, proxy As IWebProxy, method As String, Optional access_token As String = "", Optional token_type As String = "") As HttpWebRequest
            Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
            request.Proxy = proxy
            request.Method = method

            request.ContentType = "application/x-www-form-urlencoded"
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
            request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1"

            request.ContentLength = data.Length ''IIf((Not String.IsNullOrEmpty(access_token) And Not String.IsNullOrEmpty(token_type)), 0, data.Length)
            If (Not String.IsNullOrEmpty(access_token) And Not String.IsNullOrEmpty(token_type)) Then
                request.Headers.Add("Authorization", String.Concat(token_type, " ", access_token))
            End If

            Dim writer As New StreamWriter(request.GetRequestStream())
            writer.Write(data)
            writer.Close()
            Return request
        End Function

        Private Shared Function CallRegisterAPIjson(url As String, data As Object, method As String, Optional access_token As String = "", Optional token_type As String = "") As HttpWebRequest
            Dim proxy As IWebProxy = SetProxyAndCredentials()
            Dim request As HttpWebRequest = BuildBodyRequest(url, DirectCast(data, String), proxy, method, access_token, token_type)
            Return request
        End Function


        Private Shared Function GetResponse(request As HttpWebRequest) As String
            Dim responseFromServer As String = ""
            Try
                Dim response As WebResponse = request.GetResponse()
                Dim dataStream As Stream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)

                responseFromServer = reader.ReadToEnd()
                ' Clean up the streams.
                reader.Close()
                dataStream.Close()

                response.Close()
            Catch ex As Exception
                responseFromServer = "ERROR: " + ex.Message
            End Try

            Return responseFromServer

        End Function
    End Class

End Namespace