﻿Imports System
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Web
Imports Aurora.Common

Namespace WS.Caching
    Public Class CacheFactory
        Implements ICacheProvider

        Public Sub Add(key As String, value As Object) Implements ICacheProvider.Add

            If (IsKeyExist(key) = True) Then
                Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Add", "Key found", key & " does  exist! no need to add it"))
            Else
                ''this is the key for caching the vehicle list grid in the aurora boooking process
                If (key.Contains("CACHEALWAYS") = True) Then
                    Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Add", "Key with CACHEALWAYAS not found", key & " added"))
                    HttpContext.Current.Cache.Insert(key, value, Nothing, DateTime.Now.AddMinutes(AuroraCachingInMinutes), Cache.NoSlidingExpiration)
                Else
                    ''from aurora bookingprocess, the flag to determine whether the request came from aurora is the number 7.
                    ''this is dynamically appended when you select any type of alternate availability mode in aurora
                    ''ex: 1 - later pickup
                    ''ex: 2 - Earlier
                    ''ex: 3 - Reverse
                    ''ex: 4 - later pickup
                    ''ex: 5 - Fixed PickUp
                    ''ex: 6 - Return to PickUp
                    ''ex: 7 - FROM AURORA
                    ''ex: 8 - all
                    ''request thant came from b2c has no flag so it will not be cache
                    ''the key should contain the word AURORAREQUEST
                    If (IsAuroraRequest(key) = True) Then
                        Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Add", "Key with AURORAREQUEST not found", key & " added"))
                        HttpContext.Current.Cache.Insert(key, value, Nothing, DateTime.Now.AddMinutes(AuroraCachingInMinutes), Cache.NoSlidingExpiration)
                    Else
                        ''the key should contain the word B2CREQUEST
                        If (IsWebChannelRequest(key) = True) Then
                            Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Add", "Key with B2CREQUEST not found", key & " added"))
                            HttpContext.Current.Cache.Insert(key, value, Nothing, DateTime.Now.AddMinutes(b2cCachingInMinutes), Cache.NoSlidingExpiration)
                        End If
                    End If

                End If


            End If


        End Sub

        Function IsKeyExist(key As String) As Boolean
            Return IIf(HttpContext.Current.Cache(key) Is Nothing, False, True)
        End Function

        Public Sub Remove(key As String) Implements ICacheProvider.Remove
            If (IsKeyExist(key) = True) Then
                Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Remove ", "Key", key))
                HttpContext.Current.Cache.Remove(key)
            End If
        End Sub

        Public Function Retrieve(key As String) As Object Implements ICacheProvider.Retrieve
            Dim returnObject As Object = Nothing
            If (IsKeyExist(key) = True) Then
                Helper.LogInformation("CacheFactory", Helper.MessageInformationFormat("Retrieve ", "Key found", key))
                returnObject = HttpContext.Current.Cache.Get(key)
            End If
            Return returnObject
        End Function

        Public Function IsExist(key As String) As Boolean Implements ICacheProvider.IsExist
            Return IsKeyExist(key)
        End Function

        Private ReadOnly Property IsAuroraRequest(key As String) As Boolean
            Get
                ''can be considered as request that came from aurora
                Return IIf(key.ToLower.Contains("AURORAREQUEST".ToLower) = True, True, False)
            End Get
        End Property

        Private ReadOnly Property IsWebChannelRequest(key As String) As Boolean
            Get
                ''can be considered as request that came from aurora
                Return IIf(key.ToLower.Contains("B2CREQUEST".ToLower) = True, True, False)
            End Get
        End Property

        Private ReadOnly Property AuroraCachingInMinutes() As Integer
            Get
                Return Convert.ToInt16(ConfigurationManager.AppSettings("cachealways_auroravehiclegridandalternate_minutes").ToString)
            End Get
        End Property

        Private ReadOnly Property b2cCachingInMinutes() As Integer
            Get
                Return Convert.ToInt16(ConfigurationManager.AppSettings("cachealways_b2cchannelalternate_minutes").ToString)
            End Get
        End Property
    End Class

End Namespace