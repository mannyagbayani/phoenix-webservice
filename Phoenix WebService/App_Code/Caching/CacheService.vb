﻿Imports System.Data
Imports Microsoft.VisualBasic

Namespace WS.Caching

    Public Class CacheService

        Private _cacheFactory As CacheFactory
        Private _cachingObjectType As CachingObjectType

        Public Sub New(cacheFactory As CacheFactory, cachingObjectType As CachingObjectType)
            If cacheFactory Is Nothing Then
                _cacheFactory = New CacheFactory
            Else
                _cacheFactory = cacheFactory
            End If

            _cachingObjectType = cachingObjectType
        End Sub

        Public Sub Add(key As String, value As Object)
            _cacheFactory.Add(key, value)
        End Sub

        Public Function Retrieve(key As String) As Object
            Dim returnObject As Object = _cacheFactory.Retrieve(key)

            Select Case _cachingObjectType
                Case CachingObjectType.strings
                    Return TryCast(returnObject, String)
                Case CachingObjectType.datasets
                    Return TryCast(returnObject, DataSet)
                Case CachingObjectType.datatables
                    Return TryCast(returnObject, DataTable)
            End Select

            Return returnObject
        End Function

        Public Function isExist(key As String) As Boolean
            Return _cacheFactory.IsExist(key)
        End Function

    End Class

    Public Enum CachingObjectType
        strings = 0
        datasets = 1
        datatables = 2
    End Enum
End Namespace