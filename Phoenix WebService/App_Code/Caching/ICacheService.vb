﻿Imports System
Imports Microsoft.VisualBasic
Imports System.Data

Namespace WS.Caching

    Public Interface ICacheProvider
        Sub Add(key As String, value As Object)
        Sub Remove(key As String)
        Function Retrieve(key As String) As Object
        Function IsExist(key As String) As Boolean
    End Interface


    Public Interface ICacheNullProvider
    End Interface

End Namespace


